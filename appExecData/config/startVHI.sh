#!/bin/sh
#        
# This script is called by the *GO variable, i.e., we have in config.usr1 *GO=startVHI.sh
# This allows for an app to be started indirectly via the config.usr1 parameter *vhiapp,
# so it allows a sysmode user to simply edit the *vhiapp variable to be the app required to be run.
# e.g.                                                                                               
# - run release-mode version of app:
# *vhiapp=vhi            
# - but the sysmode user can change it to be the following to run the debug version of the app:
# *vhiapp=vhiD                                                                      
#     
appname=`/usr/local/bin/getenvfile *vhiapp`
if [ "$appname" = vhiD ]; then             
    ./VHIAppD.exe &
else                   
    ./VHIApp.exe &
fi                    
exit 0