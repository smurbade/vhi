/*
 * service_request.c
 *
 *  Created on: Nov 21, 2013
 *      Author: Dexter M. Alberto
 */


#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <svc.h>
#include <pthread.h>

#include "service_request.h"
#include "sca_xml_request.h"
#include "vhi_request.h"
#include "vantiv_response.h"
#include "vhi_response.h"
#include "vhi_610_group.h"
#include "vhi_610_base.h"
#include "logger.h"
#include "free_data.h"
#include "vhi_request.h"
#include "http_handler.h"
#include "help_function.h"
#include "config_data.h"
#include "vhi_610_fields.h"
#include "curl_connect.h"
#include "tcpip_server.h"
#include "appLog.h"

extern pthread_mutex_t gptTORRecordFileMutex;

static void sendSAFSaleAsPostAuth(sGLOBALS* , sVHI_REQ * , sCONFIG * , _sSCA_XML_REQUEST *, sVHI_610_BASE *, _sVHI_FIELDS_INFO *, sVHI_RESPONSE_INFO *, sR008_data *, _sVTV_FIELDS *, sVANTIV_BN *, sVANTIV_RESPONSE *);

int initializeServiceData(sGLOBALS* GLOBAL)
{
	GLOBAL->isProcessorAvailable 		= true;
	GLOBAL->http.status_code			= eHTTP_OK;
	GLOBAL->http.firstHeader			= 0;
	GLOBAL->http.requestLength			= 0;
	GLOBAL->http.responseLength			= 0;
	GLOBAL->http.content_length			= NULL;
	GLOBAL->http.content_type			= NULL;
	GLOBAL->http.expect					= NULL;
	GLOBAL->http.xml_request			= NULL;
	GLOBAL->http.xml_response			= NULL;
	GLOBAL->appErrCode					= eERR_NOERROR;
	//GCONFIG.eTokenMode = eTOKEN_TRT; //Praveen_P1: We should not setting it here
	return EXIT_SUCCESS;
}


void freeServiceData(sGLOBALS* GLOBAL)
{
	if (GLOBAL->http.content_length)
		free(GLOBAL->http.content_length);
	if (GLOBAL->http.content_type)
		free(GLOBAL->http.content_type);
	if (GLOBAL->http.expect)
		free(GLOBAL->http.expect);
	if (GLOBAL->http.xml_request)
		free(GLOBAL->http.xml_request);
	if (GLOBAL->http.xml_response)
		free(GLOBAL->http.xml_response);

	if (GCONFIG.eTokenMode == eTOKEN_TUT)
	{
		GCONFIG.eTokenMode = eTOKEN_TRT;
	}

}


int responseHttpError(int conn, sGLOBALS* GLOBAL)
{
#ifdef LOGGING_ENABLED
	char szDbgMsg[512] = {0};
#endif
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char tosend[100] = {0};

	debug_sprintf(szDbgMsg, "%s: http.status_code[%d]", __FUNCTION__, GLOBAL->http.status_code);
	APP_TRACE(szDbgMsg);

	sprintf(tosend,"HTTP/1.1 %d OK\r\n"
			"\r\n", GLOBAL->http.status_code);

	if ( write(conn, tosend, strlen(tosend) ) < 0) {
		debug_sprintf(szDbgMsg, "%s: ERROR! write error", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "Error While Posting the Vantiv Host Response to SCA. Could Not Write the Data to Client Socket");
			addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE,ENTRYID_PROCESSED, szAppLogData, NULL);
		}

		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}


int responseHttpSuccess(int conn, sGLOBALS* GLOBAL)
{
#ifdef LOGGING_ENABLED
	char szDbgMsg[512] = {0};
#endif
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char header[100]={0};

	sprintf(header, "HTTP/1.1 %d OK\r\n"
			"Content-Type: text/xml\r\n"
			"Content-Length: %d\r\n"
			"\r\n", GLOBAL->http.status_code, GLOBAL->http.responseLength);

	int totalLen = strlen(header) + GLOBAL->http.responseLength;
	char buffer[totalLen+1];

	memset(buffer, 0x00, sizeof(buffer));
	strcpy(buffer, header);
	strncat(buffer, GLOBAL->http.xml_response, GLOBAL->http.responseLength);

	if (write(conn, buffer, strlen(buffer)) < 0) {
		debug_sprintf(szDbgMsg, "%s: ERROR! write", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "Failed to Write the Response Message to the Client Socket");
			addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_SENT, szAppLogData, NULL);
		}

		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}


/*
 * This is the new connection handler using Thread
 */
void *onServiceRequestThread(int conn_socket)
{
#ifdef LOGGING_ENABLED
	char szDbgMsg[4096] = {0};
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: -------SERVICE START-------", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	int 		conn 		 		= conn_socket;
	int 		status 		 		= EXIT_SUCCESS;
	int			iAppLogEnabled		= 0;
	bool 		bHitHost 	 		= false;
	bool		bHostOnline			= false;
	FILE *		Fh		 			= NULL;
	char		szAppLogData[2560]	= "";
	char		szAppLogDiag[300]	= "";
	pthread_t	threadID			= 0L;

	/*
	 * Creating all the variables/structures required for the processing
	 * of this request
	 */

	sGLOBALS 			*GLOBAL;
	sVHI_REQ 			vhi_req_info; //VHI Request structure
	sVANTIV_RESPONSE 	vrinfo;       //vantiv response info
	_sVTV_FIELDS 		svbninfo;     //vantiv bit number info
	sVANTIV_BN 			svtvbn;
	sR017_data 			sR017_info;   //R017 group field response
    sR008_data 			sR008_info;   //R008 group field response
    sR011_data 			sR011_info;   //R011 group field response
	sVHI_RESPONSE_INFO  vhi_res_info; //VHI Response structure
	sCONFIG 			LocGCONFIG;   //To Store the Config variables
	_sSCA_XML_REQUEST   req_xml_info[eREQ_ELEMENT_MAX];
	sVHI_610_BASE       vhi610info;   //610 Base Data
	_sVHI_FIELDS_INFO   vhi_fieldinfo;//VHI Field information
    sR023_data          sR023_info;   // R023 group field response
    sR029_data          sR029_info;   // R029 group field response

	sGLOBALS sObj;
	GLOBAL	 = &sObj;

	memset(GLOBAL, 0x00, sizeof(sGLOBALS));

	memset(&vhi_req_info, 0x00, sizeof(sVHI_REQ));
	memset(&vrinfo, 0x00, sizeof(sVANTIV_RESPONSE));
	memset(&svbninfo, 0x00, sizeof(_sVTV_FIELDS));
	memset(&svtvbn, 0x00, sizeof(sVANTIV_BN));
	memset(&sR017_info, 0x00, sizeof(sR017_data));
	memset(&sR008_info, 0x00, sizeof(sR008_data));
	memset(&sR011_info, 0x00, sizeof(sR011_data));
	memset(&vhi_res_info, 0x00, sizeof(sVHI_RESPONSE_INFO));
	memset(&LocGCONFIG, 0x00, sizeof(sCONFIG));
	memset(&req_xml_info, 0x00, sizeof(_sSCA_XML_REQUEST) * eREQ_ELEMENT_MAX);
	memset(&vhi610info, 0x00, sizeof(sVHI_610_BASE));
	memset(&vhi_fieldinfo, 0x00, sizeof(_sVHI_FIELDS_INFO));
    memset(&sR023_info, 0x00, sizeof(sR023_info));
    memset(&sR029_info, 0x00, sizeof(sR029_info));

	status = initializeServiceData(GLOBAL); //To initialize the GLOBAL structure with default values

	iAppLogEnabled = isAppLogEnabled();

	threadID = pthread_self();

	debug_sprintf(szDbgMsg, "%s: Thread ID [%d] is servicing this request", __FUNCTION__, (int)threadID);
	APP_TRACE(szDbgMsg);

	if(iAppLogEnabled)
	{
		sprintf(szAppLogData, "Thread ID [%d] is Servicing This Request", (int)threadID);
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START, szAppLogData, NULL);
	}

	/* Copy the config parameter values to the local config structure */
	memcpy(&LocGCONFIG, &GCONFIG, sizeof(sCONFIG));

	while(1)
	{
		/*
		 * =========================================
		 * STEP #1 Parse HTTP headers
		 * =========================================
		 */
		if (status == EXIT_SUCCESS) {
			debug_sprintf(szDbgMsg, "%s: STEP #1 Parse HTTP header", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			status = parseHttpRequest(conn, GLOBAL);
		}
		else {
			debug_sprintf(szDbgMsg, "%s: ERROR! config data invalid", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

        debug_sprintf(szDbgMsg, "We got here with status %d", status);
        APP_TRACE(szDbgMsg);


        /*
		 * =========================================
		 * STEP #2 Parse XML request
		 * =========================================
		 */
		if (status == EXIT_SUCCESS) {
			/*
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Successfully Received the Request from the Client");
				addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_RECEIVE, szAppLogData, NULL);
			}*/

			char szDbgMsg[GLOBAL->http.requestLength + 128];
			memset(szDbgMsg, 0x00, sizeof(szDbgMsg));
#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s: [%s]", __FUNCTION__, GLOBAL->http.xml_request);
			APP_TRACE(szDbgMsg);
#endif
			debug_sprintf(szDbgMsg, "%s: STEP #2 Parse XML request", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Parsing the XML Request from the Client");
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
			}

			status = parseXmlRequest(GLOBAL->http.xml_request, GLOBAL, req_xml_info);
			if(status == EXIT_SUCCESS)
			{
				char* pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);

				debug_sprintf(szDbgMsg, "%s: Command is %s", __FUNCTION__, pCOMMAND);
				APP_TRACE(szDbgMsg);
				
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Successfully Parsed the Request from the Client");
					addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_PROCESSED, szAppLogData, NULL);

					sprintf(szAppLogData, "Processing %s Command Request from the Client", pCOMMAND);
					addAppEventLog(APP_NAME,ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
				}

				if(strncmp(pCOMMAND, COMMAND.VERSION, 7) == COMPARE_OK)
				{
					debug_sprintf(szDbgMsg, "%s: Its Version command, need to compose SCA response next", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					break; //breaking from the while loop to skip following steps
				}
				else if(strncmp(pCOMMAND, COMMAND.DEVADMIN, 8) == COMPARE_OK)
				{
					debug_sprintf(szDbgMsg, "%s: Its Device Admin registration command, need to compose SCA response next", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					break; //breaking from the while loop to skip following steps
				}
				else if(strncmp(pCOMMAND, COMMAND.LAST_TRAN, 9) == COMPARE_OK)
				{
					debug_sprintf(szDbgMsg, "%s: Its Last Tran command, need to compose SCA response next", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					break; //breaking from the while loop to skip following steps
				}
			}
			else
			{
				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Error, Failed to Parse The XML message From Client");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, NULL);
				}
			}
		}
		else {
			if(GLOBAL->http.method == eHEAD){
				debug_sprintf(szDbgMsg, "%s: Need to check the Host Connection Status", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Need To Check The Vantiv Host Connection Status");
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
				}

				bHostOnline = isProcessorOnline(&GCONFIG);

				if(bHostOnline)
				{
					debug_sprintf(szDbgMsg, "%s: Host Connection is Available", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					if(iAppLogEnabled)
					{
						sprintf(szAppLogData, "Vantiv Host Connection is Available");
						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSED, szAppLogData, NULL);
					}

					GLOBAL->http.status_code = eHTTP_OK;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Host Connection is NOT Available", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					if(iAppLogEnabled)
					{
						sprintf(szAppLogData, "Vantiv Host Connection is NOT Available");
						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSED, szAppLogData, NULL);
					}

					GLOBAL->http.status_code = eHTTP_SERVER_UNAVAILABLE;
				}

				debug_sprintf(szDbgMsg, "%s: Status is %d", __FUNCTION__, GLOBAL->http.status_code);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: STEP #1 ERROR", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Error ,Failed to Parse the HTTP Request From Client");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, NULL);
				}
			}
		}
		//COMPOSE:
		/*
		 * =========================================
		 * STEP #3 Compose Vantiv request
		 * =========================================
		 */
		if (status == EXIT_SUCCESS) {
			debug_sprintf(szDbgMsg, "%s: STEP #3 Compose request to vantiv", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			status = composeVantivRequest(GLOBAL, &vhi_req_info, &LocGCONFIG, req_xml_info, &vhi610info, &vhi_fieldinfo);
		}
		else {
			debug_sprintf(szDbgMsg, "%s: STEP #3 ERROR", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(GLOBAL->http.method != eHEAD) //Praveen_P1: Logging if it is not the Connection Status request, otherwise for connection status these statements getting logged
			{
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Error, Failed to Parse XML Message From Client.");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, NULL);
				}
			}
		}


		/*
		 * =========================================
		 * STEP #4 Send request, received response
		 * of Vantiv via Libcurl
		 * =========================================
		 */
		char pVresponse[MAX_DATA_TO_RECEIVE] = {0};
		char* pRequest = NULL;

		if (status == EXIT_SUCCESS) {

			debug_sprintf(szDbgMsg, "%s: STEP #4 Send request, received response of Vantiv via Libcurl", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			//pRequest = getVantivRequest();
			pRequest = vhi_req_info.main;


			char* pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);

			if(iAppLogEnabled)
			{
				if(strncmp(pCOMMAND, COMMAND.SIGNATURE, strlen(COMMAND.SIGNATURE)) != COMPARE_OK) //Praveen_P1: We are not looging if it is SIGNATURE command
				{
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_VANTIV_REQUEST, pRequest, NULL);
				}
			}

#ifdef DEVDEBUG
			if(strlen(pRequest) < 4050)
			{
				debug_sprintf(szDbgMsg, "%s: VHI [%s]", __FUNCTION__, pRequest);
				APP_TRACE(szDbgMsg);
			}
#endif
			bHitHost = true; //Setting this variable here since we are going to send the request to host, will use this one to store the last tran details

			status = sendRecvViaCurl(pRequest, pVresponse, GLOBAL, &LocGCONFIG, req_xml_info);
		}
		else {
			debug_sprintf(szDbgMsg, "%s: STEP #4 ERROR", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(GLOBAL->http.method != eHEAD) //Praveen_P1: Logging if it is not the Connection Status request, otherwise for connection status these statements getting logged
			{
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Error, Failed to Frame VHI Request.");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, NULL);
				}
			}
		}


		/*
		 * =========================================
		 * SPECIAL CASE: TIMEOUT REVERSAL
		 * =========================================
		 */
		if (status == EXIT_FAILURE && GLOBAL->http.status_code == eHTTP_GATEWAY_TIMEOUT) {
			char* pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);

			if (pCOMMAND == NULL) {
				debug_sprintf(szDbgMsg, "%s: ERROR! COMMAND is NULL", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				status = EXIT_FAILURE;
			}
			else {
				/*valid Commands for TOR*/
				if (strncmp(pCOMMAND, COMMAND.SALE, strlen(COMMAND.SALE)) == COMPARE_OK
				|| strncmp(pCOMMAND, COMMAND.POST_AUTH, strlen(COMMAND.POST_AUTH)) == COMPARE_OK
				|| strncmp(pCOMMAND, COMMAND.PRE_AUTH, strlen(COMMAND.PRE_AUTH)) == COMPARE_OK
				|| strncmp(pCOMMAND, COMMAND.ACTIVATE, strlen(COMMAND.ACTIVATE)) == COMPARE_OK
				|| strncmp(pCOMMAND, COMMAND.CREDIT, strlen(COMMAND.CREDIT)) == COMPARE_OK
				|| strncmp(pCOMMAND, COMMAND.ADD_VALUE, strlen(COMMAND.ADD_VALUE)) == COMPARE_OK
				|| strncmp(pCOMMAND, COMMAND.REMOVE_VALUE, strlen(COMMAND.REMOVE_VALUE)) == COMPARE_OK
				|| strncmp(pCOMMAND, COMMAND.DEACTIVATE, strlen(COMMAND.DEACTIVATE)) == COMPARE_OK
				|| strncmp(pCOMMAND, COMMAND.COMPLETION, strlen(COMMAND.COMPLETION)) == COMPARE_OK
				|| strncmp(pCOMMAND, COMMAND.ADD_TIP, strlen(COMMAND.ADD_TIP)) == COMPARE_OK )
				{
					int i = 0;

					for (i = 0; i < LocGCONFIG.torRetry; i++)
					{
						debug_sprintf(szDbgMsg, "%s: #SPECIAL CASE: TIMEOUT REVERSAL PROCCESSING for [%d] time", __FUNCTION__, i+1);
						APP_TRACE(szDbgMsg);

						if(iAppLogEnabled)
						{
							sprintf(szAppLogData, "TIMEOUT REVERSAL PROCCESSING for [%d] time",i+1);
							addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
						}

						/*if ADJUSTMENT*/
						if (strncmp(pCOMMAND, COMMAND.ADD_TIP, strlen(COMMAND.ADD_TIP)) == COMPARE_OK) {
							status = EXIT_SUCCESS;
						}
						else {
							status = composeVantivRequest(GLOBAL, &vhi_req_info, &LocGCONFIG, req_xml_info, &vhi610info, &vhi_fieldinfo);
							pRequest = NULL;
							//pRequest = getVantivRequest();
							pRequest = vhi_req_info.main;
						}
						/*end*/
				#ifdef DEVDEBUG
						if(strlen(pRequest) < 4050)
						{
							debug_sprintf(szDbgMsg, "%s: VHI TOR REQUEST [%s]", __FUNCTION__, pRequest);
							APP_TRACE(szDbgMsg);
						}
				#endif
						if (status == EXIT_SUCCESS) {

							if(iAppLogEnabled)
							{
								addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_VANTIV_REQUEST, pRequest, NULL);
							}

							memset(pVresponse, 0x00, sizeof(pVresponse));
							status = sendRecvViaCurl( pRequest, pVresponse, GLOBAL, &LocGCONFIG, req_xml_info);

							if (status == EXIT_SUCCESS) {

								char* pSAF_TRAN = getXmlTagValue(eSAF_TRAN, req_xml_info);

								if(pSAF_TRAN == NULL)
								{
									pSAF_TRAN = "0";
								}

								if(strcmp(pSAF_TRAN, "1") == 0)
								{
									if(iAppLogEnabled)
									{
										addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_VANTIV_RESPONSE, pVresponse, NULL);

										strcpy(szAppLogData, "Received Response for TOR Request, Ignoring it Since it is SAF Transaction");
										addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
									}

									debug_sprintf(szDbgMsg, "%s: Its SAF transaction, TOR success should not be considered", __FUNCTION__);
									APP_TRACE(szDbgMsg);
									status = EXIT_FAILURE;
									GLOBAL->isProcessorAvailable = false;
									GLOBAL->http.status_code = eHTTP_OK;
									GLOBAL->appErrCode = eERR_NOERROR;
								}
								else
								{
									break;
								}
							}
						}
					}
					if(status != EXIT_SUCCESS)
					{
						if(GLOBAL->http.status_code == eHTTP_GATEWAY_TIMEOUT || GLOBAL->isProcessorAvailable == false)
						{
							if(GLOBAL->http.status_code == eHTTP_GATEWAY_TIMEOUT)
							{
								GLOBAL->http.status_code = eHTTP_OK;
								GLOBAL->appErrCode = eERR_HOST_TIMED_OUT;
							}

							debug_sprintf(szDbgMsg, "%s: Maximum TOR Retries reached, need to store the TOR request to file", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							if(iAppLogEnabled)
							{
								sprintf(szAppLogData, "Maximum TOR Retries Reached, Need to Store the TOR Request to File");
								addAppEventLog(APP_NAME, ENTRYTYPE_INFO,ENTRYID_PROCESSED, szAppLogData, NULL);
							}

							/*
							 * We have a TOR request here which could not get response from processor
							 * storing the TOR request to the file so that
							 * TOR reqtires can happen on background
							 */

							//Acquire the File to add a new TOR record

							debug_sprintf(szDbgMsg, "%s: Acquiring the TOR file[%s] to add the new TOR request", __FUNCTION__, TOR_RECORD_FILE_NAME);
							APP_TRACE(szDbgMsg);

							acquireMutexLock(&gptTORRecordFileMutex, "TOR Record File");
							/*
							 * Open file for output at the end of a file.
							 * Output operations always write data at the end of the file, expanding it.
							 * The file is created if it does not exist
							 */
							Fh = fopen(TOR_RECORD_FILE_NAME, "a+");
							if(Fh != NULL)
							{
								debug_sprintf(szDbgMsg, "%s: Opened the TOR file in an append mode!!!", __FUNCTION__);
								APP_TRACE(szDbgMsg);

								/*
								 * Add the TOR request to the file
								 */
								int iRecordLength = 0;
								int iRetVal       = 0;

								iRecordLength = strlen(pRequest) + 1; //one for new line character i.e. LF

								char szTORRecord[iRecordLength + 1]; //one for NULL character

								memset(szTORRecord, 0x00, iRecordLength + 1);

								sprintf(szTORRecord,"%s%s", pRequest, LF); //LF New line field
						#ifdef DEVDEBUG
								if(strlen(szTORRecord) < 4050)
								{
									debug_sprintf(szDbgMsg, "%s: TOR Record [%s]", __FUNCTION__, szTORRecord);
									APP_TRACE(szDbgMsg);
								}
							#endif
								iRetVal = fwrite(szTORRecord, sizeof(char), iRecordLength, Fh);

								if(iRetVal == iRecordLength)
								{
									debug_sprintf(szDbgMsg, "%s: Successfully written TOR record to the file ", __FUNCTION__);
									APP_TRACE(szDbgMsg);
								}
								else
								{
									debug_sprintf(szDbgMsg, "%s: Failure while writing TOR record to the file, Actual count[%d], Written bytes [%d] ", __FUNCTION__, iRecordLength, iRetVal);
									APP_TRACE(szDbgMsg);

									if(iAppLogEnabled)
									{
										sprintf(szAppLogData, "Warning! Failure While writing TOR Record to the file");
										sprintf(szAppLogDiag, " Failure While writing TOR Record to the file");
										addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_PROCESSING,szAppLogData, szAppLogDiag);
									}
								}

								/*
								 * Close the file
								 */
								fclose(Fh);

							}
							else
							{
								debug_sprintf(szDbgMsg, "%s: Error while opening the TOR record file!!!", __FUNCTION__);
								APP_TRACE(szDbgMsg);
							}

							debug_sprintf(szDbgMsg, "%s: Releasing the TOR file[%s] after adding the new TOR request", __FUNCTION__, TOR_RECORD_FILE_NAME);
							APP_TRACE(szDbgMsg);

							//Release the file after adding new TOR record
							releaseMutexLock(&gptTORRecordFileMutex, "TOR Record File");
						}
					}
				}
				else {
					status = EXIT_FAILURE;
				}
			}
		}
		/*end*/

		/*
		 * =========================================
		 * STEP #5 Parse Vantiv response
		 * =========================================
		 */
		if (status == EXIT_SUCCESS) {
#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s: VANTIV [%s]", __FUNCTION__, pVresponse);
			APP_TRACE(szDbgMsg);
#endif
			debug_sprintf(szDbgMsg, "%s: STEP #5 Parse Vantiv response", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			status = parseVantivResponse(pVresponse, GLOBAL, &vrinfo, &svbninfo, &svtvbn, req_xml_info);

			if(GLOBAL->appErrCode != eERR_NOERROR) //If there is any error while parsing the Vantiv response, need to send the error
			{
				debug_sprintf(szDbgMsg, "%s: Some Known error, need to send the correct error code to SCA", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				status = EXIT_SUCCESS;
			}
		}
		else {
			debug_sprintf(szDbgMsg, "%s: STEP #4 ERROR", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(GLOBAL->http.method != eHEAD) //Praveen_P1: Logging if it is not the Connection Status request, otherwise for connection status these statements getting logged
			{
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Failed to Post the VHI Request to Vantiv Host.");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, NULL);
				}
			}

			char* pSAF_TRAN = getXmlTagValue(eSAF_TRAN, req_xml_info);

			if(pSAF_TRAN == NULL)
			{
				pSAF_TRAN = "0";
			}

			if((strcmp(pSAF_TRAN, "1") == 0) && (GLOBAL->appErrCode == eERR_HOST_HTTP_ERROR))
			{
				debug_sprintf(szDbgMsg, "%s: Its SAF transaction and it is HTTP error so returning host not available", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				status = EXIT_FAILURE;
				GLOBAL->isProcessorAvailable = false;
				GLOBAL->http.status_code = eHTTP_OK;
				GLOBAL->appErrCode = eERR_NOERROR;
			}

			/*if Vantiv not available*/
			if (GLOBAL->isProcessorAvailable == false) {
				status = EXIT_SUCCESS;
			}

			if(GLOBAL->appErrCode != eERR_NOERROR)
			{
				debug_sprintf(szDbgMsg, "%s: Some Known error, need to send the correct error code to SCA", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				status = EXIT_SUCCESS;
			}
			/*end*/
		}
		break; //Breaking from the while loop
	}

	/*
	 * =========================================
	 * STEP #6 Compose SCA response
	 * =========================================
	 */
	if (status == EXIT_SUCCESS) {

		/*
		 * Praveen_P1: 7 Nov 2016
		 * As per the APR#1000251 from Nordstrom, need to change
		 * the processing of SAF SALE transactions as below
		 * If a SAF transaction returns decline or referral,
		 * SCA to flip transaction to Force-Post and send to Vantiv
		 * Before sending response to SCA, Will check if it is SAF
		 * transaction and repost as POST_AUTH if required
		 *
		 */
		char* pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);
		if (pCOMMAND == NULL) {
			debug_sprintf(szDbgMsg, "%s: ERROR! COMMAND is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			pCOMMAND = "HELLO"; //Filling with dummy value so that below check would fail
		}

		char* pSAF_TRAN = getXmlTagValue(eSAF_TRAN, req_xml_info);
		if(pSAF_TRAN == NULL)
		{
			pSAF_TRAN = "0";
		}

		debug_sprintf(szDbgMsg, "%s: pSAF_TRAN[%s], GLOBAL->appErrCode[%d], pCOMMAND[%s], GCONFIG.sendSAFdeclineaspostauth[%d]", __FUNCTION__, pSAF_TRAN, GLOBAL->appErrCode, pCOMMAND, GCONFIG.sendSAFdeclineaspostauth);
		APP_TRACE(szDbgMsg);

		if( (strcmp(pSAF_TRAN, "1") == 0) &&
							(GLOBAL->appErrCode == eERR_NOERROR) &&
								(GLOBAL->isProcessorAvailable == true) &&
									(strncmp(pCOMMAND, COMMAND.SALE, strlen(COMMAND.SALE)) == COMPARE_OK) &&
										(GCONFIG.sendSAFdeclineaspostauth == true))
		{
			debug_sprintf(szDbgMsg, "%s: Its SAF-SALE transaction, checking if repost as POST_AUTH is required", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			sendSAFSaleAsPostAuth(GLOBAL, &vhi_req_info, &LocGCONFIG, req_xml_info, &vhi610info, &vhi_fieldinfo, &vhi_res_info, &sR008_info, &svbninfo, &svtvbn, &vrinfo);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Its Not Required to re-post as POST_AUTH", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		debug_sprintf(szDbgMsg, "%s: STEP #6 Compose XML response to SCA", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		if(GLOBAL->appErrCode != eERR_NOERROR)
		{
			debug_sprintf(szDbgMsg, "%s: Need to send error response to SCA", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
					sprintf(szAppLogData, "Composing Error Response to SCA" );
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
			}

			status = composeVhiXmlErrResponse(conn, GLOBAL, &vhi_res_info, req_xml_info);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Need to send response to SCA", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			status = composeVhiXmlResponse(conn, GLOBAL, &vrinfo, &svbninfo, &sR017_info, &sR008_info, &sR011_info, &sR023_info, &sR029_info, &vhi_res_info, &LocGCONFIG, req_xml_info, &vhi_fieldinfo);
		}
	}
	else {
		debug_sprintf(szDbgMsg, "%s: STEP #5 ERROR", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(GLOBAL->http.method != eHEAD) //Praveen_P1: Logging if it is not the Connection Status request, otherwise for connection status these statements getting logged
		{
			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Error! Failed to Parse the Response From Vantiv Host.");
				strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, szAppLogDiag);
			}
		}

		if(GLOBAL->appErrCode != eERR_NOERROR)
		{
			debug_sprintf(szDbgMsg, "%s: Some Known error, need to send the correct error code to SCA", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			status = EXIT_SUCCESS;
		}
	}


	/*
	 * =========================================
	 * STEP #7 Send the response
	 * =========================================
	 */
	if (status == EXIT_SUCCESS) {
#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s: XML Response[%s]", __FUNCTION__, GLOBAL->http.xml_response);
		APP_TRACE(szDbgMsg);
#endif
		debug_sprintf(szDbgMsg, "%s: STEP #7 Send HTTP response to SCA", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		status = responseHttpSuccess(conn, GLOBAL);

		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "Successfully Written Response to the Client Socket");
			addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_SENT, szAppLogData, NULL);
		}
	}
	else {
		debug_sprintf(szDbgMsg, "%s: STEP #6 ERROR", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(GLOBAL->http.method != eHEAD) //Praveen_P1: Logging if it is not the Connection Status request, otherwise for connection status these statements getting logged
		{
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Error, Composition of SCA Response Message Failed");
				strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, szAppLogDiag);
			}
		}

		status = responseHttpError(conn, GLOBAL);
	}

	if (status == EXIT_SUCCESS) {
		if (GLOBAL->appErrCode == eERR_HOST_TIMED_OUT) {
			debug_sprintf(szDbgMsg, "%s: Transaction timedout, incrementing stan", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			incrementStan(GLOBAL, req_xml_info);
		}
	}
	else {

		responseHttpError(conn, GLOBAL);
		debug_sprintf(szDbgMsg, "%s: STEP #7 ERROR", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/*
	 * ============================================================================================
	 * STEP #8 Store the current transaction details for LAST_TRAN request.
	 * ============================================================================================
	 */
	if(status == EXIT_SUCCESS) {

		char* pFUNCTION_TYPE = getXmlTagValue(eFUNCTION_TYPE, req_xml_info);
		char* pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);

		debug_sprintf(szDbgMsg, "%s: Function Type is %s", __FUNCTION__, pFUNCTION_TYPE);
		APP_TRACE(szDbgMsg);

		if ( (pFUNCTION_TYPE != NULL) && (!strcmp(pFUNCTION_TYPE, "PAYMENT")) ) {

			if((bHitHost == true) && ((strncmp(pCOMMAND, COMMAND.SIGNATURE, 7) != COMPARE_OK)) )//For Signature we need not update the last tran
			{
				/*
				 * Praveen_P1: 17 Nov 2016
				 * Fix for PTMX-1578 - Will not update Last Tran
				 * If it is SAF Tran
				 */
				char* pSAF_TRAN = getXmlTagValue(eSAF_TRAN, req_xml_info);
				if(pSAF_TRAN == NULL)
				{
					pSAF_TRAN = "0";
				}
				if( strcmp(pSAF_TRAN, "0") == 0) //Will update the LAst Tran for non-SAF Transactions
				{
					status = updateLastTranDtls(GLOBAL, &svbninfo, &sR023_info,  &sR017_info, &sR008_info, &vhi_res_info, req_xml_info, &GCONFIG, &vhi_fieldinfo);

					if (status == EXIT_SUCCESS) {

						debug_sprintf(szDbgMsg, "%s: Successfully updated the Last transaction details", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						if(iAppLogEnabled)
						{
							sprintf(szAppLogData, "Successfully Updated The Last Transaction Details");
							addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSED, szAppLogData, NULL);
						}
					}
					else {
						debug_sprintf(szDbgMsg, "%s: Failure while updating Last transaction details", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						if(iAppLogEnabled)
						{
							strcpy(szAppLogDiag, " Failure While Updating Last Transaction Details");
							addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, NULL, szAppLogDiag);
						}
					}
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: STEP #8 No need to store current transaction details, Its SAF Transaction", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: STEP #8 No need to store current transaction details, did not hit the host", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

		}
		else {
			debug_sprintf(szDbgMsg, "%s: STEP #8 No need to store current transaction details, its not payment function type", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	else {
		debug_sprintf(szDbgMsg, "%s: STEP #8 No need to store current transaction details", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	/*
	 * =========================================
	 * STEP #9 Free data
	 * =========================================
	 */
    
#ifdef OLD_EMV_TAG_METHOD
    if (sR023_info.script71 != NULL)
        free(sR023_info.script71);
    sR023_info.script71= NULL;
    if (sR023_info.script72 != NULL)
        free(sR023_info.script72);
    sR023_info.script72= NULL;
#endif

	freeAllocatedData(&vhi_req_info, &vrinfo, &vhi_res_info, req_xml_info);
	freeServiceData(GLOBAL);

	close(conn);

	debug_sprintf(szDbgMsg, "%s: -------SERVICE END-------", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(iAppLogEnabled)
	{
		sprintf(szAppLogData, "Thread ID [%d] Serviced This Request", (int)threadID);
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START, szAppLogData, NULL);

		strcpy(szAppLogData, END_INDICATOR);
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_COMMAND_END, szAppLogData, NULL);

	}


	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

    return NULL;

}

/*
 * This function would repost Declined SAF SALE transaction as POST_AUTH
 */
static void sendSAFSaleAsPostAuth(sGLOBALS* GLOBAL, sVHI_REQ * pstvhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo, sVHI_RESPONSE_INFO *vhi_res_info, sR008_data *sR008_info, _sVTV_FIELDS *svbninfo, sVANTIV_BN *svtvbn, sVANTIV_RESPONSE * vrinfo)
{
#ifdef LOGGING_ENABLED
	char szDbgMsg[5012] = {0};
#endif

	int 	status 		 			= EXIT_SUCCESS;
	int		iAppLogEnabled			= isAppLogEnabled();
	FILE *		Fh		 			= NULL;
	char		szAppLogData[2560]	= "";
	char		szAppLogDiag[300]	= "";

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
	/*
	 * Step 1 - Check if current transaction is DECLINED
	 * Step 2 - Change the command in the XML request
	 * Step 3 - Compose the Vantiv Request
	 * Step 4 - Send/Receive Request/Response To/From Vantiv
	 * Step 5 - Parse the response
	 * Step 6 - Return from this Function
	 *
	 */

	while(1)
	{
		/*
		 * ============================================================================================
		 * STEP #1 Check the Current Status of the SAF SALE transaction
		 * ============================================================================================
		 */

		VHI_RESULT(GLOBAL, svbninfo, sR008_info, vhi_res_info, GCONFIG, req_xml_info);

		debug_sprintf(szDbgMsg, "%s: The Result Code for the current SAF transaction is %s", __FUNCTION__, vhi_res_info->result_code);
		APP_TRACE(szDbgMsg);

		if( !((strcmp(vhi_res_info->result_code, "6") == 0) || (strcmp(vhi_res_info->result_code, "0") == 0)) )
		{
			debug_sprintf(szDbgMsg, "%s: Current SAF transaction status is NOT Declined, no need to post as POSTAUTH transaction", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		debug_sprintf(szDbgMsg, "%s: Its SAF Credit SALE transaction and its Declined, need to send as POST_AUTH", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		/*
		 * ============================================================================================
		 * STEP #2 Change the Command in the XML request
		 * ============================================================================================
		 */

		/*
		 * Will change the command value to POST_AUTH in the XML element buffer for internal purpose
		 */

		debug_sprintf(szDbgMsg, "%s: STEP #2 Change the Command from SALE to POST_AUTH", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(req_xml_info[eCOMMAND].element != NULL)
		{
			free(req_xml_info[eCOMMAND].element);

			req_xml_info[eCOMMAND].element = (char*)calloc(strlen("POST_AUTH")+1, sizeof(char));
			strcpy(req_xml_info[eCOMMAND].element, "POST_AUTH");
		}

		debug_sprintf(szDbgMsg, "%s: Command [%s]", __FUNCTION__, req_xml_info[eCOMMAND].element);
		APP_TRACE(szDbgMsg);

		/*
		 * ============================================================================================
		 * STEP #3 Compose the Request to Vantiv as POST_AUTH transaction
		 * ============================================================================================
		 */

		debug_sprintf(szDbgMsg, "%s: STEP #2 Compose the request to Vantiv as POST_AUTH Transaction", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		status = composeVantivRequest(GLOBAL, pstvhi_req_info, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);

		char pVresponse[MAX_DATA_TO_RECEIVE] = {0};
		char* pRequest = NULL;

		if (status == EXIT_SUCCESS)
		{
			/*
			 * ============================================================================================
			 * STEP #4 Send/Receive Request/Response To/From Vantiv
			 * ============================================================================================
			 */
			debug_sprintf(szDbgMsg, "%s: STEP #4 Send request, received response of Vantiv via Libcurl", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pRequest = pstvhi_req_info->main;

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Current SAF Transaction is Decline, Reposting as POST_AUTH Transaction");
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);

				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_VANTIV_REQUEST, pRequest, NULL);
			}

#ifdef DEVDEBUG
			if(strlen(pRequest) < 4050)
			{
				debug_sprintf(szDbgMsg, "%s: VHI [%s]", __FUNCTION__, pRequest);
				APP_TRACE(szDbgMsg);
			}
#endif
			status = sendRecvViaCurl(pRequest, pVresponse, GLOBAL, GCONFIG, req_xml_info);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: STEP #4 ERROR", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Error, Failed to Frame VHI Request While Reposting Declined SAF SALE Transaction As POST_AUTH");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, NULL);
			}

			break; //Not Modifying response fields so that original SALE response would go to SCA which is DECLINED
		}

		/*
		 * =========================================
		 * SPECIAL CASE: TIMEOUT REVERSAL
		 * =========================================
		 */
		if (status == EXIT_FAILURE && GLOBAL->http.status_code == eHTTP_GATEWAY_TIMEOUT)
		{
			int i = 0;

			for (i = 0; i < GCONFIG->torRetry; i++)
			{
				debug_sprintf(szDbgMsg, "%s: #SPECIAL CASE: TIMEOUT REVERSAL PROCCESSING for [%d] time", __FUNCTION__, i+1);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "TIMEOUT REVERSAL PROCCESSING for [%d] time",i+1);
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
				}

				status = composeVantivRequest(GLOBAL, pstvhi_req_info, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);
				pRequest = NULL;

				pRequest = pstvhi_req_info->main;

		#ifdef DEVDEBUG
				if(strlen(pRequest) < 4050)
				{
					debug_sprintf(szDbgMsg, "%s: VHI TOR REQUEST [%s]", __FUNCTION__, pRequest);
					APP_TRACE(szDbgMsg);
				}
		#endif
				if (status == EXIT_SUCCESS)
				{
					if(iAppLogEnabled)
					{
						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_VANTIV_REQUEST, pRequest, NULL);
					}

					memset(pVresponse, 0x00, sizeof(pVresponse));
					status = sendRecvViaCurl( pRequest, pVresponse, GLOBAL, GCONFIG, req_xml_info);

					if (status == EXIT_SUCCESS) {

						char* pSAF_TRAN = getXmlTagValue(eSAF_TRAN, req_xml_info);

						if(pSAF_TRAN == NULL)
						{
							pSAF_TRAN = "0";
						}

						if(strcmp(pSAF_TRAN, "1") == 0)
						{
							if(iAppLogEnabled)
							{
								addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_VANTIV_RESPONSE, pVresponse, NULL);

								strcpy(szAppLogData, "Received Response for TOR Request, Ignoring it Since it is SAF Transaction");
								addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
							}

							debug_sprintf(szDbgMsg, "%s: Its SAF transaction, TOR success should not be considered", __FUNCTION__);
							APP_TRACE(szDbgMsg);
							status = EXIT_FAILURE;
							GLOBAL->isProcessorAvailable = false;
							GLOBAL->http.status_code = eHTTP_OK;
							GLOBAL->appErrCode = eERR_NOERROR;
						}
						else
						{
							break;
						}
					}
				}
			}
			if(status != EXIT_SUCCESS)
			{
				if(GLOBAL->http.status_code == eHTTP_GATEWAY_TIMEOUT)
				{
					GLOBAL->http.status_code = eHTTP_OK;
					GLOBAL->appErrCode = eERR_HOST_TIMED_OUT;

					debug_sprintf(szDbgMsg, "%s: Maximum TOR Retries reached, need to store the TOR request to file", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					if(iAppLogEnabled)
					{
						sprintf(szAppLogData, "Maximum TOR Retries Reached, Need to Store the TOR Request to File");
						addAppEventLog(APP_NAME, ENTRYTYPE_INFO,ENTRYID_PROCESSED, szAppLogData, NULL);
					}

					/*
					 * We have a TOR request here which could not get response from processor
					 * storing the TOR request to the file so that
					 * TOR reqtires can happen on background
					 */

					//Acquire the File to add a new TOR record

					debug_sprintf(szDbgMsg, "%s: Acquiring the TOR file[%s] to add the new TOR request", __FUNCTION__, TOR_RECORD_FILE_NAME);
					APP_TRACE(szDbgMsg);

					acquireMutexLock(&gptTORRecordFileMutex, "TOR Record File");
					/*
					 * Open file for output at the end of a file.
					 * Output operations always write data at the end of the file, expanding it.
					 * The file is created if it does not exist
					 */
					Fh = fopen(TOR_RECORD_FILE_NAME, "a+");
					if(Fh != NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Opened the TOR file in an append mode!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						/*
						 * Add the TOR request to the file
						 */
						int iRecordLength = 0;
						int iRetVal       = 0;

						iRecordLength = strlen(pRequest) + 1; //one for new line character i.e. LF

						char szTORRecord[iRecordLength + 1]; //one for NULL character

						memset(szTORRecord, 0x00, iRecordLength + 1);

						sprintf(szTORRecord,"%s%s", pRequest, LF); //LF New line field
				#ifdef DEVDEBUG
						if(strlen(szTORRecord) < 4050)
						{
							debug_sprintf(szDbgMsg, "%s: TOR Record [%s]", __FUNCTION__, szTORRecord);
							APP_TRACE(szDbgMsg);
						}
					#endif
						iRetVal = fwrite(szTORRecord, sizeof(char), iRecordLength, Fh);

						if(iRetVal == iRecordLength)
						{
							debug_sprintf(szDbgMsg, "%s: Successfully written TOR record to the file ", __FUNCTION__);
							APP_TRACE(szDbgMsg);
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Failure while writing TOR record to the file, Actual count[%d], Written bytes [%d] ", __FUNCTION__, iRecordLength, iRetVal);
							APP_TRACE(szDbgMsg);

							if(iAppLogEnabled)
							{
								sprintf(szAppLogData, "Warning! Failure While writing TOR Record to the file");
								sprintf(szAppLogDiag, " Failure While writing TOR Record to the file");
								addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_PROCESSING,szAppLogData, szAppLogDiag);
							}
						}

						/*
						 * Close the file
						 */
						fclose(Fh);

					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Error while opening the TOR record file!!!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}

					debug_sprintf(szDbgMsg, "%s: Releasing the TOR file[%s] after adding the new TOR request", __FUNCTION__, TOR_RECORD_FILE_NAME);
					APP_TRACE(szDbgMsg);

					//Release the file after adding new TOR record
					releaseMutexLock(&gptTORRecordFileMutex, "TOR Record File");
				}
			}
			break; //Will send response based on the TOR response here which would be Host Not Available
		}

		if (status == EXIT_SUCCESS)
		{
			/*
			 * ============================================================================================
			 * STEP #5 Parse the Response from Vantiv
			 * ============================================================================================
			 */

#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s: VANTIV [%s]", __FUNCTION__, pVresponse);
			APP_TRACE(szDbgMsg);
#endif
			debug_sprintf(szDbgMsg, "%s: STEP #5 Parse Vantiv response", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			status = parseVantivResponse(pVresponse, GLOBAL, vrinfo, svbninfo, svtvbn, req_xml_info);

		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: STEP #4 ERROR", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(GLOBAL->appErrCode == eERR_HOST_HTTP_ERROR)
			{
				debug_sprintf(szDbgMsg, "%s: Its SAF transaction and it is HTTP error so returning host not available", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				status = EXIT_FAILURE;
				GLOBAL->isProcessorAvailable = false;
				GLOBAL->http.status_code = eHTTP_OK;
				GLOBAL->appErrCode = eERR_NOERROR;
			}
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}
