/*
 * config_data.c
 *
 *  Created on: Oct 25, 2013
 *      Author: Author: Dexter M. Alberto
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <svc.h>
#include <svcsec.h>
#include "config_data.h"
#include "logger.h"
#include "help_function.h"

int setConfigData()
{
#ifdef LOGGING_ENABLED
	char 	szDbgMsg[512] 	= "";
#endif
	char	szTemp[100]		= "";
	int		maxLen			= 0;
	char	*tempPtr		= NULL;
	char	*curPtr			= NULL;
	char	*nxtPtr			= NULL;
	int		rv				= 0;
	int		index			= 0;
	FILE* 	pFile			= NULL;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	GCONFIG.isRegistartMode				= false;
	GCONFIG.isSAFEnabled        		= false;
	GCONFIG.emvenabled					= false;
	GCONFIG.verifyHost					= true;
	GCONFIG.verifyHost2					= true;
	GCONFIG.useorigtimeforsaf			= false;
	GCONFIG.sigcapturerefenabled 		= false;
	GCONFIG.sendSAFdeclineaspostauth 	= false;
	GCONFIG.torTimeout					= 0;
	GCONFIG.torRetry					= 0;
    GCONFIG.adjcapable					= 'N';
	GCONFIG.atype1						= eRETAIL_TYPE;
	GCONFIG.eTokenMode					= eTOKEN_OFF;
	GCONFIG.vantivURL					= NULL;
	GCONFIG.vantivSecdURL				= NULL;
	GCONFIG.hostUsrPwd					= NULL;
	memset(GCONFIG.bankID, 		 0x00, sizeof(GCONFIG.bankID));
	memset(GCONFIG.laneID, 		 0x00, sizeof(GCONFIG.laneID));
	memset(GCONFIG.tid,    		 0x00, sizeof(GCONFIG.tid));
	memset(GCONFIG.mid,    		 0x00, sizeof(GCONFIG.mid));
	memset(GCONFIG.safLaneID,    0x00, sizeof(GCONFIG.safLaneID));

	addEnvSection("DHI");


	/*vsp registration flag*/
	char vspenabled[1+1] = {0};
	if (getEnvFile("dct", "vspenabled", vspenabled, sizeof(vspenabled)) <= 0)
    {
        strcpy(vspenabled, "N");
	}

	if (strncmp(vspenabled, "Y", 1) == COMPARE_OK) {
		GCONFIG.isRegistartMode = true;
	}
	else if (strncmp(vspenabled, "N", 1) == COMPARE_OK) {
		GCONFIG.isRegistartMode = false;
	}
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! vspenabled invalid", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}
	/*end*/

	/*SAF Enabled flag*/
	char safenabled[1+1] = {0};
	if (getEnvFile("pdt", "safenabled", safenabled, sizeof(safenabled)) <= 0)
    {
        strcpy(safenabled, "N");
	}

	if (strncmp(safenabled, "Y", 1) == COMPARE_OK) {
		GCONFIG.isSAFEnabled = true;
	}
	else if (strncmp(safenabled, "N", 1) == COMPARE_OK) {
		GCONFIG.isSAFEnabled = false;
	}
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! safenabled invalid", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}
	/*end*/

	/*EMV enabled flag*/
	char emvenabled[1+1] = {0};
	if (getEnvFile("dct", "emvenabled", emvenabled, sizeof(emvenabled)) <= 0)
	{
		strcpy(emvenabled, "N");
	}

	if (strncmp(emvenabled, "Y", 1) == COMPARE_OK) {
		GCONFIG.emvenabled = true;
	}
	else if (strncmp(emvenabled, "N", 1) == COMPARE_OK) {
		GCONFIG.emvenabled = false;
	}
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! emvenabled invalid", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}
	/*end*/

	/*useorigtimeinsafreq flag*/
	char useorigtimeinsafreq[1+1] = {0};
	if (getEnvFile("dhi", "useorigtimeinsafreq", useorigtimeinsafreq, sizeof(useorigtimeinsafreq)) <= 0)
	{
		strcpy(useorigtimeinsafreq, "N");
	}

	if (strncmp(useorigtimeinsafreq, "Y", 1) == COMPARE_OK) {
		GCONFIG.useorigtimeforsaf = true;
	}
	else if (strncmp(useorigtimeinsafreq, "N", 1) == COMPARE_OK) {
		GCONFIG.useorigtimeforsaf = false;
	}
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! useorigtimeinsafreq invalid, setting to Default Value", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		GCONFIG.useorigtimeforsaf = false;
	}
	/*end*/

	/*Signature Capture Reference flag*/
	char sigcatpurerefEnabled[1+1] = {0};
	if (getEnvFile("dhi", "sigcaptrref", sigcatpurerefEnabled, sizeof(sigcatpurerefEnabled)) <= 0)
	{
		strcpy(sigcatpurerefEnabled, "N");
	}

	if (strncmp(sigcatpurerefEnabled, "Y", 1) == COMPARE_OK) {
		GCONFIG.sigcapturerefenabled = true;
	}
	else if (strncmp(sigcatpurerefEnabled, "N", 1) == COMPARE_OK) {
		GCONFIG.sigcapturerefenabled = false;
	}
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! invalid value set for sigcaptrref", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	/*end*/

	/*tor_timeout*/
	char tor_timeout[3+1] = {0};
	if (getEnvFile("DHI", "tor_timeout", tor_timeout, sizeof(tor_timeout)) <= 0) {
		setEnv("DHI", "tor_timeout", "60"); //default 60
		sprintf(tor_timeout, "60");
	}

	GCONFIG.torTimeout = atoi(tor_timeout);
	if (GCONFIG.torTimeout < 0) {
		debug_sprintf(szDbgMsg, "%s: ERROR! config tor_timeout < 0", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}
	/*end*/

	/*conn_timeout*/
	char conn_timeout[3+1] = {0};
	if (getEnvFile("DHI", "conn_timeout", conn_timeout, sizeof(conn_timeout)) <= 0) {
		//setEnv("DHI", "conn_timeout", tor_timeout);
		sprintf(conn_timeout, tor_timeout); //default to what tor_timeout set to
	}

	GCONFIG.connTimeout = atoi(conn_timeout);
	if (GCONFIG.connTimeout < 0) {
		debug_sprintf(szDbgMsg, "%s: ERROR! config conn_timeout < 0", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}
	/*end*/

	/*tor_retry*/
	char tor_retry[3+1] = {0};
	if (getEnvFile("DHI", "tor_retry", tor_retry, sizeof(tor_retry)) <= 0) {
		setEnv("DHI", "tor_retry", "3"); //default 3
		sprintf(tor_retry, "3");
	}

	GCONFIG.torRetry = atoi(tor_retry);

	if (GCONFIG.torRetry < 0) {
		debug_sprintf(szDbgMsg, "%s: ERROR! tor_retry < 0", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}
	/*end*/


	/*bankid*/
	char bankid[4+1] = {0};
	if (getEnvFile("DHI", "bankid", bankid, sizeof(bankid)) <= 0) {
		//setEnv("DHI", "bankid", "1340"); //default 1340
		//sprintf(bankid, "1340");
	}
	else
	{
		strcpy(GCONFIG.bankID, bankid);
	}

	if (strlen(GCONFIG.bankID) <= 0) {
		debug_sprintf(szDbgMsg, "%s: WARNING! bankid is not present", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//return EXIT_FAILURE;
	}

	/*end*/


	/*MID*/
	char mid[12+1] = {0};
	if (getEnvFile("DHI", "mid", mid, sizeof(mid)) <= 0) {
	}
	else
	{
		strcpy(GCONFIG.mid, mid);
	}

	if (strlen(GCONFIG.mid) <= 0) {
		debug_sprintf(szDbgMsg, "%s: WARNING! mid is not present", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//return EXIT_FAILURE;
	}
	/*end*/


	/*tid*/
	char tid[3+1] = {0};
	if (getEnvFile("DHI", "tid", tid, sizeof(tid)) <= 0) {
	}
	else
	{
		strcpy(GCONFIG.tid, tid);
	}

	if (strlen(GCONFIG.tid) <= 0) {
		debug_sprintf(szDbgMsg, "%s: WARNING! tid is not present", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//return EXIT_FAILURE;
	}
	/*end*/


	/*laneid*/
	char laneid[4+1] = {0};
	if (getEnvFile("DHI", "laneid", laneid, sizeof(laneid)) <= 0) {
	}
	else
	{
		strcpy(GCONFIG.laneID, laneid);
	}

	if (strlen(GCONFIG.laneID) <= 0) {
		debug_sprintf(szDbgMsg, "%s: WARNING! LaneID is not present", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//return EXIT_FAILURE;
	}
	/*atype1*/
	/*
	 * 0-Retail
	 * 1-Restaurant
	 * 2-CAT
	 */
	char atype1[1+1] = {0};
	if (getEnvFile("dct", "atype1", atype1, sizeof(atype1)) <= 0) {
		setEnv("dct", "atype1", "0"); //deafult 0
	}

	GCONFIG.atype1 = atoi(atype1);

	if (GCONFIG.atype1 < 0 || GCONFIG.atype1 > 2) {
		debug_sprintf(szDbgMsg, "%s: ERROR! atype1 invalid", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}
	/*end*/


	/*adjcapable*/
	char adjcapable[1+1] = {0};
	if (getEnvFile("DHI", "adjcapable", adjcapable, sizeof(adjcapable)) <= 0)
    {
		setEnv("DHI", "adjcapable", "N"); //default N
        strcpy(adjcapable, "N");
	}

	if (strncmp(adjcapable, "Y", 1) == COMPARE_OK) {
		GCONFIG.adjcapable = 'Y';
	}
	else if (strncmp(adjcapable, "N", 1) == COMPARE_OK) {
		GCONFIG.adjcapable = 'N';
	}
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! adjcapable invalid! input 'Y' or 'N'", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}
	/*end*/

	/*sendSAFdeclineaspostauth*/
	char sendSAFdeclineaspostauth[1+1] = {0};
	if (getEnvFile("DHI", "sendSAFdeclineaspostauth", sendSAFdeclineaspostauth, sizeof(sendSAFdeclineaspostauth)) <= 0)
	{
		setEnv("DHI", "sendSAFsaleaspostauth", "N"); //default N
		strcpy(sendSAFdeclineaspostauth, "N");
	}

	if (strncmp(sendSAFdeclineaspostauth, "Y", 1) == COMPARE_OK) {
		GCONFIG.sendSAFdeclineaspostauth = true;
	}
	else if (strncmp(sendSAFdeclineaspostauth, "N", 1) == COMPARE_OK) {
		GCONFIG.sendSAFdeclineaspostauth = false;
	}
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! sendSAFdeclineaspostauth invalid! Setting to default value N", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		GCONFIG.sendSAFdeclineaspostauth = false;

	}
	/*end*/
    
	/*STAN*/
	char stan[9+1] = {0};
	if (getEnvFile("DHI", "STAN", stan, sizeof(stan)) <= 0) {
		setEnv("DHI", "STAN", "1"); //default 1
	}
	/*end*/


	/*token_mode*/
	/*
	 * 0 - token disable
	 * 1 - TRT request token
	 * 2 - TUT use token (dont use)
	 */
	char token_mode[1+1] = {0};
	if (getEnvFile("DHI", "token_mode", token_mode, sizeof(token_mode)) <= 0) {
		setEnv("DHI", "token_mode", "0"); //deafult 0
		sprintf(token_mode, "0");
	}

	GCONFIG.eTokenMode = atoi(token_mode);

	if (GCONFIG.eTokenMode < 0 || GCONFIG.eTokenMode > 1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! token_mode invalid! input 0-1", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}
	/*end*/

	/* Vantiv Host URL */
	maxLen = sizeof(szTemp);
	memset(szTemp, 0x00, maxLen);

	/* Get Host URL */
	rv = getEnvFile("DHI", "hosturl", szTemp, maxLen);
	if(rv > 0)
	{
#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s: Host URL = [%s]",
												__FUNCTION__, szTemp);
		APP_TRACE(szDbgMsg);
#endif
		/* NOTE: validation of url remaining */
		tempPtr = (char *) malloc((rv + 1) * sizeof(char));
		if(tempPtr)
		{
			memset(tempPtr, 0x00, rv + 1);
			memcpy(tempPtr, szTemp, rv);
			GCONFIG.vantivURL = tempPtr;
			tempPtr = NULL;
		}
		else
		{
			/* Memory allocation failed for storing host URL */
			debug_sprintf(szDbgMsg,
					"%s - memory allocation failed for host URL",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
			return EXIT_FAILURE;
		}
	}
	else
	{
		/* URL not found */
		debug_sprintf(szDbgMsg, "%s - Host URL not found",
														__FUNCTION__);
		APP_TRACE(szDbgMsg);

		//Need to have some default value
		//"https://cert.ssl53.com/AUTH"
		return EXIT_FAILURE;
	}
	/*end*/

	/* Vantiv Host Secondary URL */
	maxLen = sizeof(szTemp);
	memset(szTemp, 0x00, maxLen);

	/* Get Host URL */
	rv = getEnvFile("DHI", "hostscndurl", szTemp, maxLen);
	if(rv > 0)
	{
#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s: Host Secondary URL = [%s]",
												__FUNCTION__, szTemp);
		APP_TRACE(szDbgMsg);
#endif
		/* NOTE: validation of url remaining */
		tempPtr = (char *) malloc((rv + 1) * sizeof(char));
		if(tempPtr)
		{
			memset(tempPtr, 0x00, rv + 1);
			memcpy(tempPtr, szTemp, rv);
			GCONFIG.vantivSecdURL = tempPtr;
			tempPtr = NULL;
		}
		else
		{
			/* Memory allocation failed for storing host URL */
			debug_sprintf(szDbgMsg,
					"%s - memory allocation failed for host Secondary URL",
					__FUNCTION__);
			APP_TRACE(szDbgMsg);
			return EXIT_FAILURE;
		}
	}
	else
	{
		/* URL not found */
		debug_sprintf(szDbgMsg, "%s - Host Secondary URL not found",
														__FUNCTION__);
		APP_TRACE(szDbgMsg);

		GCONFIG.vantivSecdURL = NULL;
	}
	/*end*/

	/* Vantiv TPS Error codes to consider them as Host NOT Available */
	maxLen = sizeof(szTemp);
	memset(szTemp, 0x00, maxLen);

	/* Get Host UserId and Password */
	char* fileName = TEXTFILE_TPSCODES;

	for(index = 0; index < 21; index++)
	{
		memset(GCONFIG.safErrCodes[index], 0x00, sizeof(char) * 10);
	}

	pFile = fopen(fileName, "r");
	if(pFile == NULL)
	{
		debug_sprintf(szDbgMsg, "%s - Not able to open [%s] file", __FUNCTION__, TEXTFILE_TPSCODES);
		APP_TRACE(szDbgMsg);
	}
	else
	{

		if (fgets(szTemp, maxLen-1, pFile) != NULL)
		{
	#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s - The content of [%s] file is [%s]", __FUNCTION__, TEXTFILE_TPSCODES, szTemp);
			APP_TRACE(szDbgMsg);
	#endif
			debug_sprintf(szDbgMsg, "%s: TPS Error Codes [%s]",
															__FUNCTION__, szTemp);
			APP_TRACE(szDbgMsg);

			/*
			 * Need to parse the TPS Error Codes(Max upto 10 error codes)
			 * XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX
			 */

			curPtr = szTemp;

			for(index = 0; index < 20; index++)
			{
				nxtPtr = strchr(curPtr, ',');
				if(nxtPtr != NULL)
				{
					if((nxtPtr - curPtr) > 0)
					{
						strncpy(GCONFIG.safErrCodes[index], curPtr, nxtPtr - curPtr);
					}

					curPtr = nxtPtr + 1;
				}
				else
				{
					strncpy(GCONFIG.safErrCodes[index], curPtr, 3); //Praveen_P1: Assuming that TPS code is only 3 bytes, please correct if it is wrong

					/* Check for characters */
					if(strspn(GCONFIG.safErrCodes[index], "1234567890") != strlen(GCONFIG.safErrCodes[index]))
					{
						debug_sprintf(szDbgMsg, "%s: Characters other than 0-9 found",__FUNCTION__);
						APP_TRACE(szDbgMsg);

						memset(GCONFIG.safErrCodes[index], 0x00, sizeof(char) * 10);
					}

					break;
				}
			}

			for(index = 0; index < 20; index++)
			{
				debug_sprintf(szDbgMsg, "%s: TPS Error Codes [%d] = [%s]",__FUNCTION__, index+1, GCONFIG.safErrCodes[index]);
				APP_TRACE(szDbgMsg);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s - Error while getting the contents of [%s] file", __FUNCTION__, TEXTFILE_TPSCODES);
			APP_TRACE(szDbgMsg);

			/* TPS Error Codes not found */
			debug_sprintf(szDbgMsg, "%s - TPS Error Codes not present, so no mapping in application",
															__FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		if(pFile != NULL)
		{
			fclose(pFile);
		}
	}

	/*end*/

	/* Get Host UserId and Password */
	fileName = TEXTFILE_HOSTUSRPWD;

	maxLen = getTxtFileBufferLength(fileName);
	if(maxLen <= 0)
	{
		debug_sprintf(szDbgMsg, "%s - Empty file [%s]", __FUNCTION__, TEXTFILE_HOSTUSRPWD);
		APP_TRACE(szDbgMsg);
		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s - The length of the file [%d]", __FUNCTION__, maxLen);
	APP_TRACE(szDbgMsg);

	tempPtr = NULL;
	tempPtr = (char *) malloc((maxLen + 1) * sizeof(char));
	if(tempPtr)
	{
		memset(tempPtr, 0x00, maxLen + 1);

		pFile = fopen(fileName, "r");
		if(pFile == NULL)
		{
			debug_sprintf(szDbgMsg, "%s - Not able to open [%s] file", __FUNCTION__, TEXTFILE_HOSTUSRPWD);
			APP_TRACE(szDbgMsg);
			return EXIT_FAILURE;
		}

		if (fgets(tempPtr, maxLen+1, pFile) != NULL)
		{
#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s - The content of [%s] file is [%s]", __FUNCTION__, TEXTFILE_HOSTUSRPWD, tempPtr);
			APP_TRACE(szDbgMsg);
#endif
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s - Error while getting the contents of [%s] file", __FUNCTION__, TEXTFILE_HOSTUSRPWD);
			APP_TRACE(szDbgMsg);
			return EXIT_FAILURE;
		}

		GCONFIG.hostUsrPwd = tempPtr;
#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s - The host user passwd is [%s] ", __FUNCTION__, GCONFIG.hostUsrPwd);
		APP_TRACE(szDbgMsg);
#endif
		tempPtr = NULL;
		fclose(pFile);
	}
	else
	{
		/* Memory allocation failed for storing host URL */
		debug_sprintf(szDbgMsg,
				"%s - memory allocation failed for host userpwd",
				__FUNCTION__);
		APP_TRACE(szDbgMsg);
		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s - %s file contains %d buffer length", __FUNCTION__, TEXTFILE_HOSTUSRPWD, maxLen);
	APP_TRACE(szDbgMsg);

	/*end*/

	/* Check HostURLException file */

	fileName = TEXTFILE_URLEXCEPTION;

	if(isFileExist(fileName) == true)
	{
		if( (isFileExist(TEXTFILE_URLEXCEPTION".P7S") == true) || (isFileExist(TEXTFILE_URLEXCEPTION".p7s") == true) )
		{
			if (authFile(fileName) == 1)
			{
				debug_sprintf(szDbgMsg, "%s - HOTURL Exception File is Authenticated", __FUNCTION__);
				APP_TRACE(szDbgMsg); // TEXTFILE_URLEXCEPTION found in home directory is authenticated

				maxLen = getTxtFileBufferLength(fileName);
				if(maxLen <= 0)
				{
					debug_sprintf(szDbgMsg, "%s - Empty file [%s]", __FUNCTION__, TEXTFILE_URLEXCEPTION);
					APP_TRACE(szDbgMsg);
					return EXIT_FAILURE;
				}

				debug_sprintf(szDbgMsg, "%s - The length of the %s file is [%d]", __FUNCTION__, TEXTFILE_URLEXCEPTION, maxLen);
				APP_TRACE(szDbgMsg);


				tempPtr = NULL;
				tempPtr = (char *) malloc((maxLen + 1) * sizeof(char));
				if(tempPtr)
				{
					memset(tempPtr, 0x00, maxLen + 1);

					pFile = fopen(fileName, "r");
					if(pFile == NULL)
					{
						debug_sprintf(szDbgMsg, "%s - Not able to open [%s] file", __FUNCTION__, TEXTFILE_URLEXCEPTION);
						APP_TRACE(szDbgMsg);
						return EXIT_FAILURE;
					}

					if (fgets(tempPtr, maxLen+1, pFile) != NULL)
					{
						debug_sprintf(szDbgMsg, "%s - The content of [%s] file is [%s]", __FUNCTION__, TEXTFILE_URLEXCEPTION, tempPtr);
						APP_TRACE(szDbgMsg);

						if(tempPtr[strlen(tempPtr)-1] == 0x0A)
						{
							tempPtr[strlen(tempPtr)-1] = '\0';
						}

						if(strcasecmp(tempPtr, GCONFIG.vantivURL) == 0)
						{
							debug_sprintf(szDbgMsg, "%s - Vantiv HostURL is part of the exception file, setting GCONFIG.verifyHost to false", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							GCONFIG.verifyHost = false;
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s - Vantiv HostURL is NOT part of the exception file, setting GCONFIG.verifyHost to true", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							GCONFIG.verifyHost = true;
						}
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s - Error while getting the contents of [%s] file", __FUNCTION__, TEXTFILE_URLEXCEPTION);
						APP_TRACE(szDbgMsg);
						//return EXIT_FAILURE;
					}

					if(GCONFIG.vantivSecdURL != NULL) //secondary url is present, need to check if it is part of the exception
					{
						memset(tempPtr, 0x00, maxLen + 1);

						if (fgets(tempPtr, maxLen+1, pFile) != NULL)
						{

							debug_sprintf(szDbgMsg, "%s - The content of [%s] file is [%s]", __FUNCTION__, TEXTFILE_URLEXCEPTION, tempPtr);
							APP_TRACE(szDbgMsg);

							if(tempPtr[strlen(tempPtr)-1] == 0x0A)
							{
								tempPtr[strlen(tempPtr)-1] = '\0';
							}

							if(strcasecmp(tempPtr, GCONFIG.vantivSecdURL) == 0)
							{
								debug_sprintf(szDbgMsg, "%s - Vantiv HostURL is part of the exception file, setting GCONFIG.verifyHost2 to false", __FUNCTION__);
								APP_TRACE(szDbgMsg);

								GCONFIG.verifyHost2 = false;
							}
							else
							{
								debug_sprintf(szDbgMsg, "%s - Vantiv HostURL is NOT part of the exception file, setting GCONFIG.verifyHost2 to true", __FUNCTION__);
								APP_TRACE(szDbgMsg);

								GCONFIG.verifyHost2 = true;
							}
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s - Error while getting the contents of [%s] file", __FUNCTION__, TEXTFILE_URLEXCEPTION);
							APP_TRACE(szDbgMsg);
							//return EXIT_FAILURE;
						}
					}
					else
					{
						GCONFIG.verifyHost2 = true;
					}

					free(tempPtr);
					fclose(pFile);
				}
				else
				{
					/* Memory allocation failed for storing host URL */
					debug_sprintf(szDbgMsg,"%s - memory allocation failed for host userpwd",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					return EXIT_FAILURE;
				}
			}
			else
			{
				 debug_sprintf(szDbgMsg, "%s - HOTURL Exception is not authenticated, setting GCONFIG.verifyHost,GCONFIG.verifyHost2 to true", __FUNCTION__);
				 APP_TRACE(szDbgMsg); // TEXTFILE_URLEXCEPTION found in home directory is NOT authenticated

				 GCONFIG.verifyHost = true;
				 GCONFIG.verifyHost2 = true;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s - Signature file for %s file is not present, setting GCONFIG.verifyHost,GCONFIG.verifyHost2 to true", __FUNCTION__, TEXTFILE_URLEXCEPTION);
			APP_TRACE(szDbgMsg);

			GCONFIG.verifyHost = true;
			GCONFIG.verifyHost2 = true;
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s - %s file is not present, setting GCONFIG.verifyHost,GCONFIG.verifyHost2  to true", __FUNCTION__, TEXTFILE_URLEXCEPTION);
		APP_TRACE(szDbgMsg);

		GCONFIG.verifyHost = true;
		GCONFIG.verifyHost2 = true;
	}

	/*end*/
	debug_sprintf(szDbgMsg, "%s: Current Configuration is -", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: GCONFIG.isRegistartMode [%d]", __FUNCTION__, GCONFIG.isRegistartMode);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: GCONFIG.isSAFEnabled [%d]", __FUNCTION__, GCONFIG.isSAFEnabled);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: GCONFIG.emvenabled [%d]", __FUNCTION__, GCONFIG.emvenabled);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: GCONFIG.useorigtimeforsaf [%d]", __FUNCTION__, GCONFIG.useorigtimeforsaf);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: GCONFIG.sendSAFdeclineaspostauth [%d]", __FUNCTION__, GCONFIG.sendSAFdeclineaspostauth);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: GCONFIG.verifyHost [%d]", __FUNCTION__, GCONFIG.verifyHost);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: GCONFIG.verifyHost2 [%d]", __FUNCTION__, GCONFIG.verifyHost2);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: GCONFIG.sigcapturerefenabled [%d]", __FUNCTION__, GCONFIG.sigcapturerefenabled);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: GCONFIG.torTimeout [%d]", __FUNCTION__, GCONFIG.torTimeout);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: GCONFIG.connTimeout [%d]", __FUNCTION__, GCONFIG.connTimeout);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: GCONFIG.torRetry [%d]", __FUNCTION__, GCONFIG.torRetry);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: GCONFIG.adjcapable [%c]", __FUNCTION__, GCONFIG.adjcapable);
	APP_TRACE(szDbgMsg);
    
	debug_sprintf(szDbgMsg, "%s: GCONFIG.atype1 [%d]", __FUNCTION__, GCONFIG.atype1);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: GCONFIG.eTokenMode [%d]", __FUNCTION__, GCONFIG.eTokenMode);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: GCONFIG.vantivURL [%s]", __FUNCTION__, GCONFIG.vantivURL);
	APP_TRACE(szDbgMsg);

	if(GCONFIG.vantivSecdURL != NULL)
	{
		debug_sprintf(szDbgMsg, "%s: GCONFIG.vantivSecdURL [%s]", __FUNCTION__, GCONFIG.vantivSecdURL);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: GCONFIG.bankID [%s]", __FUNCTION__, GCONFIG.bankID);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: GCONFIG.laneID [%s]", __FUNCTION__, GCONFIG.laneID);
	APP_TRACE(szDbgMsg);
#if 0
	debug_sprintf(szDbgMsg, "%s: GCONFIG.safLaneID [%s]", __FUNCTION__, GCONFIG.safLaneID);
	APP_TRACE(szDbgMsg);
#endif
	debug_sprintf(szDbgMsg, "%s: GCONFIG.tid [%s]", __FUNCTION__, GCONFIG.tid);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: GCONFIG.mid [%s]", __FUNCTION__, GCONFIG.mid);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

int addEnvSection(char* section)
{
	return putEnvFile(section, "", "");
}

int setEnv(char* section, char* label, char* value)
{
#ifdef LOGGING_ENABLED
	char szDbgMsg[512] = {0};
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(strlen(section) > CONFIG_MAX_SECTION
	|| strlen(label) > CONFIG_MAX_LABEL
	|| strlen(value) > CONFIG_MAX_VALUE) {
		debug_sprintf(szDbgMsg, "%s: ERROR! in adding environment", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return 0;
	}

	short i;
	i = putEnvFile(section, label, ""); //clear first
	i = putEnvFile(section, label, value);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return i;
}
/*
 * Praveen_P1: 17 Feb 2016
 * Not using this function, at the caller places
 * using getenvfile directly
 */
#if 0
char* getEnv(char* section, char* label)
{
#ifdef LOGGING_ENABLED
	char szDbgMsg[512] = {0};
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if((strlen(section) > (CONFIG_MAX_SECTION+1)) || strlen(label) > CONFIG_MAX_LABEL ) {
		debug_sprintf(szDbgMsg, "%s: ERROR! in getting environment", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	memset(cENV_VAL, 0x00, sizeof(cENV_VAL));
	getEnvFile(section, label, cENV_VAL, CONFIG_MAX_VALUE );

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return cENV_VAL;
}
#endif
