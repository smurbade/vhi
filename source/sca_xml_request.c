/*
 * sca_xml_request.c
 *
 *  Created on: Nov 5, 2013
 *      Author: Dexter M. Alberto
 */


#include <string.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/tree.h>
#include "sca_xml_request.h"
#include "appLog.h"
#include "sca_xml_request.inc"
#include "logger.h"
#include "vhi_610_group.h"

const sFUNCTION_TYPE FUNCTION_TYPE = {
	"PAYMENT",
	"BATCH",
	"REPORT",
};

const sCOMMAND COMMAND = {
	"PRE_AUTH",
	"SALE",
	"CREDIT",
	"VOID",
	"POST_AUTH",
	"ADD_TIP",
	"COMPLETION",
	"ADD_VALUE",
	"REMOVE_VALUE",
	"ACTIVATE",
	"DEACTIVATE",
	"BALANCE",
	"SIGNATURE",
	"TOKEN_QUERY",
	"SETTLE",
	"DAYSUMMARY",
	"VERSION",
	"DEVADMIN",
	"TOR_REQ",
    "LAST_TRAN",
    "EMV_ADVICE",
};

static int parseAndStoreLineItem(xmlDocPtr doc, xmlNode * currentNode, _Level3Details_ * level3Dtls, int * piLevel3DtlsFilled);
static int parseAndStoreLevel3Dtls(xmlDocPtr doc, xmlNode * currentNode, _Level3Details_ * level3Dtls);

/*
 * Initialize to NULL Request XML Data
 */
void initializeXmlData(_sSCA_XML_REQUEST *req_xml_info)
{
	sca_buffer = NULL;
	int i;

	for( i=0; i < eREQ_ELEMENT_MAX; i++ ) {
		if( req_xml_info[i].element ) {
			free( req_xml_info[i].element );
		}

		req_xml_info[i].element = NULL;
	}
}


/*
 * Free Request XML Elements Data
 */
void freeXmlData(_sSCA_XML_REQUEST *req_xml_info)
{
	if(sca_buffer) {
		free(sca_buffer);
		sca_buffer = NULL;
	}

	int i;
	for(i= 0; i < eREQ_ELEMENT_MAX; i++) {
		if(req_xml_info[i].element) {
			free(req_xml_info[i].element);
			req_xml_info[i].element = NULL;
		}
	}
}


/*
 * Copy XML data from server to sca_buffer
*/
int copyXml(char* buffer)
{
#ifdef LOGGING_ENABLED
	char szDbgMsg[256] = {0};
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(sca_buffer) {
		free(sca_buffer);
		sca_buffer = NULL;
	}

	if(strstr(buffer, "?xml version") != NULL) {
		while(*buffer != '\n' )
			buffer++;
	}

	int len = strlen(buffer);
	sca_buffer = (char*)calloc(len+1, sizeof(char));

	strcpy(sca_buffer, buffer);

	if( sca_buffer == NULL ) {
		debug_sprintf(szDbgMsg, "%s: ERROR! buffer is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}


/*
 * Get element value
 */
char* getXmlTagValue( enum _eSCA_XML_REQUEST element , _sSCA_XML_REQUEST *req_xml_info)
{
	return req_xml_info[element].element;
}


/*
 * used by SCA_parseReqXMLData() function
 */
xmlXPathObjectPtr getNodeTest (xmlDocPtr doc, xmlChar *xpath)
{
#ifdef LOGGING_ENABLED
	char szDbgMsg[256] = {0};
#endif
	//debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	xmlXPathContextPtr context;
	xmlXPathObjectPtr result;

	context = xmlXPathNewContext(doc);

	if (context == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! buffer is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	result = xmlXPathEvalExpression(xpath, context);
	xmlXPathFreeContext(context);

	if (result == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! in xmlXPathEvalExpression", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		debug_sprintf(szDbgMsg, "%s: Returning NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	if(xmlXPathNodeSetIsEmpty(result->nodesetval)) {
		xmlXPathFreeObject(result);
		//debug_sprintf(szDbgMsg, "%s: Returning NULL", __FUNCTION__);
		//APP_TRACE(szDbgMsg);

		return NULL;
	}

	//debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	return result;
}


/*
 * main funtion to for parsing the SCA xml request
 */
int parseXmlRequest( char* buffer, sGLOBALS* GLOBAL, _sSCA_XML_REQUEST *req_xml_info)
{
#ifdef LOGGING_ENABLED
	char szDbgMsg[4096] = {0};
#endif
	int		iAppLogEnabled		= isAppLogEnabled();
	int		iReturnVal			= EXIT_FAILURE;
	char	szAppLogData[300]	= "";

	xmlChar *attrVal   = NULL;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
    
    debug_sprintf(szDbgMsg, "count of key elements %d, %d", _TOTALkeyELEMENTS, eREQ_ELEMENT_MAX);
    APP_TRACE(szDbgMsg);

	if( buffer == NULL ) {
		debug_sprintf(szDbgMsg, "%s: ERROR! XML is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		GLOBAL->http.status_code = eHTTP_BAD_REQUEST;
		return EXIT_FAILURE;
	}

	/*  Initialized the struct elements and sca_buffer to NULL */
	initializeXmlData(req_xml_info);

	/* Copy the XML buffer to sca_buffer variable */
	copyXml( buffer );

	xmlDocPtr doc;
	xmlNodePtr cur;

	doc = xmlParseMemory(sca_buffer,strlen(sca_buffer));

	if (doc == NULL ) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Document not parsed successfully", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "Failed to Parse the XML Document");
			addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, NULL);
		}

		GLOBAL->http.status_code = eHTTP_BAD_REQUEST;
		return EXIT_FAILURE;
	}

	cur = xmlDocGetRootElement(doc);

	if( xmlStrcmp(  cur->name, (const xmlChar *) "TRANSACTION" ) == COMPARE_OK)
	{
		debug_sprintf(szDbgMsg, "%s: Root node TRANSACTION", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else if(xmlStrcmp(  cur->name, (const xmlChar *) "MSH" ) == COMPARE_OK)
	{
		debug_sprintf(szDbgMsg, "%s: Root node MSH", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//Set function type and command name here
		req_xml_info[eFUNCTION_TYPE].element = (char*)calloc(strlen("ADMIN")+1, sizeof(char));
		strcpy(req_xml_info[eFUNCTION_TYPE].element, "ADMIN");

		req_xml_info[eCOMMAND].element = (char*)calloc(strlen("DEVADMIN")+1, sizeof(char));
		strcpy(req_xml_info[eCOMMAND].element, "DEVADMIN");

		debug_sprintf(szDbgMsg, "%s: Its Device Admin packet, not parsing it now!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_SUCCESS;
	}
	else
	{
		xmlFreeDoc(doc);
		debug_sprintf(szDbgMsg, "%s: ERROR! document of the wrong type, root node != TRANSACTION", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "Failed to Parse the XML Message, Invalid Root Node in XML Message");
			addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, NULL);
		}

		GLOBAL->http.status_code = eHTTP_BAD_REQUEST;
		return EXIT_FAILURE;
	}

	int n = 0;

	for(n = 0; n < eREQ_ELEMENT_MAX; n++)
	{
		/* declaration of variables */

		xmlChar xpath[strlen(__keyELEMENTS[n])+2];
		bzero(xpath, strlen(__keyELEMENTS[n])+2);
		strncat((char*)xpath, "//", 2);
		strcat((char*)xpath, __keyELEMENTS[n]);
#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s: KeyName [%s]", __FUNCTION__, __keyELEMENTS[n]);
		APP_TRACE(szDbgMsg); //Praveen_P1: For testing purpose remove it later
#endif
		xmlNodeSetPtr nodeset;
		xmlXPathObjectPtr result = NULL;
		int i;
		char* ElementValue;

		result = getNodeTest(doc, (xmlChar*)xpath);

		if(result != NULL)
		{
			nodeset = result->nodesetval;

#ifdef DEVDEBUG
			debug_sprintf(szDbgMsg, "%s: nodeset->nodeNr [%d]", __FUNCTION__, nodeset->nodeNr);
			APP_TRACE(szDbgMsg); //Praveen_P1: For testing purpose remove it later
#endif
			for(i=0; i < nodeset->nodeNr; i++)
			{
				ElementValue = (char*)xmlNodeListGetString(doc, nodeset->nodeTab[i]->xmlChildrenNode, 1);

				if( ElementValue != NULL )
				{
					if(req_xml_info[n].element != NULL)
					{
						free(req_xml_info[n].element); //Praveen_P1: 14 Nov Need to free this element if it is already freed if that field is present already, unlikely to happen, putting this for safety purpose
					}

					req_xml_info[n].element = (char*)calloc(strlen(ElementValue)+1, sizeof(char));
					strcpy(req_xml_info[n].element, ElementValue);
#ifdef DEVDEBUG
					if(strlen(req_xml_info[n].element) < 4000)
					{
						debug_sprintf(szDbgMsg, "%s: Element Value [%s]", __FUNCTION__, req_xml_info[n].element);
						APP_TRACE(szDbgMsg); //Praveen_P1: For testing purpose remove it later
					}
#endif
				}
				if(strcmp(__keyELEMENTS[n], "ADMIN") == COMPARE_OK)
				{
					debug_sprintf(szDbgMsg, "%s: Its ADMIN node, checking for attribute COMMAND value", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					if(nodeset->nodeTab[i]->xmlChildrenNode != NULL)
					{
						attrVal = xmlGetProp(nodeset->nodeTab[i]->xmlChildrenNode, (const xmlChar *)"COMMAND");
						//if(attrVal != NULL)
						{
							debug_sprintf(szDbgMsg, "%s: COMMAND Value [%s]", __FUNCTION__, (char *)attrVal);
							APP_TRACE(szDbgMsg);
						}
						//free attrVal
					}
				}

				if(strcmp(__keyELEMENTS[n], "LEVEL3_ITEMS") == COMPARE_OK)
				{
					debug_sprintf(szDbgMsg, "%s: Its LEVEL3_ITEMS node, Need to check its child nodes for Line items", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					xmlNode * level3Node = nodeset->nodeTab[i];

					debug_sprintf(szDbgMsg, "%s: Name of the level3Node is %s", __FUNCTION__, (char *)level3Node->name);
					APP_TRACE(szDbgMsg);

					xmlNode *	curNode = level3Node->xmlChildrenNode; //This should point to First Child Node of LEVEL3_ITEMS i.e. LINE_ITEM ideally
					if(curNode == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: There is NO Child Node for Level3 Node, no Need to Parse", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Current Node Name is %s", __FUNCTION__, (char *)curNode->name);
						APP_TRACE(szDbgMsg);

						_Level3Details_ * level3Dtls;

						level3Dtls = (_Level3Details_ *)calloc(1, sizeof(_Level3Details_));
						if(level3Dtls == NULL)
						{
							debug_sprintf(szDbgMsg, "%s: Error While Allocating memory for level3Dtls", __FUNCTION__);
							APP_TRACE(szDbgMsg);
						}

						level3Dtls->iNumberofLineItems = 0;

						iReturnVal = parseAndStoreLevel3Dtls(doc, curNode, level3Dtls);
						if(iReturnVal == EXIT_SUCCESS)
						{
							debug_sprintf(szDbgMsg, "%s: Successfully Parsed And Stored Level3 Details", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							if(level3Dtls->iNumberofLineItems) //Will be assigning only when Level3 details are filled
							{
								debug_sprintf(szDbgMsg, "%s: Storing Level3Details structure address", __FUNCTION__);
								APP_TRACE(szDbgMsg);
								req_xml_info[n].element = (char *)level3Dtls;
							}
							else
							{
								free(level3Dtls);
								level3Dtls = NULL;
							}
						}
						else
						{
							debug_sprintf(szDbgMsg, "%s: Error While Parsing And Storing Level3 Details", __FUNCTION__);
							APP_TRACE(szDbgMsg);


						}
					}
				}

				if(ElementValue != NULL)
				{
					xmlFree(ElementValue);
				}
			}

			xmlXPathFreeObject( result );
		}
	}

	xmlFreeDoc(doc);
	xmlCleanupParser();

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}


static int parseAndStoreLevel3Dtls(xmlDocPtr doc, xmlNode * currentNode, _Level3Details_ * level3Details)
{
#ifdef LOGGING_ENABLED
	char szDbgMsg[4096] = {0};
#endif
	int		iNumOfChildNodes	= 0;
	int		iLineItemFilled  	= 0;
	//int		iAppLogEnabled		= isAppLogEnabled();
	//char	szAppLogData[300]	= "";
	xmlNode	*tmpNode			= NULL;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(currentNode == NULL || level3Details == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input Parameter is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return EXIT_FAILURE;
	}

	//First Need to get number of LINE_ITEM Nodes under LEVEL3_ITEMS
	tmpNode = currentNode;
	while( (tmpNode != NULL) )
	{
		if(!strcmp((char *)tmpNode->name, "text"))
		{
			tmpNode = tmpNode->next;
			continue;
		}
		else
		{
			iNumOfChildNodes++;
			tmpNode = tmpNode->next;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Number of Child Nodes of LEVEL3_ITEMS is %d", __FUNCTION__, iNumOfChildNodes);
	APP_TRACE(szDbgMsg);

	//Need to Iterate through Child Nodes and Fill the List

	while(currentNode != NULL)
	{
		if(currentNode->type == 3)
		{
			currentNode = currentNode->next;
			continue;
		}

		if( !xmlStrcmp(currentNode->name, (const xmlChar *)"LINE_ITEM") )
		{
			debug_sprintf(szDbgMsg, "%s: Current Node is LINE_ITEM, Need to Parse and store the Node",__FUNCTION__);
			APP_TRACE(szDbgMsg);

			iLineItemFilled = 0;
			parseAndStoreLineItem(doc, currentNode->xmlChildrenNode, level3Details, &iLineItemFilled);
			if(iLineItemFilled)
			{
				debug_sprintf(szDbgMsg, "%s: Line Item Got added to Level3 Details, Incrementing the iNumberofLineItems",__FUNCTION__);
				APP_TRACE(szDbgMsg);

				level3Details->iNumberofLineItems = level3Details->iNumberofLineItems + 1;

				debug_sprintf(szDbgMsg, "%s: level3Dtls->iNumberofLineItems = %d",__FUNCTION__, level3Details->iNumberofLineItems);
				APP_TRACE(szDbgMsg);

				if(level3Details->iNumberofLineItems >=25)
				{
					debug_sprintf(szDbgMsg, "%s: We need to send upto 25 Line Items only to Vantiv in G043",__FUNCTION__);
					APP_TRACE(szDbgMsg);
					break; //Not parsing any more Level3 Line items in the request
				}
			}
		}

		iNumOfChildNodes--;

		if(iNumOfChildNodes <= 0 )
		{
			debug_sprintf(szDbgMsg, "%s: Child Elements of LEVEL3_ITEMS are over",__FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}

		/*Checking next node*/
		currentNode = currentNode->next;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

static int parseAndStoreLineItem(xmlDocPtr doc, xmlNode * currentNode, _Level3Details_ * level3Details, int * piLevel3DtlsFilled)
{
#ifdef LOGGING_ENABLED
	char szDbgMsg[4096] = {0};
#endif

	//int		iAppLogEnabled		= isAppLogEnabled();
	//char	szAppLogData[300]	= "";
	char *  ElementValue		= NULL;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(currentNode == NULL || level3Details == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input Parameter is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return EXIT_FAILURE;
	}

	while(currentNode != NULL)
	{
		if( !xmlStrcmp(currentNode->name, (const xmlChar *)"DESCRIPTION") )
		{
			ElementValue = (char*)xmlNodeListGetString(doc, currentNode->xmlChildrenNode, 1);
			if(ElementValue != NULL)
			{
				strncpy(level3Details->level3Dtls[level3Details->iNumberofLineItems].field_01, ElementValue, (sizeof(level3Details->level3Dtls[0].field_01) - 1));

				free(ElementValue);
				ElementValue = NULL;
				*piLevel3DtlsFilled = 1;
			}
		}
		else if( !xmlStrcmp(currentNode->name, (const xmlChar *)"ITEM_UOM") )
		{
			ElementValue = (char*)xmlNodeListGetString(doc, currentNode->xmlChildrenNode, 1);
			if(ElementValue != NULL)
			{
				strncpy(level3Details->level3Dtls[level3Details->iNumberofLineItems].field_02, ElementValue, (sizeof(level3Details->level3Dtls[0].field_02) - 1));

				free(ElementValue);
				ElementValue = NULL;
				*piLevel3DtlsFilled = 1;
			}
		}
		else if( !xmlStrcmp(currentNode->name, (const xmlChar *)"UNIT_PRICE") )
		{
			ElementValue = (char*)xmlNodeListGetString(doc, currentNode->xmlChildrenNode, 1);
			if(ElementValue != NULL)
			{
				strncpy(level3Details->level3Dtls[level3Details->iNumberofLineItems].field_03, ElementValue, (sizeof(level3Details->level3Dtls[0].field_03) - 1));

				free(ElementValue);
				ElementValue = NULL;
				*piLevel3DtlsFilled = 1;
			}
		}
		else if( !xmlStrcmp(currentNode->name, (const xmlChar *)"UNIT_PRICE_DECIMAL") )
		{
			ElementValue = (char*)xmlNodeListGetString(doc, currentNode->xmlChildrenNode, 1);
			if(ElementValue != NULL)
			{
				strncpy(level3Details->level3Dtls[level3Details->iNumberofLineItems].field_04, ElementValue, (sizeof(level3Details->level3Dtls[0].field_04) - 1));

				free(ElementValue);
				ElementValue = NULL;
				*piLevel3DtlsFilled = 1;
			}
		}
		else if( !xmlStrcmp(currentNode->name, (const xmlChar *)"QUANTITY") )
		{
			ElementValue = (char*)xmlNodeListGetString(doc, currentNode->xmlChildrenNode, 1);
			if(ElementValue != NULL)
			{
				strncpy(level3Details->level3Dtls[level3Details->iNumberofLineItems].field_05, ElementValue, (sizeof(level3Details->level3Dtls[0].field_05) - 1));

				free(ElementValue);
				ElementValue = NULL;
				*piLevel3DtlsFilled = 1;
			}
		}
		else if( !xmlStrcmp(currentNode->name, (const xmlChar *)"QANTITY_DECIMAL") )
		{
			ElementValue = (char*)xmlNodeListGetString(doc, currentNode->xmlChildrenNode, 1);
			if(ElementValue != NULL)
			{
				strncpy(level3Details->level3Dtls[level3Details->iNumberofLineItems].field_06, ElementValue, (sizeof(level3Details->level3Dtls[0].field_06) - 1));

				free(ElementValue);
				ElementValue = NULL;
				*piLevel3DtlsFilled = 1;
			}
		}
		else if( !xmlStrcmp(currentNode->name, (const xmlChar *)"SKU") )
		{
			ElementValue = (char*)xmlNodeListGetString(doc, currentNode->xmlChildrenNode, 1);
			if(ElementValue != NULL)
			{
				strncpy(level3Details->level3Dtls[level3Details->iNumberofLineItems].field_07, ElementValue, (sizeof(level3Details->level3Dtls[0].field_07) - 1));

				free(ElementValue);
				ElementValue = NULL;
				*piLevel3DtlsFilled = 1;
			}
		}
		else if( !xmlStrcmp(currentNode->name, (const xmlChar *)"ITEM_DISCOUNT_AMOUNT") )
		{
			ElementValue = (char*)xmlNodeListGetString(doc, currentNode->xmlChildrenNode, 1);
			if(ElementValue != NULL)
			{
				strncpy(level3Details->level3Dtls[level3Details->iNumberofLineItems].field_08, ElementValue, (sizeof(level3Details->level3Dtls[0].field_08) - 1));

				free(ElementValue);
				ElementValue = NULL;
				*piLevel3DtlsFilled = 1;
			}
		}
		else if( !xmlStrcmp(currentNode->name, (const xmlChar *)"ITEM_DISCOUNT_RATE") )
		{
			ElementValue = (char*)xmlNodeListGetString(doc, currentNode->xmlChildrenNode, 1);
			if(ElementValue != NULL)
			{
				strncpy(level3Details->level3Dtls[level3Details->iNumberofLineItems].field_09, ElementValue, (sizeof(level3Details->level3Dtls[0].field_09) - 1));

				free(ElementValue);
				ElementValue = NULL;
				*piLevel3DtlsFilled = 1;
			}
		}
		else if( !xmlStrcmp(currentNode->name, (const xmlChar *)"ITEM_DISCOUNT_RATE_DECIMAL") )
		{
			ElementValue = (char*)xmlNodeListGetString(doc, currentNode->xmlChildrenNode, 1);
			if(ElementValue != NULL)
			{
				strncpy(level3Details->level3Dtls[level3Details->iNumberofLineItems].field_10, ElementValue, (sizeof(level3Details->level3Dtls[0].field_10) - 1));

				free(ElementValue);
				ElementValue = NULL;
				*piLevel3DtlsFilled = 1;
			}
		}

		/*Checking next node*/
		currentNode = currentNode->next;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}
