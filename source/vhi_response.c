/*
 * vhi_response.c
 *
 *  Created on: Nov 7, 2013
 *      Author: Dexter M. Alberto
 */

#include <stdio.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>

#include "vhi_response.h"
#include "sca_xml_request.h"
#include "logger.h"
#include "help_function.h"
#include "vantiv_response.h"
#include "config_data.h"
#include "define_data.h"
#include "emv.h"
#include "appLog.h"

#ifdef LOGGING_ENABLED
static char    szDbgMsg[1024];
#endif

#define LAST_TRAN_ITEM_COUNT    23

static pthread_mutex_t gptLastTranFileMutex;
static int getLastTranDtls(char szLastTranDtls[LAST_TRAN_ITEM_COUNT][257]);
static int checkTPSCodeinSAFMapList(char *pszHostResponseCode, sCONFIG * GCONFIG);

extern void b64_encode(const UINT8 *pchInBuf, UINT8 *pchOutBuf, int shInSize, int *shOutSize);
extern void b64_decode(const UINT8 *pchInBuf, UINT8 *pchOutBuf, int shInSize, int *shOutSize);

void freeVHIResponseData(sVHI_RESPONSE_INFO *vhi_res_info)
{
	if (vhi_res_info->buffer != NULL) {
		free(vhi_res_info->buffer);
		vhi_res_info->buffer = NULL;
	}

	if (vhi_res_info->tosend != NULL) {
		free(vhi_res_info->tosend);
		vhi_res_info->tosend = NULL;
	}
	memset(vhi_res_info, 0x00, sizeof(sVHI_RESPONSE_INFO));
	//memset(&sR017_info, 0x00, sizeof(sR017_data));
	//memset(&sR008_info, 0x00, sizeof(sR008_data));
}

int composeVhiXmlErrResponse(int conn, sGLOBALS* GLOBAL, sVHI_RESPONSE_INFO *vhi_res_info, _sSCA_XML_REQUEST *req_xml_info)
{
	char* ptr = NULL;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	ptr = composeErrResponse(GLOBAL, vhi_res_info, req_xml_info);

	if (ptr == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INTERNAL_ERROR;
		return EXIT_FAILURE;
	}

	if (strlen(ptr) <= 0) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 0", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INTERNAL_ERROR;
		return EXIT_FAILURE;
	}

	if (GLOBAL->http.xml_response) {
		free(GLOBAL->http.xml_response);
		GLOBAL->http.xml_response = NULL;
	}

	GLOBAL->http.xml_response = (char*)calloc(strlen(ptr)+1, sizeof(char));
	GLOBAL->http.responseLength = strlen(ptr);
	strcpy(GLOBAL->http.xml_response, ptr);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}


int composeVhiXmlResponse(int conn, sGLOBALS* GLOBAL, sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo, sR017_data *sR017_info, sR008_data *sR008_info, sR011_data *sR011_info, sR023_data *sR023_info, sR029_data *sR029_info, sVHI_RESPONSE_INFO *vhi_res_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[5120]	= "";

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if (GLOBAL->isProcessorAvailable == true) {
		parseR007(vrinfo, vhi_res_info);
		parseR008(vrinfo, sR008_info);
		parseR011(vrinfo, sR011_info);
		parseR017(vrinfo, sR017_info, GCONFIG);
        parseR023(vrinfo, sR023_info);
        parseR029(vrinfo, sR029_info);
	}

	char* ptr = NULL;
	char* pFUNCTION_TYPE = getXmlTagValue(eFUNCTION_TYPE, req_xml_info);
	char* pPAYMENT_TYPE = getXmlTagValue(ePAYMENT_TYPE, req_xml_info);
	char* pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);

	/*
	 * **********************************************
	 * Identify the correct xml response for sca
	 * **********************************************
	 */
	if (pFUNCTION_TYPE == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! FUNCTION_TYPE is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	if (strncmp(pFUNCTION_TYPE, FUNCTION_TYPE.PAYMENT, strlen(FUNCTION_TYPE.PAYMENT)) == COMPARE_OK) {

		if (pPAYMENT_TYPE == NULL) {
			debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
			GLOBAL->http.status_code = eHTTP_OK;
			GLOBAL->appErrCode = eERR_INTERNAL_ERROR;

			return EXIT_FAILURE;
		}

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Framing Response For %s Command", pCOMMAND);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}

		if (GLOBAL->isProcessorAvailable == true) {

			if (pCOMMAND == NULL) {
				debug_sprintf(szDbgMsg, "%s: ERROR! COMMAND is NULL", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}

			if (strncmp(pCOMMAND, "PROCESSOR", 9) == COMPARE_OK) {
				ptr = composeProcessorInquireResponse(GLOBAL, svbninfo, sR008_info, vhi_res_info, GCONFIG, req_xml_info);
			}
			else {
				if (strncmp(pPAYMENT_TYPE, COMMAND.CREDIT, strlen(COMMAND.CREDIT)) == COMPARE_OK) {
					if (strncmp(pCOMMAND, COMMAND.SIGNATURE, strlen(COMMAND.SIGNATURE)) == COMPARE_OK) {
						ptr = composeSignatureResponse(vhi_res_info); //temporary
					}
					else {
						ptr = composeCreditResponse(GLOBAL, vrinfo, svbninfo, sR017_info, sR008_info, sR011_info, sR023_info, sR029_info, vhi_res_info, GCONFIG, req_xml_info, vhi_fieldinfo);
					}
				}
				else if (strncmp(pPAYMENT_TYPE, "DEBIT", 5) == COMPARE_OK) {
					ptr = composeDebitResponse(GLOBAL, vrinfo, svbninfo, sR017_info, sR008_info, vhi_res_info, GCONFIG, req_xml_info, vhi_fieldinfo);
				}
				else if (strncmp(pPAYMENT_TYPE, "GIFT", 4) == COMPARE_OK) {
					if (strncmp(pCOMMAND, COMMAND.PRE_AUTH, strlen(COMMAND.PRE_AUTH)) == COMPARE_OK) {
						ptr = composeGiftPreAuthResponse(GLOBAL, vrinfo, svbninfo, sR017_info, sR008_info, vhi_res_info, GCONFIG, req_xml_info, vhi_fieldinfo);
					}
					else if (strncmp(pCOMMAND, COMMAND.COMPLETION, strlen(COMMAND.COMPLETION)) == COMPARE_OK) {
						ptr = composeGiftCompletionResponse(GLOBAL, vrinfo, svbninfo, sR017_info, sR008_info, vhi_res_info, GCONFIG, req_xml_info, vhi_fieldinfo);
					}
					else {
						ptr = composeGiftResponse(GLOBAL, vrinfo, svbninfo, sR017_info, sR008_info, vhi_res_info, GCONFIG, req_xml_info, vhi_fieldinfo);
					}
				}
                else if (strncmp(pPAYMENT_TYPE, "EBT", 3) == COMPARE_OK)
                {
                    if (strlen(vhi_res_info->cb_available_balance) == 0)
                        strcpy(vhi_res_info->cb_available_balance, "0.00");
                    if (strlen(vhi_res_info->fs_available_balance) == 0)
                        strcpy(vhi_res_info->fs_available_balance, "0.00");
                    ptr = composeDebitResponse(GLOBAL, vrinfo, svbninfo, sR017_info, sR008_info, vhi_res_info, GCONFIG, req_xml_info, vhi_fieldinfo);
                }
				else if (strncmp(pPAYMENT_TYPE, "ADMIN", 5) == COMPARE_OK)
                {
					ptr = composeAdminResponse(GLOBAL, svbninfo, sR017_info, sR008_info, vhi_res_info, GCONFIG, req_xml_info, vhi_fieldinfo);
				}
				else
                {
					debug_sprintf(szDbgMsg, "%s: ERROR! not supported", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
					GLOBAL->http.status_code = eHTTP_OK;
					GLOBAL->appErrCode = eERR_INVALID_COMMAND;
					return EXIT_FAILURE;
				}
			}
		}
		else {
			ptr = composeProcessorInquireResponse(GLOBAL, svbninfo, sR008_info, vhi_res_info, GCONFIG, req_xml_info);
		}
	}
	else if (strncmp(pFUNCTION_TYPE, FUNCTION_TYPE.BATCH, strlen(FUNCTION_TYPE.BATCH)) == COMPARE_OK) {
		if (pCOMMAND == NULL) {
			debug_sprintf(szDbgMsg, "%s: ERROR! COMMAND is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return EXIT_FAILURE;
		}

		if (strncmp(pCOMMAND, COMMAND.SETTLE, 5) == COMPARE_OK) {
			ptr = composeBatchResponse(GLOBAL, svbninfo, sR008_info, vhi_res_info, GCONFIG, req_xml_info);
		}
		else {
			debug_sprintf(szDbgMsg, "%s: ERROR! not supported", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
			GLOBAL->http.status_code = eHTTP_OK;
			GLOBAL->appErrCode = eERR_INVALID_COMMAND;
			return EXIT_FAILURE;
		}
	}
	else if (strncmp(pFUNCTION_TYPE, "ADMIN", 5) == COMPARE_OK) {
#if 0 //Praveen_P1: We are supporting ADMIN function type for Get Version function
		debug_sprintf(szDbgMsg, "%s: ERROR! not yet supported", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
		return EXIT_FAILURE;
#endif
		if (strncmp(pCOMMAND, COMMAND.VERSION, 7) == COMPARE_OK) {
			ptr = composeVersionResponse(vhi_res_info, GCONFIG);
		}
		else if(strncmp(pCOMMAND, COMMAND.DEVADMIN, 8) == COMPARE_OK)
		{
			ptr = composeRegistrationResponse(vhi_res_info);
		}
		else {
			debug_sprintf(szDbgMsg, "%s: ERROR! not supported", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
			GLOBAL->http.status_code = eHTTP_OK;
			GLOBAL->appErrCode = eERR_INVALID_COMMAND;
			return EXIT_FAILURE;
		}
	}
	else if (strncmp(pFUNCTION_TYPE, FUNCTION_TYPE.REPORT, strlen(FUNCTION_TYPE.REPORT)) == COMPARE_OK) {
		if (pCOMMAND == NULL) {
			debug_sprintf(szDbgMsg, "%s: ERROR! COMMAND is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return EXIT_FAILURE;
		}

		if (strncmp(pCOMMAND, COMMAND.DAYSUMMARY, strlen(COMMAND.DAYSUMMARY)) == COMPARE_OK) {
			ptr = composeReportResponse(vhi_res_info, req_xml_info);
		}
		else if (strncmp(pCOMMAND, COMMAND.LAST_TRAN, strlen(COMMAND.LAST_TRAN)) == COMPARE_OK) {
			ptr = composeLastTranResponse(vhi_res_info);
		}
		else {
			debug_sprintf(szDbgMsg, "%s: ERROR! not supported", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
			GLOBAL->http.status_code = eHTTP_OK;
			GLOBAL->appErrCode = eERR_INVALID_COMMAND;
			return EXIT_FAILURE;
		}
	}
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! not supported FUNCTION_TYPE", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INVALID_FUNCTIONTYPE;
		return EXIT_FAILURE;
	}

	if (ptr == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INTERNAL_ERROR;
		return EXIT_FAILURE;
	}

	if (strlen(ptr) <= 0) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 0", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INTERNAL_ERROR;
		return EXIT_FAILURE;
	}

	if (GLOBAL->http.xml_response) {
		free(GLOBAL->http.xml_response);
		GLOBAL->http.xml_response = NULL;
	}

	GLOBAL->http.xml_response = (char*)calloc(strlen(ptr)+1, sizeof(char));
	GLOBAL->http.responseLength = strlen(ptr);
	strcpy(GLOBAL->http.xml_response, ptr);

	if(iAppLogEnabled)
	{
		strcpy(szAppLogData, GLOBAL->http.xml_response);
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_DHI_RESPONSE, szAppLogData, NULL);
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

char* composeErrResponse(sGLOBALS* GLOBAL, sVHI_RESPONSE_INFO *vhi_res_info, _sSCA_XML_REQUEST *req_xml_info)
{
    xmlDocPtr doc = NULL;
    xmlNodePtr root_node = NULL;
    int doc_txt_len;
    LIBXML_TEST_VERSION;
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    doc = xmlNewDoc(BAD_CAST "1.0");

    //Create the root
    root_node = xmlNewNode(NULL, BAD_CAST "RESPONSE");
    xmlDocSetRootElement(doc, root_node);
    xmlNewChild(root_node, NULL, BAD_CAST "BATCH_TRACE_ID", BAD_CAST getXmlTagValue(eBATCH_TRACE_ID, req_xml_info));

    switch(GLOBAL->appErrCode)
    {
    case eERR_INVALID_COMMAND:
    	//xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "NOT_PROCESSED_PARAMETER_ERROR");
    	xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "NOT_PROCESSED");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST "COMMAND NOT SUPPORTED");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST "FAILURE");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST "6"); //Currently returning 6 for all

		if(iAppLogEnabled)
		{
				strcpy(szAppLogData, "Composing \"COMMAND NOT SUPPORTED\" Error Response to SCA" );
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSED, szAppLogData, NULL);
		}

    	break;
    case eERR_INVALID_FUNCTIONTYPE:
    	//xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "NOT_PROCESSED_PARAMETER_ERROR");
    	xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "NOT_PROCESSED");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST "FUNCTION_TYPE NOT SUPPORTED");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST "FAILURE");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST "6"); //Currently returning 6 for all

		if(iAppLogEnabled)
		{
				strcpy(szAppLogData, "Composing \"FUNCTION_TYPE NOT SUPPORTED\" Error Response to SCA" );
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSED, szAppLogData, NULL);
		}

    	break;
    case eERR_INVALID_PAYMENTTYPE:
    	//xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "NOT_PROCESSED_PARAMETER_ERROR");
    	xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "NOT_PROCESSED");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST "PAYMENT_TYPE NOT SUPPORTED FOR THIS COMMAND");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST "FAILURE");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST "6"); //Currently returning 6 for all

		if(iAppLogEnabled)
		{
				strcpy(szAppLogData, "Composing \"PAYMENT_TYPE NOT SUPPORTED FOR THIS COMMAND\" Error Response to SCA" );
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSED, szAppLogData, NULL);
		}

    	break;
    case eERR_INTERNAL_ERROR:
    	xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "NOT_PROCESSED");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST "INTERNAL APPLICATION ERROR");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST "FAILURE");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST "6"); //Currently returning 6 for all

		if(iAppLogEnabled)
		{
				strcpy(szAppLogData, "Composing \"INTERNAL APPLICATION ERROR\" Error Response to SCA" );
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSED, szAppLogData, NULL);
		}

    	break;
    case eERR_UNSUPPORT_PROTOCOL:
    	//xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "NOT_PROCESSED_COMM_FAILURE");
    	xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "NOT_PROCESSED");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST "PROTOCOL NOT SUPPORTED");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST "FAILURE");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST "6"); //Currently returning 6 for all

		if(iAppLogEnabled)
		{
				strcpy(szAppLogData, "Composing \"PROTOCOL NOT SUPPORTED\" Error Response to SCA" );
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSED, szAppLogData, NULL);
		}

    	break;
    case eERR_HOST_URL_MALFORMAT:
    	//xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "NOT_PROCESSED_COMM_FAILURE");
    	xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "NOT_PROCESSED");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST "HOST URL MAL FORMAT");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST "FAILURE");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST "6"); //Currently returning 6 for all

		if(iAppLogEnabled)
		{
				strcpy(szAppLogData, "Composing \"HOST URL MAL FORMAT\" Error Response to SCA" );
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSED, szAppLogData, NULL);
		}

    	break;
    case eERR_HOST_HTTP_ERROR:
    	//xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "NOT_PROCESSED_COMM_FAILURE");
    	xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "NOT_PROCESSED");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST "HOST HTTP ERROR");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST "FAILURE");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST "6"); //Currently returning 6 for all

		if(iAppLogEnabled)
		{
				strcpy(szAppLogData, "Composing \"HOST HTTP ERROR\" Error Response to SCA" );
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSED, szAppLogData, NULL);
		}

    	break;
    case eERR_INVALID_CTROUTD:
    	//xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "NOT_PROCESSED_COMM_FAILURE");
    	xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "NOT_PROCESSED");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST "GIVEN CTROUTD NOT IN CURRENT BATCH");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST "FAILURE");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST "6"); //Currently returning 6 for all

		if(iAppLogEnabled)
		{
				strcpy(szAppLogData, "Composing \"GIVEN CTROUTD NOT IN CURRENT BATCH\" Error Response to SCA" );
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSED, szAppLogData, NULL);
		}

    	break;
    case eERR_INVALID_AUTH_CODE:
    	//xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "NOT_PROCESSED_COMM_FAILURE");
    	xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "NOT_PROCESSED");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST "INVALID AUTH CODE");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST "FAILURE");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST "6"); //Currently returning 6 for all

		if(iAppLogEnabled)
		{
				strcpy(szAppLogData, "Composing \"INVALID AUTH CODE\" Error Response to SCA" );
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSED, szAppLogData, NULL);
		}

    	break;
    case eERR_REQUEST_ERROR:
    	//xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "NOT_PROCESSED_COMM_FAILURE");
    	xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "NOT_PROCESSED");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST "REQUEST ERROR");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST "FAILURE");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST "6"); //Currently returning 6 for all

		if(iAppLogEnabled)
		{
				strcpy(szAppLogData, "Composing \"REQUEST ERROR\" Error Response to SCA" );
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSED, szAppLogData, NULL);
		}

    	break;
    case eERR_HOST_TIMED_OUT:
    	xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "NOT_PROCESSED");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST "PROCESSOR TIMED OUT");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST "FAILURE");
    	xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST "23"); //23 for Host timed out

		if(iAppLogEnabled)
		{
				strcpy(szAppLogData, "Composing \"PROCESSOR TIMED OUT\" Error Response to SCA" );
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSED, szAppLogData, NULL);
		}

    	break;
    case eERR_LAST_TRAN_CMD:
    	xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "NOT_PROCESSED");
		xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST "LAST TRANSACTION NOT AVAILABLE");
		xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST "FAILURE");
		xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST "6");

		if(iAppLogEnabled)
		{
				strcpy(szAppLogData, "Composing \"LAST TRANSACTION NOT AVAILABLE\" Error Response to SCA" );
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSED, szAppLogData, NULL);
		}

    	break;
    case eERR_RESP_ERR_100:
   		xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "PROCESSED");
   		xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST "Request Was received by Tandem host but packet contained no data");
   		xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST "FAILURE");
   		xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST "6");

   		if(iAppLogEnabled)
   		{
   				strcpy(szAppLogData, "Composing \"100 Response Code\" Error Response to SCA" );
   				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSED, szAppLogData, NULL);
   		}

   		break;
    case eERR_RESP_ERR_101:
   		xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "PROCESSED");
   		xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST "TCP/IP Message Header Was Not Formatted Properly. Request Was Not Submitted for Authorization");
   		xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST "FAILURE");
   		xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST "6");

   		if(iAppLogEnabled)
   		{
   				strcpy(szAppLogData, "Composing \"101 Response Code\" Error Response to SCA" );
   				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSED, szAppLogData, NULL);
   		}

   		break;
    case eERR_RESP_ERR_121:
		xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "PROCESSED");
		xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST "Connection to the Vantiv Financial Processors was Unexpectedly Shutdown Before the Response was Received");
		xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST "FAILURE");
		xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST "6");

		if(iAppLogEnabled)
		{
				strcpy(szAppLogData, "Composing \"121 Response Code\" Error Response to SCA" );
				addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_PROCESSED, szAppLogData, NULL);
		}

		break;
    default:
    	break;
    }

	if (vhi_res_info->buffer) {
		free(vhi_res_info->buffer);
		vhi_res_info->buffer = NULL;
	}

	xmlDocDumpFormatMemoryEnc(doc, &vhi_res_info->buffer, &doc_txt_len, "UTF-8", 0);
	xmlFreeDoc(doc);
	xmlCleanupParser();
	xmlMemoryDump();

	return (char*)vhi_res_info->buffer;
}

char* composeCreditResponse(sGLOBALS* GLOBAL, sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo, sR017_data *sR017_info, sR008_data *sR008_info, sR011_data *sR011_info, sR023_data *sR023_info, sR029_data *sR029_info, sVHI_RESPONSE_INFO *vhi_res_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
    xmlDocPtr   doc 			= NULL;
    xmlNodePtr  root_node 		= NULL;
    char        *value_pointer 	= NULL;
    char		szResultCode[3];

    LIBXML_TEST_VERSION;

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    doc = xmlNewDoc(BAD_CAST "1.0");

    //Create the root
    root_node = xmlNewNode(NULL, BAD_CAST "RESPONSE");
    xmlDocSetRootElement(doc, root_node);
    xmlNewChild(root_node, NULL, BAD_CAST "BATCH_TRACE_ID", BAD_CAST getXmlTagValue(eBATCH_TRACE_ID, req_xml_info));

    //Creates new child nodes
	xmlNewChild(root_node, NULL, BAD_CAST "AUTH_CODE", BAD_CAST VHI_AUTH_CODE(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "CLIENT_ID", BAD_CAST VHI_CLIENT_ID(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "COMMAND", BAD_CAST VHI_COMMAND(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "CTROUTD", BAD_CAST VHI_CTROUTD(sR008_info));
	xmlNewChild(root_node, NULL, BAD_CAST "INTRN_SEQ_NUM", BAD_CAST VHI_INTRN_SEQ_NUM(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "INVOICE", BAD_CAST VHI_INVOICE(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "PAYMENT_MEDIA", BAD_CAST VHI_PAYMENT_MEDIA(svbninfo, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "PAYMENT_TYPE", BAD_CAST VHI_PAYMENT_TYPE(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "REFERENCE", BAD_CAST VHI_REFERENCE(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST VHI_RESULT(GLOBAL, svbninfo, sR008_info, vhi_res_info, GCONFIG, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST VHI_RESPONSE_TEXT(GLOBAL, svbninfo, vhi_res_info, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST VHI_RESULT_CODE(vhi_res_info));
	xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST VHI_TERMINATION_STATUS(vhi_res_info));
	if(VHI_HOST_RESP_CODE(vhi_res_info) != NULL)
	{
		xmlNewChild(root_node, NULL, BAD_CAST "HOST_RESPCODE", BAD_CAST VHI_HOST_RESP_CODE(vhi_res_info));
	}
	//xmlNewChild(root_node, NULL, BAD_CAST "TRANS_AMOUNT", BAD_CAST VHI_TRANS_AMOUNT(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "TRANS_DATE", BAD_CAST VHI_TRANS_DATE(vhi_res_info));
	xmlNewChild(root_node, NULL, BAD_CAST "TRANS_SEQ_NUM", BAD_CAST VHI_TRANS_SEQ_NUM(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "TRANS_TIME", BAD_CAST VHI_TRANS_TIME(vhi_res_info, vhi_fieldinfo));
	xmlNewChild(root_node, NULL, BAD_CAST "TROUTD", BAD_CAST VHI_TROUTD(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "CARD_TOKEN", BAD_CAST VHI_CARD_TOKEN(sR017_info));

	if ((value_pointer= VHI_APPROVED_AMOUNT(svbninfo, vhi_res_info, req_xml_info)) != NULL)
		xmlNewChild(root_node, NULL, BAD_CAST "APPROVED_AMOUNT", BAD_CAST value_pointer);
	if ((value_pointer= VHI_AMOUNT_BALANCE(vrinfo, svbninfo, vhi_res_info, req_xml_info)) != NULL)
		xmlNewChild(root_node, NULL, BAD_CAST "AMOUNT_BALANCE", BAD_CAST value_pointer);
	/*
	 * Praveen_P1: 07 Dec 2015
	 * Added CVV2_CODE and AVS_CODE to send them back to SCA in SSI response
	 * if we get these codes from Vantiv
	 */
	memset(szResultCode, 0x00, sizeof(szResultCode));
	VHI_CVV2_CODE(svbninfo, szResultCode);
	if(strlen(szResultCode) > 0)
	{
		xmlNewChild(root_node, NULL, BAD_CAST "CVV2_CODE", BAD_CAST szResultCode);
	}
	memset(szResultCode, 0x00, sizeof(szResultCode));
	VHI_AVS_CODE(svbninfo, szResultCode);
	if(strlen(szResultCode) > 0)
	{
		xmlNewChild(root_node, NULL, BAD_CAST "AVS_CODE", BAD_CAST szResultCode);
	}
	xmlNewChild(root_node, NULL, BAD_CAST "MERCHID", BAD_CAST GCONFIG->mid);
	xmlNewChild(root_node, NULL, BAD_CAST "TERMID", BAD_CAST GCONFIG->tid);

	/*
	 * Praveen_P1: Dec 15 15
	 * FRD3.65
	 * Need to send Signature Capture Reference
	 */
	if(strlen(sR011_info->signatureCapRef) > 0)
	{
		xmlNewChild(root_node, NULL, BAD_CAST "SIGNATUREREF", BAD_CAST VHI_SIGNATUREREF(sR011_info));
	}

	if(VHI_VSP_CODE(vhi_res_info) != NULL)
	{
		xmlNewChild(root_node, NULL, BAD_CAST "VSP_CODE", BAD_CAST VHI_VSP_CODE(vhi_res_info));
		xmlNewChild(root_node, NULL, BAD_CAST "VSP_RESULTDESC", BAD_CAST VHI_VSP_RESULT_DESC(vhi_res_info));
	}
    
    composeR023Response(root_node, sR023_info);

    composeR029Response(root_node, sR029_info);

    int doc_txt_len;

	if (vhi_res_info->buffer) {
		free(vhi_res_info->buffer);
		vhi_res_info->buffer = NULL;
	}

    xmlDocDumpFormatMemoryEnc(doc, &vhi_res_info->buffer, &doc_txt_len, "UTF-8", 0);
    xmlFreeDoc(doc);
    xmlCleanupParser();
    xmlMemoryDump();

    return (char*)vhi_res_info->buffer;
}

char* composeDebitResponse(sGLOBALS* GLOBAL, sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo, sR017_data *sR017_info, sR008_data *sR008_info, sVHI_RESPONSE_INFO *vhi_res_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
    xmlDocPtr doc = NULL;
    xmlNodePtr root_node = NULL;

    LIBXML_TEST_VERSION;

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    doc = xmlNewDoc(BAD_CAST "1.0");

    //Create the root
    root_node = xmlNewNode(NULL, BAD_CAST "RESPONSE");
    xmlDocSetRootElement(doc, root_node);
    xmlNewChild(root_node, NULL, BAD_CAST "BATCH_TRACE_ID", BAD_CAST getXmlTagValue(eBATCH_TRACE_ID, req_xml_info));

    //Creates new child nodes
	xmlNewChild(root_node, NULL, BAD_CAST "AUTH_CODE", BAD_CAST VHI_AUTH_CODE(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "CTROUTD", BAD_CAST VHI_CTROUTD(sR008_info));
	xmlNewChild(root_node, NULL, BAD_CAST "INTRN_SEQ_NUM", BAD_CAST VHI_INTRN_SEQ_NUM(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "INVOICE", BAD_CAST VHI_INVOICE(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "PAYMENT_MEDIA", BAD_CAST VHI_PAYMENT_MEDIA(svbninfo, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "PAYMENT_TYPE", BAD_CAST VHI_PAYMENT_TYPE(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "REFERENCE", BAD_CAST VHI_REFERENCE(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST VHI_RESULT(GLOBAL, svbninfo, sR008_info, vhi_res_info, GCONFIG, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST VHI_RESPONSE_TEXT(GLOBAL, svbninfo, vhi_res_info, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST VHI_RESULT_CODE(vhi_res_info));
	xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST VHI_TERMINATION_STATUS(vhi_res_info));
	if(VHI_HOST_RESP_CODE(vhi_res_info) != NULL)
	{
		xmlNewChild(root_node, NULL, BAD_CAST "HOST_RESPCODE", BAD_CAST VHI_HOST_RESP_CODE(vhi_res_info));
	}
	xmlNewChild(root_node, NULL, BAD_CAST "TRACE_CODE", BAD_CAST VHI_TRACE_CODE(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "TRANS_SEQ_NUM", BAD_CAST VHI_TRANS_SEQ_NUM(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "TROUTD", BAD_CAST VHI_TROUTD(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "TRANS_DATE", BAD_CAST VHI_TRANS_DATE(vhi_res_info));
	xmlNewChild(root_node, NULL, BAD_CAST "TRANS_TIME", BAD_CAST VHI_TRANS_TIME(vhi_res_info, vhi_fieldinfo));
	xmlNewChild(root_node, NULL, BAD_CAST "CARD_TOKEN", BAD_CAST VHI_CARD_TOKEN(sR017_info));

	if (VHI_APPROVED_AMOUNT(svbninfo, vhi_res_info, req_xml_info) != NULL)
		xmlNewChild(root_node, NULL, BAD_CAST "APPROVED_AMOUNT", BAD_CAST VHI_APPROVED_AMOUNT(svbninfo, vhi_res_info, req_xml_info));
	if (VHI_AMOUNT_BALANCE(vrinfo, svbninfo, vhi_res_info, req_xml_info) != NULL)
		xmlNewChild(root_node, NULL, BAD_CAST "AMOUNT_BALANCE", BAD_CAST VHI_AMOUNT_BALANCE(vrinfo, svbninfo, vhi_res_info, req_xml_info));
    
	xmlNewChild(root_node, NULL, BAD_CAST "MERCHID", BAD_CAST GCONFIG->mid);
	xmlNewChild(root_node, NULL, BAD_CAST "TERMID", BAD_CAST GCONFIG->tid);

    // For EBT balances
    if (strlen(vhi_res_info->cb_available_balance) > 0)
        xmlNewChild(root_node, NULL, BAD_CAST "CB_AVAIL_BALANCE", BAD_CAST vhi_res_info->cb_available_balance);
    if (strlen(vhi_res_info->cb_start_balance) > 0)
        xmlNewChild(root_node, NULL, BAD_CAST "CB_BEGIN_BALANCE", BAD_CAST vhi_res_info->cb_start_balance);
    if (strlen(vhi_res_info->cb_end_balance) > 0)
        xmlNewChild(root_node, NULL, BAD_CAST "CB_LEDGER_BALANCE", BAD_CAST vhi_res_info->cb_end_balance);
    if (strlen(vhi_res_info->fs_available_balance) > 0)
        xmlNewChild(root_node, NULL, BAD_CAST "FS_AVAIL_BALANCE", BAD_CAST vhi_res_info->fs_available_balance);
    if (strlen(vhi_res_info->fs_start_balance) > 0)
        xmlNewChild(root_node, NULL, BAD_CAST "FS_BEGIN_BALANCE", BAD_CAST vhi_res_info->fs_start_balance);
    if (strlen(vhi_res_info->fs_end_balance) > 0)
        xmlNewChild(root_node, NULL, BAD_CAST "FS_LEDGER_BALANCE", BAD_CAST vhi_res_info->fs_end_balance);
    
	if(VHI_VSP_CODE(vhi_res_info) != NULL)
	{
		xmlNewChild(root_node, NULL, BAD_CAST "VSP_CODE", BAD_CAST VHI_VSP_CODE(vhi_res_info));
		xmlNewChild(root_node, NULL, BAD_CAST "VSP_RESULTDESC", BAD_CAST VHI_VSP_RESULT_DESC(vhi_res_info));
	}

    int doc_txt_len;

	if (vhi_res_info->buffer) {
		free(vhi_res_info->buffer);
		vhi_res_info->buffer = NULL;
	}

    xmlDocDumpFormatMemoryEnc(doc, &vhi_res_info->buffer, &doc_txt_len, "UTF-8", 0);
    xmlFreeDoc(doc);
    xmlCleanupParser();
    xmlMemoryDump();

    return (char*)vhi_res_info->buffer;
}

char* composeGiftResponse(sGLOBALS* GLOBAL, sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo, sR017_data *sR017_info, sR008_data *sR008_info, sVHI_RESPONSE_INFO *vhi_res_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
    xmlDocPtr 	doc 				  = NULL;
    xmlNodePtr 	root_node 			  = NULL;
    char		szResultCode[3]		  = "";
    LIBXML_TEST_VERSION;

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    doc = xmlNewDoc(BAD_CAST "1.0");

    //Create the root
    root_node = xmlNewNode(NULL, BAD_CAST "RESPONSE");
    xmlDocSetRootElement(doc, root_node);
    xmlNewChild(root_node, NULL, BAD_CAST "BATCH_TRACE_ID", BAD_CAST getXmlTagValue(eBATCH_TRACE_ID, req_xml_info));

    //Creates new child nodes
	xmlNewChild(root_node, NULL, BAD_CAST "TRANS_SEQ_NUM", BAD_CAST VHI_TRANS_SEQ_NUM(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "INTRN_SEQ_NUM", BAD_CAST VHI_INTRN_SEQ_NUM(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST VHI_RESULT(GLOBAL, svbninfo, sR008_info, vhi_res_info, GCONFIG, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST VHI_RESULT_CODE(vhi_res_info));
	xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST VHI_TERMINATION_STATUS(vhi_res_info));
	xmlNewChild(root_node, NULL, BAD_CAST "TRANSACTION_CODE", BAD_CAST VHI_TRANSACTION_CODE());
	xmlNewChild(root_node, NULL, BAD_CAST "TROUTD", BAD_CAST VHI_TROUTD(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "CTROUTD", BAD_CAST VHI_CTROUTD(sR008_info));
    xmlNewChild(root_node, NULL, BAD_CAST "AUTH_CODE", BAD_CAST VHI_AUTH_CODE(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST VHI_RESPONSE_TEXT(GLOBAL, svbninfo, vhi_res_info, req_xml_info));
	if(VHI_HOST_RESP_CODE(vhi_res_info) != NULL)
	{
		xmlNewChild(root_node, NULL, BAD_CAST "HOST_RESPCODE", BAD_CAST VHI_HOST_RESP_CODE(vhi_res_info));
	}
	xmlNewChild(root_node, NULL, BAD_CAST "REFERENCE", BAD_CAST VHI_REFERENCE(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "PAYMENT_MEDIA", BAD_CAST VHI_PAYMENT_MEDIA(svbninfo, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "PAYMENT_TYPE", BAD_CAST VHI_PAYMENT_TYPE(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "AMOUNT_BALANCE", BAD_CAST VHI_AMOUNT_BALANCE(vrinfo, svbninfo, vhi_res_info, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "TRANS_DATE", BAD_CAST VHI_TRANS_DATE(vhi_res_info));
	xmlNewChild(root_node, NULL, BAD_CAST "TRANS_TIME", BAD_CAST VHI_TRANS_TIME(vhi_res_info, vhi_fieldinfo));
	//xmlNewChild(root_node, NULL, BAD_CAST "TRANS_AMOUNT", BAD_CAST VHI_TRANS_AMOUNT(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "CARD_TOKEN", BAD_CAST VHI_CARD_TOKEN(sR017_info));

	if (VHI_APPROVED_AMOUNT(svbninfo, vhi_res_info, req_xml_info) != NULL)
		xmlNewChild(root_node, NULL, BAD_CAST "APPROVED_AMOUNT", BAD_CAST VHI_APPROVED_AMOUNT(svbninfo, vhi_res_info, req_xml_info));

	/*
	 * Praveen_P1: 07 Dec 2015
	 * Added CVV2_CODE and AVS_CODE to send them back to SCA in SSI response
	 * if we get these codes from Vantiv
	 */
	memset(szResultCode, 0x00, sizeof(szResultCode));
	VHI_CVV2_CODE(svbninfo, szResultCode);
	if(strlen(szResultCode) > 0)
	{
		xmlNewChild(root_node, NULL, BAD_CAST "CVV2_CODE", BAD_CAST szResultCode);
	}
	memset(szResultCode, 0x00, sizeof(szResultCode));
	VHI_AVS_CODE(svbninfo, szResultCode);
	if(strlen(szResultCode) > 0)
	{
		xmlNewChild(root_node, NULL, BAD_CAST "AVS_CODE", BAD_CAST szResultCode);
	}

	xmlNewChild(root_node, NULL, BAD_CAST "MERCHID", BAD_CAST GCONFIG->mid);
	xmlNewChild(root_node, NULL, BAD_CAST "TERMID", BAD_CAST GCONFIG->tid);

	if(VHI_VSP_CODE(vhi_res_info) != NULL)
	{
		xmlNewChild(root_node, NULL, BAD_CAST "VSP_CODE", BAD_CAST VHI_VSP_CODE(vhi_res_info));
		xmlNewChild(root_node, NULL, BAD_CAST "VSP_RESULTDESC", BAD_CAST VHI_VSP_RESULT_DESC(vhi_res_info));
	}

    int doc_txt_len;

	if (vhi_res_info->buffer) {
		free(vhi_res_info->buffer);
		vhi_res_info->buffer = NULL;
	}

    xmlDocDumpFormatMemoryEnc(doc, &vhi_res_info->buffer, &doc_txt_len, "UTF-8", 0);
    xmlFreeDoc(doc);
    xmlCleanupParser();
    xmlMemoryDump();

    return (char*)vhi_res_info->buffer;
}

char* composeGiftPreAuthResponse(sGLOBALS* GLOBAL, sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo, sR017_data *sR017_info, sR008_data *sR008_info, sVHI_RESPONSE_INFO *vhi_res_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
    xmlDocPtr 	doc 					= NULL;
    xmlNodePtr 	root_node 				= NULL;

    LIBXML_TEST_VERSION;

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    doc = xmlNewDoc(BAD_CAST "1.0");

    //Create the root
    root_node = xmlNewNode(NULL, BAD_CAST "RESPONSE");
    xmlDocSetRootElement(doc, root_node);
    xmlNewChild(root_node, NULL, BAD_CAST "BATCH_TRACE_ID", BAD_CAST getXmlTagValue(eBATCH_TRACE_ID, req_xml_info));

    //Creates new child nodes
	xmlNewChild(root_node, NULL, BAD_CAST "ACCT_CURRENCY_CODE", BAD_CAST "0840");
	xmlNewChild(root_node, NULL, BAD_CAST "ACCT_NUM", BAD_CAST VHI_ACCT_NUM(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "APPROVED_AMOUNT", BAD_CAST VHI_APPROVED_AMOUNT(svbninfo, vhi_res_info, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "AVAIL_BALANCE", BAD_CAST VHI_AMOUNT_BALANCE(vrinfo, svbninfo, vhi_res_info, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "CARD_BAL", BAD_CAST VHI_AMOUNT_BALANCE(vrinfo, svbninfo, vhi_res_info, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "CLIENT_ID", BAD_CAST VHI_CLIENT_ID(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "COMMAND", BAD_CAST VHI_COMMAND(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "CTROUTD", BAD_CAST VHI_CTROUTD(sR008_info));
    xmlNewChild(root_node, NULL, BAD_CAST "AUTH_CODE", BAD_CAST VHI_AUTH_CODE(svbninfo));
    xmlNewChild(root_node, NULL, BAD_CAST "INTRN_SEQ_NUM", BAD_CAST VHI_INTRN_SEQ_NUM(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "INV_NUM", BAD_CAST VHI_INVOICE(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "INVOICE", BAD_CAST VHI_INVOICE(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "LCL_TRANS_DATE", BAD_CAST NULL);
	xmlNewChild(root_node, NULL, BAD_CAST "LCL_TRANS_TIME", BAD_CAST NULL);
	xmlNewChild(root_node, NULL, BAD_CAST "LOCAL_TRANS_DATE", BAD_CAST VHI_LOCAL_TRANS_DATE(svbninfo, vhi_res_info));
	xmlNewChild(root_node, NULL, BAD_CAST "LOCAL_TRANS_TIME", BAD_CAST VHI_LOCAL_TRANS_TIME(svbninfo, vhi_res_info));
	xmlNewChild(root_node, NULL, BAD_CAST "PAYMENT_MEDIA", BAD_CAST VHI_PAYMENT_MEDIA(svbninfo, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "PAYMENT_TYPE", BAD_CAST VHI_PAYMENT_TYPE(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "PROCESSING_CODE", BAD_CAST VHI_PROCESSING_CODE(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "REMAINING_BALANCE", BAD_CAST VHI_AMOUNT_BALANCE(vrinfo, svbninfo, vhi_res_info, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST VHI_RESULT(GLOBAL, svbninfo, sR008_info, vhi_res_info, GCONFIG, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_CODE", BAD_CAST NULL);
	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST VHI_RESPONSE_TEXT(GLOBAL, svbninfo, vhi_res_info, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST VHI_RESULT_CODE(vhi_res_info));
	if(VHI_HOST_RESP_CODE(vhi_res_info) != NULL)
	{
		xmlNewChild(root_node, NULL, BAD_CAST "HOST_RESPCODE", BAD_CAST VHI_HOST_RESP_CODE(vhi_res_info));
	}
	xmlNewChild(root_node, NULL, BAD_CAST "STORE_DIV_NUM", BAD_CAST NULL);
	xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST VHI_TERMINATION_STATUS(vhi_res_info));
	xmlNewChild(root_node, NULL, BAD_CAST "TRACE_AUDIT_NUM", BAD_CAST VHI_TROUTD(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "TRANS_AMOUNT", BAD_CAST VHI_TRANS_AMOUNT(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "TRANS_CONVERSION_RATE", BAD_CAST NULL);
	xmlNewChild(root_node, NULL, BAD_CAST "TRANS_CURRENCY_CODE", BAD_CAST "0840");
	xmlNewChild(root_node, NULL, BAD_CAST "TRANS_DATE", BAD_CAST VHI_TRANS_DATE(vhi_res_info));
	xmlNewChild(root_node, NULL, BAD_CAST "TRANS_SEQ_NUM", BAD_CAST VHI_TRANS_SEQ_NUM(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "TRANS_TIME", BAD_CAST VHI_TRANS_TIME(vhi_res_info, vhi_fieldinfo));
	xmlNewChild(root_node, NULL, BAD_CAST "TROUTD", BAD_CAST VHI_TROUTD(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "CARD_TOKEN", BAD_CAST VHI_CARD_TOKEN(sR017_info));

	xmlNewChild(root_node, NULL, BAD_CAST "MERCHID", BAD_CAST GCONFIG->mid);
	xmlNewChild(root_node, NULL, BAD_CAST "TERMID", BAD_CAST GCONFIG->tid);

	if(VHI_VSP_CODE(vhi_res_info) != NULL)
	{
		xmlNewChild(root_node, NULL, BAD_CAST "VSP_CODE", BAD_CAST VHI_VSP_CODE(vhi_res_info));
		xmlNewChild(root_node, NULL, BAD_CAST "VSP_RESULTDESC", BAD_CAST VHI_VSP_RESULT_DESC(vhi_res_info));
	}

    int doc_txt_len;

	if (vhi_res_info->buffer) {
		free(vhi_res_info->buffer);
		vhi_res_info->buffer = NULL;
	}

    xmlDocDumpFormatMemoryEnc(doc, &vhi_res_info->buffer, &doc_txt_len, "UTF-8", 0);
    xmlFreeDoc(doc);
    xmlCleanupParser();
    xmlMemoryDump();

    return (char*)vhi_res_info->buffer;
}

char* composeGiftCompletionResponse(sGLOBALS* GLOBAL, sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo, sR017_data *sR017_info, sR008_data *sR008_info, sVHI_RESPONSE_INFO *vhi_res_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
    xmlDocPtr 	doc 					= NULL;
    xmlNodePtr 	root_node 				= NULL;

    LIBXML_TEST_VERSION;

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    doc = xmlNewDoc(BAD_CAST "1.0");

    //Create the root
    root_node = xmlNewNode(NULL, BAD_CAST "RESPONSE");
    xmlDocSetRootElement(doc, root_node);
    xmlNewChild(root_node, NULL, BAD_CAST "BATCH_TRACE_ID", BAD_CAST getXmlTagValue(eBATCH_TRACE_ID, req_xml_info));

    //Creates new child nodes
	xmlNewChild(root_node, NULL, BAD_CAST "ACCT_CURRENCY_CODE", BAD_CAST "0840");
	xmlNewChild(root_node, NULL, BAD_CAST "ACCT_NUM", BAD_CAST VHI_ACCT_NUM(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "APPROVED_AMOUNT", BAD_CAST VHI_APPROVED_AMOUNT(svbninfo, vhi_res_info, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "AVAIL_BALANCE", BAD_CAST VHI_AMOUNT_BALANCE(vrinfo, svbninfo, vhi_res_info, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "CARD_BAL", BAD_CAST VHI_AMOUNT_BALANCE(vrinfo, svbninfo, vhi_res_info, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "CLIENT_ID", BAD_CAST VHI_CLIENT_ID(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "COMMAND", BAD_CAST VHI_COMMAND(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "CTROUTD", BAD_CAST VHI_CTROUTD(sR008_info));
    xmlNewChild(root_node, NULL, BAD_CAST "AUTH_CODE", BAD_CAST VHI_AUTH_CODE(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "INTRN_SEQ_NUM", BAD_CAST VHI_INTRN_SEQ_NUM(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "INV_NUM", BAD_CAST VHI_INVOICE(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "INVOICE", BAD_CAST VHI_INVOICE(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "LCL_TRANS_DATE", BAD_CAST NULL);
	xmlNewChild(root_node, NULL, BAD_CAST "LCL_TRANS_TIME", BAD_CAST NULL);
	xmlNewChild(root_node, NULL, BAD_CAST "LOCAL_TRANS_DATE", BAD_CAST VHI_LOCAL_TRANS_DATE(svbninfo, vhi_res_info));
	xmlNewChild(root_node, NULL, BAD_CAST "LOCAL_TRANS_TIME", BAD_CAST VHI_LOCAL_TRANS_TIME(svbninfo, vhi_res_info));
	xmlNewChild(root_node, NULL, BAD_CAST "PAYMENT_MEDIA", BAD_CAST VHI_PAYMENT_MEDIA(svbninfo, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "PAYMENT_TYPE", BAD_CAST VHI_PAYMENT_TYPE(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "PROCESSING_CODE", BAD_CAST VHI_PROCESSING_CODE(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "REMAINING_BALANCE", BAD_CAST VHI_AMOUNT_BALANCE(vrinfo, svbninfo, vhi_res_info, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST VHI_RESULT(GLOBAL, svbninfo, sR008_info, vhi_res_info, GCONFIG, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_CODE", BAD_CAST NULL);
	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST VHI_RESPONSE_TEXT(GLOBAL, svbninfo, vhi_res_info, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST VHI_RESULT_CODE(vhi_res_info));
	if(VHI_HOST_RESP_CODE(vhi_res_info) != NULL)
	{
		xmlNewChild(root_node, NULL, BAD_CAST "HOST_RESPCODE", BAD_CAST VHI_HOST_RESP_CODE(vhi_res_info));
	}
	xmlNewChild(root_node, NULL, BAD_CAST "STORE_DIV_NUM", BAD_CAST NULL);
	xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST VHI_TERMINATION_STATUS(vhi_res_info));
	xmlNewChild(root_node, NULL, BAD_CAST "TRACE_AUDIT_NUM", BAD_CAST VHI_TROUTD(svbninfo));
	//xmlNewChild(root_node, NULL, BAD_CAST "TRANS_AMOUNT", BAD_CAST VHI_TRANS_AMOUNT(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "TRANS_CONVERSION_RATE", BAD_CAST NULL);
	xmlNewChild(root_node, NULL, BAD_CAST "TRANS_CURRENCY_CODE", BAD_CAST "0840");
	xmlNewChild(root_node, NULL, BAD_CAST "TRANS_DATE", BAD_CAST VHI_TRANS_DATE(vhi_res_info));
	xmlNewChild(root_node, NULL, BAD_CAST "TRANS_SEQ_NUM", BAD_CAST VHI_TRANS_SEQ_NUM(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "TRANS_TIME", BAD_CAST VHI_TRANS_TIME(vhi_res_info, vhi_fieldinfo));
	xmlNewChild(root_node, NULL, BAD_CAST "TROUTD", BAD_CAST VHI_TROUTD(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "CARD_TOKEN", BAD_CAST VHI_CARD_TOKEN(sR017_info));

	xmlNewChild(root_node, NULL, BAD_CAST "MERCHID", BAD_CAST GCONFIG->mid);
	xmlNewChild(root_node, NULL, BAD_CAST "TERMID", BAD_CAST GCONFIG->tid);

	if(VHI_VSP_CODE(vhi_res_info) != NULL)
	{
		xmlNewChild(root_node, NULL, BAD_CAST "VSP_CODE", BAD_CAST VHI_VSP_CODE(vhi_res_info));
		xmlNewChild(root_node, NULL, BAD_CAST "VSP_RESULTDESC", BAD_CAST VHI_VSP_RESULT_DESC(vhi_res_info));
	}

    int doc_txt_len;

	if (vhi_res_info->buffer) {
		free(vhi_res_info->buffer);
		vhi_res_info->buffer = NULL;
	}

    xmlDocDumpFormatMemoryEnc(doc, &vhi_res_info->buffer, &doc_txt_len, "UTF-8", 0);
    xmlFreeDoc(doc);
    xmlCleanupParser();
    xmlMemoryDump();

    return (char*)vhi_res_info->buffer;
}

char* composeSignatureResponse(sVHI_RESPONSE_INFO *vhi_res_info)
{
    xmlDocPtr doc = NULL;
    xmlNodePtr root_node = NULL;

    LIBXML_TEST_VERSION;

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    doc = xmlNewDoc(BAD_CAST "1.0");

    //Create the root
    root_node = xmlNewNode(NULL, BAD_CAST "RESPONSE");
    xmlDocSetRootElement(doc, root_node);

    //Creates new child nodes
	xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "SUCCESS");
	xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST "-1");
	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST "SUCCESS");

    int doc_txt_len;

	if (vhi_res_info->buffer) {
		free(vhi_res_info->buffer);
		vhi_res_info->buffer = NULL;
	}

    xmlDocDumpFormatMemoryEnc(doc, &vhi_res_info->buffer, &doc_txt_len, "UTF-8", 0);
    xmlFreeDoc(doc);
    xmlCleanupParser();
    xmlMemoryDump();

    return (char*)vhi_res_info->buffer;
}

char* composeBatchResponse(sGLOBALS* GLOBAL, _sVTV_FIELDS *svbninfo, sR008_data *sR008_info, sVHI_RESPONSE_INFO *vhi_res_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info)
{
    xmlDocPtr doc = NULL;
    xmlNodePtr root_node = NULL;

    LIBXML_TEST_VERSION;

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    doc = xmlNewDoc(BAD_CAST "1.0");

    //Create the root
    root_node = xmlNewNode(NULL, BAD_CAST "RESPONSE");
    xmlDocSetRootElement(doc, root_node);
    xmlNewChild(root_node, NULL, BAD_CAST "BATCH_TRACE_ID", BAD_CAST getXmlTagValue(eBATCH_TRACE_ID, req_xml_info));

    //Creates new child nodes
	//VHI_RESULT(); //perform this to get result_code and termination_status
	xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST VHI_RESULT(GLOBAL, svbninfo, sR008_info, vhi_res_info, GCONFIG, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST VHI_RESPONSE_TEXT(GLOBAL, svbninfo, vhi_res_info, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST VHI_RESULT_CODE(vhi_res_info));
	xmlNewChild(root_node, NULL, BAD_CAST "TERMINTAION_STATUS", BAD_CAST VHI_TERMINATION_STATUS(vhi_res_info));
	if(VHI_HOST_RESP_CODE(vhi_res_info) != NULL)
	{
		xmlNewChild(root_node, NULL, BAD_CAST "HOST_RESPCODE", BAD_CAST VHI_HOST_RESP_CODE(vhi_res_info));
	}

	if(VHI_VSP_CODE(vhi_res_info) != NULL)
	{
		xmlNewChild(root_node, NULL, BAD_CAST "VSP_CODE", BAD_CAST VHI_VSP_CODE(vhi_res_info));
		xmlNewChild(root_node, NULL, BAD_CAST "VSP_RESULTDESC", BAD_CAST VHI_VSP_RESULT_DESC(vhi_res_info));
	}

    int doc_txt_len;

	if (vhi_res_info->buffer) {
		free(vhi_res_info->buffer);
		vhi_res_info->buffer = NULL;
	}

    xmlDocDumpFormatMemoryEnc(doc, &vhi_res_info->buffer, &doc_txt_len, "UTF-8", 0);
    xmlFreeDoc(doc);
    xmlCleanupParser();
    xmlMemoryDump();

    /*reset STAN count if SETTLE is approved*/
    char* result_code = VHI_RESULT_CODE(vhi_res_info);
    if (strncmp(result_code, "2", 1)==COMPARE_OK) {
    	setEnv("DHI", "STAN", "1");
    	removeTextFile(TEXTFILE_VOIDS);
    	removeTextFile(TEXTFILE_TIPADJUST);
    }
    /*end*/

    return (char*)vhi_res_info->buffer;
}

char* composeReportResponse(sVHI_RESPONSE_INFO *vhi_res_info, _sSCA_XML_REQUEST *req_xml_info)
{
    xmlDocPtr doc = NULL;
    xmlNodePtr root_node = NULL;
    xmlNodePtr searchfields_node = NULL;
    xmlNodePtr records_node = NULL;

    LIBXML_TEST_VERSION;

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    doc = xmlNewDoc(BAD_CAST "1.0");

    //Create the root
    root_node = xmlNewNode(NULL, BAD_CAST "RESPONSE");
    xmlDocSetRootElement(doc, root_node);
    xmlNewChild(root_node, NULL, BAD_CAST "BATCH_TRACE_ID", BAD_CAST getXmlTagValue(eBATCH_TRACE_ID, req_xml_info));

    //Creates new child nodes
	xmlNewChild(root_node, NULL, BAD_CAST "RETURN_FLD_HDRS", BAD_CAST VHI_RETURN_FLD_HDRS(req_xml_info));
   	xmlNewChild(root_node, NULL, BAD_CAST "CLIENT_ID", BAD_CAST VHI_CLIENT_ID(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "FORMAT", BAD_CAST VHI_FORMAT(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "SERIAL_NUM", BAD_CAST VHI_SERIAL_NUM(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "COMMAND", BAD_CAST VHI_COMMAND(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "MAX_NUM_RECORDS_RETURNED", BAD_CAST VHI_MAX_NUM_RECORDS_RETURNED(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "DEVICEKEY", BAD_CAST VHI_DEVICEKEY(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "DELIMITER", BAD_CAST VHI_DELIMITER(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "FUNCTION_TYPE", BAD_CAST VHI_FUNCTION_TYPE(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "DEVTYPE", BAD_CAST VHI_DEVTYPE(req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "NUM_RECORDS_FOUND", BAD_CAST VHI_NUM_RECORDS_FOUND());

	searchfields_node = xmlNewChild(root_node, NULL, BAD_CAST "SEARCHFIELDS", BAD_CAST NULL);
	{
		xmlNewChild(searchfields_node, NULL, BAD_CAST "START_TRANS_DATE", BAD_CAST VHI_START_TRANS_DATE(req_xml_info));
		xmlNewChild(searchfields_node, NULL, BAD_CAST "END_TRANS_DATE", BAD_CAST VHI_END_TRANS_DATE(req_xml_info));
	}

	records_node = xmlNewChild(root_node, NULL, BAD_CAST "RECORDS", BAD_CAST NULL);
	{
		xmlNewChild(records_node, NULL, BAD_CAST "COMMAND", BAD_CAST NULL);
	}

	int doc_txt_len;

	if (vhi_res_info->buffer) {
		free(vhi_res_info->buffer);
		vhi_res_info->buffer = NULL;
	}

    xmlDocDumpFormatMemoryEnc(doc, &vhi_res_info->buffer, &doc_txt_len, "UTF-8", 0);
    xmlFreeDoc(doc);
    xmlCleanupParser();
    xmlMemoryDump();

    return (char*)vhi_res_info->buffer;
}

char* composeLastTranResponse(sVHI_RESPONSE_INFO *vhi_res_info)
{
    xmlDocPtr doc = NULL;
    xmlNodePtr root_node = NULL;
    int	 iIndex ;
    int	 iStatus = EXIT_FAILURE;
    char szLastTranDtls[LAST_TRAN_ITEM_COUNT][256+1];

    LIBXML_TEST_VERSION;

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    for(iIndex = 0; iIndex < LAST_TRAN_ITEM_COUNT; iIndex++)
    {
    	memset(szLastTranDtls[iIndex], 0x00, 256+1);
    }

    iStatus = getLastTranDtls(szLastTranDtls);

    doc = xmlNewDoc(BAD_CAST "1.0");

	//Create the root
	root_node = xmlNewNode(NULL, BAD_CAST "RESPONSE");
	xmlDocSetRootElement(doc, root_node);

    if(iStatus != EXIT_SUCCESS)
    {
    	xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST "NOT_PROCESSED");
		xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST "LAST TRANSACTION NOT AVAILABLE");
		xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST "FAILURE");
		xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST "6");
    }
    else
    {
		iIndex = 0;

		if(strlen(szLastTranDtls[iIndex]) > 0)
		{
			xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST szLastTranDtls[iIndex]);
		}

		iIndex++;

		if(strlen(szLastTranDtls[iIndex]) > 0)
		{
			xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST szLastTranDtls[iIndex]);
		}

		iIndex++;

		if(strlen(szLastTranDtls[iIndex]) > 0)
		{
			xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST szLastTranDtls[iIndex]);
		}

		iIndex++;

		if(strlen(szLastTranDtls[iIndex]) > 0)
		{
			xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST szLastTranDtls[iIndex]);
		}

		iIndex++;

		if(strlen(szLastTranDtls[iIndex]) > 0)
		{
			xmlNewChild(root_node, NULL, BAD_CAST "HOST_RESPCODE", BAD_CAST szLastTranDtls[iIndex]);
		}

		iIndex++;

		if(strlen(szLastTranDtls[iIndex]) > 0)
		{
			xmlNewChild(root_node, NULL, BAD_CAST "CLIENT_ID", BAD_CAST szLastTranDtls[iIndex]);
		}

		iIndex++;

		if(strlen(szLastTranDtls[iIndex]) > 0)
		{
			xmlNewChild(root_node, NULL, BAD_CAST "TROUTD", BAD_CAST szLastTranDtls[iIndex]);
		}

		iIndex++;

		if(strlen(szLastTranDtls[iIndex]) > 0)
		{
			xmlNewChild(root_node, NULL, BAD_CAST "CTROUTD", BAD_CAST szLastTranDtls[iIndex]);
		}

		iIndex++;

		if(strlen(szLastTranDtls[iIndex]) > 0)
		{
			xmlNewChild(root_node, NULL, BAD_CAST "INTRN_SEQ_NUM", BAD_CAST szLastTranDtls[iIndex]);
		}

		iIndex++;

		if(strlen(szLastTranDtls[iIndex]) > 0)
		{
			xmlNewChild(root_node, NULL, BAD_CAST "COMMAND", BAD_CAST szLastTranDtls[iIndex]);
		}

		iIndex++;

		if(strlen(szLastTranDtls[iIndex]) > 0)
		{
			xmlNewChild(root_node, NULL, BAD_CAST "ACCT_NUM", BAD_CAST szLastTranDtls[iIndex]);
		}

		iIndex++;

		if(strlen(szLastTranDtls[iIndex]) > 0)
		{
			xmlNewChild(root_node, NULL, BAD_CAST "CARDHOLDER", BAD_CAST szLastTranDtls[iIndex]);
		}

		iIndex++;

		if(strlen(szLastTranDtls[iIndex]) > 0)
		{
			xmlNewChild(root_node, NULL, BAD_CAST "TRANS_AMOUNT", BAD_CAST szLastTranDtls[iIndex]);
		}

		iIndex++;

		if(strlen(szLastTranDtls[iIndex]) > 0)
		{
			xmlNewChild(root_node, NULL, BAD_CAST "APPROVED_AMOUNT", BAD_CAST szLastTranDtls[iIndex]);
		}

		iIndex++;

		if(strlen(szLastTranDtls[iIndex]) > 0)
		{
			xmlNewChild(root_node, NULL, BAD_CAST "TRANS_DATE", BAD_CAST szLastTranDtls[iIndex]);
		}

		iIndex++;

		if(strlen(szLastTranDtls[iIndex]) > 0)
		{
			xmlNewChild(root_node, NULL, BAD_CAST "TRANS_TIME", BAD_CAST szLastTranDtls[iIndex]);
		}

		iIndex++;

		if(strlen(szLastTranDtls[iIndex]) > 0)
		{
			xmlNewChild(root_node, NULL, BAD_CAST "INVOICE", BAD_CAST szLastTranDtls[iIndex]);
		}

		iIndex++;

		if(strlen(szLastTranDtls[iIndex]) > 0)
		{
			xmlNewChild(root_node, NULL, BAD_CAST "PAYMENT_TYPE", BAD_CAST szLastTranDtls[iIndex]);
		}

		iIndex++;

		if(strlen(szLastTranDtls[iIndex]) > 0)
		{
			xmlNewChild(root_node, NULL, BAD_CAST "PAYMENT_MEDIA", BAD_CAST szLastTranDtls[iIndex]);
		}

		iIndex++;

		if(strlen(szLastTranDtls[iIndex]) > 0)
		{
			xmlNewChild(root_node, NULL, BAD_CAST "CARD_TOKEN", BAD_CAST szLastTranDtls[iIndex]);
		}

		iIndex++;
        
        if(strlen(szLastTranDtls[iIndex]) > 0)
        {
            xmlNewChild(root_node, NULL, BAD_CAST "AUTH_CODE", BAD_CAST szLastTranDtls[iIndex]);
        }
        
        iIndex++;
        
        if(strlen(szLastTranDtls[iIndex]) > 0)
        {
            xmlNewChild(root_node, NULL, BAD_CAST "REFERENCE", BAD_CAST szLastTranDtls[iIndex]);
        }
        
        iIndex++;
       
        if(strlen(szLastTranDtls[iIndex]) > 0)
        {
            xmlNewChild(root_node, NULL, BAD_CAST "EMV_TAGS_RESPONSE", BAD_CAST szLastTranDtls[iIndex]);
        }
        iIndex++;

        
#ifdef OLD_EMV_TAG_METHOD
        // items from R023 response
        if(strlen(szLastTranDtls[iIndex]) > 0)
        {
            xmlNewChild(root_node, NULL, BAD_CAST "AUTHORIZATION_RESPONSE_CODE", BAD_CAST szLastTranDtls[iIndex]);
        }
        iIndex++;
        
        if(strlen(szLastTranDtls[iIndex]) > 0)
        {
            xmlNewChild(root_node, NULL, BAD_CAST "EMV_ISSUER_SCRIPT_DATA1", BAD_CAST szLastTranDtls[iIndex]);
        }
        iIndex++;
        
        if(strlen(szLastTranDtls[iIndex]) > 0)
        {
            xmlNewChild(root_node, NULL, BAD_CAST "EMV_ISSUER_SCRIPT_DATA2", BAD_CAST szLastTranDtls[iIndex]);
        }
        iIndex++;
        
        if(strlen(szLastTranDtls[iIndex]) > 0)
        {
            xmlNewChild(root_node, NULL, BAD_CAST "ISSUER_AUTHENTICATION_DATA", BAD_CAST szLastTranDtls[iIndex]);
        }
        iIndex++;
        
        if(strlen(szLastTranDtls[iIndex]) > 0)
        {
            xmlNewChild(root_node, NULL, BAD_CAST "ISSUER_SCRIPT_COMMAND", BAD_CAST szLastTranDtls[iIndex]);
        }
        iIndex++;
        
        if(strlen(szLastTranDtls[iIndex]) > 0)
        {
            xmlNewChild(root_node, NULL, BAD_CAST "ISSUER_SCRIPT_IDENTIFIER", BAD_CAST szLastTranDtls[iIndex]);
        }
        iIndex++;
        
        if(strlen(szLastTranDtls[iIndex]) > 0)
        {
            xmlNewChild(root_node, NULL, BAD_CAST "ISSUER_SCRIPT_RESULTS", BAD_CAST szLastTranDtls[iIndex]);
        }
        iIndex++;
#endif
    }

	int doc_txt_len;

	if (vhi_res_info->buffer) {
		free(vhi_res_info->buffer);
		vhi_res_info->buffer = NULL;
	}

    xmlDocDumpFormatMemoryEnc(doc, &vhi_res_info->buffer, &doc_txt_len, "UTF-8", 0);
    xmlFreeDoc(doc);
    xmlCleanupParser();
    xmlMemoryDump();

    return (char*)vhi_res_info->buffer;
}

char* composeRegistrationResponse(sVHI_RESPONSE_INFO *vhi_res_info) //temporary admin response
{
	xmlDocPtr doc = NULL;
	xmlAttrPtr newattr;
    xmlNodePtr root_node = NULL;
    xmlNodePtr admin_node = NULL;
    //xmlNodePtr card_type_node = NULL;
    //xmlNodePtr card_types_node = NULL;
    xmlNodePtr supp_funcs_node = NULL;
    xmlNodePtr supp_func_node = NULL;
    //xmlNodePtr trans_types_node = NULL;
    //xmlNodePtr trans_type_node = NULL;

    LIBXML_TEST_VERSION;

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    doc = xmlNewDoc(BAD_CAST "1.0");

    //Create the root
    root_node = xmlNewNode(NULL, BAD_CAST "MSH");
    xmlDocSetRootElement(doc, root_node);

    /*
     * add elem (ADMIN) in root RESPONSE
     * add attrib COMMAND="SETUP_RESPONSEV3"
     */
    admin_node = xmlNewChild(root_node, NULL, BAD_CAST"ADMIN", NULL);
    newattr = xmlNewProp(admin_node, BAD_CAST"COMMAND", BAD_CAST"SETUP_RESPONSEV3");
#if 0
	/*
	 * add elem CARD_TYPES inside ADMIN
	 */
	card_types_node = xmlNewChild (admin_node, NULL, BAD_CAST "CARD_TYPES", NULL);

	/*
	 * add multiple elem (CARD_TYPE) inside CARD_TYPES
	 * add also each attributes
	 */
	card_type_node = xmlNewChild (card_types_node, NULL, BAD_CAST"CARD_TYPE", BAD_CAST"VISA");
	newattr = xmlNewProp (card_type_node, BAD_CAST"ENABLED", BAD_CAST"1");
	card_type_node = xmlNewChild (card_types_node, NULL, BAD_CAST"CARD_TYPE", BAD_CAST"MC");
	newattr = xmlNewProp (card_type_node, BAD_CAST"ENABLED", BAD_CAST"1");
	card_type_node = xmlNewChild (card_types_node, NULL, BAD_CAST"CARD_TYPE", BAD_CAST"AMEX");
	newattr = xmlNewProp (card_type_node, BAD_CAST"ENABLED", BAD_CAST"1");
	card_type_node = xmlNewChild (card_types_node, NULL, BAD_CAST"CARD_TYPE", BAD_CAST"DISC");
	newattr = xmlNewProp (card_type_node, BAD_CAST"ENABLED", BAD_CAST"0");
	card_type_node = xmlNewChild (card_types_node, NULL, BAD_CAST"CARD_TYPE", BAD_CAST"CBLN");
	newattr = xmlNewProp (card_type_node, BAD_CAST"ENABLED", BAD_CAST"0");
	card_type_node = xmlNewChild (card_types_node, NULL, BAD_CAST"CARD_TYPE", BAD_CAST"JAL");
	newattr = xmlNewProp (card_type_node, BAD_CAST"ENABLED", BAD_CAST"0");
	card_type_node = xmlNewChild (card_types_node, NULL, BAD_CAST"CARD_TYPE", BAD_CAST"JCB");
	newattr = xmlNewProp (card_type_node, BAD_CAST"ENABLED", BAD_CAST"0");
	card_type_node = xmlNewChild (card_types_node, NULL, BAD_CAST"CARD_TYPE", BAD_CAST"ENRT");
	newattr = xmlNewProp (card_type_node, BAD_CAST"ENABLED", BAD_CAST"0");
	card_type_node = xmlNewChild (card_types_node, NULL, BAD_CAST"CARD_TYPE", BAD_CAST"DCCB");
	newattr = xmlNewProp (card_type_node, BAD_CAST"ENABLED", BAD_CAST"0");
	card_type_node = xmlNewChild (card_types_node, NULL, BAD_CAST"CARD_TYPE", BAD_CAST"UNKNW");
	newattr = xmlNewProp (card_type_node, BAD_CAST"ENABLED", BAD_CAST"0");
	card_type_node = xmlNewChild (card_types_node, NULL, BAD_CAST"CARD_TYPE", BAD_CAST"ALL");
	newattr = xmlNewProp (card_type_node, BAD_CAST"ENABLED", BAD_CAST"");
#endif


	/*
	 * add elem SUPPORTED_FUNCTIONS inside ADMIN
	 */
    supp_funcs_node = xmlNewChild(admin_node, NULL, BAD_CAST "SUPPORTED_FUNCTIONS", NULL);

    //add elem (SUPPORTED_FUNCTION) inside SUPPORTED_FUNCTIONS
	supp_func_node = xmlNewChild (supp_funcs_node, NULL, BAD_CAST"SUPPORTED_FUNCTION", NULL);
	xmlNewProp (supp_func_node, BAD_CAST"PROCESSOR_ID", BAD_CAST"SP53");
	xmlNewProp (supp_func_node, BAD_CAST"ID", BAD_CAST"CREDIT");
#if 0
	//add elem TRANSACTION_TYPES inside SUPPORTED_FUNCTION
	trans_types_node = xmlNewChild (supp_func_node, NULL, BAD_CAST"TRANSACTION_TYPES", NULL);
	//add elem TRANSACTION_TYPE inside TRANSACTION_TYPES
	trans_type_node = xmlNewChild (trans_types_node, NULL, BAD_CAST"TRANSACTION_TYPE", BAD_CAST"SALE");
	xmlNewProp (trans_type_node, BAD_CAST"ENABLED", BAD_CAST"0");
	trans_type_node = xmlNewChild (trans_types_node, NULL, BAD_CAST"TRANSACTION_TYPE", BAD_CAST"CREDIT");
	xmlNewProp (trans_type_node, BAD_CAST"ENABLED", BAD_CAST"0");
	trans_type_node = xmlNewChild (trans_types_node, NULL, BAD_CAST"TRANSACTION_TYPE", BAD_CAST"VOID");
	xmlNewProp (trans_type_node, BAD_CAST"ENABLED", BAD_CAST"0");
	trans_type_node = xmlNewChild (trans_types_node, NULL, BAD_CAST"TRANSACTION_TYPE", BAD_CAST"POST_AUTH");
	xmlNewProp (trans_type_node, BAD_CAST"ENABLED", BAD_CAST"0");
	trans_type_node = xmlNewChild (trans_types_node, NULL, BAD_CAST"TRANSACTION_TYPE", BAD_CAST "PRE_AUTH");
	xmlNewProp (trans_type_node, BAD_CAST"ENABLED", BAD_CAST"0");
#endif

    //add elem (SUPPORTED_FUNCTION) inside SUPPORTED_FUNCTIONS
	supp_func_node = xmlNewChild (supp_funcs_node, NULL, BAD_CAST"SUPPORTED_FUNCTION", NULL);
	xmlNewProp (supp_func_node, BAD_CAST"PROCESSOR_ID", BAD_CAST"SP53");
	xmlNewProp (supp_func_node, BAD_CAST"ID", BAD_CAST"DEBIT");
#if 0
	//add elem TRANSACTION_TYPES inside SUPPORTED_FUNCTION
	trans_types_node = xmlNewChild (supp_func_node, NULL, BAD_CAST"TRANSACTION_TYPES", NULL);
	//add elem TRANSACTION_TYPE inside TRANSACTION_TYPES
	trans_type_node = xmlNewChild (trans_types_node, NULL, BAD_CAST"TRANSACTION_TYPE", BAD_CAST"SALE");
	xmlNewProp (trans_type_node, BAD_CAST"ENABLED", BAD_CAST"0");
	trans_type_node = xmlNewChild (trans_types_node, NULL, BAD_CAST"TRANSACTION_TYPE", BAD_CAST"VOID");
	xmlNewProp (trans_type_node, BAD_CAST"ENABLED", BAD_CAST"0");
#endif

    //add elem (SUPPORTED_FUNCTION) inside SUPPORTED_FUNCTIONS
	supp_func_node = xmlNewChild (supp_funcs_node, NULL, BAD_CAST"SUPPORTED_FUNCTION", NULL);
	xmlNewProp (supp_func_node, BAD_CAST"PROCESSOR_ID", BAD_CAST"SP53");
	xmlNewProp (supp_func_node, BAD_CAST"ID", BAD_CAST"GIFT");

#if 0
	//add elem TRANSACTION_TYPES inside SUPPORTED_FUNCTION
	trans_types_node = xmlNewChild (supp_func_node, NULL, BAD_CAST"TRANSACTION_TYPES", NULL);
	//add elem TRANSACTION_TYPE inside TRANSACTION_TYPES
	trans_type_node = xmlNewChild (trans_types_node, NULL, BAD_CAST"TRANSACTION_TYPE", BAD_CAST"ACTIVATE");
	xmlNewProp (trans_type_node, BAD_CAST"ENABLED", BAD_CAST"0");
	trans_type_node = xmlNewChild (trans_types_node, NULL, BAD_CAST"TRANSACTION_TYPE", BAD_CAST"ADD_VALUE");
	xmlNewProp (trans_type_node, BAD_CAST"ENABLED", BAD_CAST"0");
	trans_type_node = xmlNewChild (trans_types_node, NULL, BAD_CAST"TRANSACTION_TYPE", BAD_CAST"SALE");
	xmlNewProp (trans_type_node, BAD_CAST"ENABLED", BAD_CAST"0");
	trans_type_node = xmlNewChild (trans_types_node, NULL, BAD_CAST"TRANSACTION_TYPE", BAD_CAST"VOID");
	xmlNewProp (trans_type_node, BAD_CAST"ENABLED", BAD_CAST"0");
#endif
    
    //add elem (SUPPORTED_FUNCTION) inside SUPPORTED_FUNCTIONS
    supp_func_node = xmlNewChild (supp_funcs_node, NULL, BAD_CAST"SUPPORTED_FUNCTION", NULL);
    xmlNewProp (supp_func_node, BAD_CAST"PROCESSOR_ID", BAD_CAST"SP53");
    xmlNewProp (supp_func_node, BAD_CAST"ID", BAD_CAST"EBT");

	/*
	 * add elem CLIENT_ID inside ADMIN
	 */
	xmlNewChild(admin_node, NULL, BAD_CAST "CLIENT_ID", BAD_CAST "100010001");

	/*
	 * add elem CLIENT_ID inside DEVICEKEY
	 */
	xmlNewChild(admin_node, NULL, BAD_CAST "DEVICEKEY", BAD_CAST "DUMMY DEVICE KEY");

	/*
	 * add elem TERMINAL_NAME inside ADMIN
	 */
    //xmlNewChild(admin_node, NULL, BAD_CAST "TERMINAL_NAME", BAD_CAST NULL);

	/*
	 * add elem STATUSS inside ADMIN
	 */
    xmlNewChild(admin_node, NULL, BAD_CAST "STATUS", BAD_CAST "1");


    int doc_txt_len;

	if (vhi_res_info->buffer) {
		free(vhi_res_info->buffer);
		vhi_res_info->buffer = NULL;
	}

    xmlDocDumpFormatMemoryEnc(doc, &vhi_res_info->buffer, &doc_txt_len, "UTF-8", 1);
    xmlFreeDoc(doc);
    xmlCleanupParser();
    xmlMemoryDump();

    return (char*)vhi_res_info->buffer;
}
char *composeVersionResponse(sVHI_RESPONSE_INFO *vhi_res_info, sCONFIG * GCONFIG)
{
	xmlDocPtr 	doc 			= NULL;
	xmlNodePtr 	root_node 		= NULL;
	char		szVersion[100] 	= "";

	LIBXML_TEST_VERSION;

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
	doc = xmlNewDoc(BAD_CAST "1.0");

	//Create the root
	root_node = xmlNewNode(NULL, BAD_CAST "RESPONSE");
	xmlDocSetRootElement(doc, root_node);

	//Creates new child nodes
	//VHI_RESULT(); //perform this to get result_code and termination_status
	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST "VERSION INFO CAPTURED");
	xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST "4");
	xmlNewChild(root_node, NULL, BAD_CAST "TERMINTAION_STATUS", BAD_CAST "SUCCESS");
	xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST "OK");

	memset(szVersion, 0x00, sizeof(szVersion));
	sprintf(szVersion, "%s|%s|MID=%s|TID=%s|Vantiv", VHI_APP_VERSION, VHI_BUILD_NUMBER, GCONFIG->mid, GCONFIG->tid);

	/* Add the Version Node */
	xmlNewChild(root_node, NULL, BAD_CAST "VERSION_INFO", BAD_CAST szVersion);

	int doc_txt_len;

	if (vhi_res_info->buffer) {
		free(vhi_res_info->buffer);
		vhi_res_info->buffer = NULL;
	}

	xmlDocDumpFormatMemoryEnc(doc, &vhi_res_info->buffer, &doc_txt_len, "UTF-8", 0);
	xmlFreeDoc(doc);
	xmlCleanupParser();
	xmlMemoryDump();

	/*end*/

	return (char*)vhi_res_info->buffer;
}
char* composeTemporaryResponse(sVHI_RESPONSE_INFO *vhi_res_info) //temporary debugging response
{
    xmlDocPtr doc = NULL;
    xmlNodePtr root_node = NULL;

    LIBXML_TEST_VERSION;

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    doc = xmlNewDoc(BAD_CAST "1.0");

    //Create the root
    root_node = xmlNewNode(NULL, BAD_CAST "RESPONSE");
    xmlDocSetRootElement(doc, root_node);

    //Creates new child nodes
	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST "TEMPORARY");
    xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST "APPROVED");
	xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST "4");

    int doc_txt_len;

	if (vhi_res_info->buffer) {
		free(vhi_res_info->buffer);
		vhi_res_info->buffer = NULL;
	}

    xmlDocDumpFormatMemoryEnc(doc, &vhi_res_info->buffer, &doc_txt_len, "UTF-8", 0);
    xmlFreeDoc(doc);
    xmlCleanupParser();
    xmlMemoryDump();

    return (char*)vhi_res_info->buffer;
}

char* composeProcessorInquireResponse(sGLOBALS* GLOBAL, _sVTV_FIELDS *svbninfo, sR008_data *sR008_info, sVHI_RESPONSE_INFO *vhi_res_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info)
{
    xmlDocPtr doc = NULL;
    xmlNodePtr root_node = NULL;

    LIBXML_TEST_VERSION;

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    doc = xmlNewDoc(BAD_CAST "1.0");

    //Create the root
    root_node = xmlNewNode(NULL, BAD_CAST "RESPONSE");
    xmlDocSetRootElement(doc, root_node);

    //Creates new child nodes
    xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST VHI_RESULT(GLOBAL, svbninfo, sR008_info, vhi_res_info, GCONFIG, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST VHI_RESPONSE_TEXT(GLOBAL, svbninfo, vhi_res_info, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST VHI_RESULT_CODE(vhi_res_info));
	xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST VHI_TERMINATION_STATUS(vhi_res_info));

	if(VHI_VSP_CODE(vhi_res_info) != NULL)
	{
		xmlNewChild(root_node, NULL, BAD_CAST "VSP_CODE", BAD_CAST VHI_VSP_CODE(vhi_res_info));
		xmlNewChild(root_node, NULL, BAD_CAST "VSP_RESULTDESC", BAD_CAST VHI_VSP_RESULT_DESC(vhi_res_info));
	}

    int doc_txt_len;

	if (vhi_res_info->buffer) {
		free(vhi_res_info->buffer);
		vhi_res_info->buffer = NULL;
	}

    xmlDocDumpFormatMemoryEnc(doc, &vhi_res_info->buffer, &doc_txt_len, "UTF-8", 0);
    xmlFreeDoc(doc);
    xmlCleanupParser();
    xmlMemoryDump();

    return (char*)vhi_res_info->buffer;
}

char* composeAdminResponse(sGLOBALS* GLOBAL, _sVTV_FIELDS *svbninfo, sR017_data *sR017_info, sR008_data *sR008_info, sVHI_RESPONSE_INFO *vhi_res_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
    xmlDocPtr doc = NULL;
    xmlNodePtr root_node = NULL;

    LIBXML_TEST_VERSION;

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    doc = xmlNewDoc(BAD_CAST "1.0");

    //Create the root
    root_node = xmlNewNode(NULL, BAD_CAST "RESPONSE");
    xmlDocSetRootElement(doc, root_node);

    //Creates new child nodes
    xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST VHI_RESULT(GLOBAL, svbninfo, sR008_info, vhi_res_info, GCONFIG, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST VHI_RESPONSE_TEXT(GLOBAL, svbninfo, vhi_res_info, req_xml_info));
	xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST VHI_RESULT_CODE(vhi_res_info));
	xmlNewChild(root_node, NULL, BAD_CAST "TERMINATION_STATUS", BAD_CAST VHI_TERMINATION_STATUS(vhi_res_info));
	if(VHI_HOST_RESP_CODE(vhi_res_info) != NULL)
	{
		xmlNewChild(root_node, NULL, BAD_CAST "HOST_RESPCODE", BAD_CAST VHI_HOST_RESP_CODE(vhi_res_info));
	}
	xmlNewChild(root_node, NULL, BAD_CAST "CTROUTD", BAD_CAST VHI_CTROUTD(sR008_info));
	xmlNewChild(root_node, NULL, BAD_CAST "TROUTD", BAD_CAST VHI_TROUTD(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "INTRN_SEQ_NUM", BAD_CAST VHI_INTRN_SEQ_NUM(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "TRANS_SEQ_NUM", BAD_CAST VHI_TRANS_SEQ_NUM(svbninfo));
	xmlNewChild(root_node, NULL, BAD_CAST "TRANS_DATE", BAD_CAST VHI_TRANS_DATE(vhi_res_info));
	xmlNewChild(root_node, NULL, BAD_CAST "TRANS_TIME", BAD_CAST VHI_TRANS_TIME(vhi_res_info, vhi_fieldinfo));
	xmlNewChild(root_node, NULL, BAD_CAST "CARD_TOKEN", BAD_CAST VHI_CARD_TOKEN(sR017_info));

	if(VHI_VSP_CODE(vhi_res_info) != NULL)
	{
		xmlNewChild(root_node, NULL, BAD_CAST "VSP_CODE", BAD_CAST VHI_VSP_CODE(vhi_res_info));
		xmlNewChild(root_node, NULL, BAD_CAST "VSP_RESULTDESC", BAD_CAST VHI_VSP_RESULT_DESC(vhi_res_info));
	}

    int doc_txt_len;

	if (vhi_res_info->buffer) {
		free(vhi_res_info->buffer);
		vhi_res_info->buffer = NULL;
	}

    xmlDocDumpFormatMemoryEnc(doc, &vhi_res_info->buffer, &doc_txt_len, "UTF-8", 0);
    xmlFreeDoc(doc);
    xmlCleanupParser();
    xmlMemoryDump();

    return (char*)vhi_res_info->buffer;
}

void composeErrorResponse(sGLOBALS* GLOBAL, sVHI_RESPONSE_INFO *vhi_res_info)
{
    xmlDocPtr doc = NULL;
    xmlNodePtr root_node = NULL;

    LIBXML_TEST_VERSION;

    doc = xmlNewDoc(BAD_CAST "1.0");

    //Create the root
    root_node = xmlNewNode(NULL, BAD_CAST "RESPONSE");
    xmlDocSetRootElement(doc, root_node);

    //Creates new child nodes
	xmlNewChild(root_node, NULL, BAD_CAST "RESPONSE_TEXT", BAD_CAST "SSI LISTENER ERROR!");
    xmlNewChild(root_node, NULL, BAD_CAST "RESULT", BAD_CAST "DECLINED");
	xmlNewChild(root_node, NULL, BAD_CAST "RESULT_CODE", BAD_CAST "6");

    int doc_txt_len;

	if (vhi_res_info->buffer) {
		free(vhi_res_info->buffer);
		vhi_res_info->buffer = NULL;
	}

    xmlDocDumpFormatMemoryEnc(doc, &vhi_res_info->buffer, &doc_txt_len, "UTF-8", 0);
    xmlFreeDoc(doc);
    xmlCleanupParser();
    xmlMemoryDump();

    GLOBAL->http.xml_response = (char*)calloc(strlen((char*)vhi_res_info->buffer)+1, sizeof(char));
    strcpy(GLOBAL->http.xml_response, (char*)vhi_res_info->buffer);
    GLOBAL->http.responseLength = strlen(GLOBAL->http.xml_response);
}


char* VHI_AUTH_CODE(_sVTV_FIELDS *svbninfo)
{
	//Authorization Identification Response in Host Capture
	return getVantivBNResponse(eBN65, svbninfo);
}

char* VHI_AMOUNT_BALANCE(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo, sVHI_RESPONSE_INFO *vhi_res_info, _sSCA_XML_REQUEST *req_xml_info)
{
    char    *resp_field_ptr;
    
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
    
    strcpy(vhi_res_info->cb_available_balance, "");
    strcpy(vhi_res_info->cb_start_balance, "");
    strcpy(vhi_res_info->cb_end_balance, "");
    strcpy(vhi_res_info->fs_available_balance, "");
    strcpy(vhi_res_info->fs_start_balance, "");
    strcpy(vhi_res_info->fs_end_balance, "");

	char* pPAYMENT_TYPE = getXmlTagValue(ePAYMENT_TYPE, req_xml_info);

	if (pPAYMENT_TYPE != NULL) {
		if (strncmp(pPAYMENT_TYPE, COMMAND.CREDIT, strlen(COMMAND.CREDIT)) == COMPARE_OK || strncmp(pPAYMENT_TYPE, "DEBIT", 5) == COMPARE_OK) {
            debug_sprintf(szDbgMsg, "AMOUNT BALANCE is CREDIT or DEBIT");
            APP_TRACE(szDbgMsg);
			if (strlen(vhi_res_info->r007_available_balance) > 0) {
				return vhi_res_info->r007_available_balance;
			}
			else {
				return NULL;
			}
		}
	}

	//continue to Gift
	_sVTV_FIELDS fields; //use only to know the field size
	char* tmp = NULL;

	if (vrinfo->RESPONSEID == e0110_90_GC) { //GC GC/POSA/Debit/Credit Balance Inquiry Approval
		char* bn04 = getVantivBNResponse(eBN04, svbninfo);

        debug_sprintf(szDbgMsg, "AMOUNT BALANCE is 0110_90");
        APP_TRACE(szDbgMsg);
		if (bn04 == NULL) {
			debug_sprintf(szDbgMsg, "%s: ERROR! Trans amount is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NULL;
		}

		int bn04len = strlen(bn04);
		tmp = (char*)calloc(bn04len+1, sizeof(char));
		strcpy(tmp, bn04);
		addDecimalPoint(tmp);
		memset(vhi_res_info->amnt_bal, 0x00, sizeof(vhi_res_info->amnt_bal));
		strcpy(vhi_res_info->amnt_bal, tmp);

		if (tmp)
			free(tmp);

		if (strlen(vhi_res_info->amnt_bal) <= 0 )
			return NULL;

		return vhi_res_info->amnt_bal;
	}

    debug_sprintf(szDbgMsg, "AMOUNT BALANCE uses field 128");
    APP_TRACE(szDbgMsg);
	char* ptr = getVantivBNResponse(eBN128, svbninfo);
	char amount_bal[sizeof(vhi_res_info->amnt_bal)] = {0};
	memset(vhi_res_info->amnt_bal, 0x00, sizeof(vhi_res_info->amnt_bal));
	/*
	 * 40 - gift card
	 * 02 - available balance
	 * 840 - US
	 */
    
    /*
     * 06/96 - Cash Benefits | 08/98 - Food Stamp
     * 01 - ending balance | 02 - available balance | 18 - begining balance
     */

	if (ptr == NULL) {
		return NULL;
	}

	if (strlen(ptr) != sizeof(fields.bn128)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! bn128 is not 120 bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	while( strlen(ptr) > 0)
    {
		if (strncmp(ptr, "40", 2) == COMPARE_OK)
        { //40 � Gift Card --------------------
			ptr+=2; //skip field 1
			if (strncmp(ptr, "02", 2) == COMPARE_OK) { //02 - Available balance
				ptr+=2; //skip field 2
				if (strncmp(ptr, "840", 3) == COMPARE_OK) {
					ptr+=3; //skip field 3
					ptr++; //skip c,0, or D 1byte
                    strcpy(amount_bal, "");
					strncat(amount_bal, ptr, 12); //copy the field 4 to amounts 12 bytes
					ptr+=12; //skip field 4
				}
				else {
					debug_sprintf(szDbgMsg, "%s: ERROR! Not a US dollar", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					ptr+=16;
					return NULL;
				}
			}
			else {
				ptr+=18; //skip field 2, 3, 4 if not 02 Available balance
			}
		}
        else if ((strncmp(ptr, "06", 2) == COMPARE_OK) || (strncmp(ptr, "96", 2) == COMPARE_OK))
        {   // Cash Benefit --------------------
            int     length_without_zeros;
            resp_field_ptr= NULL;
            ptr+=2; //skip field 1
            if (strncmp(ptr, "01", 2) == COMPARE_OK)
            {   // ending balance
                resp_field_ptr= vhi_res_info->cb_end_balance;
            }
            else if (strncmp(ptr, "02", 2) == COMPARE_OK)
            {   // available balance
                resp_field_ptr= vhi_res_info->cb_available_balance;
            }
            else if (strncmp(ptr, "18", 2) == COMPARE_OK)
            {   // beginning balance
                resp_field_ptr= vhi_res_info->cb_start_balance;
            }
            ptr+=5; //skip field 2 & 3
            if (resp_field_ptr != NULL)
            {
                if (*ptr == 'D')
                    strcpy(resp_field_ptr, "-");
                else
                    strcpy(resp_field_ptr, "");
                ptr++;
                
                // remove leading zeros
                length_without_zeros= sizeof(vhi_res_info->fs_available_balance)- 5;
                while ((*ptr == '0') && (length_without_zeros > 1))
                {
                    length_without_zeros--;
                    ptr++;
                }
                
                strncat(resp_field_ptr, ptr, length_without_zeros);
                ptr += length_without_zeros;        // non-leading zero dollar digits
                strcat(resp_field_ptr, ".");
                strncat(resp_field_ptr, ptr, 2);
                ptr += 2;                           // cents digits
            }
            else
            {
                ptr += 13;
            }
        }
        else if ((strncmp(ptr, "08", 2) == COMPARE_OK) || (strncmp(ptr, "98", 2) == COMPARE_OK))
        {   // Food Stamp --------------------
            int     length_without_zeros;
            resp_field_ptr= NULL;
            ptr+=2; //skip field 1
            if (strncmp(ptr, "01", 2) == COMPARE_OK)
            {   // ending balance
                resp_field_ptr= vhi_res_info->fs_end_balance;
            }
            else if (strncmp(ptr, "02", 2) == COMPARE_OK)
            {   // available balance
                resp_field_ptr= vhi_res_info->fs_available_balance;
            }
            else if (strncmp(ptr, "18", 2) == COMPARE_OK)
            {   // beginning balance
                resp_field_ptr= vhi_res_info->fs_start_balance;
            }
            ptr+=5; //skip field 2 & 3
            if (resp_field_ptr != NULL)
            {
                if (*ptr == 'D')
                    strcpy(resp_field_ptr, "-");
                else
                    strcpy(resp_field_ptr, "");
                ptr++;
                
                // remove leading zeros
                length_without_zeros= sizeof(vhi_res_info->fs_available_balance)- 5;
                while ((*ptr == '0') && (length_without_zeros > 1))
                {
                    length_without_zeros--;
                    ptr++;
                }
                
                strncat(resp_field_ptr, ptr, length_without_zeros);
                ptr += length_without_zeros;        // non-leading zero dollar digits
                strcat(resp_field_ptr, ".");
                strncat(resp_field_ptr, ptr, 2);
                ptr += 2;                           // cents digits
            }
            else
            {
                ptr += 13;
            }
        }
		else
        {
            debug_sprintf(szDbgMsg, "Unused balance %.7s", ptr);
            APP_TRACE(szDbgMsg);
			ptr+=20; //skip all fields if not Gift Card or EBT
		}
	}

	if (strlen(amount_bal) != 12 )
		return NULL;

	/*get available balance*/
	int tmplen = strlen(amount_bal);
	tmp = (char*)calloc(tmplen+1, sizeof(char));
	strcpy(tmp, amount_bal);
	addDecimalPoint( tmp );
	strcpy(vhi_res_info->amnt_bal, tmp);

	if (tmp) {
		free(tmp);
	}
	/*end*/

	if (strlen(vhi_res_info->amnt_bal) <= 0 )
		return NULL;

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi_res_info->amnt_bal;
}


char* VHI_EMBOSSED_ACCT_NUM(_sSCA_XML_REQUEST *req_xml_info, char *pszEmbossedCardNum)
{
	char	*pszValue			= NULL;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	pszValue = VHI_ACCT_NUM(req_xml_info);

	if(pszValue != NULL) //ACCT_NUM field is present
	{
		debug_sprintf(szDbgMsg, "%s: Account Number is present, sending it as Embossed card number", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		strcpy(pszEmbossedCardNum, pszValue);
	}
	else
	{
		pszValue = VHI_TRACK_DATA(req_xml_info);
		if(pszValue != NULL) //TRACK_DATA field is present
		{
			debug_sprintf(szDbgMsg, "%s: Track Data is present, need to extract PAN from Track data", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			char* pTRACKIND = getXmlTagValue(eTRACK_IND, req_xml_info);
			if (pTRACKIND == NULL) {
				debug_sprintf(szDbgMsg, "%s: pTRACKIND is NULL, so track indicator is missing", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				pTRACKIND = "2";
			}

			/* Determining track1 or Track2 based on the
			 * track indicator field
			 */
			getPANFromTrackData(pszValue, atoi(pTRACKIND), pszEmbossedCardNum, 29); //Praveen_P1: this function is not called or used, so sending hard coded value here
		}
	}

	return pszValue;
}

char* VHI_APPROVED_AMOUNT(_sVTV_FIELDS *svbninfo, sVHI_RESPONSE_INFO *vhi_res_info, _sSCA_XML_REQUEST *req_xml_info)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* pPAYMENT_TYPE = getXmlTagValue(ePAYMENT_TYPE, req_xml_info);

	if (pPAYMENT_TYPE != NULL) {
		if (strncmp(pPAYMENT_TYPE, COMMAND.CREDIT, strlen(COMMAND.CREDIT)) == COMPARE_OK) {
			if (strlen(vhi_res_info->r007_authorize_amount) > 0) {

				debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_res_info->r007_authorize_amount);
				APP_TRACE(szDbgMsg);

				return vhi_res_info->r007_authorize_amount;
			}
			else {
				return NULL;
			}
		}
		else if (strncmp(pPAYMENT_TYPE, "DEBIT", 5) == COMPARE_OK) {
			if (strlen(vhi_res_info->r007_authorize_amount) > 0) {
				debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_res_info->r007_authorize_amount);
				APP_TRACE(szDbgMsg);

				return vhi_res_info->r007_authorize_amount;
			}
			else {
				return NULL;
			}
		}
        else if (strncmp(pPAYMENT_TYPE, "EBT", 3) == COMPARE_OK) {
            if (strlen(vhi_res_info->r007_authorize_amount) > 0) {
                debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_res_info->r007_authorize_amount);
                APP_TRACE(szDbgMsg);
                
                return vhi_res_info->r007_authorize_amount;
            }
            else {
            	/*
            	 * Praveen_P1: Vantiv is not giving approved amount in the response for the
            	 * EBT transactions. Fix for PTMX-322
            	 * if Transaction is successful, then we will put transaction amount to approved amount
            	 */
            	if(strlen(vhi_res_info->hostresponse_code) > 0 &&
            			(strcmp(vhi_res_info->hostresponse_code, "000") == COMPARE_OK))//We are filling approved amount only if host response code is 000
            	{
            		debug_sprintf(szDbgMsg, "%s: Host Response Code is 000 for EBT transaction, filling Approved amount with transaction amount", __FUNCTION__);
            		APP_TRACE(szDbgMsg);

            		char* pTRANS_AMOUNT = getXmlTagValue(eTRANS_AMOUNT, req_xml_info);

            		if(pTRANS_AMOUNT == NULL)
            		{
            			return NULL;
            		}
            		strcpy(vhi_res_info->r007_authorize_amount, pTRANS_AMOUNT);

            		return vhi_res_info->r007_authorize_amount;
            	}
            	else
            	{
            		return NULL;
            	}
            }
        }
	}

	_sVTV_FIELDS fields; //use only to know the field size
	char* tmp = NULL;
	char* ptr = getVantivBNResponse(eBN128, svbninfo);
	char auth_amnt[sizeof(vhi_res_info->auth_amnt)] = {0};
	memset(vhi_res_info->auth_amnt, 0x00, sizeof(vhi_res_info->auth_amnt));
	/*
	 * 40 - gift card
	 * 02 - available balance
	 * 840 - US
	 */

	if (ptr == NULL) {
		return NULL;
	}

	if (strlen(ptr) != sizeof(fields.bn128)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! bn128 is not 120 bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	while( strlen(ptr) > 0) {
		if (strncmp(ptr, "40", 2) == COMPARE_OK) { //40 � Gift Card
			ptr+=2; //skip field 1
			if (strncmp(ptr, "03", 2) == COMPARE_OK) { //03 - Gift Card/WIC Authorized Amount
				ptr+=2; //skip field 2
				if (strncmp(ptr, "840", 3) == COMPARE_OK) {
					ptr+=3; //skip field 3
					ptr++; //skip c,0, or D 1byte
                    strcpy(auth_amnt, "");
					strncat(auth_amnt, ptr, 12); //copy the field 4 to amounts 12 bytes
					ptr+=12; //skip field 4
				}
				else {
					debug_sprintf(szDbgMsg, "%s: ERROR! Not a US dollar", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					ptr+=16;
					return NULL;
				}
			}
			else {
				ptr+=18; //skip field 2, 3, 4 if not 02 Available balance
			}
		}
		else {
			ptr+=20; //skip all fields if not Gift Card
		}
	}

	if (strlen(auth_amnt) != 12 )
		return NULL;

	debug_sprintf(szDbgMsg, "%s: auth_amnt [%s]", __FUNCTION__, auth_amnt);
	APP_TRACE(szDbgMsg);

	/*get authorized amount*/
	int tmplen = strlen(auth_amnt);
	tmp = (char*)calloc(tmplen+1, sizeof(char));
	strcpy(tmp, auth_amnt);
	addDecimalPoint( tmp );

	strcpy(vhi_res_info->auth_amnt, tmp);

	if (tmp)
		free(tmp);
	/*end*/

	if (strlen(vhi_res_info->auth_amnt) <= 0 )
		return NULL;

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_res_info->auth_amnt);
	APP_TRACE(szDbgMsg);

	return vhi_res_info->auth_amnt;
}

char* VHI_CARDHOLDER(_sSCA_XML_REQUEST *req_xml_info)
{
	return getXmlTagValue(eCARDHOLDER, req_xml_info);
}

char* VHI_ACCT_NUM(_sSCA_XML_REQUEST *req_xml_info)
{
	return getXmlTagValue(eACCT_NUM, req_xml_info);
}

char* VHI_TRACK_DATA(_sSCA_XML_REQUEST *req_xml_info)
{
	return getXmlTagValue(eTRACK_DATA, req_xml_info);
}

char* VHI_CLIENT_ID(_sSCA_XML_REQUEST *req_xml_info)
{
	//CLIENT_ID in SSI
	return getXmlTagValue(eCLIENT_ID, req_xml_info);
}

char* VHI_COMMAND(_sSCA_XML_REQUEST *req_xml_info)
{
	return getXmlTagValue(eCOMMAND, req_xml_info);
}

char* VHI_CTROUTD(sR008_data *sR008_info)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if (strlen(sR008_info->ORRN) <= 0) {
		return NULL;
	}
	else if (strlen(sR008_info->ORRN) != sizeof(sR008_info->ORRN)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! RRN not 9 bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return sR008_info->ORRN;
}

char* VHI_SIGNATUREREF(sR011_data *sR011_info)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if (strlen(sR011_info->signatureCapRef) <= 0) {
		return NULL;
	}
	else if (strlen(sR011_info->signatureCapRef) != sizeof(sR011_info->signatureCapRef)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Signature Ref not 11 bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return sR011_info->signatureCapRef;
}

char* VHI_DEVICEKEY(_sSCA_XML_REQUEST *req_xml_info)
{
	return getXmlTagValue(eDEVICEKEY, req_xml_info);
}

char* VHI_DELIMITER(_sSCA_XML_REQUEST *req_xml_info)
{
	return getXmlTagValue(eDELIMITER, req_xml_info);
}

char* VHI_DEVTYPE(_sSCA_XML_REQUEST *req_xml_info)
{
	return getXmlTagValue(eDEVTYPE, req_xml_info);
}

char* VHI_END_TRANS_DATE(_sSCA_XML_REQUEST *req_xml_info)
{
	return getXmlTagValue(eEND_TRANS_DATE, req_xml_info);
}

char* VHI_FORMAT(_sSCA_XML_REQUEST *req_xml_info)
{
	return getXmlTagValue(eFORMAT, req_xml_info);
}

char* VHI_FUNCTION_TYPE(_sSCA_XML_REQUEST *req_xml_info)
{
	return getXmlTagValue(eFUNCTION_TYPE, req_xml_info);
}

char* VHI_INTRN_SEQ_NUM(_sVTV_FIELDS *svbninfo)
{
	return getVantivBNResponse(eBN11, svbninfo);
}

char* VHI_INVOICE(_sSCA_XML_REQUEST *req_xml_info)
{
	return getXmlTagValue(eINVOICE, req_xml_info);
}

char* VHI_LOCAL_TRANS_DATE(_sVTV_FIELDS *svbninfo, sVHI_RESPONSE_INFO *vhi_res_info) {
	//YYYY.MM.DD
	char YY[2+1] = {0};
	char MM[2+1] = {0};
	char DD[2+1] = {0};

	char* ptr = getVantivBNResponse(eBN12, svbninfo); //MMDDYY

	if (ptr != NULL) {
		memset(vhi_res_info->YYYYMMDD, 0x00, sizeof(vhi_res_info->YYYYMMDD));
		strncat(MM, ptr, 2); //copy MM
		ptr+=2; //skip MM -> DDYY
		strncat(DD, ptr, 2); //copy DD
		ptr+=2; //skip MM -> YY
		strncat(YY, ptr, 2); //copy YY

        strcpy(vhi_res_info->YYYYMMDD, "");
        strncat(vhi_res_info->YYYYMMDD, "20", 2);
		strncat(vhi_res_info->YYYYMMDD, YY, 2);
		strncat(vhi_res_info->YYYYMMDD, ".", 1);
		strncat(vhi_res_info->YYYYMMDD, MM, 2);
		strncat(vhi_res_info->YYYYMMDD, ".", 1);
		strncat(vhi_res_info->YYYYMMDD, DD, 2);

		return vhi_res_info->YYYYMMDD;
	}

	return ptr; //MMDDYY
}

char* VHI_LOCAL_TRANS_TIME(_sVTV_FIELDS *svbninfo, sVHI_RESPONSE_INFO *vhi_res_info) {
	//hh:mm:ss
	char* ptr = getVantivBNResponse(eBN13, svbninfo); //hhmmss

	if (ptr != NULL) {
		memset(vhi_res_info->hhmmss, 0x00, sizeof(vhi_res_info->hhmmss));

		strncat(vhi_res_info->hhmmss, ptr, 2); //copy hh
		ptr += 2; //skip hh -> mmss
		strncat(vhi_res_info->hhmmss, ":", 1);
		strncat(vhi_res_info->hhmmss, ptr, 2); //copy mm
		ptr += 2; //skip mm -> ss
		strncat(vhi_res_info->hhmmss, ":", 1);
		strncat(vhi_res_info->hhmmss, ptr, 2); //copy ss

		return vhi_res_info->hhmmss;
	}

	return ptr; //hhmmss
}

char* VHI_MAX_NUM_RECORDS_RETURNED(_sSCA_XML_REQUEST *req_xml_info)
{
	return getXmlTagValue(eMAX_NUM_RECORDS_RETURNED, req_xml_info);
}

char* VHI_NUM_RECORDS_FOUND()
{
	return "0";
}

char* VHI_PAYMENT_MEDIA(_sVTV_FIELDS *svbninfo, _sSCA_XML_REQUEST *req_xml_info)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * Valid Return Value
	 * VISA - Visa
	 * MC - Mastercard
	 * AMEX - American Express
	 * DISC - Discover
	 * CBLN - Carte Blance
	 * JAL - JAL
	 * JCB - JCB
	 * ENRT - Enroute
	 * DCCB - Diner's Club
	 * GIFT - Gift
	 * DEBIT - Debit
	 */

	char* pPAYMENT_TYPE = getXmlTagValue(ePAYMENT_TYPE, req_xml_info);

	//Network Mnemonic/Card Type
	char* payment_media = getVantivBNResponse(eBN120_3, svbninfo);

	if (payment_media == NULL) {
		return NULL;
	}

	if (strncmp(payment_media, "VI", 2) == COMPARE_OK) {
		return "VISA";
	}
	else if (strncmp(payment_media, "MC", 2) == COMPARE_OK) {
		return "MC";
	}
	else if (strncmp(payment_media, "AE", 2) == COMPARE_OK) {
		return "AMEX";
	}
	else if (strncmp(payment_media, "DS", 2) == COMPARE_OK) {
		return "DISC";
	}
	else if (strncmp(payment_media, "CB", 2) == COMPARE_OK) {
		return "CBLN";
	}
	else if (strncmp(payment_media, "JC", 2) == COMPARE_OK) {
		return "JCB";
	}
	else if (strncmp(payment_media, "DC", 2) == COMPARE_OK) {
		return "DCCB";
	}
	else if (strncmp(payment_media, "GC", 2) == COMPARE_OK) {
		return "GIFT";
	}
	else{
		if (strncmp(pPAYMENT_TYPE, "DEBIT", 5) == COMPARE_OK) {
			return "DEBIT";
		}
		else if ( strncmp(pPAYMENT_TYPE, "GIFT", 4) == COMPARE_OK) {
			return "GIFT";
		}

		debug_sprintf(szDbgMsg, "%s: ERROR! NOT SUPPORTED", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

char* VHI_PAYMENT_TYPE(_sSCA_XML_REQUEST *req_xml_info)
{
	return getXmlTagValue(ePAYMENT_TYPE, req_xml_info);
}

char* VHI_SERVERID(_sSCA_XML_REQUEST *req_xml_info)
{
	return getXmlTagValue(eSERVER_ID, req_xml_info);
}

char* VHI_PROCESSING_CODE(_sVTV_FIELDS *svbninfo)
{
	return getVantivBNResponse(eBN03, svbninfo);
}

char* VHI_REFERENCE(_sVTV_FIELDS *svbninfo)
{
	//Transaction Identifier Banknet/POSA SAF Ref. Num.
	return getVantivBNResponse(eBN105_3, svbninfo);
}

char* VHI_CVV2_CODE(_sVTV_FIELDS *svbninfo, char *pszResultCode)
{
	char *pszCode;
	/*
	 * Praveen_P1: 07 Dec 2015
	 * Additional Response Data/AVS Result Code
	 * The field contains the CVV2 and address verification (AVS) result codes
	 * The first character is the CVV2 result code,
	 * the second character is the AVS result code (space when the transaction does not qualify for AVS)
	 */

	pszCode = getVantivBNResponse(eBN105_1, svbninfo);

	if(pszCode != NULL && (strlen(pszCode) > 0))
	{
		if(pszCode[0] != ' ')//Space means no data available in the response from Vantiv
		{
			strncpy(pszResultCode, &pszCode[0], 1);
		}
	}

	return pszCode; //dummy return value, the value is being filled upin the parameter that is passed
}

char* VHI_AVS_CODE(_sVTV_FIELDS *svbninfo, char *pszResultCode)
{
	char *pszCode;
	/*
	 * Praveen_P1: 07 Dec 2015
	 * Additional Response Data/AVS Result Code
	 * The field contains the CVV2 and address verification (AVS) result codes
	 * The first character is the CVV2 result code,
	 * the second character is the AVS result code (space when the transaction does not qualify for AVS)
	 */

	pszCode = getVantivBNResponse(eBN105_1, svbninfo);

	if(pszCode != NULL && (strlen(pszCode) > 0))
	{
		if(pszCode[1] != ' ')//Space means no data available in the response from Vantiv
		{
			strncpy(pszResultCode, &pszCode[1], 1);
		}
	}

	return pszCode; //dummy return value, the value is being filled upin the parameter that is passed
}

char* VHI_RESPONSE_TEXT(sGLOBALS* GLOBAL, _sVTV_FIELDS *svbninfo, sVHI_RESPONSE_INFO *vhi_res_info, _sSCA_XML_REQUEST *req_xml_info)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if (GLOBAL->isProcessorAvailable == false) {
		strcpy(vhi_res_info->response_text, "PROCESSOR NOT AVAILABLE");
		return vhi_res_info->response_text;
	}
	else {
		char* pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);
		if (strncmp(pCOMMAND, "PROCESSOR", 9) == COMPARE_OK) {
			strcpy(vhi_res_info->response_text, "PROCESSOR AVAILABLE");
			return vhi_res_info->response_text;
		}
	}

	char* bit_map = getVantivBNResponse(eBNBMT, svbninfo);

	if (bit_map==NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! bit_map is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	char* error_text = getVantivBNResponse(eBN123_1, svbninfo);
	char* response_code = getVantivBNResponse(eBN123_2, svbninfo);

	memset(vhi_res_info->response_text, 0x00, sizeof(vhi_res_info->response_text));

	if ((strncmp(bit_map, "99", 2)==COMPARE_OK || strncmp(bit_map, "62", 2)==COMPARE_OK)
			&& error_text!=NULL) {
		strcpy(vhi_res_info->response_text, error_text);
		if (response_code!=NULL) {
			strncat(vhi_res_info->response_text, " ", 1); //append 1 space
			strncat(vhi_res_info->response_text, response_code, strlen(response_code));
		}

		if (strlen(vhi_res_info->response_text) <= 0 )
			return NULL;

		//Incase of error we are sending the TPS code in the host response code
		memset(vhi_res_info->hostresponse_code, 0x00, sizeof(vhi_res_info->hostresponse_code));
		strcpy(vhi_res_info->hostresponse_code, response_code);

		return vhi_res_info->response_text;
	}

	strcpy(vhi_res_info->response_text, "APPROVAL");

	memset(vhi_res_info->hostresponse_code, 0x00, sizeof(vhi_res_info->hostresponse_code));
	strcpy(vhi_res_info->hostresponse_code, "000"); //Praveen_P1: Incase of success we are sending 000 in the host response code

	if (strlen(vhi_res_info->response_text) <= 0 )
		return NULL;

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi_res_info->response_text; //temporary default
}

char* VHI_RESULT(sGLOBALS* GLOBAL, _sVTV_FIELDS *svbninfo, sR008_data *sR008_info, sVHI_RESPONSE_INFO *vhi_res_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info)
{
	char *pszRespTxt;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * Valid Return Values
	 * SUCCESS
	 * UNKNOWN
	 * CAPTURED
	 * APPROVED
	 * DECLINED
	 * VOIDED
	 * DEFERRED
	 * COMPLETED
	 * PARTCOMP
	 * TIP MODIFIED
	 * ERROR
	 * SETTLED
	 */
	memset(vhi_res_info->result, 0x00, sizeof(vhi_res_info->result));
	memset(vhi_res_info->result_code, 0x00, sizeof(vhi_res_info->result_code));
	memset(vhi_res_info->termination_stat, 0x00, sizeof(vhi_res_info->termination_stat));

	if (GLOBAL->isProcessorAvailable == false) {
		strcpy(vhi_res_info->result, "FAILURE");
		strcpy(vhi_res_info->result_code, "24"); //24 for the host not available
		strcpy(vhi_res_info->termination_stat, "SUCCESS");

		return vhi_res_info->result;
	}
	else {
		char* pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);

		if (strncmp(pCOMMAND, "PROCESSOR", 9) == COMPARE_OK) {
			strcpy(vhi_res_info->result, "SUCCESS");
			strcpy(vhi_res_info->result_code, "-1");
			strcpy(vhi_res_info->termination_stat, "SUCCESS");

			return vhi_res_info->result;
		}
	}

	char* msg_type = getVantivBNResponse(eBN00, svbninfo);

	if (msg_type == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! msg_type_id is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(vhi_res_info->result, "ERROR");
		strcpy(vhi_res_info->result_code, "0");
		strcpy(vhi_res_info->termination_stat, "UNKNOWN");

		return vhi_res_info->result;
	}

	char* bit_map = getVantivBNResponse(eBNBMT, svbninfo);
	if (bit_map == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! bit_map_type is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(vhi_res_info->result, "ERROR");
		strcpy(vhi_res_info->result_code, "0");
		strcpy(vhi_res_info->termination_stat, "UNKNOWN");

		return vhi_res_info->result;
	}

	/* *****************
	 * AUTHORIZATION
	 */
	if (strncmp(msg_type, "0110", 4) == COMPARE_OK)
    {
		if (strncmp(bit_map, "90", 2) == COMPARE_OK || strncmp(bit_map, "53", 2) == COMPARE_OK || strncmp(bit_map, "61", 2) == COMPARE_OK)
        {
			strcpy(vhi_res_info->result, "APPROVED");
			strcpy(vhi_res_info->result_code, "5");
			strcpy(vhi_res_info->termination_stat, "SUCCESS");

			/*if new batch*/
			char* bn120_1 = getVantivBNResponse(eBN120_1, svbninfo);
			if (isNewBatch(bn120_1) == true) {
				removeTextFile(TEXTFILE_VOIDS);
				removeTextFile(TEXTFILE_TIPADJUST);
			}
			/*end*/

            //debug_sprintf(szDbgMsg, "AUTH save void data");
            //APP_TRACE(szDbgMsg);
			//saveVoidData(sR008_info->ORRN, GLOBAL, svbninfo, req_xml_info);
		}
		else if (strncmp(bit_map, "99", 2)==COMPARE_OK || strncmp(bit_map, "62", 2)==COMPARE_OK)
        {
			strcpy(vhi_res_info->result, "DECLINED");
			strcpy(vhi_res_info->result_code, "6");
			strcpy(vhi_res_info->termination_stat, "SUCCESS");
		}
		else
        {
			strcpy(vhi_res_info->result, "UNKNOWN");
			strcpy(vhi_res_info->result_code, "0");
			strcpy(vhi_res_info->termination_stat, "SUCCESS");
			debug_sprintf(szDbgMsg, "%s: ERROR! bit_map not supported by VHI", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	/* *****************
	 * FINANCIAL
	 */
	else if (strncmp(msg_type, "0210", 4) == COMPARE_OK
	|| strncmp(msg_type, "0230", 4) == COMPARE_OK)
	{
		if (strncmp(bit_map, "91", 2) == COMPARE_OK
		|| strncmp(bit_map, "61", 2) == COMPARE_OK
		|| strncmp(bit_map, "63", 2) == COMPARE_OK)
		{
			strcpy(vhi_res_info->result, "CAPTURED");
			strcpy(vhi_res_info->result_code, "4");
			strcpy(vhi_res_info->termination_stat, "SUCCESS");

			/*if new batch*/
			char* bn120_1 = getVantivBNResponse(eBN120_1, svbninfo);
			if (isNewBatch(bn120_1) == true) {
				removeTextFile(TEXTFILE_VOIDS);
				removeTextFile(TEXTFILE_TIPADJUST);
			}
			/*end*/

            //debug_sprintf(szDbgMsg, "FIN save void data");
            //APP_TRACE(szDbgMsg);
			//saveVoidData(VHI_CTROUTD(sR008_info), GLOBAL, svbninfo, req_xml_info);
			saveAdjustmentData(VHI_CTROUTD(sR008_info), VHI_APPROVED_AMOUNT(svbninfo, vhi_res_info, req_xml_info), GLOBAL, GCONFIG, req_xml_info);
		}
		else if (strncmp(bit_map, "99", 2) == COMPARE_OK
		|| strncmp(bit_map, "62", 2) == COMPARE_OK)
		{
			//Just checking if it is for VSP registration
			pszRespTxt = VHI_RESPONSE_TEXT(GLOBAL, svbninfo, vhi_res_info, req_xml_info);

			debug_sprintf(szDbgMsg, "%s: Response Text [%s]", __FUNCTION__, pszRespTxt);
			APP_TRACE(szDbgMsg);

			if((strncmp(bit_map, "99", 2) == COMPARE_OK)
			&& (strstr(pszRespTxt, "418") != NULL)) //Checking if response_text contains 418
			{
				/*
				 * Praveen_P1: For Successful VSP registration, Vantiv is returning
				 * 99 as the bit map which is error for other transactions
				 * Thats why this work around
				 * TO_DO: Please make it better
				 */
				debug_sprintf(szDbgMsg, "%s: VSP Registration Case, its success", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				strcpy(vhi_res_info->result, "CAPTURED");
				strcpy(vhi_res_info->result_code, "905");
				strcpy(vhi_res_info->termination_stat, "SUCCESS");
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Non-VSP Registration Case, its error", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if((strstr(pszRespTxt, "406") != NULL))
				{
					debug_sprintf(szDbgMsg, "%s: Response text contains 406 as error, it is E2EE error!!!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					strcpy(vhi_res_info->vsp_code, "327");
					sprintf(vhi_res_info->vsp_result_desc, "%s-%s", pszRespTxt, "VCL Registration Required");
				}
				/*
				 * Praveen_P1: As per the FRD 3.55, we have to consider some of TPS error codes
				 * as host not available case...so checking for them here
				 */
				if(checkTPSCodeinSAFMapList(vhi_res_info->hostresponse_code, GCONFIG) == 1)
				{
					debug_sprintf(szDbgMsg, "%s: TPS Code is present in the Mapping list, considering this as host not available case", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					strcpy(vhi_res_info->result, "FAILURE");
					strcpy(vhi_res_info->result_code, "24"); //24 for the host not available
					strcpy(vhi_res_info->termination_stat, "SUCCESS");
				}
				else
				{
					strcpy(vhi_res_info->result, "DECLINED");
					strcpy(vhi_res_info->result_code, "6");
					strcpy(vhi_res_info->termination_stat, "SUCCESS");
				}
			}
		}
		else {
			strcpy(vhi_res_info->result, "UNKNOWN");
			strcpy(vhi_res_info->result_code, "0");
			strcpy(vhi_res_info->termination_stat, "SUCCESS");
			debug_sprintf(szDbgMsg, "%s: ERROR! bit_map not supported by VHI", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	/* *****************
	 * REVERSAL
	 */
	else if (strncmp(msg_type, "0410", 4) == COMPARE_OK) {
		if (strncmp(bit_map, "91", 2) == COMPARE_OK
		|| strncmp(bit_map, "61", 2) == COMPARE_OK
		|| strncmp(bit_map, "63", 2) == COMPARE_OK)
		{
			char* pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);
			if(pCOMMAND != NULL)
			{
				/*
				 * For TIME OUT REVERSED transactions,
				 */
				if (strncmp(pCOMMAND, COMMAND.SALE, strlen(COMMAND.SALE)) == COMPARE_OK)
				{
					strcpy(vhi_res_info->result_code, "22"); //22 is for reversed
					strcpy(vhi_res_info->result, "REVERSED");
				}
				else
				{
					strcpy(vhi_res_info->result_code, "7");
					strcpy(vhi_res_info->result, "VOIDED");
				}
			}
			else
			{
				strcpy(vhi_res_info->result_code, "7");
				strcpy(vhi_res_info->result, "VOIDED");
			}
			strcpy(vhi_res_info->termination_stat, "SUCCESS");

			/*if new batch*/
			char* bn120_1 = getVantivBNResponse(eBN120_1, svbninfo);
			if (isNewBatch(bn120_1) == true) {
				removeTextFile(TEXTFILE_VOIDS);
				removeTextFile(TEXTFILE_TIPADJUST);
			}
			/*end*/

            //debug_sprintf(szDbgMsg, "REVERSE save void data");
            //APP_TRACE(szDbgMsg);
			//saveVoidData(sR008_info->ORRN, GLOBAL, svbninfo, req_xml_info);
		}
		else if (strncmp(bit_map, "99", 2) == COMPARE_OK
		|| strncmp(bit_map, "62", 2) == COMPARE_OK)
		{
			strcpy(vhi_res_info->result, "DECLINED");
			strcpy(vhi_res_info->result_code, "6");
			strcpy(vhi_res_info->termination_stat, "SUCCESS");
		}
		else {
			strcpy(vhi_res_info->result, "UNKNOWN");
			strcpy(vhi_res_info->result_code, "0");
			strcpy(vhi_res_info->termination_stat, "SUCCESS");
			debug_sprintf(szDbgMsg, "%s: ERROR! bit_map not supported by VHI", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
	/* *****************
	 * RECONCILIATION
	 */
	else if (strncmp(msg_type, "0510", 4) == COMPARE_OK) {
		if (strncmp(bit_map, "92", 2) == COMPARE_OK) {
			strcpy(vhi_res_info->result, "SETTLED");
			strcpy(vhi_res_info->result_code, "2");
			strcpy(vhi_res_info->termination_stat, "SUCCESS");
		}
		else if (strncmp(bit_map, "99", 2) == COMPARE_OK) {
			strcpy(vhi_res_info->result, "DECLINED");
			strcpy(vhi_res_info->result_code, "6");
			strcpy(vhi_res_info->termination_stat, "SUCCESS");
		}
		else {
			strcpy(vhi_res_info->result, "UNKNOWN");
			strcpy(vhi_res_info->result_code, "0");
			strcpy(vhi_res_info->termination_stat, "SUCCESS");
			debug_sprintf(szDbgMsg, "%s: ERROR! bit_map not supported by VHI", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
	}
    /* *****************
     * NETWORK ERROR
     */
    else if (strncmp(msg_type, "0810", 4) == COMPARE_OK)
    {
        if (strncmp(bit_map, "99", 2) == COMPARE_OK) {
            strcpy(vhi_res_info->result, "ERROR");
            strcpy(vhi_res_info->result_code, "0");
            strcpy(vhi_res_info->termination_stat, "SUCCESS");
        }
        else {
            strcpy(vhi_res_info->result, "UNKNOWN");
            strcpy(vhi_res_info->result_code, "0");
            strcpy(vhi_res_info->termination_stat, "SUCCESS");
            debug_sprintf(szDbgMsg, "%s: ERROR! bit_map not supported by VHI", __FUNCTION__);
            APP_TRACE(szDbgMsg);
        }
        
    }
	/* *****************
	 * NOT SUPPORTED
	 */
	else {
		strcpy(vhi_res_info->result, "UNKNOWN");
		strcpy(vhi_res_info->result_code, "0");
		strcpy(vhi_res_info->termination_stat, "SUCCESS");
		debug_sprintf(szDbgMsg, "%s: ERROR! Message type not supported by VHI", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	if (strlen(vhi_res_info->result) <= 0 )
		return NULL;

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi_res_info->result;
}

char* VHI_RESULT_CODE(sVHI_RESPONSE_INFO *vhi_res_info)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

//	return NULL;
	/*
	 * Valid Return Values:
	 * -2 - XML Format Incorrect
	 * -1 - SUCCESS
	 * 0 - UNKNOWN
	 * 2 - SETTLED
	 * 4 - CAPTURED
	 * 5 - APPROVED
	 * 6 - DECLINED
	 * 7 - VOIDED
	 * 10 - COMPLETED
	 * 93 - Invalid Card Number
	 * 97 - Invalid Expiration Date
	 * 1010 - Invalid Amount
	 * 3100 - Incorrect USER_ID or USER_PW
	 * 3705 - Invalid Reference.
	 * 3745 -
	 * 2029999 - Incorrect CLIENT_ID, Merchant Key or Command not authorized
	 * 30000 - Backend Payment Engine is not accessible
	 */

	/*
	 * VHI_RESULT() should be first executed
	 * this is dependent in RESULT
	 */

	if (strlen(vhi_res_info->result_code) <= 0) {
		debug_sprintf(szDbgMsg, "%s: ERROR! RESULT_CODE is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi_res_info->result_code;
}

char* VHI_RETURN_FLD_HDRS(_sSCA_XML_REQUEST *req_xml_info)
{
	return getXmlTagValue(eRETURN_FLD_HDRS, req_xml_info);
}

char* VHI_SERIAL_NUM(_sSCA_XML_REQUEST *req_xml_info)
{
	return getXmlTagValue(eSERIAL_NUM, req_xml_info);
}

char* VHI_START_TRANS_DATE(_sSCA_XML_REQUEST *req_xml_info)
{
	return getXmlTagValue(eSTART_TRANS_DATE, req_xml_info);
}

char* VHI_TERMINATION_STATUS(sVHI_RESPONSE_INFO *vhi_res_info)
{
	/*
	 * Valid Return Values
	 * SUCCESS
	 * NOT_PROCESSED
	 * NOT_PROCESSED_DB_FAILURE
	 * NOT_PROCESSED_PARAMETER_ERROR
	 * NOT_PROCESSED_COMM_FAILURE
	 * NOT_PROCESSED_ENGINE_NOT_ACCESSIBLE
	 * INDETERMINATE_STATUS
	 * PROCESSED_DB_FAILURE
	 * UNKNOWN
	 * Note: check RESULT or RESULT_CODE value
	 */

	/*
	 * VHI_RESULT() should be first executed
	 * this is dependent in RESULT and RESULT CODE
	 */

	if (strlen(vhi_res_info->termination_stat) <= 0 )
		return NULL;

	return vhi_res_info->termination_stat;
}

char* VHI_HOST_RESP_CODE(sVHI_RESPONSE_INFO *vhi_res_info)
{
	/*
	 * VHI_RESPONSE_TEXT() should be first executed
	 * before calling this function, this value is getting
	 * filled in VHI_RESPONSE_TEXT function
	 */

	if (strlen(vhi_res_info->hostresponse_code) <= 0 )
		return NULL;

	return vhi_res_info->hostresponse_code;
}

char* VHI_VSP_CODE(sVHI_RESPONSE_INFO *vhi_res_info)
{
	/*
	 * VHI_RESULT() should be first executed
	 * this is dependent in RESULT and RESULT CODE
	 */

	if (strlen(vhi_res_info->vsp_code) <= 0 )
		return NULL;

	return vhi_res_info->vsp_code;
}

char* VHI_VSP_RESULT_DESC(sVHI_RESPONSE_INFO *vhi_res_info)
{
	/*
	 * VHI_RESULT() should be first executed
	 * this is dependent in RESULT and RESULT CODE
	 */

	if (strlen(vhi_res_info->vsp_result_desc) <= 0 )
		return NULL;

	return vhi_res_info->vsp_result_desc;
}

char* VHI_TRANS_AMOUNT(_sSCA_XML_REQUEST *req_xml_info)
{
	return getXmlTagValue(eTRANS_AMOUNT, req_xml_info);
}

char* VHI_TRANS_DATE(sVHI_RESPONSE_INFO *vhi_res_info)
{
	char date_time[11+1] = "";

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//YYYY.MM.DD
	//system local time
	memset(vhi_res_info->trans_date, 0x00, sizeof(vhi_res_info->trans_date));
	memset(date_time, 0x00, sizeof(date_time));

    strcpy(vhi_res_info->trans_date, "");
    strncat(vhi_res_info->trans_date, getDateTime(eYYYY_MM_DD, date_time), sizeof(vhi_res_info->trans_date)-1);

	if (strlen(vhi_res_info->trans_date) != sizeof(vhi_res_info->trans_date)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi_res_info->trans_date;
}

char* VHI_TRANS_SEQ_NUM(_sVTV_FIELDS *svbninfo)
{
	return getVantivBNResponse(eBN11, svbninfo);
}

char* VHI_TRANS_TIME(sVHI_RESPONSE_INFO *vhi_res_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	char date_time[11+1] = "";
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//HH:MM:SS
	//system current time
	memset(vhi_res_info->trans_time, 0x00, sizeof(vhi_res_info->trans_time));
	memset(date_time, 0x00, sizeof(date_time));
    strcpy(vhi_res_info->trans_time, "");

    /*
     * Praveen_P1: Fix for Amdocs Case# 160602-18908
     * Issue- There was some disturbance between the time which we sent in field13 in Vantiv
     * request compared to TRANS_TIME field value
     * At both places, we are getting current system time at the time and sending it.
     * Fix is to send same value which we sent in 13 field in the request to Vantiv
     * Since we have to send with : sepearted in TRANS_TIME, manually adding those colons
     */

    if(vhi_fieldinfo != NULL && strlen(vhi_fieldinfo->bn13) > 0)
    {
    	debug_sprintf(szDbgMsg, "%s: vhi_fieldinfo->bn13 is %s", __FUNCTION__, vhi_fieldinfo->bn13);
    	APP_TRACE(szDbgMsg);

    	strncat(date_time, vhi_fieldinfo->bn13, 2);
		strncat(date_time, ":", 1);

		strncat(date_time, vhi_fieldinfo->bn13+2, 2);
		strncat(date_time, ":", 1);

		strncat(date_time, vhi_fieldinfo->bn13+4, 2);

		debug_sprintf(szDbgMsg, "%s: date_time is %s", __FUNCTION__, date_time);
		APP_TRACE(szDbgMsg);

		strncat(vhi_res_info->trans_time, date_time, sizeof(vhi_res_info->trans_time)-1);
    }
    else
    {
    	strncat(vhi_res_info->trans_time, getDateTime(eHH_MM_SS, date_time), sizeof(vhi_res_info->trans_time)-1);
    }


	if (strlen(vhi_res_info->trans_time) != sizeof(vhi_res_info->trans_time)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi_res_info->trans_time;
}

char* VHI_TROUTD(_sVTV_FIELDS *svbninfo)
{
	return getVantivBNResponse(eBN11, svbninfo);
}

char* VHI_TRACE_CODE(_sVTV_FIELDS *svbninfo)
{
	//Transaction Identifier Banknet/POSA SAF Ref. Num.
	return getVantivBNResponse(eBN105_3, svbninfo);
}

char* VHI_TRANSACTION_CODE()
{
	return NULL;
}

char* VHI_CARD_TOKEN(sR017_data *sR017_info)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

//	if (strlen(sR017_info->token) <= 0 || strlen(sR017_info->token_id) <= 0) {
	if (strlen(sR017_info->token) <= 0 && strlen(sR017_info->token_id) <= 0) {
		return NULL;
	}
#ifdef DEVDEBUG
	debug_sprintf(szDbgMsg, "%s: Card Token[%s], Token ID[%s]", __FUNCTION__, sR017_info->token, sR017_info->token_id);
	APP_TRACE(szDbgMsg);
#endif
	if(strspn(sR017_info->token, " ") == strlen(sR017_info->token)) //Checking if token contains only spaces
	{
		debug_sprintf(szDbgMsg, "%s: Card Token contains only spaces, so token query was unsuccessful", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	memset(sR017_info->card_token, 0x00, sizeof(sR017_info->card_token));
	strcpy(sR017_info->card_token, sR017_info->token);

	if(strlen(sR017_info->token_id) > 0)
	{
		debug_sprintf(szDbgMsg, "%s: Token ID is present, adding it to the CARD_TOKEN", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		strncat(sR017_info->card_token, "|", 1);
		strcat(sR017_info->card_token, sR017_info->token_id);
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return sR017_info->card_token;
}

void parseR007(sVANTIV_RESPONSE * vrinfo, sVHI_RESPONSE_INFO *vhi_res_info)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* ptr = getR007(vrinfo);

	memset(vhi_res_info->r007_available_balance, 0x00, sizeof(vhi_res_info->r007_available_balance));
	memset(vhi_res_info->r007_authorize_amount, 0x00, sizeof(vhi_res_info->r007_authorize_amount));

	if (ptr == NULL)
		return; //if ptr is NULL just return

	ptr += 4; //skip R007 chars

	if (strlen(ptr) < 20 || strlen(ptr) > 80) { //if not 20-80 chars there must be error in response
		debug_sprintf(szDbgMsg, "%s: ERROR! r007 must be 20-80 chars only", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

	while (strlen(ptr) > 0) {
		ptr+=2; //skip field 1 2bytes
		if (strncmp(ptr, "02", 2) == COMPARE_OK) { //Available Balance
			ptr+=2; //skip field 2 2bytes
			if (strncmp(ptr,"840",3) == COMPARE_OK) {
				ptr+=3; //skip field 3 3bytes
				ptr+=1; //skip C
                strcpy(vhi_res_info->r007_available_balance, "");
				strncat(vhi_res_info->r007_available_balance, ptr, 12); //copy field 4 to available amount
				char* tmp = NULL;
				tmp = (char*)calloc(12+1, sizeof(char));
                strcpy( tmp, "");
                strncat( tmp, vhi_res_info->r007_available_balance, 12);
				addDecimalPoint(tmp);
				memset(vhi_res_info->r007_available_balance, 0x00, sizeof(vhi_res_info->r007_available_balance));
                strcpy(vhi_res_info->r007_available_balance, "");
                strncat(vhi_res_info->r007_available_balance, tmp, 12);
				if (tmp) free(tmp);
			}
			else {
				ptr+=4;
				debug_sprintf(szDbgMsg, "%s: ERROR! Not 840=US", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			ptr+=12;
		}
		else if (strncmp(ptr, "03", 2) == COMPARE_OK) { //Authorized Amount
			ptr+=2; //skip field 2 2bytes
			if (strncmp(ptr,"840",3) == COMPARE_OK) {
				ptr+=3; //skip field 3 3bytes
				ptr+=1; //skip C
                strcpy(vhi_res_info->r007_authorize_amount, "");
				strncat(vhi_res_info->r007_authorize_amount, ptr, 12); //copy field 4 to authorized amount
				char* tmp = NULL;
				tmp = (char*)calloc(12+1, sizeof(char));
                strcpy( tmp, "");
                strncat( tmp, vhi_res_info->r007_authorize_amount, 12);
				addDecimalPoint(tmp);
				memset(vhi_res_info->r007_authorize_amount, 0x00, sizeof(vhi_res_info->r007_authorize_amount));
                strcpy(vhi_res_info->r007_authorize_amount, "");
                strncat(vhi_res_info->r007_authorize_amount, tmp, 12);
				if (tmp) free(tmp);
			}
			else {
				ptr+=4;
				debug_sprintf(szDbgMsg, "%s: ERROR! Not 840=US", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			ptr+=12;
		}
		else {
			ptr+= 18; //skip field 2, 3, 4 18bytes
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

void parseR008(sVANTIV_RESPONSE * vrinfo, sR008_data *sR008_info)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//Original Authorization Retrieval Reference Number
	memset(sR008_info->ORRN, 0x00, sizeof(sR008_info->ORRN));

	char* ptr = getR008(vrinfo);

	if (ptr == NULL)
		return;

	ptr+=4; //skip R008 chars

	if (strlen(ptr) > (sizeof(sR008_info->ORRN)-1)) {
		debug_sprintf(szDbgMsg, "%s: ERROR! R008 over 9 bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return;
	}
	else if (strlen(ptr) != (sizeof(sR008_info->ORRN)-1)) {
		debug_sprintf(szDbgMsg, "%s: ERROR! R008 is not 9 bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

    strcpy(sR008_info->ORRN, "");
    strncat(sR008_info->ORRN, ptr, sizeof(sR008_info->ORRN)-1);
}

void parseR011(sVANTIV_RESPONSE * vrinfo, sR011_data *sR011_info)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//Original Authorization Retrieval Reference Number
	memset(sR011_info->signatureCapRef, 0x00, sizeof(sR011_info->signatureCapRef));

	char* ptr = getR011(vrinfo);

	if (ptr == NULL)
		return;

	ptr+=4; //skip R011 chars

	if (strlen(ptr) > (sizeof(sR011_info->signatureCapRef)-1)) {
		debug_sprintf(szDbgMsg, "%s: ERROR! R011 over 11 bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return;
	}
	else if (strlen(ptr) != (sizeof(sR011_info->signatureCapRef)-1)) {
		debug_sprintf(szDbgMsg, "%s: ERROR! R011 is not 11 bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return;
	}

    strcpy(sR011_info->signatureCapRef, "");
    strncat(sR011_info->signatureCapRef, ptr, sizeof(sR011_info->signatureCapRef)-1);

    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);
}

void parseR017(sVANTIV_RESPONSE * vrinfo, sR017_data *sR017_info, sCONFIG * GCONFIG)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* r017 = getR017(vrinfo);

	if (r017 == NULL)
		return;



	char clen[3] = {0};
	char pcount[2] = {0};
	int ilen = 0;
	int count = 0;

#ifdef DEVDEBUG
	debug_sprintf(szDbgMsg, "%s: r017 [%s]", __FUNCTION__, r017);
	APP_TRACE(szDbgMsg);
#endif

	r017 += 4; //skip R017

	if (strlen(r017) < 5 || strlen(r017) > 68) {
		debug_sprintf(szDbgMsg, "%s: ERROR! R017 is not 8-68 in bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return;
	}

	strncat(pcount, r017, 1);
	r017++; //skip field 1

	count = atoi(pcount); //number of entries

	while(1) {
		if (*r017 == '1') { //clear acct no. E2EE & Tok'n/De-Tok'n
			memset(clen, 0, sizeof(clen));
			r017++; //go to field 3
			strncat(clen, r017, 2); //copy field 3 (field length)
			r017 += 2; //go to field 4 (value)
			ilen = atoi(clen); //field length
            strcpy(sR017_info->clearacc, "");
			strncat(sR017_info->clearacc, r017, ilen); //copy the clear account number
			r017 += ilen; //go to next field type
		}
		else if (*r017 == '2') { //masked acct no. Tok'n/De-Tok'n
			memset(clen, 0, sizeof(clen));
			r017++; //go to field 3
			strncat(clen, r017, 2); //copy field 3 (field length)
			r017 += 2; //go to field 4 (value)
			ilen = atoi(clen); //field length
            strcpy(sR017_info->maskacc, "");
			strncat(sR017_info->maskacc, r017, ilen); //copy the masked account number
			r017 += ilen; //go to next field type
		}
		else if (*r017 == '3') { //truncated, last 4 digits of acct no.
			memset(clen, 0, sizeof(clen));
			r017++; //go to field 3
			strncat(clen, r017, 2); //copy field 3 (field length)
			r017 += 2; //go to field 4 (value)
			ilen = atoi(clen); //field length
            strcpy(sR017_info->truncated, "");
			strncat(sR017_info->truncated, r017, ilen); //copy truncated
			r017 += ilen; //go to next field type
		}
		else if (*r017 == '4') { //token/de-token result
			memset(clen, 0, sizeof(clen));
			r017++; //go to field 3
			strncat(clen, r017, 2); //copy field 3 (field length)
			r017 += 2; //go to field 4 (value)
			ilen = atoi(clen); //field length
            strcpy(sR017_info->result, "");
			strncat(sR017_info->result, r017, ilen); //copy result
			r017 += ilen; //go to next field type
		}
		else if (*r017 == '5') { //token
			memset(clen, 0, sizeof(clen));
			r017++; //go to field 3
			strncat(clen, r017, 2); //copy field 3 (field length)
			r017 += 2; //go to field 4 (value)
			ilen = atoi(clen); //field length
            strcpy(sR017_info->token, "");
			strncat(sR017_info->token, r017, ilen); //copy token
			r017 += ilen; //go to next field type
		}
		else if (*r017 == '6') { //token ID
			memset(clen, 0, sizeof(clen));
			r017++; //go to field 3
			strncat(clen, r017, 2); //copy field 3 (field length)
			r017 += 2; //go to field 4 (value)
			ilen = atoi(clen); //field length
            strcpy(sR017_info->token_id, "");
			strncat(sR017_info->token_id, r017, ilen); //copy token id
			r017 += ilen; //go to next field type
		}
		else {
			break;
		}
	}

	if (GCONFIG->eTokenMode == eTOKEN_TRT) {
		debug_sprintf(szDbgMsg, "%s: CARD_TOKEN Token+Token ID[%s%s]", __FUNCTION__, sR017_info->token, sR017_info->token_id);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

/***
 R029 - Synchrony Promo Code

 EThis group contains the Base64 response data returned by Synchrony for Promo code transactions.
 Will be sent if G045 was sent

 Since the 610 message formats do not allow for the presence of binary data,
 the POS device must convert the ASCII Base64 data back to binary tag data for TLV decoding.

 All tags are optional.

 Tags are stored as binary in sR029_info.
 Data in sR029_info may not be binary, it in the format specified by the EMV tag.

 ***/

void parseR029(sVANTIV_RESPONSE * vrinfo, sR029_data *sR029_info)
{
    char    *ptr;

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);

    memset(sR029_info, 0x00, sizeof(*sR029_info));

    ptr = getR029(vrinfo);

    if (ptr == NULL)
        return;

    debug_sprintf(szDbgMsg, "Length from host %d", strlen(ptr+ 4));
    APP_TRACE(szDbgMsg);

    debug_sprintf(szDbgMsg, "Base64 encoded data is [%s]", (ptr+ 4));
    APP_TRACE(szDbgMsg);

#ifdef LOGGING_ENABLED
    int i;
    for (i=0; i<strlen(ptr+ 4); i += 8)
    {
        debug_sprintf(szDbgMsg, "Host-b64 %04X: %02x, %02x, %02x, %02x, %02x, %02x, %02x, %02x", i, *(ptr+i+4), *(ptr+i+5), *(ptr+i+6), *(ptr+i+7), *(ptr+i+8), *(ptr+i+9), *(ptr+i+10), *(ptr+i+11));
        APP_TRACE(szDbgMsg);
    }
#endif

    b64_decode((UINT8*)(ptr+ 4), sR029_info->tlv_data, (int)strlen(ptr+ 4), &sR029_info->tlv_length);    // skip over "R029"

    debug_sprintf(szDbgMsg, "Length decoded %d", sR029_info->tlv_length);
    APP_TRACE(szDbgMsg);

    debug_sprintf(szDbgMsg, "Decoded Data [%s]", sR029_info->tlv_data);
    APP_TRACE(szDbgMsg);

#ifdef LOGGING_ENABLED
    for (i=0; i<sR029_info->tlv_length; i += 8)
    {
        debug_sprintf(szDbgMsg, "Host-postb64 %04X: %02x, %02x, %02x, %02x, %02x, %02x, %02x, %02x", i, sR029_info->tlv_data[i+0], sR029_info->tlv_data[i+1], sR029_info->tlv_data[i+2], sR029_info->tlv_data[i+3], sR029_info->tlv_data[i+4], sR029_info->tlv_data[i+5], sR029_info->tlv_data[i+6], sR029_info->tlv_data[i+7]);
        APP_TRACE(szDbgMsg);
    }
#endif
}

void composeR029Response(xmlNodePtr root_node, sR029_data *sR029_info)
{
    char 	*buffer;
    int 	iBuffSize	  = 0;
    int 	iLengthParsed = 0;
    unsigned int 			tag;
    char 					szTag[7]     		= "";
    char					*startTag			= NULL;
    char 					*ptrTagValue		= NULL;
    char					*pszTagValue		= NULL;
    int						numBytesinCurrTAG	= 0;
    unsigned short  		Temp;
    unsigned short 			len;

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);

    if(strlen((char *)sR029_info->tlv_data) == 0)
    {
    	debug_sprintf(szDbgMsg, "%s: No Data, returning", __FUNCTION__);
    	APP_TRACE(szDbgMsg);
    	return;
    }

    buffer = (char *)sR029_info->tlv_data;

    iBuffSize = sR029_info->tlv_length;

    debug_sprintf(szDbgMsg, "%s: Buffer Size %d", __FUNCTION__, iBuffSize);
    APP_TRACE(szDbgMsg);

    if(iBuffSize > 0)
    {
    	startTag = buffer;
    	while(iLengthParsed < iBuffSize)
    	{

			/*Get first byte of the Tag Name. If first byte is of format xxx11111,
			 * then tag name contains more than one byte.
			 */
			tag = 0;
			tag = *(startTag++);
			iLengthParsed++;
			numBytesinCurrTAG = 1;

			if( (tag & 0x1F) == 0x1F )
			{
				do {
					tag = tag << 8;
					tag = tag | *(startTag++);
					iLengthParsed++;
					numBytesinCurrTAG++;
				} while((tag & 0x80) == 0x80);
				//last byte in Tag name should be of 0xxxxxxx format, for a multi-byte tag name.
			}

			memset(szTag, 0x00, sizeof(szTag));
			sprintf(szTag, "%X", tag);

			if(numBytesinCurrTAG > 3)
			{
				debug_sprintf(szDbgMsg, "%s: ERROR: Tag More than three bytes", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}

			//Getting Length
			/*If Length field is of format 0xxxxxxx, len is single byte, which represents len from 0 to 127
			 *If Length field is of format 1xxxxxxx, len is multi byte field.
			 *1st byte in multi-byte length field represents the number of bytes in length field.
			 */
			len= *startTag++;
			iLengthParsed += 1;
			if (len & 0x80 )
			{
				Temp= len & 0x7f;
				len= 0;
				while (Temp)
				{
					len *= 256;
					len += *startTag++;
					iLengthParsed += 1;
					Temp--; // Temp should decremented for exiting from the closed loop.
				}
			}

			//Allocating memory using malloc as we are calling this function again as recursive when we got E2 Tag
			if(ptrTagValue == NULL)
			{
				ptrTagValue = (char *) malloc (sizeof(char)* (len + 1));
				if( ptrTagValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s:memory allocation failure", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					break;
				}
				memset(ptrTagValue, 0x00, (sizeof(char)* (len + 1)));
			}

			if(pszTagValue == NULL)
			{
				pszTagValue = (char *) malloc (sizeof(char)* ((len * 2) + 1));
				if( pszTagValue == NULL)
				{
					debug_sprintf(szDbgMsg, "%s:memory allocation failure", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					break;
				}
				memset(pszTagValue, 0x00, (sizeof(char)* ((len * 2) + 1)));
			}

			// store the value
			memcpy(ptrTagValue, startTag, len);
			iLengthParsed += len;
			startTag += len;

			hex2char((UINT8*)pszTagValue, (UINT8*)ptrTagValue, len);

			//Convert tag(Hex value) to ASCII string[for e.g., "0" is stores as "30" on szTag]
			memset(szTag, 0x00, sizeof(szTag));
			sprintf(szTag, "%X", tag);


			if(strcmp(szTag, "70") == 0)
			{
				debug_sprintf(szDbgMsg, "%s: Tag [%s] Length [%d] Value [%s]", __FUNCTION__, szTag, len, pszTagValue);
				APP_TRACE(szDbgMsg);

				xmlNewChild(root_node, NULL, BAD_CAST "PROMO_NEEDED", BAD_CAST pszTagValue); //Tag 70
			}
			else if(strcmp(szTag, "72") == 0)
			{
				debug_sprintf(szDbgMsg, "%s: Tag [%s] Length [%d] Value [%s]", __FUNCTION__, szTag, len, pszTagValue);
				APP_TRACE(szDbgMsg);

				xmlNewChild(root_node, NULL, BAD_CAST "PROMO_APR_FLAG", BAD_CAST pszTagValue); //Tag 72
			}
			else if(strcmp(szTag, "73") == 0)
			{
				debug_sprintf(szDbgMsg, "%s: Tag [%s] Length [%d] Value [%s]", __FUNCTION__, szTag, len, pszTagValue);
				APP_TRACE(szDbgMsg);

				xmlNewChild(root_node, NULL, BAD_CAST "AFTER_PROMO_FLAG", BAD_CAST pszTagValue); //Tag 73
			}
			else if(strcmp(szTag, "7F02") == 0)
			{
				debug_sprintf(szDbgMsg, "%s: Tag [%s] Length [%d] Value [%s]", __FUNCTION__, szTag, len, pszTagValue);
				APP_TRACE(szDbgMsg);

				xmlNewChild(root_node, NULL, BAD_CAST "DURING_PROMO_APR", BAD_CAST pszTagValue); //Tag 7F02
			}
			else if(strcmp(szTag, "7F03") == 0)
			{
				debug_sprintf(szDbgMsg, "%s: Tag [%s] Length [%d] Value [%s]", __FUNCTION__, szTag, len, pszTagValue);
				APP_TRACE(szDbgMsg);

				xmlNewChild(root_node, NULL, BAD_CAST "AFTER_PROMO_APR", BAD_CAST pszTagValue); //Tag 7F03
			}
			else if(strcmp(szTag, "7F51") == 0)
			{
				debug_sprintf(szDbgMsg, "%s: Tag [%s] Length [%d] Value [%s]", __FUNCTION__, szTag, len, ptrTagValue);
				APP_TRACE(szDbgMsg);

				xmlNewChild(root_node, NULL, BAD_CAST "PROMO_DURATION", BAD_CAST ptrTagValue); //Tag 7F51
			}
			else if(strcmp(szTag, "7F52") == 0)
			{
				debug_sprintf(szDbgMsg, "%s: Tag [%s] Length [%d] Value [%s]", __FUNCTION__, szTag, len, ptrTagValue);
				APP_TRACE(szDbgMsg);

				xmlNewChild(root_node, NULL, BAD_CAST "PROMO_DESCRIPTION", BAD_CAST ptrTagValue); //Tag 7F52
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Invalid Tag, Tag [%s] Length [%d] Value [%s]", __FUNCTION__, szTag, len, pszTagValue);
				APP_TRACE(szDbgMsg);
			}

			if(ptrTagValue != NULL)
			{
				free(ptrTagValue);
				ptrTagValue = NULL;
			}

			if(pszTagValue != NULL)
			{
				free(pszTagValue);
				pszTagValue = NULL;
			}

			debug_sprintf(szDbgMsg, "%s: Length Parsed %d", __FUNCTION__, iLengthParsed);
			APP_TRACE(szDbgMsg);

    	}
    }

    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: getLastTranDtls
 *
 * Description	: This function gets the Last Transaction Details
 * 				  from the file
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
static int getLastTranDtls(char szLastTranDtls[LAST_TRAN_ITEM_COUNT][257])
{
	int		rv					= EXIT_SUCCESS;
	int		iIndex				= 0;
	int		iBufRead			= 0;
	int		iLen				= 0;
	FILE	*Fh                 = NULL;
	char	szTempBuffer[256+1] = "";
	struct	stat st;

#ifdef DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(stat(LAST_TRAN_FILE_NAME, &st) != 0)
		{
			debug_sprintf(szDbgMsg, "%s: %s doesn't exist", __FUNCTION__, LAST_TRAN_FILE_NAME);
			APP_TRACE(szDbgMsg);

			rv = EXIT_FAILURE;

			break;
		}

		acquireMutexLock(&gptLastTranFileMutex, "Last Tran File");

		Fh = fopen(LAST_TRAN_FILE_NAME, "r+");
		if(Fh == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while opening the file, ", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			rv = EXIT_FAILURE;

			releaseMutexLock(&gptLastTranFileMutex, "Last Tran File");

			break;
		}

		iIndex = 0;
		iBufRead = sizeof(szTempBuffer) - 1;

		/* Format of the LAST TRAN File
		 * <RESULT><LF><RESULT_CODE><LF><TERMINATION_STATUS><LF><RESPONSE_TEXT><LF><HOST_RESPCODE><LF><CLIENTID><LF><TROUTD><LF><CTROUTD><LF><INTRN_SEQ_NUM><LF><COMMAND><LF><ACCT_NUM><LF>
		 * <CARDHOLDER><LF><TRANS_AMOUNT><LF><APPROVED_AMOUNT><LF><TRANS_DATE><LF><TRANS_TIME><LF><INVOICE><LF><PAYMENT_TYPE><LF><PAYMENT_MEDIA><LF><CARD_TOKEN><LF>
		 * 7 R023 items
         * <EMV_TAGS_RESPONSE><LF>
         */

		while(1)
		{
			if(iIndex >= LAST_TRAN_ITEM_COUNT) // Make sure array isn't overflowed
			{
				debug_sprintf(szDbgMsg, "%s: Array is full", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
            
            // fgets will return NULL at end of file
			memset(szTempBuffer, 0x00, sizeof(szTempBuffer));
			if( fgets(szTempBuffer, iBufRead, Fh) != NULL) //Record contains RESULT field
			{
				if(strlen(szTempBuffer) > 1) //Contains more than one character i.e. <LF>
				{
					szTempBuffer[strlen(szTempBuffer) - 1] = 0x00; //Putting NULL at the <LF> place

					iLen = strlen(szTempBuffer);

					debug_sprintf(szDbgMsg, "%s: Read [%s], Len [%d]", __FUNCTION__, szTempBuffer, iLen);
					APP_TRACE(szDbgMsg);

					memset(szLastTranDtls[iIndex], 0x00, iLen+1);
					strcpy(szLastTranDtls[iIndex], szTempBuffer);
				}
			}
			else
			{
				if(feof(Fh))
				{
					debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					break;
				}
			}

			iIndex++;
		}


		fclose(Fh);

		releaseMutexLock(&gptLastTranFileMutex, "Last Tran File");

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

static int checkTPSCodeinSAFMapList(char *pszHostResponseCode, sCONFIG * GCONFIG)
{
	int rv 		= 0;
	int iIndex 	= 0;

#ifdef DEBUG
	char			szDbgMsg[1024]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszHostResponseCode == NULL || (strlen(pszHostResponseCode) == 0))
	{
		return rv;
	}

	for(iIndex = 0; iIndex < 20; iIndex++)
	{
		if(strlen(GCONFIG->safErrCodes[iIndex]) > 0)
		{
			if(strcmp(GCONFIG->safErrCodes[iIndex], pszHostResponseCode) == 0)
			{
				debug_sprintf(szDbgMsg, "%s: Found the Matching, breaking", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				rv = 1;
				break;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Did Not Find the Matching, breaking", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: updateLastTranDtls
 *
 * Description	: This function updates the Last Transaction Details
 * 				  data fields in the structure
 *
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int updateLastTranDtls(sGLOBALS* GLOBAL, _sVTV_FIELDS *svbninfo, sR023_data *sR023_info, sR017_data *sR017_info, sR008_data *sR008_info, sVHI_RESPONSE_INFO *vhi_res_info, _sSCA_XML_REQUEST *req_xml_info, sCONFIG * GCONFIG, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	int		rv					= EXIT_SUCCESS;
	int		iBufLen				= 0;
	FILE	*Fh     	  		= NULL;
	char	*pszValue			= NULL;
	char	szBuffer[2048]		= "";
	char	szAcctNum[30]		= "";
	char	szMaskAcctNum[30]	= "";
    char    temp[1500];

#ifdef DEBUG
	char			szDbgMsg[2048]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	acquireMutexLock(&gptLastTranFileMutex, "Last Tran File");

	Fh = fopen(LAST_TRAN_FILE_NAME, "w"); //Create an empty file for output operations. If a file with the same name already exists, its contents are discarded and the file is treated as a new empty file.
	if(Fh == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Error while opening %s file ", __FUNCTION__, LAST_TRAN_FILE_NAME);
		APP_TRACE(szDbgMsg);
		releaseMutexLock(&gptLastTranFileMutex, "Last Tran File");
		return EXIT_FAILURE;
	}
    
    debug_sprintf(szDbgMsg, "Save void data");
    APP_TRACE(szDbgMsg);
    saveVoidData(sR008_info->ORRN, GLOBAL, svbninfo, req_xml_info);

	memset(szBuffer, 0x00, sizeof(szBuffer));

	/* Format of the LAST TRAN File
	 * <RESULT>|<RESULT_CODE>|<TERMINATION_STATUS>|<RESPONSE_TEXT>|<HOST_RESPCODE>|<CLIENTID>|<TROUTD>|<CTROUTD>|<INTRN_SEQ_NUM>|<COMMAND>|<ACCT_NUM>|
	 * <CARDHOLDER>|<TRANS_AMOUNT>|<APPROVED_AMOUNT>|<TRANS_DATE>|<TRANS_TIME>|<INVOICE>|<PAYMENT_TYPE>|<PAYMENT_MEDIA>|<CARD_TOKEN>
	 * ;
     * <EMV_TAGS_RESPONSE><LF>
	 */

	pszValue = VHI_RESULT(GLOBAL, svbninfo, sR008_info, vhi_res_info, GCONFIG, req_xml_info);

	if(pszValue != NULL) //RESULT field is present
	{
		strcat(szBuffer, pszValue);
	}
	strcat(szBuffer, "\n");

	pszValue = VHI_RESULT_CODE(vhi_res_info);

	if(pszValue != NULL) //RESULT_CODE field is present
	{
		strcat(szBuffer, pszValue);
	}
	strcat(szBuffer, "\n");

	pszValue = VHI_TERMINATION_STATUS(vhi_res_info);

	if(pszValue != NULL) //TERMINATION_STATUS field is present
	{
		strcat(szBuffer, pszValue);
	}
	strcat(szBuffer, "\n");

	pszValue = VHI_RESPONSE_TEXT(GLOBAL, svbninfo, vhi_res_info, req_xml_info);

	if(pszValue != NULL) //RESPONSE_TEXT field is present
	{
		strcat(szBuffer, pszValue);
	}
	strcat(szBuffer, "\n");

	pszValue = VHI_HOST_RESP_CODE(vhi_res_info);

	if(pszValue != NULL) //HOST_RESPCODE field is present
	{
		strcat(szBuffer, pszValue);
	}

	strcat(szBuffer, "\n");

	pszValue = VHI_CLIENT_ID(req_xml_info);

	if(pszValue != NULL) //CLIENT_ID field is present
	{
		strcat(szBuffer, pszValue);
	}
	strcat(szBuffer, "\n");

	pszValue = VHI_TROUTD(svbninfo);

	if(pszValue != NULL) //TROUTD field is present
	{
		strcat(szBuffer, pszValue);
	}
	strcat(szBuffer, "\n");

	pszValue = VHI_CTROUTD(sR008_info);

	if(pszValue != NULL) //CTROUTD field is present
	{
		strcat(szBuffer, pszValue);
	}
	strcat(szBuffer, "\n");

	pszValue = VHI_INTRN_SEQ_NUM(svbninfo);

	if(pszValue != NULL) //INTRN_SEQ_NUM field is present
	{
		strcat(szBuffer, pszValue);
	}
	strcat(szBuffer, "\n");

	pszValue = VHI_COMMAND(req_xml_info);

	if(pszValue != NULL) //COMMAND field is present
	{
		strcat(szBuffer, pszValue);
	}
	strcat(szBuffer, "\n");

    // items from sR023_data converted to strings
    
	pszValue = VHI_ACCT_NUM(req_xml_info);

	if(pszValue != NULL) //ACCT_NUM field is present
	{
		memset(szMaskAcctNum, 0x00, sizeof(szMaskAcctNum));
		getMaskedPAN(pszValue, szMaskAcctNum); //Need to mask the account number before storing
		strcat(szBuffer, szMaskAcctNum);
	}
	else
	{
		pszValue = VHI_TRACK_DATA(req_xml_info);
		if(pszValue != NULL) //TRACK_DATA field is present
		{
			memset(szMaskAcctNum, 0x00, sizeof(szMaskAcctNum));
			memset(szAcctNum, 0x00, sizeof(szAcctNum));

			char* pTRACKIND = getXmlTagValue(eTRACK_IND, req_xml_info);
			if (pTRACKIND == NULL) {
				debug_sprintf(szDbgMsg, "%s: pTRACKIND is NULL, so track indicator is missing", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				pTRACKIND = "2";
			}

			/* Determining track1 or Track2 based on the
			 * track indicator field
			 */
			getPANFromTrackData(pszValue, atoi(pTRACKIND), szAcctNum, (sizeof(szAcctNum) - 1));

#if 0
			if(strchr(pszValue, '=') != NULL) //TODO Use if some beter technique is available
			{
				getPANFromTrackData(pszValue, atoi(pTRACKIND), szAcctNum);
			}
			else if(strchr(pszValue, '^') != NULL)
			{
				getPANFromTrackData(pszValue, atoi, szAcctNum);
			}
			else
			{
				getPANFromTrackData(pszValue, 2, szAcctNum);
			}
#endif
			getMaskedPAN(szAcctNum, szMaskAcctNum); //Need to mask the account number before storing
			strcat(szBuffer, szMaskAcctNum);
		}
	}

	strcat(szBuffer, "\n");

	pszValue = VHI_CARDHOLDER(req_xml_info);

	if(pszValue != NULL) //CARDHOLDER field is present
	{
		strcat(szBuffer, pszValue);
	}
	strcat(szBuffer, "\n");

	pszValue = VHI_TRANS_AMOUNT(req_xml_info);

	if(pszValue != NULL) //TRANS_AMOUNT field is present
	{
		strcat(szBuffer, pszValue);
	}
	strcat(szBuffer, "\n");

	pszValue = VHI_APPROVED_AMOUNT(svbninfo, vhi_res_info, req_xml_info);

	if(pszValue != NULL) //APPROVED_AMOUNT field is present
	{
		strcat(szBuffer, pszValue);
	}
	strcat(szBuffer, "\n");

	pszValue = VHI_TRANS_DATE(vhi_res_info);

	if(pszValue != NULL) //TRANS_DATE field is present
	{
		strcat(szBuffer, pszValue);
	}
	strcat(szBuffer, "\n");

	pszValue = VHI_TRANS_TIME(vhi_res_info, vhi_fieldinfo);

	if(pszValue != NULL) //TRANS_TIME field is present
	{
		strcat(szBuffer, pszValue);
	}
	strcat(szBuffer, "\n");

	pszValue = VHI_INVOICE(req_xml_info);

	if(pszValue != NULL) //INVOICE field is present
	{
		strcat(szBuffer, pszValue);
	}
	strcat(szBuffer, "\n");

	pszValue = VHI_PAYMENT_TYPE(req_xml_info);

	if(pszValue != NULL) //PAYMENT_TYPE field is present
	{
		strcat(szBuffer, pszValue);
	}
	strcat(szBuffer, "\n");

	pszValue = VHI_PAYMENT_MEDIA(svbninfo, req_xml_info);

	if(pszValue != NULL) //PAYMENT_MEDIA field is present
	{
		strcat(szBuffer, pszValue);
	}
	strcat(szBuffer, "\n");

	pszValue = VHI_CARD_TOKEN(sR017_info);

	if(pszValue != NULL) //CARD_TOKEN field is present
	{
		strcat(szBuffer, pszValue);
	}
	strcat(szBuffer, "\n");
    
    pszValue = VHI_AUTH_CODE(svbninfo);
    
    if(pszValue != NULL) //AUTH_CODE field is present
    {
        strcat(szBuffer, pszValue);
    }
    strcat(szBuffer, "\n");
    
    pszValue = VHI_REFERENCE(svbninfo);
    
    if(pszValue != NULL) //REFERNCE field is present
    {
        strcat(szBuffer, pszValue);
    }
    strcat(szBuffer, "\n");
    
    if (sR023_info->tlv_length > 0)
    {
        hex2char((UINT8*)temp, sR023_info->tlv_data,  sR023_info->tlv_length);
        temp[sR023_info->tlv_length* 2]= '\0';
        strcat(szBuffer, temp);
    }
    strcat(szBuffer, "\n");
    
#ifdef OLD_EMV_TAG_METHOD
    // AUTHORIZATION_RESPONSE_CODE
    if(sR023_info->auth_response_code_len > 0)
    {
        strncat(szBuffer, sR023_info->auth_response_code, sR023_info->auth_response_code_len);
    }
    strcat(szBuffer, "\n");

    // EMV_ISSUER_SCRIPT_DATA1
    if(sR023_info->script71_len > 0)
    {
        hex2char((UINT8*)temp, sR023_info->script71,  sR023_info->script71_len);
        temp[sR023_info->script71_len* 2]= '\0';
        strcat(szBuffer, temp);
    }
    strcat(szBuffer, "\n");
    
    // EMV_ISSUER_SCRIPT_DATA2
    if(sR023_info->script72_len > 0)
    {
        hex2char((UINT8*)temp, sR023_info->script72,  sR023_info->script72_len);
        temp[sR023_info->script72_len* 2]= '\0';
        strcat(szBuffer, temp);
    }
    strcat(szBuffer, "\n");
    
    // ISSUER_AUTHENTICATION_DATA
    if(sR023_info->issuer_auth_data_len > 0)
    {
        hex2char((UINT8*)temp, sR023_info->issuer_auth_data,  sR023_info->issuer_auth_data_len);
        temp[sR023_info->issuer_auth_data_len* 2]= '\0';
        strcat(szBuffer, temp);
    }
    strcat(szBuffer, "\n");
    
    // ISSUER_SCRIPT_COMMAND
    if(sR023_info->issuer_script_command_len > 0)
    {
        hex2char((UINT8*)temp, sR023_info->issuer_script_command,  sR023_info->issuer_script_command_len);
        temp[sR023_info->issuer_script_command_len* 2]= '\0';
        strcat(szBuffer, temp);
    }
    strcat(szBuffer, "\n");
    
    // ISSUER_SCRIPT_IDENTIFIER
    if(sR023_info->issuer_script_command_len > 0)
    {
        hex2char((UINT8*)temp, sR023_info->issuer_script_id,  sR023_info->issuer_script_id_len);
        temp[sR023_info->issuer_script_id_len* 2]= '\0';
        strcat(szBuffer, temp);
    }
    strcat(szBuffer, "\n");
    
    // ISSUER_SCRIPT_RESULTS
    if(sR023_info->issuer_script_results_len)
    {
        hex2char((UINT8*)temp, sR023_info->issuer_script_results,  sR023_info->issuer_script_results_len);
        temp[sR023_info->issuer_script_results_len* 2]= '\0';
        strcat(szBuffer, temp);
    }
    strcat(szBuffer, "\n");
#endif

	iBufLen = strlen(szBuffer);

#ifdef DEVDEBUG
	debug_sprintf(szDbgMsg, "%s -Updated Last Tran data [%s] and its length is %d", __FUNCTION__, szBuffer, iBufLen);
	APP_TRACE(szDbgMsg);
#endif

	rv = fwrite(szBuffer, sizeof(char), iBufLen, Fh);

	if(rv == iBufLen)
	{
		debug_sprintf(szDbgMsg, "%s- Successfully updated the Last Tran Data file", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		rv = EXIT_SUCCESS;
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s- Error while updating Last Tran data file, Number of bytes written(%d), Actual count (%d)", __FUNCTION__, rv, iBufLen);
		APP_TRACE(szDbgMsg);
		rv = EXIT_FAILURE;
	}

	fclose(Fh);

	releaseMutexLock(&gptLastTranFileMutex, "Last Tran File");

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}
