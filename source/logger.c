/*
 * Logger.c
 *
 *  Created on: Sep 25, 2013
 *      Author: Dexter M. Alberto
 */


#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <sys/ioctl.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/syslog.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <tcpipdbg.h>
#include <sys/timeb.h>
#include <pthread.h>

#include "logger.h"
#include "config_data.h"
#include "main.h"

/*
* ============================================================================
* Function Name: setupDebug
*
* Description  :
*
* Input Params :
*
* Output Params:
* ============================================================================
*/
#ifdef LOGGING_ENABLED
void setupDebug()
{
	char *  cpDbgFlag       = NULL;

	chDebug = NO_DEBUG;

	if((cpDbgFlag = (char *)getenv(VHI_DEBUG)) == NULL)
	{
		chDebug = NO_DEBUG; // Debug runtime disabled
	}
	else
	{
		if(cpDbgFlag[0] == '1') // Spew on the console
		{
			chDebug = CONSOLE_DEBUG;
		}
		else if(cpDbgFlag[0] == '2') // Ethernet Debug
		{
			chDebug = ETHERNET_DEBUG;
		}
		else if(cpDbgFlag[0] == '0')
		{
			chDebug = NO_DEBUG; // Debug runtime disabled
		}
	}

	if(chDebug == ETHERNET_DEBUG)
	{
		iOpenDebugSocket();
		sleep(2);
	}

	return;
}
#endif

/*
* ============================================================================
* Function Name: APP_TRACE
*
* Description  :
*
* Input Params :
*
* Output Params:
* ============================================================================
*/
#ifdef LOGGING_ENABLED
void APP_TRACE(char * pszMsg)
{
	int                     iPrefixLen              = 0;
	int                     iDbgMsgSize             = 0;
	int                     iMsgLen                 = 0;
	pid_t                   processID               = 0;
	pthread_t               threadID                = 0L;
	char                    szTSPrefix[41]  = ""; /* MM/DD/YYYY HH:MM:SS:sss */
	char                    szDbgMsg[MAX_DEBUG_MSG+1] = "";
	char *                  pszMsgPrefix    = "VHI";
	struct tm *             tm_ptr                  = NULL;
	struct timeb    the_time;

	if(chDebug == NO_DEBUG)
	{
			return;
	}

	/* Get the starting values */
	iDbgMsgSize = sizeof(szDbgMsg) - 1;
	iMsgLen = strlen(pszMsg);
	processID = getpid();
	threadID = pthread_self();

	ftime(&the_time);
	tm_ptr = gmtime(&the_time.time);
	if(the_time.millitm >= 1000)
	{
			/* Keep within 3 digits, since sprintf's width.precision specification
			* does not truncate large values */
			the_time.millitm = 999;
	}

	sprintf(szTSPrefix, "%02d/%02d/%04d %02d:%02d:%02d:%03d", tm_ptr->tm_mon+1,
							tm_ptr->tm_mday, 1900+tm_ptr->tm_year, tm_ptr->tm_hour,
							tm_ptr->tm_min, tm_ptr->tm_sec, the_time.millitm);

	iPrefixLen = sprintf(szDbgMsg, "%s %04d %04d %s: ", szTSPrefix, processID,
																							(int) threadID, pszMsgPrefix);

	if(chDebug == ETHERNET_DEBUG)
	{
			/* If addition of full datasize will exceed szDbgMsg */
			if((iPrefixLen + iMsgLen) >= iDbgMsgSize)
			{
					/* Chop iMsgLen down */
					iMsgLen = iDbgMsgSize - iPrefixLen;
			}

			memcpy(szDbgMsg + iPrefixLen, pszMsg, iMsgLen);
			szDbgMsg[iPrefixLen + iMsgLen] = 0;

			DebugMsg(szDbgMsg);
	}
	else if(chDebug == CONSOLE_DEBUG)
	{
			/* If addition of full datasize will exceed szDbgMsg */
			if((iPrefixLen + iMsgLen) >= (iDbgMsgSize - 1))
			{
					/* Chop iMsgLen down; save last 2 slots for '\n' and '\0' */
					iMsgLen = iDbgMsgSize - iPrefixLen - 2;
			}

			memcpy(szDbgMsg + iPrefixLen, pszMsg, iMsgLen);
			szDbgMsg[iPrefixLen + iMsgLen] = '\n';
			szDbgMsg[iPrefixLen + iMsgLen + 1] = 0;

			printf(szDbgMsg);
	}

	return;
}
#endif

#if 0
/*
 * UDP broadcast
 */
void broadcastLog(const char * buffer)
{
	int socketId, length, n;
	struct sockaddr_in server;
	struct hostent *hp;
	int broadcast = 1;

	socketId = socket(AF_INET, SOCK_DGRAM, 0);

	setsockopt(socketId, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof(broadcast));

	server.sin_family = AF_INET;

	char* ipptr = getEnv("DHI", "debug_ip");
	char debug_ip[30]={0};
	strcpy(debug_ip, ipptr);

	hp = gethostbyname(debug_ip); //IPADDRESS

	bcopy((char *)hp->h_addr,
	(char *)&server.sin_addr
	,hp->h_length);

	server.sin_port = htons(1234); //PORT
	length=sizeof(struct sockaddr_in);

	n=sendto(socketId, buffer, strlen(buffer), 0,
	(struct sockaddr *)&server, length);

	close(socketId);
}

/*
 * Substitute for printf
 */
void logger(int entryType, const char* file, int line, const char* func, const char* format, ...)
{
	if(isDebugMode()==true) {
		char *buffer = (char *)malloc (16384);
		va_list args;
		va_start (args, format);
		vsprintf (buffer,format, args);
		va_end (args);

		int ilen = strlen(file) + strlen(func) + strlen(buffer) + 22;
		char* log1 = (char *)malloc(ilen);

#if 0
		/*mask the TRACK DATA on logs*/
		char* pch=NULL;
		char* tmp=NULL;
		int i=0, iIsFound=0;
		pch=strchr(buffer, '=');

		while(pch!=NULL) {
			tmp = buffer;
			i = (pch-buffer);
			tmp+=i-1;

			if(isdigit(*tmp)) {
				i=(tmp-buffer)-11;
				iIsFound=1;
				break;
			}
			pch=strchr(pch+1, '=');
		}
		if(iIsFound==1) {
			pch=NULL;
			pch=buffer;
			pch+=i;
			strcpy(pch,"********");
		}
		/*end*/

		/*encrypt ACCT_NUM*/
		pch=NULL;
		if((pch = strstr(buffer, "<ACCT_NUM>"))!=NULL) {
			i=0;
			i = (pch-buffer)+14;
			pch=NULL;
			pch=buffer;
			pch+=i;
			strcpy(pch, "********");
		}
		/*end*/
#endif

		sprintf(log1, "[LOGS]%s|%s|%d: %s\n", file, func, line, buffer);

//		broadcastLog(log1);

//		APP_TRACE(log1);

		if(log1!=NULL)
			free(log1);

		if(buffer!=NULL)
			free(buffer);
	}
}

bool isDebugMode()
{
	char* ptr = getEnv("DHI", "debug_mode");
	char debug[1+1] = {0};
	strcpy(debug, ptr);

	if( strncmp(debug, "Y", 1) == COMPARE_OK ) {
		return true;
	}

	return false;
}
#endif
