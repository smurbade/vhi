/*
 * vhi_610_group.c
 *
 *  Created on: Nov 12, 2013
 *      Author: Dexter M. Alberto
 */


#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "vhi_610_group.h"
#include "help_function.h"
#include "sca_xml_request.h"
#include "vantiv_response.h"
#include "logger.h"
#include "config_data.h"

#ifdef LOGGING_ENABLED
static char    szDbgMsg[1024];
#endif

extern void b64_encode(const UINT8 *pchInBuf, UINT8 *pchOutBuf, int shInSize, int *shOutSize);
extern void b64_decode(const UINT8 *pchInBuf, UINT8 *pchOutBuf, int shInSize, int *shOutSize);

void free610GroupData()
{

}

/*
 * Praveen_P1: 21 Sep 2016
 * FIELD #     REQUEST OPTIONAL GROUP DATA DESCRIPTION  DATA TYPE POSITION LENGTH    VALID VALUES
 *  01 					Draft Locator Id					ans	   1 - 11	 11
 *	02				Merchant Reference Number				ans	   12 - 28  1 - 17   Variable 17 maximum
 */
void composeG001(char** ptr, _sSCA_XML_REQUEST *req_xml_info)
{
    char buffer[4+11+16+1] = {0};
    int  length = 0;

    _sG001_ info;

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);

    char* pREFERENCE 	= getXmlTagValue(eREFERENCE, req_xml_info);
    char* pLOCATOR_ID 	= getXmlTagValue(eLOCATOR_ID, req_xml_info);


	if (pREFERENCE == NULL && pLOCATOR_ID == NULL) {
		debug_sprintf(szDbgMsg, "%s: no Reference field/Locator ID in the request, Returning", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return ;
	}

	memset(&info, 0x00, sizeof(_sG001_));

	strcpy(info.group, "G001");

	if(pLOCATOR_ID != NULL)
	{
		length = strlen(pLOCATOR_ID);

		if(length > 11) //Field 02 of G001 can hold upto 17 chars maximum. Copying only 17 chars if it contains more
		{
			length = 11;
		}
		strncpy(info.field_01, pLOCATOR_ID, length); //Filling Draft Locator ID with value from LOCATOR_ID field

		appendSpace(sizeof(info.field_01), info.field_01);
	}
	else
	{
		fillSpace(sizeof(info.field_01), info.field_01); //Filling spaces in place of Draft Locator ID
	}

	if(pREFERENCE != NULL)
	{
		length = strlen(pREFERENCE);

		if(length > 17) //Field 02 of G001 can hold upto 17 chars maximum. Copying only 17 chars if it contains more
		{
			length = 17;
		}
		strncpy(info.field_02, pREFERENCE, length); //Filling Merchant Reference Number with value from REFERENCE field

		appendSpace(sizeof(info.field_02), info.field_02);
	}
	else
	{
		fillSpace(sizeof(info.field_02), info.field_02); //Filling spaces in place of Merchant Reference Number
	}

    strcpy(buffer, info.group);
    strcat(buffer, info.field_01);
    strcat(buffer, info.field_02);

    int len = strlen(buffer);

    debug_sprintf(szDbgMsg, "length= %d, buffer= [%s]", len, buffer);
    APP_TRACE(szDbgMsg);

    *ptr = (char*)calloc(len+1, sizeof(char));
    strcpy(*ptr, buffer);

    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);
}

/*
 * Praveen_P1: 04 Nov 2016
 * FIELD #     REQUEST OPTIONAL GROUP DATA DESCRIPTION  DATA TYPE POSITION LENGTH    VALID VALUES
 *  01 					Lane Number							n	   1 - 3	 3
 *	02				Cashier Number							n	   4 - 11  	 8
 *	03				Merchant Category Code					n	  12 - 15	 4
 */
void composeG004(char** ptr, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info)
{
    char buffer[4+3+8+4+1] = {0};

    _sG004_ info;

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);

    char* pCASHIER_NUM 			= getXmlTagValue(eCASHIER_NUM, req_xml_info);
    char* pLANE 				= getXmlTagValue(eLANE, req_xml_info);
    char* pMERCHANT_CAT_CODE 	= getXmlTagValue(eMERCHANT_CAT_CODE, req_xml_info);


	if (pMERCHANT_CAT_CODE == NULL ) { //We would be sending this group only if it contains MERCHANT_CAT_CODE field
		debug_sprintf(szDbgMsg, "%s: no Merchant Category Code in the request, Returning", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return ;
	}

	memset(&info, 0x00, sizeof(_sG004_));

	strcpy(info.group, "G004");

	//Filling Lane Number Field
	if( (strlen(GCONFIG->laneID) > 0) || (pLANE != NULL) )
	{
		memset(info.field_01, 0x00, sizeof(info.field_01));

		if(pLANE == NULL)
		{
			if(strlen(GCONFIG->laneID) > 1)
			{
				strncpy(info.field_01, GCONFIG->laneID, sizeof(info.field_01)-1); //Filling Lane Number Field value With Config Lane Number
			}
			else
			{
				strcpy(info.field_01, "001"); //default for pLANE is 01 to 99
			}
		}
		else
		{
			strncpy(info.field_01, pLANE, sizeof(info.field_01)-1); //Filling Lane Number Field value With LANE field from SSI request
		}
		precedeZero(sizeof(info.field_01), info.field_01);
	}
	else
	{
		strcpy(info.field_01, "001"); //default for pLANE is 01 to 99
	}

	//Filling Cashier Number
	if(pCASHIER_NUM != NULL)
	{
		strncpy(info.field_02, pCASHIER_NUM, sizeof(info.field_02)-1); //Filling Cashier Number Field value With CASHIER_NUM field from SSI request

		precedeZero(sizeof(info.field_02), info.field_02);
	}
	else
	{
		fillZero(sizeof(info.field_02), info.field_02); //Filling zero in place of Cashier Number if the field is not present in the SSI request
	}

	//Filling Merchant Category Code
	if(pMERCHANT_CAT_CODE != NULL)
	{
		strncpy(info.field_03, pMERCHANT_CAT_CODE, sizeof(info.field_03)-1); //Filling Cashier Number Field value With CASHIER_NUM field from SSI request

		precedeZero(sizeof(info.field_03), info.field_03);
	}
	else //This condition should not get executed since we are returning above itself if it is NULL
	{
		fillZero(sizeof(info.field_03), info.field_03); //Filling spaces in place of Merchant Category Code if the field is not present in the SSI request
	}

    strcpy(buffer, info.group);
    strcat(buffer, info.field_01);
    strcat(buffer, info.field_02);
    strcat(buffer, info.field_03);

    int len = strlen(buffer);

    debug_sprintf(szDbgMsg, "length= %d, buffer= [%s]", len, buffer);
    APP_TRACE(szDbgMsg);

    *ptr = (char*)calloc(len+1, sizeof(char));
    strcpy(*ptr, buffer);

    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);
}

void composeG009(char** ptr, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info)
{
	int   iPartialAuth  = 0;
	char* pPAYMENT_TYPE = NULL;
	char* pCOMMAND      = NULL;
	char buffer[4+16+1] = {0};
	_sG009_ info;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * Praveen_P1: 30 Nov 2015
	 * In VHI we are not paying attention to partial auth
	 * flag which comes in the SSI request. we are always setting
	 * in the Vantiv request to allow for partial authorization
	 * making correction to set in the Vantiv request based on the
	 * PARTIAL_AUTH field in the SSI request
	 */
	char* pPartialAuth = getXmlTagValue(ePARTIAL_AUTH, req_xml_info);

	/*
	 * Praveen_P1: 28 Jan 2016
	 * Fix for PTMX-775 where we are not returning Approved amount back
	 * Earlier we were not used to honor this flag, but when we started
	 * Honoring this flag, if this field is not present in the request also
	 * we are setting to 0 which breaks the backward compactability.
	 * so if Partial_auth field is present in the request then we will set accordingly
	 * if it is not set, then we will assume that it is one
	 */
	if(pPartialAuth == NULL)
	{
		iPartialAuth = 1;
	}
	else
	{
		if(strcmp(pPartialAuth, "1") == 0)
		{
			iPartialAuth = 1;
		}
		else
		{
			iPartialAuth = 0;
		}
	}

	memset(&info, 0x00, sizeof(info));
	memset(info.group, 0x00, sizeof(info.group));

	strcpy(info.group, "G009");

	/*
	 * Field01 - Visa/MasterCard Partial Approval Indicator:- 0 � Partial approval is not supported, 1 � Partial approvals supported
	 * Field02 - Discover Partial Approval Indicator       :- 0 � Partial Approval Not Supported, 2 � Partial Approval Supported
	 * Field03 - AMEX Partial Approval Indicator           :- 0 � partial approval is not supported, 1 � partial approvals supported
	 */
	if(iPartialAuth) //Partial Auth is set in the SSI request so allowing partial authorization
	{
		strcpy(info.field_01, "1");
		strcpy(info.field_02, "2");
		strcpy(info.field_03, "1");
	}
	else//Partial Auth is not set in the SSI request, so not allowing partial authorization
	{
		strcpy(info.field_01, "0");
		strcpy(info.field_02, "0");
		strcpy(info.field_03, "0");
	}

	strcpy(info.field_04, "N");
	strcpy(info.field_05, "Y");
	strcpy(info.field_06, "Y");
	strcpy(info.field_07, "N");

	/*
	 * Praveen_P1: Dec 15 2015
	 * FRD-3.65 Signature Capture Reference
	 * Field08 - Application requests Group R011 � Signature Capture Data generated by host in response
	 * N � Not supported by front-end device
	 * Y � Indicates that the terminal application requests response group R011
	 * IF config variable is set then we set Field08 of G009 to Y to send
	 * the signatureref in R011
	 */
	if(GCONFIG->sigcapturerefenabled == true)
	{
		/*
		 * Praveen_P1: 28 Jan 2016
		 * Fix for PTMX-784, sending signature reference field only for SALE/POST_AUTH CREDIT transactions
		 * since we capture signature for only these commands, we will request refernce field for these commands
		 */
		pPAYMENT_TYPE = getXmlTagValue( ePAYMENT_TYPE, req_xml_info );
		pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);

		if(pPAYMENT_TYPE == NULL || pCOMMAND == NULL)
		{
			strcpy(info.field_08, "N");
		}
		else
		{
			if ((strncmp(pPAYMENT_TYPE, "CREDIT", strlen("CREDIT")) == COMPARE_OK) &&
					((strncmp(pCOMMAND, COMMAND.SALE, strlen(COMMAND.SALE)) == COMPARE_OK) || (strncmp(pCOMMAND, COMMAND.POST_AUTH, strlen(COMMAND.POST_AUTH)) == COMPARE_OK) ))
			{
				strcpy(info.field_08, "Y");
			}
			else
			{
				strcpy(info.field_08, "N");
			}
		}
	}
	else
	{
		strcpy(info.field_08, "N");
	}

	strcpy(info.field_09, "N");
	strcpy(info.field_10, "N");
	strcpy(info.field_11, "N");

	/*
	 * As per vantiv new specs
	 * position 12 = Y for restaurant
	 * position 13 = 0 for restaurant
	 */
	if (GCONFIG->atype1 == eRESTAURANT_TYPE) {
		strcpy(info.field_12, "Y");
		strcpy(info.field_13, "0");
	}
	else {
		if (GCONFIG->adjcapable == 'Y') {
			strcpy(info.field_12, "Y");
		}
		else {
			strcpy(info.field_12, "N");
		}

		/*
		 * Field13 - Debit Partial Approval Indicator:- 0 � Partial approval is not supported, 1 � Partial approvals supported
		 */
		if(iPartialAuth) //Partial Auth is set in the SSI request so allowing partial authorization
		{
			strcpy(info.field_13, "1");
		}
		else
		{
			strcpy(info.field_13, "0");
		}
	}

	strcpy(info.field_14, "N");
	strcpy(info.field_15, "Y");
	strcpy(info.field_16, "N");

	strcpy(buffer, info.group);
	strcat(buffer, info.field_01);
	strcat(buffer, info.field_02);
	strcat(buffer, info.field_03);
	strcat(buffer, info.field_04);
	strcat(buffer, info.field_05);
	strcat(buffer, info.field_06);
	strcat(buffer, info.field_07);
	strcat(buffer, info.field_08);
	strcat(buffer, info.field_09);
	strcat(buffer, info.field_10);
	strcat(buffer, info.field_11);
	strcat(buffer, info.field_12);
	strcat(buffer, info.field_13);
	strcat(buffer, info.field_14);
	strcat(buffer, info.field_15);
	strcat(buffer, info.field_16);

	if (strlen(buffer)!= sizeof(buffer)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! buffer not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

	int len = strlen(buffer);
	*ptr = (char*)calloc(len+1, sizeof(char));
	strcpy(*ptr, buffer);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

void composeG014(char** ptr, sGLOBALS* GLOBAL, _sSCA_XML_REQUEST *req_xml_info)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* pCTROUTD = getXmlTagValue(eCTROUTD, req_xml_info);

	if (GLOBAL->http.status_code == eHTTP_GATEWAY_TIMEOUT) {
		//return if TOR
		return;
	}

	if (pCTROUTD == NULL && GLOBAL->http.status_code != eHTTP_GATEWAY_TIMEOUT) {
		debug_sprintf(szDbgMsg, "%s: ERROR! CTROUTD is null", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

	char buffer[4+9+1] = {0};
	_sG014_ info;

	memset(&info, 0x00, sizeof(info));
	memset(info.group, 0x00, sizeof(info.group));

	strcpy(info.group, "G014");

	if (GLOBAL->http.status_code == eHTTP_GATEWAY_TIMEOUT) { //TOR case
		fillZero(sizeof(info.field_01),info.field_01);
	}
	else {
        strcpy(info.field_01, "");
        strncat(info.field_01, pCTROUTD, sizeof(info.field_01)-1);
	}

	precedeZero(sizeof(info.field_01),info.field_01);

	strcpy(buffer, info.group);
	strcat(buffer, info.field_01);

	if (strlen(buffer)!= sizeof(buffer)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! buffer not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

	int len = strlen(buffer);
	*ptr = (char*)calloc(len+1, sizeof(char));
	strcpy(*ptr, buffer);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

void composeG015(char** ptr, _sSCA_XML_REQUEST *req_xml_info)
{
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    char    buffer[120+ 1];
    int     index= 0;
    _sG015_ info;
    memset(&info, 0x00, sizeof(info));

    strcpy(info.group, "G015");

    char *pAMOUNT_HEALTHCARE = getXmlTagValue(eAMOUNT_HEALTHCARE, req_xml_info);
    if (pAMOUNT_HEALTHCARE == NULL)
        return;                         // no data for group 15
    if (strlen(pAMOUNT_HEALTHCARE) == 0)
        return;                         // empty data for group 15
    // TODO: perhaps there will be WIC data
    
    strcpy(info.field_01[index], "00");
    strcpy(info.field_02[index], "4S");         // total amount
    strcpy(info.field_03[index], "840");        // USA
    strcpy(info.field_04[index], "C");          // positive
    strcpy(buffer, pAMOUNT_HEALTHCARE);
    removeDecimalPoint(buffer);
    strncat(info.field_04[index], "000000000000", 12- strlen(buffer));
    strcat(info.field_04[index], buffer);       // amount is cents

    pAMOUNT_HEALTHCARE = getXmlTagValue(eAMOUNT_PRESCRIPTION, req_xml_info);
    if ((pAMOUNT_HEALTHCARE != NULL) && (strlen(pAMOUNT_HEALTHCARE) >= 4))
    {
        index++;
        strcpy(info.field_01[index], "00");
        strcpy(info.field_02[index], "4U");     // prescriptions
        strcpy(info.field_03[index], "840");    // USA
        strcpy(info.field_04[index], "C");      // positive
        strcpy(buffer, pAMOUNT_HEALTHCARE);
        removeDecimalPoint(buffer);
        strncat(info.field_04[index], "000000000000", 12- strlen(buffer));
        strcat(info.field_04[index], buffer);   // amount is cents
    }
    
    pAMOUNT_HEALTHCARE = getXmlTagValue(eAMOUNT_VISION, req_xml_info);
    if ((pAMOUNT_HEALTHCARE != NULL) && (strlen(pAMOUNT_HEALTHCARE) >= 4))
    {
        index++;
        strcpy(info.field_01[index], "00");
        strcpy(info.field_02[index], "4V");     // vision
        strcpy(info.field_03[index], "840");    // USA
        strcpy(info.field_04[index], "C");      // positive
        strcpy(buffer, pAMOUNT_HEALTHCARE);
        removeDecimalPoint(buffer);
        strncat(info.field_04[index], "000000000000", 12- strlen(buffer));
        strcat(info.field_04[index], buffer);   // amount is cents
    }
    
    pAMOUNT_HEALTHCARE = getXmlTagValue(eAMOUNT_CLINIC, req_xml_info);
    if ((pAMOUNT_HEALTHCARE != NULL) && (strlen(pAMOUNT_HEALTHCARE) >= 4))
    {
        index++;
        strcpy(info.field_01[index], "00");
        strcpy(info.field_02[index], "4W");     // clinic
        strcpy(info.field_03[index], "840");    // USA
        strcpy(info.field_04[index], "C");      // positive
        strcpy(buffer, pAMOUNT_HEALTHCARE);
        removeDecimalPoint(buffer);
        strncat(info.field_04[index], "000000000000", 12- strlen(buffer));
        strcat(info.field_04[index], buffer);   // amount is cents
    }
    
    pAMOUNT_HEALTHCARE = getXmlTagValue(eAMOUNT_DENTAL, req_xml_info);
    if ((pAMOUNT_HEALTHCARE != NULL) && (strlen(pAMOUNT_HEALTHCARE) >= 4))
    {
        index++;
        strcpy(info.field_01[index], "00");
        strcpy(info.field_02[index], "4X");     // dental
        strcpy(info.field_03[index], "840");    // USA
        strcpy(info.field_04[index], "C");      // positive
        strcpy(buffer, pAMOUNT_HEALTHCARE);
        removeDecimalPoint(buffer);
        strncat(info.field_04[index], "000000000000", 12- strlen(buffer));
        strcat(info.field_04[index], buffer);   // amount is cents
    }
    
    strcpy(buffer, "");
    strncat(buffer, info.group, sizeof(info.group)-1);
    int     i;
    for (i=0; i<=index; i++)
    {
        strncat(buffer, info.field_01[i], sizeof(info.field_01[0])-1);
        strncat(buffer, info.field_02[i], sizeof(info.field_02[0])-1);
        strncat(buffer, info.field_03[i], sizeof(info.field_03[0])-1);
        strncat(buffer, info.field_04[i], sizeof(info.field_04[0])-1);
    }
    
    debug_sprintf(szDbgMsg, "G15 buffer= %s.", buffer);
    APP_TRACE(szDbgMsg);

    int len = strlen(buffer);
    *ptr = (char*)calloc(len+1, sizeof(char));
    strcpy(*ptr, buffer);

    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);
}

void composeG018(char** ptr, sGLOBALS* GLOBAL, _sSCA_XML_REQUEST *req_xml_info)
{
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    char* pPinCode = getXmlTagValue(ePIN_CODE, req_xml_info);
    char* pPAYMENT_TYPE = getXmlTagValue( ePAYMENT_TYPE, req_xml_info );
    
    if ( (pPinCode == NULL) || (strncmp(pPAYMENT_TYPE, "GIFT", 4 ) != COMPARE_OK) ) {
        debug_sprintf(szDbgMsg, "%s: ERROR! PIN CODE is null or Its not Gift transaction", __FUNCTION__);
        APP_TRACE(szDbgMsg);
        
        return;
    }
    
    char buffer[4+1+4+12+1] = {0};
    _sG018_ info;
    
    memset(&info, 0x00, sizeof(info));
    memset(info.group, 0x00, sizeof(info.group));
    
    strcpy(info.group, "G018");
    
    if (GLOBAL->http.status_code == eHTTP_GATEWAY_TIMEOUT) { //TOR case, not sure what we should be filling with
        fillZero(sizeof(info.field_01),info.field_01);
    }
    else {
        strncpy(info.field_01, "0", 1);
        fillSpace(sizeof(info.field_02), info.field_02);
        strncpy(info.field_03, pPinCode, strlen(pPinCode));
    }
    
    precedeZero(sizeof(info.field_01),info.field_01);
    
    strcpy(buffer, info.group);
    strcat(buffer, info.field_01);
    strcat(buffer, info.field_02);
    strcat(buffer, info.field_03);
#if 0
    if (strlen(buffer)!= sizeof(buffer)-1) {
        debug_sprintf(szDbgMsg, "%s: ERROR! buffer not exact bytes", __FUNCTION__);
        APP_TRACE(szDbgMsg);
        
        return;
    }
#endif
    int len = strlen(buffer);
    *ptr = (char*)calloc(len+1, sizeof(char));
    strcpy(*ptr, buffer);
    
    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);
}

void composeG023(char** ptr, _sSCA_XML_REQUEST *req_xml_info)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char buffer[4+9+1]={0};
	char* pTIP_AMOUNT = NULL;
	_sG023_ info;

	memset(&info, 0x00, sizeof(info));
	memset(info.group, 0x00, sizeof(info.group));
	strcpy(info.group, "G023");
	pTIP_AMOUNT = getXmlTagValue(eTIP_AMOUNT, req_xml_info);

	if (pTIP_AMOUNT==NULL) {
		fillZero(sizeof(info.field_01), info.field_01);
	}
	else {
		if (strlen(pTIP_AMOUNT) > sizeof(info.field_01)-1) { //if bytes over
			debug_sprintf(szDbgMsg, "%s: ERROR! pTIP amount bytes over", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			fillZero(sizeof(info.field_01), info.field_01); //fill with zero
		}
		else {
			strcpy(info.field_01, pTIP_AMOUNT);
			removeDecimalPoint(info.field_01);
			precedeZero(sizeof(info.field_01), info.field_01);
		}
	}

    strcpy(buffer, "");
    strncat(buffer, info.group, sizeof(info.group)-1);
	strncat(buffer, info.field_01, sizeof(info.field_01)-1);

	if (strlen(buffer)!= sizeof(buffer)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! buffer not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

	int len = strlen(buffer);
	*ptr = (char*)calloc(len+1, sizeof(char));
	strcpy(*ptr, buffer);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

void composeG026(char** ptr, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* pCARD_TOKEN = getXmlTagValue(eCARD_TOKEN, req_xml_info);

	if (pCARD_TOKEN != NULL && GCONFIG->eTokenMode == eTOKEN_TUT) {
		return;
	}

	/*
	 * According to Sharvani
	 * f1 = S
	 * f2 = M
	 * f3 = varies on entry
	 * f10 = S
	 */

	char buffer[4+1008+1] = {0};
	_sG026_ info;

	memset(&info, 0x00, sizeof(info));
	memset(info.group, 0x00, sizeof(info.group));

	strcpy(info.group, "G026");
	strcpy(info.field_01, "S");
	strcpy(info.field_02, "M");

	/*
	 * T - swiped
	 * E - manual
	 * P - PAN
	 */

	char* pFUNCTION_TYPE = getXmlTagValue(eFUNCTION_TYPE, req_xml_info);
	char* pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);
	char* pPRESENT_FLAG = getXmlTagValue(ePRESENT_FLAG, req_xml_info);
	char* pENCRYPTION_PAYLOAD = getXmlTagValue(eENCRYPTION_PAYLOAD, req_xml_info);

	if (pENCRYPTION_PAYLOAD == NULL)
		return;

#if 0
	if (GCONFIG->isRegistartMode == true) {
		pPRESENT_FLAG = SWIPED_ENTRY;
	}
#endif

	if (strncmp(pFUNCTION_TYPE, FUNCTION_TYPE.PAYMENT, strlen(FUNCTION_TYPE.PAYMENT)) == COMPARE_OK) {
		if (pCOMMAND != NULL) {

			if (strncmp(pCOMMAND, COMMAND.VOID, strlen(COMMAND.VOID)) == COMPARE_OK || GLOBAL->http.status_code == eHTTP_GATEWAY_TIMEOUT) {
				info.field_03[0] = 'P';
			}
			else {
				if (pPRESENT_FLAG != NULL) {
					if ((strncmp(pPRESENT_FLAG, SWIPED_ENTRY,    1) == COMPARE_OK) ||
						(strncmp(pPRESENT_FLAG, PROXIMITY_ENTRY, 1) == COMPARE_OK)) {
						info.field_03[0] = 'T';
					}
					else if (strncmp(pPRESENT_FLAG, MANUAL_ENTRY, 1) == COMPARE_OK) {
						info.field_03[0] = 'E';
					}
					else {
						debug_sprintf(szDbgMsg, "%s: ERROR! PRESENT_FLAG not supported", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						info.field_03[0] = 'T'; //Defaulting to T, please see if it is ok!!!
						//return;
					}
				}
				else {
					debug_sprintf(szDbgMsg, "%s: ERROR! PRESENT_FLAG is NULL", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					return;
				}
			}
		}
		else {
			debug_sprintf(szDbgMsg, "%s: ERROR! COMMAND is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return;
		}
	}
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! group is for PAYMENT only", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

	fillSpace(sizeof(info.field_04), info.field_04);
	fillSpace(sizeof(info.field_05), info.field_05);
	fillSpace(sizeof(info.field_06), info.field_06);
	fillSpace(sizeof(info.field_07), info.field_07);
	fillSpace(sizeof(info.field_08), info.field_08);
	fillSpace(sizeof(info.field_09), info.field_09);
	strcpy(info.field_10, pENCRYPTION_PAYLOAD);

	strcpy(buffer, info.group);
	strcat(buffer, info.field_01);
	strcat(buffer, info.field_02);
	strcat(buffer, info.field_03);
	strcat(buffer, info.field_04);
	strcat(buffer, info.field_05);
	strcat(buffer, info.field_06);
	strcat(buffer, info.field_07);
	strcat(buffer, info.field_08);
	strcat(buffer, info.field_09);
	strncat(buffer, info.field_10, strlen(info.field_10));

	int len = strlen(buffer);
	*ptr = (char*)calloc(len+1, sizeof(char));
	strcpy(*ptr, buffer);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

void composeG027(char** ptr, _sSCA_XML_REQUEST *req_xml_info)
{
	return;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char buffer[4+77+1] = {0};
	_sG027_ info;

	memset(&info, 0x00, sizeof(info));
	char* pTRACK_DATA = NULL;

	pTRACK_DATA = getXmlTagValue(eTRACK_DATA, req_xml_info);

	if (pTRACK_DATA == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! TRACK_DATA is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

	strcpy(info.group, "G027");

	if (strchr(pTRACK_DATA, '=') != NULL) {
		strcpy(info.field_01, "2");
	} else {
		strcpy(info.field_01, "1");
	}

	int flen = sizeof(info.field_02) - 1;
    strcpy(info.field_02, "");
    strncat(info.field_02, pTRACK_DATA, flen);
	precedeSpace(sizeof(info.field_02), info.field_02);

	strcpy(buffer, info.group);
	strcat(buffer, info.field_01);
	strcat(buffer, info.field_02);

	if (strlen(buffer)!= sizeof(buffer)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! buffer not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

	int len = strlen(buffer);
	*ptr = (char*)calloc(len+1, sizeof(char));
	strcpy(*ptr, buffer);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

void composeG028(char** ptr, sGLOBALS* GLOBAL ,sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info)
{
	char *pszPipe		= NULL;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if (GCONFIG->eTokenMode != eTOKEN_TUT)
		return;

	/*
	 *						Token Utilization
	 *
	 * field	description		data	position	length	valid
	 * #						type						values
	 *
	 * 01		Token			an		1-19		19		Right-justified, space filled
	 * 02		Token ID		an		20-25		6
	 * 03		Card Exp Date	an		26-29		4		MMYY. use space when not applicable or de-token conversion
	 */

	char buffer[4+29+1] = {0};
	_sG028_ info;

	char* pCARD_TOKEN = getXmlTagValue(eCARD_TOKEN, req_xml_info);

	char* pSAF_TRAN = getXmlTagValue(eSAF_TRAN, req_xml_info);
	if (pSAF_TRAN == NULL) {
		debug_sprintf(szDbgMsg, "%s: pSAFTRAN is NULL, so it is not SAF transaction", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pSAF_TRAN = "0";
	}

	if (pCARD_TOKEN == NULL) {

		char* pCTROUTD = getXmlTagValue(eCTROUTD, req_xml_info);

		if (pCTROUTD == NULL) {
			debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return ;
		}

		char cCardToken[76+1] = {0};
		char* pEnd = NULL;
		char *pCardTkn = NULL;
		int iLen = 0;

		memset(&info, 0x00, sizeof(info));
		getVoidData(pCTROUTD,cCardToken, NULL, NULL,GLOBAL);

		/*
		When Card token was stored in vhi_voides.txt,
		'C' was prepended to card token ,
		Retrieving Card Token without prepended 'C'*/

		if('C' == cCardToken[0])
		{
			pCardTkn = &(cCardToken[1]);
			pEnd = strchr(cCardToken, '=');

			if (pEnd != NULL) {
				iLen = pEnd - pCardTkn;
				strcpy(info.field_01, "");
				strncat(info.field_01, pCardTkn, iLen);
				precedeSpace(sizeof(info.field_01), info.field_01);
			}
			strcpy(info.group, "G028");
			fillSpace(sizeof(info.field_02), info.field_02);
			fillSpace(sizeof(info.field_03), info.field_03);

			strcpy(buffer, info.group);
			strcat(buffer, info.field_01);
			strcat(buffer, info.field_02);
			strcat(buffer, info.field_03);


			if (strlen(buffer) != sizeof(buffer)-1) {
				debug_sprintf(szDbgMsg, "%s: ERROR! bytes not exact", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return;
			}

			int len = strlen(buffer);
			*ptr = (char*)calloc(len+1, sizeof(char));
			strcpy(*ptr, buffer);

			debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return;
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: ERROR! CARD_TOKEN is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return;
		}
	}

	memset(&info, 0x00, sizeof(info));
	strcpy(info.group, "G028");

	if(strncmp(pSAF_TRAN, "1", 1) == COMPARE_OK)
	{
		pszPipe = strchr(pCARD_TOKEN, '/'); //Praveen_P1: 24 June 2016 For SAFed transaction, LEGACY tokens with / instead of |
	}
	else
	{
		pszPipe = strchr(pCARD_TOKEN, '|');
	}

	if(pszPipe == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Token Id not present, so OMNI token", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(info.field_01, pCARD_TOKEN); //added

		precedeSpace(sizeof(info.field_01), info.field_01);

		fillSpace(sizeof(info.field_02), info.field_02); //Use 6 spaces for Omni Token

	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Token Id present, so LEGACY token", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		/*token*/

		strncpy(info.field_01, pCARD_TOKEN, (pszPipe - pCARD_TOKEN));

		debug_sprintf(szDbgMsg, "%s: info.field_01  = %s", __FUNCTION__, info.field_01);
		APP_TRACE(szDbgMsg); //Praveen_P1: Please delete this one!

		precedeSpace(sizeof(info.field_01), info.field_01);

		/*end*/
#if 0
		/*token id*/
		pCARD_TOKEN += token_len;
		if (strncmp(pCARD_TOKEN, "|", 1) == COMPARE_OK) {
			pCARD_TOKEN++; //skip pipe |
		}
#endif
		pszPipe++; //Skip PIPE |

        strcpy(info.field_02, "");
        strncat(info.field_02, pszPipe, sizeof(info.field_02)-1);

		/*end*/
	}

	/*card exp*/
	//MMYY
	char* pEXP_MONTH = getXmlTagValue(eEXP_MONTH, req_xml_info);
	char* pEXP_YEAR = getXmlTagValue(eEXP_YEAR, req_xml_info);

	if (pEXP_MONTH != NULL && pEXP_YEAR != NULL) {
        strcpy(info.field_03, "");
		strncat(info.field_03, pEXP_MONTH, 2);
		strncat(info.field_03, pEXP_YEAR, 2);


	} else {
#if 0
		char tmpdate[4+1] = {0};
		strcpy(tmpdate, getEnv("DHI", "debug_expdate"));

		if (strlen(tmpdate) > 1) { //temporary
			strncpy(info.field_03, tmpdate, 4);
		} else {
#endif
			char* pTRACK_DATA = getXmlTagValue(eTRACK_DATA, req_xml_info);

			if (pTRACK_DATA != NULL) {
				char* endptr = strchr(pTRACK_DATA, '=');
				int i = endptr - pTRACK_DATA;
				char YY[2+1] = {0};
				char MM[2+1] = {0};

				pTRACK_DATA += i; //skip PAN -> =YYMMXXXX

				if (*pTRACK_DATA == '=')
					pTRACK_DATA++; //skip = -> YYMMXXXX

				strncat(YY, pTRACK_DATA, 2); //copy YY
				pTRACK_DATA += 2; //skip YY -> MMXXX
				strncat(MM, pTRACK_DATA, 2); //copy MM

                strcpy(info.field_03, "");
				strncat(info.field_03, MM, 2);
				strncat(info.field_03, YY, 2);
			}
			else {
				fillSpace(sizeof(info.field_03), info.field_03);
			}
		//}
	}
	/*end*/

	strcpy(buffer, info.group);
	strcat(buffer, info.field_01);
	strcat(buffer, info.field_02);
	strcat(buffer, info.field_03);

	if (strlen(buffer) != sizeof(buffer)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! bytes not exact", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

	int len = strlen(buffer);
	*ptr = (char*)calloc(len+1, sizeof(char));
	strcpy(*ptr, buffer);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}
    
//  EBT WIC Merchant ID
void composeG029(char** ptr, _sSCA_XML_REQUEST *req_xml_info)
{
    char buffer[4+15+1] = {0};
    _sG029_ info;
    
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    // TODO: WIC not implemented
    return;
    
    memset(&info, 0x00, sizeof(info));
    strcpy(info.group, "G029");
    
    // TODO: fill field_01 with WIC merchant ID
    
    strcpy(buffer, info.group);
    strcat(buffer, info.field_01);
    
    int len = strlen(buffer);
    *ptr = (char*)calloc(len+1, sizeof(char));
    strcpy(*ptr, buffer);
    
    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);
}

//  EBT WIC Pass-thru Data Field #1
void composeG030(char** ptr, _sSCA_XML_REQUEST *req_xml_info)
{
    char buffer[4+999+1] = {0};
    _sG030_ info;
    
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    // TODO: WIC not implemented
    return;

    memset(&info, 0x00, sizeof(info));
    strcpy(info.group, "G030");
    
    // TODO: fill field_01 with pass through data

    strcpy(buffer, info.group);
    strcat(buffer, info.field_01);

    int len = strlen(buffer);
    *ptr = (char*)calloc(len+1, sizeof(char));
    strcpy(*ptr, buffer);
    
    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);
}

//  EBT WIC Pass-thru Data Field #2
void composeG031(char** ptr, _sSCA_XML_REQUEST *req_xml_info)
{
    char buffer[4+999+1] = {0};
    _sG031_ info;
    
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    // TODO: WIC not implemented
    return;

    memset(&info, 0x00, sizeof(info));
    strcpy(info.group, "G031");
    
    // TODO: fill field_01 with pass through data
    
    strcpy(buffer, info.group);
    strcat(buffer, info.field_01);
    
    int len = strlen(buffer);
    *ptr = (char*)calloc(len+1, sizeof(char));
    strcpy(*ptr, buffer);
    
    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);
}

    // composeG035() is in emv.c
    // composeG036() is in emv.c

void composeG038(char** ptr, _sSCA_XML_REQUEST *req_xml_info)
{
    char buffer[4+75+1] = {0};

    _sG038_ info;

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);

    char* pUSER_DEFINED1 = getXmlTagValue(eUSER_DEFINED1, req_xml_info);
    char* pUSER_DEFINED2 = getXmlTagValue(eUSER_DEFINED2, req_xml_info);
    char* pUSER_DEFINED3 = getXmlTagValue(eUSER_DEFINED3, req_xml_info);

	if (pUSER_DEFINED1 == NULL &&
			pUSER_DEFINED2 == NULL &&
			pUSER_DEFINED3 == NULL) {
		debug_sprintf(szDbgMsg, "%s: no User Defined Tags, Returning", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return ;
	}

	 memset(&info, 0x00, sizeof(info));
	 strcpy(info.group, "G038");

	 if(pUSER_DEFINED1 == NULL)
	 {
		 fillSpace(sizeof(info.field_01), info.field_01);
	 }
	 else
	 {
		 strcpy(info.field_01, "");
		 strncat(info.field_01, pUSER_DEFINED1, (sizeof(info.field_01) - 1));
		 appendSpace(sizeof(info.field_01), info.field_01);
	 }

	 if(pUSER_DEFINED2 == NULL)
	 {
		 fillSpace(sizeof(info.field_02), info.field_02);
	 }
	 else
	 {
		 strcpy(info.field_02, "");
		 strncat(info.field_02, pUSER_DEFINED2, (sizeof(info.field_02) - 1));
		 appendSpace(sizeof(info.field_02), info.field_02);
	 }

	 if(pUSER_DEFINED3 == NULL)
	 {
		 fillSpace(sizeof(info.field_03), info.field_03);
	 }
	 else
	 {
		 strcpy(info.field_03, "");
		 strncat(info.field_03, pUSER_DEFINED3, (sizeof(info.field_03) - 1));
		 appendSpace(sizeof(info.field_03), info.field_03);
	 }

	 strcpy(buffer, info.group);
	 strcat(buffer, info.field_01);
	 strcat(buffer, info.field_02);
	 strcat(buffer, info.field_03);


	if (strlen(buffer) != sizeof(buffer)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! bytes not exact", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

	int len = strlen(buffer);
	*ptr = (char*)calloc(len+1, sizeof(char));
	strcpy(*ptr, buffer);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

void composeG043(char** ptr, _sSCA_XML_REQUEST *req_xml_info)
{
    char buffer[4+2650+1] 		 	= {0};
    int  index						= 0;

    _sG043_ info;

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);

    _Level3Details_ * level3Details = (_Level3Details_ *)getXmlTagValue(eLEVEL3_ITEMS, req_xml_info);

	if (level3Details == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: no Level 3 Items, Returning", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return ;
	}

	 memset(&info, 0x00, sizeof(info));

	 strcpy(buffer, "G043");

	 debug_sprintf(szDbgMsg, "%s: Number of Line Items in Level3 %d", __FUNCTION__, level3Details->iNumberofLineItems);
	 APP_TRACE(szDbgMsg);

	 for(index = 0; index < level3Details->iNumberofLineItems; index++)
	 {

		 debug_sprintf(szDbgMsg, "%s: Adding %d Line item to G043", __FUNCTION__, index+1);
		 APP_TRACE(szDbgMsg);

		 if(strlen(level3Details->level3Dtls[index].field_01) > 0)
		 {
			 precedeSpace(sizeof(level3Details->level3Dtls[index].field_01), level3Details->level3Dtls[index].field_01);
		 }
		 else
		 {
			 fillSpace(sizeof(level3Details->level3Dtls[index].field_01), level3Details->level3Dtls[index].field_01);
		 }
		 strcat(buffer, level3Details->level3Dtls[index].field_01);

		 if(strlen(level3Details->level3Dtls[index].field_02) > 0)
		 {
			 precedeSpace(sizeof(level3Details->level3Dtls[index].field_02), level3Details->level3Dtls[index].field_02);
		 }
		 else
		 {
			 fillSpace(sizeof(level3Details->level3Dtls[index].field_02), level3Details->level3Dtls[index].field_02);
		 }
		 strcat(buffer, level3Details->level3Dtls[index].field_02);

		 if(strlen(level3Details->level3Dtls[index].field_03) > 0)
		 {
			 removeDecimalPoint( level3Details->level3Dtls[index].field_03); //Praveen_P1: Not sure whether it is correct, need to get clarity from Vantiv
			 precedeZero(sizeof(level3Details->level3Dtls[index].field_03), level3Details->level3Dtls[index].field_03);
		 }
		 else
		 {
			 fillZero(sizeof(level3Details->level3Dtls[index].field_03), level3Details->level3Dtls[index].field_03);
		 }
		 strcat(buffer, level3Details->level3Dtls[index].field_03);

		 if(strlen(level3Details->level3Dtls[index].field_04) > 0)
		 {
			 //Only one char, nothing do do
		 }
		 else
		 {
			 fillZero(sizeof(level3Details->level3Dtls[index].field_04), level3Details->level3Dtls[index].field_04);
		 }
		 strcat(buffer, level3Details->level3Dtls[index].field_04);

		 if(strlen(level3Details->level3Dtls[index].field_05) > 0)
		 {
			 precedeZero(sizeof(level3Details->level3Dtls[index].field_05), level3Details->level3Dtls[index].field_05);
		 }
		 else
		 {
			 fillZero(sizeof(level3Details->level3Dtls[index].field_05), level3Details->level3Dtls[index].field_05);
		 }
		 strcat(buffer, level3Details->level3Dtls[index].field_05);

		 if(strlen(level3Details->level3Dtls[index].field_06) > 0)
		 {
			 //Only one char, nothing do do
		 }
		 else
		 {
			 fillZero(sizeof(level3Details->level3Dtls[index].field_06), level3Details->level3Dtls[index].field_06);
		 }
		 strcat(buffer, level3Details->level3Dtls[index].field_06);

		 if(strlen(level3Details->level3Dtls[index].field_07) > 0)
		 {
			 precedeSpace(sizeof(level3Details->level3Dtls[index].field_07), level3Details->level3Dtls[index].field_07);
		 }
		 else
		 {
			 fillZero(sizeof(level3Details->level3Dtls[index].field_07), level3Details->level3Dtls[index].field_07);
		 }
		 strcat(buffer, level3Details->level3Dtls[index].field_07);

		 if(strlen(level3Details->level3Dtls[index].field_08) > 0)
		 {
			 removeDecimalPoint( level3Details->level3Dtls[index].field_08); //Praveen_P1: Not sure whether it is correct, need to get clarity from Vantiv
			 precedeZero(sizeof(level3Details->level3Dtls[index].field_08), level3Details->level3Dtls[index].field_08);
		 }
		 else
		 {
			 fillZero(sizeof(level3Details->level3Dtls[index].field_08), level3Details->level3Dtls[index].field_08);
		 }
		 strcat(buffer, level3Details->level3Dtls[index].field_08);

		 if(strlen(level3Details->level3Dtls[index].field_09) > 0)
		 {
			 precedeZero(sizeof(level3Details->level3Dtls[index].field_09), level3Details->level3Dtls[index].field_09);
		 }
		 else
		 {
			 fillZero(sizeof(level3Details->level3Dtls[index].field_09), level3Details->level3Dtls[index].field_09);
		 }
		 strcat(buffer, level3Details->level3Dtls[index].field_09);

		 if(strlen(level3Details->level3Dtls[index].field_10) > 0)
		 {
			 //Only one char, nothing do do
		 }
		 else
		 {
			 fillZero(sizeof(level3Details->level3Dtls[index].field_10), level3Details->level3Dtls[index].field_10);
		 }
		 strcat(buffer, level3Details->level3Dtls[index].field_10);
	 }

	 int len = strlen(buffer);

	 if(len < 1000) //Printing only if length of the buffer is less than szDbgMsg
	 {
		 debug_sprintf(szDbgMsg, "length= %d, buffer= [%s]", len, buffer);
		 APP_TRACE(szDbgMsg);
	 }

	 *ptr = (char*)calloc(len+1, sizeof(char));
	 strcpy(*ptr, buffer);

	 debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	 APP_TRACE(szDbgMsg);
}

void composeG045(char** ptr, _sSCA_XML_REQUEST *req_xml_info)
{
    char buffer[4+999+1] = {0};
    char temp[10];
    int  length = 0;
    int  final_length = 0;

    _sG045_ info;

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);

    char* pPROMOCODE = getXmlTagValue(ePROMO_NEEDED, req_xml_info);

	if (pPROMOCODE == NULL) {
		debug_sprintf(szDbgMsg, "%s: no Promo Code, Returning", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return ;
	}
#if 0
	else if(strcmp(pPROMOCODE, "FALSE") == 0)
	{
		debug_sprintf(szDbgMsg, "%s: Promo Code Needed is FALSE, Returning", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return ;
	}
#endif
	memset(temp, 0x00, sizeof(temp));
    memset(&info, 0x00, sizeof(info));
    strcpy(info.group, "G045");

    temp[0] = 0X70;
    temp[1] = 0X01;
    temp[2] = 0X00;
    temp[3] = 0X62;
    temp[4] = 0X04;

    memcpy(&temp[5], pPROMOCODE, 4); //Assuming that we have tag of only 4

    length = 5 + strlen(pPROMOCODE);

    b64_encode((UINT8*)temp, (UINT8*)info.field_01, length, &final_length);

    debug_sprintf(szDbgMsg, "length= %d, final= %d, info.field_01 = %s", length, final_length, info.field_01);
    APP_TRACE(szDbgMsg);


    strcpy(buffer, info.group);
    strcat(buffer, info.field_01);

    int len = strlen(buffer);

    debug_sprintf(szDbgMsg, "length= %d, buffer= [%s]", len, buffer);
    APP_TRACE(szDbgMsg);

    *ptr = (char*)calloc(len+1, sizeof(char));
    strcpy(*ptr, buffer);

    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);
}

void append610Group(enum eGROUP_IDX group, char** buffer, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* pGroup = NULL;
	int grouplen = 0;
	int bufferlen = 0;

	if (*buffer == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Base request is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

	bufferlen = strlen(*buffer);
	if (bufferlen <= 0) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Base request 0", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

	switch( group)
	{

	case eGROUP001:{ //Praveen_P1: 21 Sep 2016, sending Merchant Reference Number if present
	    	composeG001(&pGroup, req_xml_info);
			if (pGroup == NULL)
			{
				return;
			}

			grouplen = strlen(pGroup);
			if (grouplen <= 0)
			{
				debug_sprintf(szDbgMsg, "%s: ERROR! Group message", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if (pGroup)
					free(pGroup);
				return;
			}

			*buffer = (char*)realloc(*buffer, bufferlen + grouplen + 2); // +2 = GS + NULL
			strcat(*buffer, pGroup);
			appendHexGs(*buffer);
			break;
	}

	case eGROUP004:{ //Praveen_P1: 04 Nov 2016, sending Merchant Category Code if present
			composeG004(&pGroup, GCONFIG, req_xml_info);
			if (pGroup == NULL)
			{
				return;
			}

			grouplen = strlen(pGroup);
			if (grouplen <= 0)
			{
				debug_sprintf(szDbgMsg, "%s: ERROR! Group message", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if (pGroup)
					free(pGroup);
				return;
			}

			*buffer = (char*)realloc(*buffer, bufferlen + grouplen + 2); // +2 = GS + NULL
			strcat(*buffer, pGroup);
			appendHexGs(*buffer);
			break;
	}

	case eGROUP009: {
		composeG009(&pGroup, GCONFIG, req_xml_info);
		if (pGroup == NULL) {
			return;
		}

		grouplen = strlen(pGroup);
		if (grouplen <= 0) {
			debug_sprintf(szDbgMsg, "%s: ERROR! Group message", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if (pGroup)
				free(pGroup);
			return;
		}

		*buffer = (char*)realloc(*buffer, bufferlen + grouplen + 2); // +2 = GS + NULL
		strcat(*buffer, pGroup);
		appendHexGs(*buffer);
		break;
	}
	case eGROUP014: {
		composeG014(&pGroup, GLOBAL, req_xml_info);
		if (pGroup == NULL) {
			return;
		}

		grouplen = strlen(pGroup);
		if (grouplen <= 0) {
			debug_sprintf(szDbgMsg, "%s: ERROR! Group message", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if (pGroup)
				free(pGroup);
			return;
		}

		*buffer = (char*)realloc(*buffer, bufferlen + grouplen +2); // +2 = GS + NULL
		strcat(*buffer, pGroup);
		appendHexGs(*buffer);
		break;
	}
    // G015 used for EBT and FSA. Multiple amounts in group.
    case eGROUP015:
        composeG015(&pGroup, req_xml_info);
        if (pGroup == NULL) {
            return;
        }
        
        grouplen = strlen(pGroup);
        if (grouplen <= 0) {
            debug_sprintf(szDbgMsg, "%s: ERROR! Group message", __FUNCTION__);
            APP_TRACE(szDbgMsg);
            
            if (pGroup)
                free(pGroup);
            return;
        }
        
        *buffer = (char*)realloc(*buffer, bufferlen + grouplen + 2); // +2 = GS + NULL
        strcat(*buffer, pGroup);
        appendHexGs(*buffer);
        break;
            
    case eGROUP018:
        composeG018(&pGroup, GLOBAL, req_xml_info);
        if (pGroup == NULL) {
            return;
        }
        
        grouplen = strlen(pGroup);
        if (grouplen <= 0) {
            debug_sprintf(szDbgMsg, "%s: ERROR! Group message", __FUNCTION__);
            APP_TRACE(szDbgMsg);
            
            if (pGroup)
                free(pGroup);
            return;
        }
        
        *buffer = (char*)realloc(*buffer, bufferlen + grouplen +2); // +2 = GS + NULL
        strcat(*buffer, pGroup);
        appendHexGs(*buffer);
        break;
            
	case eGROUP023: {
		composeG023(&pGroup, req_xml_info);
		if (pGroup == NULL) {
			return;
		}

		grouplen = strlen(pGroup);
		if (grouplen <= 0) {
			debug_sprintf(szDbgMsg, "%s: ERROR! Group message", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if (pGroup)
				free(pGroup);
			return;
		}

		char* pTIP = NULL;

		if ((pTIP = getXmlTagValue(eTIP_AMOUNT, req_xml_info)) != NULL) {
			*buffer = (char*)realloc(*buffer, bufferlen + grouplen + 2); // +2 = GS + NULL
			strcat(*buffer, pGroup);
			appendHexGs(*buffer);
		}
		else {
			if (pGroup)
				free(pGroup);
			return; //return if TIP_AMOUNT is NULL
		}

		break;
	}
	case eGROUP026: {
		composeG026(&pGroup, GLOBAL, GCONFIG, req_xml_info);

		if (pGroup == NULL) {
			return;
		}

		grouplen = strlen(pGroup);
		if (grouplen <= 0) {
			debug_sprintf(szDbgMsg, "%s: ERROR! Group message", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if (pGroup)
				free(pGroup);
			return;
		}

		*buffer = (char*)realloc(*buffer, bufferlen + grouplen + 2); // +2 = GS + NULL
		strcat(*buffer, pGroup);
		appendHexGs(*buffer);
		break;
	}
	case eGROUP027: {
		composeG027(&pGroup, req_xml_info);
		if (pGroup == NULL) {
			return;
		}

		grouplen = strlen(pGroup);
		if (grouplen <= 0) {
			debug_sprintf(szDbgMsg, "%s: ERROR! Group message", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if (pGroup)
				free(pGroup);
			return;
		}

		*buffer = (char*)realloc(*buffer, bufferlen + grouplen + 2); // +2 = GS + NULL
		strcat(*buffer, pGroup);
		appendHexGs(*buffer);
		break;
	}
	case eGROUP028: {
		composeG028(&pGroup,GLOBAL, GCONFIG, req_xml_info);
		if (pGroup == NULL) {
			return;
		}

		grouplen = strlen(pGroup);
		if (grouplen <= 0) {
			debug_sprintf(szDbgMsg, "%s: ERROR! Group message", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if (pGroup)
				free(pGroup);
			return;
		}

		*buffer = (char*)realloc(*buffer, bufferlen + grouplen + 2); // +2 = GS + NULL
		strcat(*buffer, pGroup);
		appendHexGs(*buffer);
		break;
	}
            
    case eGROUP029:
        composeG029(&pGroup, req_xml_info);
        if (pGroup == NULL) {
            return;
        }
        *buffer = (char*)realloc(*buffer, bufferlen + grouplen + 2); // +2 = GS + NULL
        strcat(*buffer, pGroup);
        appendHexGs(*buffer);
        break;
        
    case eGROUP030:
        composeG030(&pGroup, req_xml_info);
        if (pGroup == NULL) {
            return;
        }
        *buffer = (char*)realloc(*buffer, bufferlen + grouplen + 2); // +2 = GS + NULL
        strcat(*buffer, pGroup);
        appendHexGs(*buffer);
        break;
        
    case eGROUP031:
        composeG031(&pGroup, req_xml_info);
        if (pGroup == NULL) {
            return;
        }
        *buffer = (char*)realloc(*buffer, bufferlen + grouplen + 2); // +2 = GS + NULL
        strcat(*buffer, pGroup);
        appendHexGs(*buffer);
        break;
       
    // eGROUP034 - fill in values
    case eGROUP034:
        composeG034(&pGroup, req_xml_info, GCONFIG);
        if (pGroup == NULL)
        {
            return;
        }
        
        grouplen = strlen(pGroup);
        if (grouplen <= 0)
        {
            debug_sprintf(szDbgMsg, "%s: ERROR! Group message", __FUNCTION__);
            APP_TRACE(szDbgMsg);
            
            if (pGroup)
                free(pGroup);
            return;
        }
        
        *buffer = (char*)realloc(*buffer, bufferlen + grouplen + 2); // +2 = GS + NULL
        strcat(*buffer, pGroup);
        appendHexGs(*buffer);
        break;
            
    // eGROUP035 has EMV data
    case eGROUP035:
        composeG035(&pGroup, req_xml_info, GLOBAL);
        if (pGroup == NULL)
        {
            return;
        }
        
        grouplen = strlen(pGroup);
        if (grouplen <= 0)
        {
            debug_sprintf(szDbgMsg, "%s: ERROR! Group message", __FUNCTION__);
            APP_TRACE(szDbgMsg);
            
            if (pGroup)
                free(pGroup);
            return;
        }
        
        *buffer = (char*)realloc(*buffer, bufferlen + grouplen + 2); // +2 = GS + NULL
        strcat(*buffer, pGroup);
        appendHexGs(*buffer);
        break;
            
    // eGroup036 has EMV online PIN block
    case eGROUP036:
        composeG036(&pGroup, req_xml_info, GLOBAL);
        if (pGroup == NULL)
        {
            return;
        }
        
        grouplen = strlen(pGroup);
        if (grouplen <= 0)
        {
            debug_sprintf(szDbgMsg, "%s: ERROR! Group message", __FUNCTION__);
            APP_TRACE(szDbgMsg);
            
            if (pGroup)
                free(pGroup);
            return;
        }
        
        *buffer = (char*)realloc(*buffer, bufferlen + grouplen + 2); // +2 = GS + NULL
        strcat(*buffer, pGroup);
        appendHexGs(*buffer);
        break;

        // eGroup038 has User Defined Data
	case eGROUP038:
		composeG038(&pGroup, req_xml_info);
		if (pGroup == NULL)
		{
			return;
		}

		grouplen = strlen(pGroup);
		if (grouplen <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: ERROR! Group message", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if (pGroup)
				free(pGroup);
			return;
		}

		*buffer = (char*)realloc(*buffer, bufferlen + grouplen + 2); // +2 = GS + NULL
		strcat(*buffer, pGroup);
		appendHexGs(*buffer);
		break;

	// eGroup043 has Level 3 Descriptors
	case eGROUP043:
		composeG043(&pGroup, req_xml_info);
		if (pGroup == NULL)
		{
			return;
		}

		grouplen = strlen(pGroup);
		if (grouplen <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: ERROR! Group message", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if (pGroup)
				free(pGroup);
			return;
		}

		*buffer = (char*)realloc(*buffer, bufferlen + grouplen + 2); // +2 = GS + NULL
		strcat(*buffer, pGroup);
		appendHexGs(*buffer);
		break;

    // eGroup036 has Synchrony Promo Request
    case eGROUP045:
    	composeG045(&pGroup, req_xml_info);
		if (pGroup == NULL)
		{
			return;
		}

		grouplen = strlen(pGroup);
		if (grouplen <= 0)
		{
			debug_sprintf(szDbgMsg, "%s: ERROR! Group message", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if (pGroup)
				free(pGroup);
			return;
		}

		*buffer = (char*)realloc(*buffer, bufferlen + grouplen + 2); // +2 = GS + NULL
		strcat(*buffer, pGroup);
		appendHexGs(*buffer);
		break;

	default: {
		debug_sprintf(szDbgMsg, "%s: ERROR! Invalid ID", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		break;
	}
	}

	if (pGroup)
		free( pGroup);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

