/******************************************************************
*                      threadPool.c                               *
*******************************************************************
* Application: VHI                                                *
* Platform:    Mx9XX                                              *
* Language:    C                                                  *
* Lib used:    none                                               *
* Purpose:     Contains the apis related to thread pool           *
* 			                                                      *
*                                                                 *
* Created on: 10-Jul-2014                                         *
* History:                                                        *
* Date     Ver   Developer     Description                        *
* -------- ----  ------------  -----------------------------      *
*                 VFI                                             *
*                                                                 *
* ================================================================*
*                   Copyright, 1995 - 2002 VeriFone, Inc.         *
*                   2455 Augustine Drive                          *
*                   Santa Clara, CA 95054                         *
*                                                                 *
*                   All Rights Reserved.                          *
* ================================================================*/

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <semaphore.h>
#include <pthread.h>

#include "threadPool.h"
#include "appLog.h"
#include "logger.h"
#include "help_function.h"

/* Static Function Declarations */
static void * threadAction(void *);
static int addNewJobToQueue(THPOOLJOB_PTYPE );
static int removeJobFromQueue();

/* Static variables */
static THPOOL_STYPE		stThreadPool;
static int				giKeepRunning = 0;
static pthread_mutex_t 	gptJobQueueMutex;

/*
 * ============================================================================
 * Function Name: createThreadPool
 *
 * Description	: This api would create the pool of threads to do given JOB
 *
 * Input Params	: number of threads to create
 *
 * Output Params: EXIT_SUCCESS/EXIT_FAILURE
 * ============================================================================
 */
int createThreadPool(int iThreadCount)
{
	int			rv					= EXIT_SUCCESS;
	int			iIndex				= 0;
	int			iAppLogEnabled		= 0;
	char		szAppLogData[256]	= "";
	char		szAppLogDiag[256]	= "";

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));
	memset(szAppLogDiag, 0x00, sizeof(szAppLogDiag));

	iAppLogEnabled = isAppLogEnabled();

	while(1)
	{
		if(iThreadCount < 0)
		{
			debug_sprintf(szDbgMsg, "%s: Invalid Number of thread count passed!!! ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = EXIT_FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Thread Count [%d] ", __FUNCTION__, iThreadCount);
		APP_TRACE(szDbgMsg);

		memset(&stThreadPool, 0x00, sizeof(THPOOL_STYPE));

		/* Assigning thread count */
		stThreadPool.iThreadCount = iThreadCount;

		/* Allocating memory for thread ids */
		stThreadPool.ptThreads = (pthread_t *)malloc(iThreadCount * sizeof(pthread_t));
		if(stThreadPool.ptThreads == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while allocation memory for thread ids!!! ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Memory Allocation Failed For Thread IDs");
				strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
			}
			rv = EXIT_FAILURE;
			break;
		}
		memset(stThreadPool.ptThreads, 0x00, iThreadCount * sizeof(pthread_t));

		/* Allocating memory for Thread Pool Job Queue */
		stThreadPool.pstThPooljobqueue = (THPOOLJOBQUE_PTYPE)malloc(sizeof(THPOOLJOBQUE_STYPE));
		if(stThreadPool.pstThPooljobqueue == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while allocation memory for thread pool Job Queue!!! ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Memory Allocation Failed For Thread Pool Job Queues");
				strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
			}
			rv = EXIT_FAILURE;
			break;
		}
		memset(stThreadPool.pstThPooljobqueue, 0x00, sizeof(THPOOLJOBQUE_STYPE));

		/* Initialize semaphore */
		sem_init(&stThreadPool.pstThPooljobqueue->semaphore, 0, 0);  /* shared among threads, initial value */

		/* setting running flag for thread pool */
		giKeepRunning = 1;

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Creating %d Threads For Service Requests", iThreadCount);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
		}

		/* create threads in a pool */
		for(iIndex = 0; iIndex < iThreadCount; iIndex++)
		{
			pthread_create(&(stThreadPool.ptThreads[iIndex]), NULL, threadAction, NULL/* need to pass arguments*/);

			debug_sprintf(szDbgMsg, "%s: %d Thread created ", __FUNCTION__, iIndex+1);
			APP_TRACE(szDbgMsg);
		}

		if(pthread_mutex_init(&gptJobQueueMutex, NULL))//Initialize the Job Queue Mutex
		{
			debug_sprintf(szDbgMsg, "%s - Failed to create Job Queue mutex!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Failed To Initialize Job Queue Mutex");
				strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
			}
			rv = EXIT_FAILURE;
			break;
		}

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: deleteThreadPool
 *
 * Description	: This api would delete the pool of threads
 *
 * Input Params	: number of threads to create
 *
 * Output Params: EXIT_SUCCESS/EXIT_FAILURE
 * ============================================================================
 */
int deleteThreadPool()
{
	int				rv				= EXIT_SUCCESS;
	int				iIndex			= 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Ending thread pool running */
		giKeepRunning = 0;

		/* Awake idle threads waiting at semaphore */
		for(iIndex = 0; iIndex < stThreadPool.iThreadCount; iIndex++)
		{
			/*
			 * sem_post() increments (unlocks) the semaphore pointed to by sem.
			 * If the semaphore's value consequently becomes greater than zero,
			 * then another process or thread blocked in a sem_wait(3) call will be woken up
			 * and proceed to lock the semaphore
			 */
			if (sem_post(&stThreadPool.pstThPooljobqueue->semaphore))
			{
				debug_sprintf(szDbgMsg, "%s: Could not bypass sem_wait() ", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}

		/* Destroying Semaphore */
		if (sem_destroy(&stThreadPool.pstThPooljobqueue->semaphore) != 0)
		{
			debug_sprintf(szDbgMsg, "%s: Could not destroy semaphore", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		/* Wait for threads to finish */
		for(iIndex = 0; iIndex < stThreadPool.iThreadCount; iIndex++)
		{
				pthread_join(stThreadPool.ptThreads[iIndex], NULL);
		}

		/*Empty the JOB queue */
		//TODO

		/* Deallocate memory */
		free(stThreadPool.ptThreads);
		free(stThreadPool.pstThPooljobqueue);

		memset(&stThreadPool, 0x00, sizeof(THPOOL_STYPE));

		break;

	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: addJobToThreadPool
 *
 * Description	: This api would add the work to the thread pool
 *
 * Input Params	: Function pointer for thread to assign, arguments to that function
 *
 * Output Params: EXIT_SUCCESS/EXIT_FAILURE
 * ============================================================================
 */
int addJobToThreadPool(void *(*function_p)(int ), int arg_p)
{
	int				rv				= EXIT_SUCCESS;
	THPOOLJOB_PTYPE pstNewJob		= NULL;
	int				*piClientID		= NULL;


#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		/* Create New job */
		pstNewJob = (THPOOLJOB_PTYPE)malloc(sizeof(THPOOLJOB_STYPE));
		if(pstNewJob == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while allocation memory for new Job!!! ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = EXIT_FAILURE;
			break;
		}
		memset(pstNewJob, 0x00, sizeof(THPOOLJOB_STYPE));

		piClientID = (int *)malloc(sizeof(int) * 1);
		if(piClientID == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while allocation memory for client ID!!! ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = EXIT_FAILURE;
			break;
		}
		*piClientID = arg_p;

		debug_sprintf(szDbgMsg, "%s: arg_p [%d],  piClientID [%d]", __FUNCTION__, arg_p, *piClientID);
		APP_TRACE(szDbgMsg);

		/* Assign values to New Job */
		pstNewJob->function = function_p;
		pstNewJob->arg      = piClientID; //TODO Please make sure that you free this later

		/* Add New Job to the queue */

		/*Acquire the mutex to add new work to the JOB queue */
		acquireMutexLock(&gptJobQueueMutex, "Adding New Job");

		rv = addNewJobToQueue(pstNewJob);

		if(rv != EXIT_SUCCESS)
		{
			debug_sprintf(szDbgMsg, "%s: Error while adding new Job to the queue!!! ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			//TODO What to do here??
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Added new Job to the queue!!! ", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		/* Release the mutex after adding new work to the JOB queue */
		releaseMutexLock(&gptJobQueueMutex, "Adding New Job");
		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: addNewJobToQueue
 *
 * Description	: This api would add the given job to the queue
 *
 * Input Params	: Job
 *
 * Output Params: EXIT_SUCCESS/EXIT_FAILURE
 * ============================================================================
 */
static int addNewJobToQueue(THPOOLJOB_PTYPE pstNewJob)
{
	int				rv				= EXIT_SUCCESS;
	int				iVal			= 0;
	THPOOLJOB_PTYPE pstFistJob		= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		if(pstNewJob == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Input param is NULL!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = EXIT_FAILURE;
			break;
		}

		if(stThreadPool.pstThPooljobqueue == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No queue to add new Job!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = EXIT_FAILURE;
			break;
		}

		/* Setting next and previous of current job to NULL */
		pstNewJob->next = NULL;
		pstNewJob->prev = NULL;

		debug_sprintf(szDbgMsg, "%s: Current Job count [%d]", __FUNCTION__, stThreadPool.pstThPooljobqueue->iJobsCount);
		APP_TRACE(szDbgMsg);

		if(stThreadPool.pstThPooljobqueue->iJobsCount == 0)
		{
			debug_sprintf(szDbgMsg, "%s: No jobs in the queue, current new job is first one", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			stThreadPool.pstThPooljobqueue->head = pstNewJob;

			stThreadPool.pstThPooljobqueue->tail = pstNewJob;

		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Jobs are present in the queue, need to add this too", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* Adding new job at the starting of the linked list */

			pstFistJob = stThreadPool.pstThPooljobqueue->head;

			pstFistJob->prev = pstNewJob;

			pstNewJob->next = pstFistJob;

			stThreadPool.pstThPooljobqueue->head = pstNewJob;
		}

		/* Increment the Job count */
		(stThreadPool.pstThPooljobqueue->iJobsCount)++;

		debug_sprintf(szDbgMsg, "%s: Current Job count after adding new job[%d]", __FUNCTION__, stThreadPool.pstThPooljobqueue->iJobsCount);
		APP_TRACE(szDbgMsg);

		/*
		 * sem_post() increments (unlocks) the semaphore pointed to by sem.
		 * If the semaphore's value consequently becomes greater than zero,
		 * then another process or thread blocked in a sem_wait(3) call will be woken up
		 * and proceed to lock the semaphore
		 */
		sem_post(&stThreadPool.pstThPooljobqueue->semaphore); /* Increment the semaphore */

		/* Geting the semaphore value */
		sem_getvalue(&stThreadPool.pstThPooljobqueue->semaphore, &iVal); //This call is only for information and debugging purpose

		debug_sprintf(szDbgMsg, "%s: Semaphore value [%d]", __FUNCTION__, iVal);
		APP_TRACE(szDbgMsg);

		break;

	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}


/*
 * ============================================================================
 * Function Name: removeJobFromQueue
 *
 * Description	: This api would delete last job from the queue
 *
 * Input Params	: Job
 *
 * Output Params: EXIT_SUCCESS/EXIT_FAILURE
 * ============================================================================
 */
static int removeJobFromQueue()
{
	int				rv				= EXIT_SUCCESS;
	int				iVal			= 0;
	THPOOLJOB_PTYPE pstRemovedJob	= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{

		if(stThreadPool.pstThPooljobqueue == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: No queue to delete Job!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			rv = EXIT_FAILURE;
			break;
		}

		debug_sprintf(szDbgMsg, "%s: Current Job count [%d]", __FUNCTION__, stThreadPool.pstThPooljobqueue->iJobsCount);
		APP_TRACE(szDbgMsg);

		if(stThreadPool.pstThPooljobqueue->iJobsCount == 0)
		{
			debug_sprintf(szDbgMsg, "%s: No jobs in Queue to remove!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			break; //Nothing to do in this case
		}
		else if(stThreadPool.pstThPooljobqueue->iJobsCount == 1)
		{
			pstRemovedJob = stThreadPool.pstThPooljobqueue->tail;

			stThreadPool.pstThPooljobqueue->tail = NULL;
			stThreadPool.pstThPooljobqueue->head = NULL;
		}
		else
		{
			pstRemovedJob = stThreadPool.pstThPooljobqueue->tail;

			pstRemovedJob->prev->next = NULL;

			stThreadPool.pstThPooljobqueue->tail = pstRemovedJob->prev;
		}

		if(pstRemovedJob->arg != NULL)
		{
			free(pstRemovedJob->arg); //Memory is allocated for this so freeing it here!!!
		}

		/* Free the removed Job */
		free(pstRemovedJob);

		pstRemovedJob = NULL;

		/* Decrement the Job count */
		(stThreadPool.pstThPooljobqueue->iJobsCount)--;

		debug_sprintf(szDbgMsg, "%s: Current Job count after removing job[%d]", __FUNCTION__, stThreadPool.pstThPooljobqueue->iJobsCount);
		APP_TRACE(szDbgMsg);

		/* Geting the semaphore value */
		sem_getvalue(&stThreadPool.pstThPooljobqueue->semaphore, &iVal); //This call is only for information and debugging purpose

		debug_sprintf(szDbgMsg, "%s: Semaphore value [%d]", __FUNCTION__, iVal);
		APP_TRACE(szDbgMsg);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

/*
 * ============================================================================
 * Function Name: threadAction
 *
 * Description	: This api is the action done by each thread in the pool
 *
 * Input Params	:
 *
 * Output Params: None
 * ============================================================================
 */
static void * threadAction(void *arg)
{
	int				rv						= EXIT_SUCCESS;
	int  			arg_buff				= 0;
	int				iAppLogEnabled			= 0;
	pthread_t		threadID				= 0L;
	void*			(*func_buff)(int arg);
	char			szAppLogData[256]		= "";


#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter --- ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(szAppLogData, 0x00, sizeof(szAppLogData));

	iAppLogEnabled = isAppLogEnabled();

	threadID = pthread_self();

	debug_sprintf(szDbgMsg, "%s: My thread ID [%d]", __FUNCTION__, (int)threadID);
	APP_TRACE(szDbgMsg);

	if(iAppLogEnabled)
	{
		sprintf(szAppLogData, "Thread ID [%d] Got Created To Honor Service Requests", (int)threadID);
		addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_START_UP, szAppLogData, NULL);
	}

	while(giKeepRunning)
	{
		/* Wait on the semaphore */
		/*
		 * sem_wait() decrements (locks) the semaphore pointed to by sem.
		 * If the semaphore's value is greater than zero, then the decrement proceeds,
		 * and the function returns, immediately.
		 * If the semaphore currently has the value zero,
		 * then the call blocks until either it becomes possible to perform the decrement
		 * (i.e., the semaphore value rises above zero), or a signal handler interrupts the call.
		 */
		if (sem_wait(&stThreadPool.pstThPooljobqueue->semaphore))  /* WAITING until there is work in the queue */
		{
			debug_sprintf(szDbgMsg, "%s: Error while waiting for semaphore!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			//TODO What to do here???
		}

		/*Acquire the mutex to read the Job from the queue */
		acquireMutexLock(&gptJobQueueMutex, "Reading Job");

		if(stThreadPool.pstThPooljobqueue->tail != NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Job Present in the queue", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			/* assigning the function pointer and arg from the queue */
			func_buff = stThreadPool.pstThPooljobqueue->tail->function;

			arg_buff =  *(int *)stThreadPool.pstThPooljobqueue->tail->arg;

			/* Need to remove the read job from the queue */
			rv = removeJobFromQueue();
			if(rv != EXIT_SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while removing job from the queue", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				// TODO What to do here??
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: No Job in the queue to take up", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		/* Release the mutex after reading the job from the queue */
		releaseMutexLock(&gptJobQueueMutex, "Reading Job");

		if(func_buff != NULL)
		{
			func_buff(arg_buff);  /* run the assigned function */

			func_buff = NULL;
			arg_buff  = -1;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d] thread", __FUNCTION__, (int)threadID);
	APP_TRACE(szDbgMsg);

	return NULL;
}
