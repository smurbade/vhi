/*
 * free_data.c
 *
 *  Created on: Nov 25, 2013
 *      Author: Dexter M. Alberto
 */


#include "free_data.h"
#include "sca_xml_request.h"
#include "vhi_request.h"
#include "help_function.h"
#include "vhi_response.h"
#include "vhi_610_group.h"
#include "http_handler.h"
#include "vantiv_response.h"
#include "curl_connect.h"

void freeAllocatedData(sVHI_REQ * vhi_req_info, sVANTIV_RESPONSE * vrinfo, sVHI_RESPONSE_INFO *vhi_res_info, _sSCA_XML_REQUEST *req_xml_info)
{
	freeXmlData(req_xml_info);
	freeVhiRequestData(vhi_req_info);
	//freeHelpData();
	freeVHIResponseData(vhi_res_info);
	free610GroupData();
	freeVantivResponseData(vrinfo);
	freeCurlData();
}
