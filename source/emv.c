
#include <stdlib.h>
//#include <unistd.h>
#include <string.h>
#include <stdio.h>
//#include <svc.h>

#include "emv.h"
#include "logger.h"
#include "ber-tlv.h"
#include "vhi_610_group.h"
#include "help_function.h"

#ifdef LOGGING_ENABLED
static char    szDbgMsg[1024];
#endif

void b64_encode(const UINT8 *pchInBuf, UINT8 *pchOutBuf, int shInSize, int *shOutSize);
void b64_decode(const UINT8 *pchInBuf, UINT8 *pchOutBuf, int shInSize, int *shOutSize);
//static void char2bcd(UINT8* destination, UINT8* source, int sourceSize);

#ifdef OLD_EMV_TAG_METHOD
#define REQUIRED_COUNT  (eAPPINTERCHANGEPROFILE+ eAPPLICATIONVERSION+ eAPPPANSEQNUM+ eAPPTRANSACTIONCOUNTER+ eAUTHAMOUNT+ eCRYPTOGRAMINFODATA+ eCRYPTOGRAMTRANSACTIONTYPE+ eCRYPTOGRAMCURRCODE+ eCVMRESULT+ eINTERFACE_DEVICE_SERIAL_NUMBER+ eISSUERAPPLICATIONDATA+ eTERMINAL_APP_ID+ eTERMINAL_TRANSACTION_CODE+ eTERMINAL_TRANSACTION_DATE+ eTERMINALVERIFICATIONRESULT+ eTRANSACTIONCRYPTOGRAM+ eUNPREDICTABLENUMBER)

static const UINT8 abyTagHEADER_VERSION[] =                     { 0xDF, 0x01 };
static const UINT8 abyTagSOFTWARE_VERSION[] =                   { 0xDF, 0x21 };

static const UINT8 abyTagAPPINTERCHANGEPROFILE[] =              { 0x82 };
static const UINT8 abyTagAPPLICATIONVERSION[] =                 { 0x9F, 0x09 };
static const UINT8 abyTagAPPPANSEQNUM[] =                       { 0x5F, 0x34 };
static const UINT8 abyTagAPPTRANSACTIONCOUNTER[] =              { 0x9F, 0x36 };
static const UINT8 abyTagAUTHAMOUNT[] =                         { 0x9F, 0x02 };
static const UINT8 abyTagCRYPTOGRAMINFODATA[] =                 { 0x9F, 0x27 };
static const UINT8 abyTagCRYPTOGRAMTRANSACTIONTYPE[] =          { 0x9C };
static const UINT8 abyTagCRYPTOGRAMCURRCODE[] =                 { 0x5F, 0x2A };
static const UINT8 abyTagCUSTOMER_EXCLUSIVE_DATA[] =            { 0x9F, 0x7C };
static const UINT8 abyTagCVMRESULT[] =                          { 0x9F, 0x34 };
static const UINT8 abyTagDEDICATEDFILENAME[] =                  { 0x84 };
static const UINT8 abyTagEMV_CASHBACK_AMNT[] =                  { 0x9F, 0x03 };
static const UINT8 abyTagICC_FORM_FACTOR[] =                    { 0x9F, 0x6E };
static const UINT8 abyTagINTERFACE_DEVICE_SERIAL_NUMBER[] =     { 0x9F, 0x1E };
static const UINT8 abyTagISSUERAPPLICATIONDATA[] =              { 0x9F, 0x10 };
static const UINT8 abyTagTERMINAL_APP_ID[] =                    { 0x9F, 0x06 };
static const UINT8 abyTagTERMINAL_CAPABILITY_PROFILE[] =        { 0x9F, 0x33 };
static const UINT8 abyTagTERMINAL_TRANSACTION_CODE[] =          { 0x9F, 0x1A };
static const UINT8 abyTagTERMINAL_TRANSACTION_DATE[] =          { 0x9A };
static const UINT8 abyTagTERMINAL_TRANSACTION_QUALIFIERS[] =    { 0x9F, 0x66 };
static const UINT8 abyTagTERMINAL_TRANS_SEQ_CTR[] =             { 0x9F, 0x41 };
static const UINT8 abyTagTERMINALTYPE[] =                       { 0x9F, 0x35 };
static const UINT8 abyTagTERMINALVERIFICATIONRESULT[] =         { 0x95 };
static const UINT8 abyTagTRANSACTIONCRYPTOGRAM[] =              { 0x9F, 0x26 };
static const UINT8 abyTagTRANSACTION_TIME[] =                   { 0x9F, 0x21 };
static const UINT8 abyTagTRANS_CAT_CODE[] =                     { 0x9F, 0x53 };
static const UINT8 abyTagUNPREDICTABLENUMBER[] =                { 0x9F, 0x37 };

static const UINT8 abyTagAUTHORIZATION_RESPONSE_CODE[] =        { 0x8A };
static const UINT8 abyTagISSUER_AUTHENTICATION_DATA[] =         { 0x91 };
static const UINT8 abyTagEMV_ISSUER_SCRIPT_DATA71[] =           { 0x71 };
static const UINT8 abyTagEMV_ISSUER_SCRIPT_DATA72[] =           { 0x72 };
static const UINT8 abyTagISSUER_SCRIPT_COMMAND[] =              { 0x86 };
static const UINT8 abyTagISSUER_SCRIPT_IDENTIFIER[] =           { 0x9F, 0x18 };
static const UINT8 abyTagISSUER_SCRIPT_RESULTS[] =              { 0x9F, 0x5B };
#endif

static UINT8    header_version[5]=      {0xFF, 0x01, 0x02, 0x00, 0x01};

#define MIN(a,b) (((a)<(b))?(a):(b))

/***
 G034 - POS Identification Data
 
 These fields provide identification data for POS applications and associated devices. 
 It is required for applications using EMV chip card technology, and will ultimately be required for all 610 applications in the future.
 
 10 fields, a total of 82 bytes. Space fill if no data.
 ***/
void composeG034(char** ptr, _sSCA_XML_REQUEST *req_xml_info, sCONFIG * GCONFIG)
{
    _sG034_ info;
    
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    if (GCONFIG->emvenabled == false)       // true= G34 for all transactions, false= for
    {
    	debug_sprintf(szDbgMsg, "No group 34 - NOT EMV CAPABLE terminal");
    	APP_TRACE(szDbgMsg);
    	return;
    }
    
    char *pDEVTYPE= getXmlTagValue(eDEVTYPE, req_xml_info);
    char *pSERIAL_NUM= getXmlTagValue(eSERIAL_NUM, req_xml_info);
    
    memset(&info, 0x00, sizeof(_sG034_));

    memcpy(info.group, "G034", 4);

    // Field 1 & 2 - VAR name & version from
    sprintf(info.field_01, "%*s", (int)sizeof(info.field_01)- 1, "");
    sprintf(info.field_02, "%*s", (int)sizeof(info.field_02)- 1, "");
    
    // Field 3 & 4 - Gateway name & version from
    sprintf(info.field_03, "%*s", (int)sizeof(info.field_03)- 1, "");
    sprintf(info.field_04, "%*s", (int)sizeof(info.field_04)- 1, "");
    
    // Field 5 & 6 - POS name & version from
    sprintf(info.field_05, "%-*s", (int)sizeof(info.field_05)- 1, "VERIFONE");
    sprintf(info.field_06, "%*s", (int)sizeof(info.field_06)- 1, "");
    
    // Field 7 - Terminal make/ model from
    if (pDEVTYPE == NULL)
        sprintf(info.field_07, "%-*s", (int)sizeof(info.field_07)- 1, "MX9XX");      // just in case
    else
        sprintf(info.field_07, "%-*s", (int)sizeof(info.field_07)- 1, pDEVTYPE);
    
    // Field 8 & 9 -Terminal name & version from
    sprintf(info.field_08, "%-*s", (int)sizeof(info.field_08)- 1, "VHI");
    sprintf(info.field_09, "%-*s", (int)sizeof(info.field_09)- 1, G34_APP_VERSION);
    
    // Field 10 - Serial number
    if (pSERIAL_NUM == NULL)
        sprintf(info.field_10, "%*s", (int)sizeof(info.field_10)- 1, "");           // just in case
    else
        sprintf(info.field_10, "%-*s", (int)sizeof(info.field_10)- 1, pSERIAL_NUM);
    
    *ptr = (char*)calloc(sizeof(info), sizeof(char));
    
    strcpy(*ptr, info.group);
    strcat(*ptr, info.field_01);
    strcat(*ptr, info.field_02);
    strcat(*ptr, info.field_03);
    strcat(*ptr, info.field_04);
    strcat(*ptr, info.field_05);
    strcat(*ptr, info.field_06);
    strcat(*ptr, info.field_07);
    strcat(*ptr, info.field_08);
    strcat(*ptr, info.field_09);
    strcat(*ptr, info.field_10);
    
    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);
}

/***
 G035 – EMV Tag Data
 
 EMV tag data format is BER-TLV as defined in ISO/IEC 8825. The resultant encoded tags must then be Base64
 encoded into ASCII string format.
 
 All EMV tags arrive inside <EMV_TAGS>. They are TLV encoded as stings of hex digits.
 For example 0x82 0x02 0x58 0x00 will be "82025800".
 
 One field - Variable 999 maximum Base64 encoded ASCII string.
 
 TLV data is binary but after base64 encoding it can be treated as a string.
 
 Add binary FF01 to temp. Convert EMV_TAGS to binary at end of temp. Encode temp to bas634 in _sG065.
 
 FF20/21/22 sent by SCA if required.
***/
void composeG035(char** ptr, _sSCA_XML_REQUEST *req_xml_info, sGLOBALS* GLOBAL)
{
    _sG035_ info;
    int     final_length;
    int     length;
    int     tag_offset;
    char    temp[1000];     // tag data + FF01
    char* 	pCOMMAND = NULL;
    
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);

    pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);

    char* pPRESENT_FLAG = getXmlTagValue(ePRESENT_FLAG, req_xml_info);
    if ((pPRESENT_FLAG == NULL) || ((strncmp(pPRESENT_FLAG, EMV_CHIP_ENTRY, 1) != COMPARE_OK) && (strncmp(pPRESENT_FLAG, EMV_CTLS_ENTRY, 1) != COMPARE_OK)))
    {
    	if(pCOMMAND != NULL && (strcasecmp(pCOMMAND, "VOID") == COMPARE_OK) )
    	{
    		debug_sprintf(szDbgMsg, "Its Void Transaction, whether to include G035 depends on card type and presence of EMV tags");
    		APP_TRACE(szDbgMsg);
    	}
    	else
    	{
			debug_sprintf(szDbgMsg, "No group 35 - NOT EMV");
			APP_TRACE(szDbgMsg);
			return;
    	}
    }
    
    /*
     * Praveen_P1: According to EMV reversals requirements, we need to include EMV tags
     * based on the card type
     * BRAND		VOID [Card Not Present]		TOR				Reversal after card rejection
     * Visa			Don�t include				Don�t include 	Include
     * Mastercard	Don�t Include				Don�t Include	Don�t Include
     * Amex			Don�t Include				Include			Don�t Include
     * Discover		Don�t include				Include			Include
     *
     * for VOID card not present, we dont get EMV tags from the SCA thats why not checking specially for that case
     */

    if (GLOBAL->http.status_code == eHTTP_GATEWAY_TIMEOUT) {
		pCOMMAND = NULL;
		pCOMMAND = "VOID";
	}

    if(pCOMMAND == NULL)
    {
    	debug_sprintf(szDbgMsg, "COMMAND is NULL in the request"); //should not be the case
    	APP_TRACE(szDbgMsg);
    }
    else
    {
    	if(strcasecmp(pCOMMAND, "VOID") == COMPARE_OK) //VOID Command
    	{
			debug_sprintf(szDbgMsg, "%s: Its VOID Command", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			char* pPAYMENT_MEDIA = NULL;

			 if (GLOBAL->http.status_code == eHTTP_GATEWAY_TIMEOUT)
			 {
				 pPAYMENT_MEDIA = getXmlTagValue(ePAYMENT_MEDIA, req_xml_info);
			 }
			 else
			 {
				 char* pCTROUTD = getXmlTagValue(eCTROUTD, req_xml_info);

				 char cPaymentMedia[20 + 1];
				 memset(cPaymentMedia, 0x00, sizeof(cPaymentMedia));

				 getVoidData(pCTROUTD, NULL, NULL, cPaymentMedia, GLOBAL);

				 debug_sprintf(szDbgMsg, "%s: Payment media from the VOID file is [%s]", __FUNCTION__, cPaymentMedia);
				 APP_TRACE(szDbgMsg);

				 pPAYMENT_MEDIA = cPaymentMedia;
			 }

			if(pPAYMENT_MEDIA == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: No payment media in the request", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				if(strcasecmp(pPAYMENT_MEDIA, "VISA") == COMPARE_OK)
				{
					if(GLOBAL->http.status_code == eHTTP_GATEWAY_TIMEOUT)
					{
						debug_sprintf(szDbgMsg, "Its TOR case, VISA card type, no need to include G035");
						APP_TRACE(szDbgMsg);
						return;
					}
					else
					{
						debug_sprintf(szDbgMsg, "VISA card type, need to include G035 if EMV tags are present");
						APP_TRACE(szDbgMsg);
					}
				}
				else if((strcasecmp(pPAYMENT_MEDIA, "MASTERCARD") == COMPARE_OK) || ((strcasecmp(pPAYMENT_MEDIA, "MC") == COMPARE_OK)))
				{
					debug_sprintf(szDbgMsg, "MASTERCARD card type, no need to include G035");
					APP_TRACE(szDbgMsg);
					return;
				}
				else if(strcasecmp(pPAYMENT_MEDIA, "AMEX") == COMPARE_OK)
				{
					if(GLOBAL->http.status_code == eHTTP_GATEWAY_TIMEOUT)
					{
						debug_sprintf(szDbgMsg, "Its TOR case, AMEX card type, need to include G035 if EMV tags are present");
						APP_TRACE(szDbgMsg);
					}
					else
					{
						debug_sprintf(szDbgMsg, "AMEX card type, no need to include G035");
						APP_TRACE(szDbgMsg);
						return;
					}
				}
				else if((strcasecmp(pPAYMENT_MEDIA, "DISCOVER") == COMPARE_OK) || ((strcasecmp(pPAYMENT_MEDIA, "DISC") == COMPARE_OK)))
				{
					debug_sprintf(szDbgMsg, "DISCOVER card type, need to include G035 if EMV tags are present");
					APP_TRACE(szDbgMsg);
				}
				else if(strcasecmp(pPAYMENT_MEDIA, "JCB") == COMPARE_OK)
				{
					//Not sure what to do here
				}
				else if((strcasecmp(pPAYMENT_MEDIA, "DCCB") == COMPARE_OK) || ((strcasecmp(pPAYMENT_MEDIA, "DINERS") == COMPARE_OK)))
				{
					//Not sure what to do here
				}
			}
		}
    }

    char* pEMV_TAGS = getXmlTagValue(eEMV_TAGS, req_xml_info);
    if (pEMV_TAGS == NULL)
    {
        debug_sprintf(szDbgMsg, "Missing <EMV_TAGS>");
        APP_TRACE(szDbgMsg);
        return;
    }
    
    strcpy(info.group, "G035");
    
    memcpy(temp, header_version, sizeof(header_version));
    tag_offset= sizeof(header_version);
    char2hex((UINT8*)&temp[tag_offset], (UINT8*)pEMV_TAGS, strlen(pEMV_TAGS));
    length= strlen(pEMV_TAGS)/2 + tag_offset;
    
#ifdef LOGGING_ENABLED
    int i;
    for (i=0; i<length; i += 8)
    {
        debug_sprintf(szDbgMsg, "pre-b64 %04X: %02x, %02x, %02x, %02x, %02x, %02x, %02x, %02x", i, temp[i+ 0], temp[i+ 1], temp[i+ 2], temp[i+ 3], temp[i+ 4], temp[i+ 5], temp[i+ 6], temp[i+ 7]);
        APP_TRACE(szDbgMsg);
    }
#endif
    
    b64_encode((UINT8*)temp, (UINT8*)info.field_01, length, &final_length);

    debug_sprintf(szDbgMsg, "length= %d, final= %d", length, final_length);
    APP_TRACE(szDbgMsg);
    
#ifdef LOGGING_ENABLED
    for (i=0; i<final_length; i += 8)
    {
        debug_sprintf(szDbgMsg, "post-b64 %04X: %02x, %02x, %02x, %02x, %02x, %02x, %02x, %02x", i, info.field_01[i+ 0], info.field_01[i+ 1], info.field_01[i+ 2], info.field_01[i+ 3], info.field_01[i+ 4], info.field_01[i+ 5], info.field_01[i+ 6], info.field_01[i+ 7]);
        APP_TRACE(szDbgMsg);
    }
#endif

    *ptr = (char*)calloc(final_length+ strlen(info.group)+ 1, sizeof(char));
    strcpy(*ptr, info.group);
    memcpy(*(ptr)+ strlen(info.group), info.field_01, final_length);
    *(*(ptr)+ strlen(info.group)+ final_length)= '\0';
    final_length += sizeof(info.group);
    
    debug_sprintf(szDbgMsg, "lengths: final= %d", final_length);
    APP_TRACE(szDbgMsg);
    
    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);
}


/***
 G036 – EMV DUKPT PIN block for online PIN
 ***/
void composeG036(char** ptr, _sSCA_XML_REQUEST *req_xml_info, sGLOBALS* GLOBAL)
{
    char buffer[4+38+1] = {0};
    _sG036_ info;
    
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    memset(&info, 0x00, sizeof(info));
    char* pKEY_SERIAL_NUMBER = NULL;
    char* pPIN_BLOCK = NULL;
    
    char* pPAYMENT_TYPE = getXmlTagValue( ePAYMENT_TYPE, req_xml_info );

    if (pPAYMENT_TYPE != NULL && (strncmp(pPAYMENT_TYPE, "DEBIT", 5 ) == COMPARE_OK) ) {
		debug_sprintf(szDbgMsg, "%s: Its Debit Transaction, no need to send this group", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

    pPIN_BLOCK= getXmlTagValue(ePIN_BLOCK, req_xml_info);
    if ((pPIN_BLOCK == NULL) || (strlen(pPIN_BLOCK) != 16)) // 32 character PIN block not supported
    {
        debug_sprintf(szDbgMsg, "No PIN block to send");
        APP_TRACE(szDbgMsg);
        return;
    }
    
    pKEY_SERIAL_NUMBER = getXmlTagValue(eKEY_SERIAL_NUMBER, req_xml_info);
    if ((pKEY_SERIAL_NUMBER == NULL) || (strlen(pKEY_SERIAL_NUMBER) != 20))
    {
        debug_sprintf(szDbgMsg, "No serial number to send");
        APP_TRACE(szDbgMsg);
        return;
    }
    
    /*
     * Praveen_P1: As per Vantiv, this group should not be sent for
     * Time Out Reversals cases
     */
    if (GLOBAL->http.status_code == eHTTP_GATEWAY_TIMEOUT) {
    	debug_sprintf(szDbgMsg, "%s: Its TOR Reversal, no need to send this group", __FUNCTION__);
    	APP_TRACE(szDbgMsg);

    	return;
	}


    strcpy(info.group, "G036");
    strcpy(info.field_01, pKEY_SERIAL_NUMBER);
    strcpy(info.field_02, pPIN_BLOCK);
    strcpy(info.field_03, "01");    // unknown max PIN length capability
    
    strcpy(buffer, info.group);
    strcat(buffer, info.field_01);
    strcat(buffer, info.field_02);
    strcat(buffer, info.field_03);
    
    int len = strlen(buffer);
    *ptr = (char*)calloc(len+1, sizeof(char));
    strcpy(*ptr, buffer);
    
    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);
}

/***
 R023 - EMV Response Tags
 
 EMV tag data format is BER-TLV as defined in ISO/IEC 8825. The tag data is Base64 encoded.
 
 Look for EMV tags in decoded response and convert to SSI XML
 All tags are in TLV as a hex string. These are in <EMV_TAGS_RESPONSE> XML tag.
 
 All tags are optional.
 
 Tags are stored as binary in sR023_info.
 Data in sR023_info may not be binary, it in the format specified by the EMV tag.
 Binary tags are sent in XML as text: 0x23, 0xAB is sent as "23AB".
 This means sRo23_info needs processing as it is put into XML.
 ***/

void parseR023(sVANTIV_RESPONSE * vrinfo, sR023_data *sR023_info)
{
    char    *ptr;
    
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    memset(sR023_info, 0x00, sizeof(*sR023_info));
    
    ptr = getR023(vrinfo);
    
    if (ptr == NULL)
        return;
    
    debug_sprintf(szDbgMsg, "Length from host %d", strlen(ptr+ 4));
    APP_TRACE(szDbgMsg);
    
#ifdef LOGGING_ENABLED
    int i;
    for (i=0; i<strlen(ptr+ 4); i += 8)
    {
        debug_sprintf(szDbgMsg, "Host-b64 %04X: %02x, %02x, %02x, %02x, %02x, %02x, %02x, %02x", i, *(ptr+i+4), *(ptr+i+5), *(ptr+i+6), *(ptr+i+7), *(ptr+i+8), *(ptr+i+9), *(ptr+i+10), *(ptr+i+11));
        APP_TRACE(szDbgMsg);
    }
#endif
    
    b64_decode((UINT8*)(ptr+ 4), sR023_info->tlv_data, (int)strlen(ptr+ 4), &sR023_info->tlv_length);    // skip over "R023"
    
    debug_sprintf(szDbgMsg, "Length decoded %d", sR023_info->tlv_length);
    APP_TRACE(szDbgMsg);
    
#ifdef LOGGING_ENABLED
    for (i=0; i<sR023_info->tlv_length; i += 8)
    {
        debug_sprintf(szDbgMsg, "Host-postb64 %04X: %02x, %02x, %02x, %02x, %02x, %02x, %02x, %02x", i, sR023_info->tlv_data[i+0], sR023_info->tlv_data[i+1], sR023_info->tlv_data[i+2], sR023_info->tlv_data[i+3], sR023_info->tlv_data[i+4], sR023_info->tlv_data[i+5], sR023_info->tlv_data[i+6], sR023_info->tlv_data[i+7]);
        APP_TRACE(szDbgMsg);
    }
#endif
}

void composeR023Response(xmlNodePtr root_node, sR023_data *sR023_info)
{
    xmlChar buffer[1600];

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    memset(buffer, 0x00, sizeof(xmlChar) * 1600);

    hex2char(buffer, sR023_info->tlv_data, sR023_info->tlv_length);

    if(strlen((char *)buffer) > 0)
    {
    	xmlNewChild(root_node, NULL, BAD_CAST "EMV_TAGS_RESPONSE", buffer);
    }
}

// Taken from Moneris SoftPay 410 - base64 encoding

static const char cb64[]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
static const char cd64[]="|$$$}rstuvwxyz{$$$$$$$>?@ABCDEFGHIJKLMNOPQRSTUVW$$$$$$XYZ[\\]^_`abcdefghijklmnopq";

static void encodeblock(const UINT8 in[3], UINT8 out[4], int len)
{
    out[0] = cb64[ in[0] >> 2 ];
    out[1] = cb64[ ((in[0] & 0x03) << 4) | ((in[1] & 0xf0) >> 4) ];
    out[2] = (UINT8) (len > 1 ? cb64[ ((in[1] & 0x0f) << 2) | ((in[2] & 0xc0) >> 6) ] : '=');
    out[3] = (UINT8) (len > 2 ? cb64[ in[2] & 0x3f ] : '=');
} // end encodeblock

//
// Base64 encode
// inbuf: input buffer
// outbuf: output buffer
// insize: length of data in input buffer
// outsize: length of data in output buffer
// NOTE: Size of outbuf must be atleast size(inbuf)*1.33
// On encoding every three bytes (or parthere of) gets converted to 4bytes.
// For example 30bytes gets convereted to 40bytes, while 31bytes takes 44bytes
//
void b64_encode(const UINT8 *pchInBuf, UINT8 *pchOutBuf, int shInSize, int *shOutSize)
{
    UINT8 chIn[3], chOut[4];
    
    int shLen;
    int shIndex = 0;
    int shOutdex = 0;
    
    while (shIndex < shInSize)
    {
        memset(&chIn[0], 0, sizeof(chIn));
        if ((shLen = shInSize - shIndex) > sizeof(chIn))
        shLen = sizeof(chIn);
        memcpy(&chIn[0], &pchInBuf[shIndex], shLen);
        shIndex += shLen;
        
        if (shLen)
        {
            encodeblock(chIn, chOut, shLen);
            memcpy(&pchOutBuf[shOutdex], chOut, sizeof(chOut));
            shOutdex += sizeof(chOut);
        }
    } // end while
    *shOutSize = shOutdex;
} // b64_encode


static void decodeblock(UINT8 in[4], UINT8 out[3])
{
    out[ 0 ] = (UINT8 ) (in[0] << 2 | in[1] >> 4);
    out[ 1 ] = (UINT8 ) (in[1] << 4 | in[2] >> 2);
    out[ 2 ] = (UINT8 ) (((in[2] << 6) & 0xc0) | in[3]);
} // end decodeblock

//
//  Need to remove trailing padding ("=" characters) from input.
//  If these are not removed than output can have extra 0x00 bytes added to the end.
//
void b64_decode(const UINT8 *pchInBuf, UINT8 *pchOutBuf, int shInSize, int *shOutSize)
{
    UINT8 in[4], out[3], v;
    int i;
    
    int shLen;
    int shIndex = 0;
    int shOutdex = 0;
    
    // Remove 0, 1 or 2 trailing padding characters (=).
    while ((*(pchInBuf+ shInSize- 1) == '=') && (shInSize > 0))
        shInSize--;
    
    while(shIndex < shInSize)
    {
        for (shLen=0, i=0; ((i < 4) && (shIndex < shInSize)); i++)
        {
            v = 0;
            while( (shIndex < shInSize) && (v == 0) )
            {
                v = pchInBuf[shIndex++];
                v = (UINT8) ((v < 43 || v > 122) ? 0 : cd64[ v - 43 ]);
                if ( v )
                {
                    v = (UINT8) ((v == '$') ? 0 : v - 61);
                }
            } // end while
            if (shIndex <= shInSize)
            {
                shLen++;
                if ( v ) 
                {
                    in[ i ] = (UINT8) (v - 1);
                }
            }
            else 
            {
                in[i] = 0;
            }
        } // end for
        
        if (shLen) 
        {
            decodeblock(in, out);
            for(i = 0; i < shLen - 1; i++ ) 
            {
                pchOutBuf[shOutdex++] = out[i];
            }
        }
    } // end while
    *shOutSize = shOutdex;
} // end b64_decode

// sourceSize should be an even number
// destination will be half the size of source
void char2hex(UINT8* destination, UINT8* source, int sourceSize)
{
    UINT8   temp[2];
    UINT8   *src = source;
    UINT8   one_char;
    int     i, j;
    
    for( i = 0; i < sourceSize; i += 2 )
    {
        for (j = 0; j < 2; ++j, ++src)
        {
            if (*src > 'F')
                one_char= *src- 'a'+ 'A';   // convert a-f to upper case
            else
                one_char= *src;
            temp[j] = one_char <= '9' ? one_char - '0' : one_char - '7';
        }
        
        *destination++ = (temp[0] << 4) | (temp[1] &  0x0F);
    }
}

// Produces upper case A to F
// destination will be twice the size of sourceSize
void hex2char(UINT8* destination, UINT8* source,  int sourceSize)
{
    UINT8 byte = 0;
    int i;
    
    for (i = 0; i < sourceSize; i++)
    {
        *destination++ = (( byte = (*source  ) >> 4   ) <= 9) ? byte + '0' : byte + '7';
        *destination++ = (( byte = (*source++) &  0x0f) <= 9) ? byte + '0' : byte + '7';
    }
}

#ifdef NEVER
static void char2bcd(UINT8* destination, UINT8* source, int sourceSize)
{
    UINT8  temp[2];
    UINT8* src = source;
    int i, j;
    
    //debug_sprintf(szDbgMsg, "char2bcd src= %s, len= %d", source, sourceSize);
    //APP_TRACE(szDbgMsg);
    
    for( i = 0; i < sourceSize;  )
    {
        // If source is odd length then fill first nibble with 0x0
        if ((sourceSize% 2) && (i == 0))
        {
            temp[0]= 0;
            temp[1]= *src - '0';
            src++;
            i++;
        }
        else
        {
            for (j = 0; j < 2; ++j, ++src)
            {
                temp[j] = *src - '0';
                i++;
            }
        }
        
        //debug_sprintf(szDbgMsg, "char2bcd tmp0= %02X, tmp1= %02X", temp[0], temp[1]);
        //APP_TRACE(szDbgMsg);
        
        *destination++ = (temp[0] << 4) | (temp[1] &  0x0F);
    }
}
#endif

static const UINT8 abyTagwanted[] = { 0x8A };
static const UINT8 abyTagtest[] = { 0x9F, 0x09 };

#ifdef TEST_TLV
void tlv_test_code(void)
{
    BER_TLV *tlv_element;
    BER_TLV *found_tag;
    char    temp[15]= {0x82, 0x01, 0x55, 0x9f, 0x34, 0x02, 0x44, 0x66, 0x8a, 0x02, 0x30, 0x30};
    //UINT8   buffer[999];
    unsigned long   bytes_used;
    //short   length;

    
    tlv_element= pstTLV_make_element();
    
    debug_sprintf(szDbgMsg, "START TEST");
    APP_TRACE(szDbgMsg);
    
    if (usnTLV_parse_buffer(tlv_element, (UINT8*)temp, 12) == 0)
    {
        debug_sprintf(szDbgMsg, "Parse done");
        APP_TRACE(szDbgMsg);
        found_tag= pstTLV_find_element(tlv_element, TAG_DET(abyTagwanted));
        if (found_tag == NULL)
        {
            debug_sprintf(szDbgMsg, "Tag not found");
            APP_TRACE(szDbgMsg);
        }
        else
        {
            // output: found tag 9f 34
            debug_sprintf(szDbgMsg, "found tag %02x %02x", found_tag->abyTag[0], found_tag->abyTag[1]);
            APP_TRACE(szDbgMsg);
        }
    }
    debug_sprintf(szDbgMsg, "After parse");
    APP_TRACE(szDbgMsg);

    temp[0]= 0x12;
    temp[1]= 0xab;
    vTLV_set_up_primitive_element(tlv_element, TAG_DET(abyTagtest), (UINT8*)temp, 2);
    // output: tag 9f 09
    debug_sprintf(szDbgMsg, "tag %02x %02x", tlv_element->abyTag[0], tlv_element->abyTag[1]);
    APP_TRACE(szDbgMsg);
    
    memset(temp, 0, sizeof(temp));
    if (usnTLV_flatten_element((UINT8*)temp, &bytes_used, 10, tlv_element, TRUE) == 0)
    {
        // output: Flat 9f, 09, 02, 12, ab
        debug_sprintf(szDbgMsg, "Flat %02x, %02x, %02x, %02x, %02x", temp[0], temp[1], temp[2], temp[3], temp[4]);
        APP_TRACE(szDbgMsg);
    }
    vTLV_destroy_element(tlv_element);
    
    temp[0]= 0x12;
    temp[1]= 0xab;
    tlv_element= pstTLV_make_primitive_element(TAG_DET(abyTagwanted), (UINT8*)temp, 2);
    temp[0]= 0x55;
    temp[1]= 0x66;
    temp[2]= 0x77;
    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagtest), (UINT8*)temp, 3);
    
    memset(temp, 0, sizeof(temp));
    if (usnTLV_flatten_element((UINT8*)temp, &bytes_used, 15, tlv_element, TRUE) == 0)
    {
        // output: Flat 9f, 09, 02, 12, ab
        debug_sprintf(szDbgMsg, "Second flat %02x, %02x, %02x, %02x, %02x", temp[0], temp[1], temp[2], temp[3], temp[4]);
        APP_TRACE(szDbgMsg);
        debug_sprintf(szDbgMsg, "Second ---- %02x, %02x, %02x, %02x, %02x", temp[5], temp[6], temp[7], temp[8], temp[9]);
        APP_TRACE(szDbgMsg);
    }

    vTLV_destroy_element(tlv_element);
}
#endif


#ifdef OLD_EMV_TAG_METHOD
/***
 G035 – EMV Tag Data
 
 EMV tag data format is BER-TLV as defined in ISO/IEC 8825. The resultant encoded tags must then be Base64
 encoded into ASCII string format.
 
 One field - Variable 999 maximum Base64 encoded ASCII string.
 
 TLV data is bianry but after base64 encoding it can be treated as a string.
 
 Make sure all required tags are present.
 ***/
void composeG035(char** ptr, _sSCA_XML_REQUEST *req_xml_info)
{
    _sG035_ info;
    BER_TLV *tlv_element;
    short   final_length;
    int     tags_present= 0;
    int     tag_offset;
    int     temp_item;
    UINT8   *xml_value= NULL;
    UINT8   temp[1000];             // has to be the max size of G035
    UINT8   format[50];             // to pre-format data
    char    version[20+ 1];
    ERROR_CODE      status;
    unsigned long   length;
    enum _eSCA_XML_REQUEST     emv_tag;
    
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    debug_sprintf(szDbgMsg, "EMV tags from %d to %d", FIRST_EMV_REQUEST_TAG, LAST_EMV_REQUEST_TAG);
    APP_TRACE(szDbgMsg);
    
    char* pPRESENT_FLAG = getXmlTagValue(ePRESENT_FLAG, req_xml_info);
    if ((strncmp(pPRESENT_FLAG, EMV_CHIP_ENTRY, 1) != COMPARE_OK) && (strncmp(pPRESENT_FLAG, EMV_CTLS_ENTRY, 1) != COMPARE_OK))
    {
        return;
    }
    
    strcpy(info.group, "G035");
    
    tlv_element= pstTLV_make_element();
    
    // The TLV library requires the first tag in a list must be set up.
    // The following tags are appended. That is why this tag is done out of the while.
    emv_tag= eAUTHAMOUNT;
    xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
    if (xml_value != NULL)
    {
        tags_present += emv_tag;
        length= 6;
        memset(format, '0', length* 2);
        temp_item= MIN(strlen((char*)xml_value), 12);
        strcpy((char*)&format[12- temp_item], "");
        strncat((char*)&format[12- temp_item], (char*)xml_value, temp_item);
        char2bcd(temp, format, (int)length* 2);
        
        debug_sprintf(szDbgMsg, "Amount in BCD %02x, %02x, %02x, %02x, %02x, %02x", temp[0], temp[1], temp[2], temp[3], temp[4], temp[5]);
        APP_TRACE(szDbgMsg);
        
        // Start the TLV chain
        vTLV_set_up_primitive_element( tlv_element, TAG_DET(abyTagAUTHAMOUNT), temp, length);
    }
    else
    {
        // Transaction needs this tag. Code needs this tag to start the TLV chain.
        debug_sprintf(szDbgMsg, "Missing auth amount tag");
        APP_TRACE(szDbgMsg);
        vTLV_destroy_element(tlv_element);
        return;
    }
    
    emv_tag= FIRST_EMV_REQUEST_TAG;
    while (emv_tag <= LAST_EMV_REQUEST_TAG)
    {
        debug_sprintf(szDbgMsg, "tag %d, present= %d", emv_tag, tags_present);
        APP_TRACE(szDbgMsg);
        switch (emv_tag)
        {
            case eAPPINTERCHANGEPROFILE:            // tag 82 (b 2) - required
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    tags_present += emv_tag;
                    length= 2;
                    
                    char2hex(temp, xml_value, (int)length* 2);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagAPPINTERCHANGEPROFILE), temp, length);
                }
                emv_tag++;
                break;
                
            case eAPPLICATIONVERSION:               // tag 9F09 (b 2) - required
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    tags_present += emv_tag;
                    length= 2;
                    char2hex(temp, xml_value, (int)length* 2);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagAPPLICATIONVERSION), temp, length);
                }
                emv_tag++;
                break;
                
            case eAPPPANSEQNUM:                     // tag 5F34 (n2 1) - required
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    tags_present += emv_tag;
                    length= 1;
                    char2bcd(temp, xml_value, (int)length* 2);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagAPPPANSEQNUM), temp, length);
                }
                emv_tag++;
                break;
                
            case eAPPTRANSACTIONCOUNTER:            // tag 9F36 (b 2) - required
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    tags_present += emv_tag;
                    length= 2;
                    char2hex(temp, xml_value, (int)length* 2);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagAPPTRANSACTIONCOUNTER), temp, length);
                }
                emv_tag++;
                break;
                
            case eAUTHAMOUNT:                       // tag 9F02 (n12 6) - required
                // This tag is done first to start the TLV chain.
                emv_tag++;
                break;
                
            case eAUTHORIZATION_RESPONSE_CODE:      // tag 8A (an2)
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    length= 2;
                    memcpy(temp, xml_value, length);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagAUTHORIZATION_RESPONSE_CODE), temp, length);
                }
                emv_tag++;
                break;
                
            case eCRYPTOGRAMINFODATA:               // tag 9F27 (b 1) - required
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    tags_present += emv_tag;
                    length= 1;
                    char2hex(temp, xml_value, (int)length* 2);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagCRYPTOGRAMINFODATA), temp, length);
                }
                emv_tag++;
                break;
                
            case eCRYPTOGRAMTRANSACTIONTYPE:        // tag 9C (n2 1) - required
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    tags_present += emv_tag;
                    length= 1;
                    char2bcd(temp, xml_value, (int)length* 2);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagCRYPTOGRAMTRANSACTIONTYPE), temp, length);
                }
                emv_tag++;
                break;
                
            case eCRYPTOGRAMCURRCODE:               // tag 5F2A (n3 2) - required
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    tags_present += emv_tag;
                    length= 2;
                    char2bcd(temp, xml_value, (int)strlen((char*)xml_value));   // tag is input as "124"
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagCRYPTOGRAMCURRCODE), temp, length);
                }
                emv_tag++;
                break;
                
            case eCUSTOMER_EXCLUSIVE_DATA:          // tag 9F7C (var)
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    length= strlen((char*)xml_value)/ 2;        // binary data in text is 2 times the size
                    char2hex(temp, xml_value, (int)length* 2);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagCUSTOMER_EXCLUSIVE_DATA), temp, length);
                }
                emv_tag++;
                break;
                
            case eCVMRESULT:                        // tag 9f34 (b 3) - required
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    tags_present += emv_tag;
                    length= 3;
                    char2hex(temp, xml_value, (int)length* 2);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagCVMRESULT), temp, length);
                }
                emv_tag++;
                break;
                
            case eDEDICATEDFILENAME:                // tag 84 (b 5-16)
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    length= strlen((char*)xml_value)/ 2;        // binary data in text is 2 times the size
                    char2hex(temp, xml_value, (int)length* 2);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagDEDICATEDFILENAME), temp, length);
                }
                emv_tag++;
                break;
                
            case eEMV_CASHBACK_AMNT:                // tag 9F03 (n12 6)
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    length= 6;
                    char2bcd(temp, xml_value, (int)length* 2);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagEMV_CASHBACK_AMNT), temp, length);
                }
                emv_tag++;
                break;
                
            case eICC_FORM_FACTOR:                  // tag 9F6E (b)
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    length= strlen((char*)xml_value)/ 2;        // binary data in text is 2 times the size
                    char2hex(temp, xml_value, (int)length* 2);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagICC_FORM_FACTOR), temp, length);
                }
                emv_tag++;
                break;
                
            case eINTERFACE_DEVICE_SERIAL_NUMBER:   // tag 9F1E (an8) - required
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    tags_present += emv_tag;
                    length= 8;
                    memcpy(temp, xml_value, length);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagINTERFACE_DEVICE_SERIAL_NUMBER), temp, length);
                }
                emv_tag++;
                break;
                
            case eISSUERAPPLICATIONDATA:            // tag 9F10 (b max32) - required
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    tags_present += emv_tag;
                    length= strlen((char*)xml_value)/ 2;        // binary data in text is 2 times the size
                    char2hex(temp, xml_value, (int)length* 2);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagISSUERAPPLICATIONDATA), temp, length);
                }
                emv_tag++;
                break;
                
            case eISSUER_AUTHENTICATION_DATA:       // tag 91 (b 8-16)
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    length= strlen((char*)xml_value)/ 2;        // binary data in text is 2 times the size
                    char2hex(temp, xml_value, (int)length* 2);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagISSUER_AUTHENTICATION_DATA), temp, length);
                }
                emv_tag++;
                break;
                
            case eISSUER_SCRIPT_RESULTS:            // tag 9F5B (b var) - doc indicates for reversals only
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    length= strlen((char*)xml_value)/ 2;        // binary data in text is 2 times the size
                    char2hex(temp, xml_value, (int)length* 2);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagISSUER_SCRIPT_RESULTS), temp, length);
                }
                emv_tag++;
                break;
                
            case eTERMINAL_APP_ID:                  // tag 9F06 (b 5-16) - required
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    tags_present += emv_tag;
                    length= strlen((char*)xml_value)/ 2;        // binary data in text is 2 times the size
                    char2hex(temp, xml_value, (int)length* 2);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagTERMINAL_APP_ID), temp, length);
                }
                emv_tag++;
                break;
                
            case eTERMINAL_CAPABILITY_PROFILE:      // tag 9F33 (b 3)
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    length= 3;
                    char2hex(temp, xml_value, (int)length* 2);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagTERMINAL_CAPABILITY_PROFILE), temp, length);
                }
                emv_tag++;
                break;
                
                // This is the country code
            case eTERMINAL_TRANSACTION_CODE:        // tag 9F1A (n3 2) - required
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    tags_present += emv_tag;
                    length= 2;
                    char2bcd(temp, xml_value, (int)strlen((char*)xml_value));   // tag is input as "124"
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagTERMINAL_TRANSACTION_CODE), temp, length);
                }
                emv_tag++;
                break;
                
            case eTERMINAL_TRANSACTION_DATE:        // tag 9A (n6 3) - required
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    tags_present += emv_tag;
                    length= 3;
                    char2bcd(temp, xml_value, (int)length* 2);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagTERMINAL_TRANSACTION_DATE), temp, length);
                }
                emv_tag++;
                break;
                
            case eTERMINAL_TRANSACTION_QUALIFIERS:  // tag 9F66 (b)
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    length= strlen((char*)xml_value)/ 2;        // binary data in text is 2 times the size
                    char2hex(temp, xml_value, (int)length* 2);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagTERMINAL_TRANSACTION_QUALIFIERS), temp, length);
                }
                emv_tag++;
                break;
                
            case eTERMINAL_TRANS_SEQ_CTR:           // tag 9F41 (n4-8 2-4)
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    length= (strlen((char*)xml_value)+ 1)/ 2;        // odd number of digits needs to get extra byte (eg n3 => 3 bytes)
                    char2bcd(temp, xml_value, (int)length* 2);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagTERMINAL_TRANS_SEQ_CTR), temp, length);
                }
                emv_tag++;
                break;
                
            case eTERMINALTYPE:                     // tag 9F35 (n2 1)
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    length= 1;
                    char2bcd(temp, xml_value, (int)length* 2);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagTERMINALTYPE), temp, length);
                }
                emv_tag++;
                break;
                
            case eTERMINALVERIFICATIONRESULT:       // tag 95 (b 5) - required
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    tags_present += emv_tag;
                    length= 5;
                    char2hex(temp, xml_value, (int)length* 2);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagTERMINALVERIFICATIONRESULT), temp, length);
                }
                emv_tag++;
                break;
                
            case eTRANSACTIONCRYPTOGRAM:            // tag 9F26 (b 8) - required
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    tags_present += emv_tag;
                    length= 8;
                    char2hex(temp, xml_value, (int)length* 2);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagTRANSACTIONCRYPTOGRAM), temp, length);
                }
                emv_tag++;
                break;
                
            case eTRANSACTION_TIME:                 // tag 9F21 (n6 3)
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    length= 3;
                    char2bcd(temp, xml_value, (int)length* 2);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagTRANSACTION_TIME), temp, length);
                }
                emv_tag++;
                break;
                
            case eTRANS_CAT_CODE:                   // tag 9F53 (an 1)
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    length= 1;
                    memcpy(temp, xml_value, length);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagTRANS_CAT_CODE), temp, length);
                }
                emv_tag++;
                break;
                
            case eUNPREDICTABLENUMBER:              // tag 9F37 (b 4) - required
                xml_value = (UINT8*)getXmlTagValue(emv_tag, req_xml_info);
                if (xml_value != NULL)
                {
                    tags_present += emv_tag;
                    length= 4;
                    char2hex(temp, xml_value, (int)length* 2);
                    
                    vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagUNPREDICTABLENUMBER), temp, length);
                }
                emv_tag++;
                break;
                
            default:
                debug_sprintf(szDbgMsg, "Uprocessed tag= %04x", emv_tag);
                APP_TRACE(szDbgMsg);
                break;
        }
        
#ifdef NEVER
        // JUST FOR DEBUG
        if (xml_value != NULL)
        {
            debug_sprintf(szDbgMsg, "temp len= %ld, %02x, %02x, %02x, %02x", length, temp[0], temp[1], temp[2], temp[3]);
            APP_TRACE(szDbgMsg);
            //vTLV_set_up_primitive_element( pstTLV_append_element(tlv_element), TAG_DET(abyTagAPPINTERCHANGEPROFILE), temp, length);
            
            status= usnTLV_flatten_element((UINT8*)info.field_01, &length, sizeof(info.field_01), tlv_element, TRUE);
            debug_sprintf(szDbgMsg, "FLAT stat= %d, len= %ld, %02x, %02x, %02x, %02x, %02x, %02x, %02x, %02x, %02x, %02x", status, length, info.field_01[0], info.field_01[1], info.field_01[2], info.field_01[3], info.field_01[4], info.field_01[5], info.field_01[6], info.field_01[7], info.field_01[8], info.field_01[9]);
            APP_TRACE(szDbgMsg);
        }
#endif
    }
    
    // Check for required tags
    if (tags_present != REQUIRED_COUNT)
    {
        debug_sprintf(szDbgMsg, "required: count= %d, actual= %d, diff= %d", REQUIRED_COUNT, tags_present, REQUIRED_COUNT- tags_present);
        APP_TRACE(szDbgMsg);
    }
    
    // abyTagHEADER_VERSION and abyTagSOFTWARE_VERSION tags have to be manually produced
    // TLV routines don't process FF tags as expected - no value data is produced
    memcpy(info.field_01, header_version, sizeof(header_version));
    tag_offset= sizeof(header_version);
    memcpy(&info.field_01[tag_offset], software_version, 3);
    tag_offset += 3;
    strcpy(version, VHI_APP_VERSION);
    length= strlen(version);
    memcpy(&info.field_01[tag_offset], version, length);
    info.field_01[tag_offset- 1]= (UINT8)length;
    tag_offset += length;
    
    status= usnTLV_flatten_element((UINT8*)&info.field_01[tag_offset], &length, sizeof(info.field_01), tlv_element, TRUE);
    if (status != 0)
    {
        debug_sprintf(szDbgMsg, "%s: ERROR! TLV flatten failed %d", __FUNCTION__, status);
        APP_TRACE(szDbgMsg);
        vTLV_destroy_element(tlv_element);
        return;
    }
    
    debug_sprintf(szDbgMsg, "Length from flatten %ld, tag_offset %d", length, tag_offset);
    APP_TRACE(szDbgMsg);
    
#ifdef LOGGING_ENABLED
    int i;
    for (i=0; i<length; i += 8)
    {
        debug_sprintf(szDbgMsg, "pre-b64 %04X: %02x, %02x, %02x, %02x, %02x, %02x, %02x, %02x", i, info.field_01[i+ 0], info.field_01[i+ 1], info.field_01[i+ 2], info.field_01[i+ 3], info.field_01[i+ 4], info.field_01[i+ 5], info.field_01[i+ 6], info.field_01[i+ 7]);
        APP_TRACE(szDbgMsg);
    }
#endif
    
    b64_encode((UINT8*)info.field_01, (UINT8*)temp, length+ tag_offset, &final_length);
    
    debug_sprintf(szDbgMsg, "post-b64 len= %d, %02x, %02x, %02x, %02x, %02x, %02x, %02x, %02x", final_length, temp[0], temp[1], temp[2], temp[3], temp[4], temp[5], temp[6], temp[7]);
    APP_TRACE(szDbgMsg);
    
    *ptr = (char*)calloc(final_length+ strlen(info.group)+ 1, sizeof(char));
    strcpy(*ptr, info.group);
    memcpy(*(ptr)+ strlen(info.group), temp, final_length);
    *(*(ptr)+ strlen(info.group)+ final_length)= '\0';
    final_length += sizeof(info.group);
    
    vTLV_destroy_element(tlv_element);
    
    debug_sprintf(szDbgMsg, "lengths: final= %d", final_length);
    APP_TRACE(szDbgMsg);
    
    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);
}

/***
 R023 - EMV Response Tags
 
 EMV tag data format is BER-TLV as defined in ISO/IEC 8825. The tag data is Base64 encoded.
 
 Look for EMV tags in decoded response and convert to SSI XML
 
 All tags are optional.
 
 Tags are stored as binary in sR023_info.
 Data in sR023_info may not be binary, it in the format specified by the EMV tag.
 Binary tags are sent in XML as text: 0x23, 0xAB is sent as "23AB".
 This means sRo23_info needs processing as it is put into XML.
 ***/

void parseR023(sVANTIV_RESPONSE * vrinfo, sR023_data *sR023_info)
{
    short   length;
    //int     tag;
    char    *ptr;
    BER_TLV *tlv_element;
    BER_TLV *found_tag;
    UINT8   buffer[999];
    //unsigned long   bytes_used;
    
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    memset(sR023_info, 0x00, sizeof(sR023_info));
    
    ptr = getR023(vrinfo);
    
    if (ptr == NULL)
        return;
    
    debug_sprintf(szDbgMsg, "Length from host %d", strlen(ptr+ 4));
    APP_TRACE(szDbgMsg);
    
#ifdef LOGGING_ENABLED
    int i;
    length= strlen(ptr+ 4);
    for (i=0; i<length; i += 8)
    {
        debug_sprintf(szDbgMsg, "Host-b64 %04X: %02x, %02x, %02x, %02x, %02x, %02x, %02x, %02x", i, *(ptr+i+4), *(ptr+i+5), *(ptr+i+6), *(ptr+i+7), *(ptr+i+8), *(ptr+i+9), *(ptr+i+10), *(ptr+i+11));
        APP_TRACE(szDbgMsg);
    }
#endif
    
    b64_decode((UINT8*)(ptr+ 4), buffer, strlen(ptr+ 4), &length);    // skip over "R023"
    
    debug_sprintf(szDbgMsg, "Length decoded %d", length);
    APP_TRACE(szDbgMsg);
    
#ifdef LOGGING_ENABLED
    for (i=0; i<length; i += 8)
    {
        debug_sprintf(szDbgMsg, "Host-postb64 %04X: %02x, %02x, %02x, %02x, %02x, %02x, %02x, %02x", i, buffer[i+0], buffer[i+1], buffer[i+2], buffer[i+3], buffer[i+4], buffer[i+5], buffer[i+6], buffer[i+7]);
        APP_TRACE(szDbgMsg);
    }
#endif
    
    tlv_element= pstTLV_make_element();
    if (usnTLV_parse_buffer(tlv_element, buffer, (unsigned long)length) == 0)
    {
        // deal with one tag at a time
        found_tag= pstTLV_find_element(tlv_element, TAG_DET(abyTagAUTHORIZATION_RESPONSE_CODE));
        if (found_tag)
        {
            debug_sprintf(szDbgMsg, "Found tag %02x %02x (8A an 2)", found_tag->abyTag[0], found_tag->abyTag[1]);
            APP_TRACE(szDbgMsg);
            // Use MIN() to ensure the length from Vantiv is not greater than the receiving array in sR023_info.
            length= MIN(found_tag->ulnLen, sizeof(sR023_info->auth_response_code));
            memcpy(sR023_info->auth_response_code, found_tag->pbyValue, length);
            sR023_info->auth_response_code_len= length;
        }
        
        found_tag= pstTLV_find_element(tlv_element, TAG_DET(abyTagISSUER_AUTHENTICATION_DATA));
        if (found_tag)
        {
            debug_sprintf(szDbgMsg, "Found tag %02x %02x (91 b 8-16)", found_tag->abyTag[0], found_tag->abyTag[1]);
            APP_TRACE(szDbgMsg);
            length= MIN(found_tag->ulnLen, sizeof(sR023_info->issuer_auth_data));
            memcpy(sR023_info->issuer_auth_data, found_tag->pbyValue, length);
            sR023_info->issuer_auth_data_len= length;
        }
        
        found_tag= pstTLV_find_element(tlv_element, TAG_DET(abyTagEMV_ISSUER_SCRIPT_DATA71));
        if (found_tag)
        {
            debug_sprintf(szDbgMsg, "Found tag %02x %02x (71 b var) len= %ld", found_tag->abyTag[0], found_tag->abyTag[1], found_tag->ulnLen);
            APP_TRACE(szDbgMsg);
            // This tag is malloc'd to deal with variable length
            length= found_tag->ulnLen;
            if (sR023_info->script71 != NULL)
                free(sR023_info->script71);
            sR023_info->script71= malloc(length);
            memcpy(sR023_info->script71, found_tag->pbyValue, length);
            sR023_info->script71_len= length;
        }
        
        found_tag= pstTLV_find_element(tlv_element, TAG_DET(abyTagEMV_ISSUER_SCRIPT_DATA71));
        if (found_tag)
        {
            // Tag 72 b var
            debug_sprintf(szDbgMsg, "Found tag %02x %02x (72 b var) len= %ld", found_tag->abyTag[0], found_tag->abyTag[1], found_tag->ulnLen);
            APP_TRACE(szDbgMsg);
            // This tag is malloc'd to deal with variable length
            length= found_tag->ulnLen;
            if (sR023_info->script72 != NULL)
                free(sR023_info->script72);
            sR023_info->script72= malloc(length);
            memcpy(sR023_info->script72, found_tag->pbyValue, length);
            sR023_info->script72_len= length;
        }
        
        found_tag= pstTLV_find_element(tlv_element, TAG_DET(abyTagISSUER_SCRIPT_COMMAND));
        if (found_tag)
        {
            debug_sprintf(szDbgMsg, "Found tag %02x %02x (86 b 261)", found_tag->abyTag[0], found_tag->abyTag[1]);
            APP_TRACE(szDbgMsg);
            length= MIN(found_tag->ulnLen, sizeof(sR023_info->issuer_script_command));
            memcpy(sR023_info->issuer_script_command, found_tag->pbyValue, length);
            sR023_info->issuer_script_command_len= length;
        }
        
        found_tag= pstTLV_find_element(tlv_element, TAG_DET(abyTagISSUER_SCRIPT_IDENTIFIER));
        if (found_tag)
        {
            debug_sprintf(szDbgMsg, "Found tag %02x %02x (9F18 b 4)", found_tag->abyTag[0], found_tag->abyTag[1]);
            APP_TRACE(szDbgMsg);
            length= MIN(found_tag->ulnLen, sizeof(sR023_info->issuer_script_id));
            memcpy(sR023_info->issuer_script_id, found_tag->pbyValue, length);
            sR023_info->issuer_script_id_len= length;
        }
        
        // Not sure why but doc says they may be here.
        found_tag= pstTLV_find_element(tlv_element, TAG_DET(abyTagISSUER_SCRIPT_RESULTS));
        if (found_tag)
        {
            debug_sprintf(szDbgMsg, "Found tag %02x %02x (9F5B b var) len= %ld", found_tag->abyTag[0], found_tag->abyTag[1], found_tag->ulnLen);
            APP_TRACE(szDbgMsg);
            length= MIN(found_tag->ulnLen, sizeof(sR023_info->issuer_script_results));
            memcpy(sR023_info->issuer_script_results, found_tag->pbyValue, length);
            sR023_info->issuer_script_results_len= length;
        }
        
        // Found this tag even though not documenetd
        found_tag= pstTLV_find_element(tlv_element, TAG_DET(abyTagAPPTRANSACTIONCOUNTER));
        if (found_tag)
        {
            debug_sprintf(szDbgMsg, "Found tag %02x %02x (9F36 b 2) len= %ld", found_tag->abyTag[0], found_tag->abyTag[1], found_tag->ulnLen);
            APP_TRACE(szDbgMsg);
        }
    }
    else
    {
        debug_sprintf(szDbgMsg, "usnTLV_parse_buffer failed to parse");
        APP_TRACE(szDbgMsg);
    }
    vTLV_destroy_element(tlv_element);
    
    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    return;
}
#define MAX_ISSUER_SCRIPT   1024
void composeR023Response(xmlNodePtr root_node, sR023_data *sR023_info)
{
    xmlChar buffer[MAX_ISSUER_SCRIPT];
    int     length;
    
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    if (sR023_info->auth_response_code_len > 0)
    {
        // tag 82 is an 2
        length= sizeof(sR023_info->auth_response_code);
        memcpy(buffer, sR023_info->auth_response_code, length);
        buffer[length]= '\0';
        xmlNewChild(root_node, NULL, BAD_CAST "AUTHORIZATION_RESPONSE_CODE", buffer);
        
        debug_sprintf(szDbgMsg, "EMV response code tag 82 is %s", buffer);
        APP_TRACE(szDbgMsg);
    }
    
    if (sR023_info->issuer_auth_data_len > 0)
    {
        // tag 91 is b 8-16
        length= sR023_info->issuer_auth_data_len;
        hex2char(buffer, sR023_info->issuer_auth_data, length);
        buffer[length* 2]= '\0';
        xmlNewChild(root_node, NULL, BAD_CAST "ISSUER_AUTHENTICATION_DATA", buffer);
    }
    
    if (sR023_info->script71_len > 0)
    {
        if (2* sR023_info->script71_len >= MAX_ISSUER_SCRIPT)
        {
            debug_sprintf(szDbgMsg, "Script 71 too long %d", sR023_info->script72_len);
            APP_TRACE(szDbgMsg);
        }
        else
        {
            // tag 71 is b var
            length= sR023_info->script71_len;
            hex2char(buffer, sR023_info->script71, length);
            buffer[length* 2]= '\0';
            xmlNewChild(root_node, NULL, BAD_CAST "EMV_ISSUER_SCRIPT_DATA1", buffer);
        }
    }
    
    if (sR023_info->script72_len > 0)
    {
        if (2* sR023_info->script72_len >= MAX_ISSUER_SCRIPT)
        {
            debug_sprintf(szDbgMsg, "Script 72 too long %d", sR023_info->script72_len);
            APP_TRACE(szDbgMsg);
        }
        else
        {
            // tag 72 is b var
            length= sR023_info->script72_len;
            hex2char(buffer, sR023_info->script72, length);
            buffer[length* 2]= '\0';
            xmlNewChild(root_node, NULL, BAD_CAST "EMV_ISSUER_SCRIPT_DATA2", buffer);
        }
    }
    
    if (sR023_info->issuer_script_command_len > 0)
    {
        // tag 86 is b 1-261
        length= sR023_info->issuer_script_command_len;
        hex2char(buffer, sR023_info->issuer_script_command, length);
        buffer[length* 2]= '\0';
        xmlNewChild(root_node, NULL, BAD_CAST "ISSUER_SCRIPT_COMMAND", buffer);
    }
    
    if (sR023_info->issuer_script_id_len > 0)
    {
        // tag 9F18 is b 4
        length= sR023_info->issuer_script_id_len;
        hex2char(buffer, sR023_info->issuer_script_id, length);
        buffer[length* 2]= '\0';
        xmlNewChild(root_node, NULL, BAD_CAST "ISSUER_SCRIPT_IDENTIFIER", buffer);
    }
    
    if (sR023_info->issuer_script_results_len > 0)
    {
        // tag 9F5B is b 1-21
        length= sR023_info->issuer_script_results_len;
        hex2char(buffer, sR023_info->issuer_script_results, length);
        buffer[length* 2]= '\0';
        xmlNewChild(root_node, NULL, BAD_CAST "ISSUER_SCRIPT_RESULTS", buffer);
    }
}
#endif

