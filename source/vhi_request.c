/*
 * vhi_request.c
 *
 *  Created on: Nov 5, 2013
 *      Author: Dexter M. Alberto
 */


#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "vhi_request.h"
#include "sca_xml_request.h"
#include "help_function.h"
#include "logger.h"
#include "vhi_610_fields.h"
#include "vhi_610_group.h"
#include "define_data.h"
#include "vhi_610_base.h"
#include "service_request.h"
#include "config_data.h"
#include "appLog.h"

#ifdef LOGGING_ENABLED
static char    szDbgMsg[512];
#endif

void freeVhiRequestData(sVHI_REQ * vhi_req_info)
{
	if (vhi_req_info->main) {
		free(vhi_req_info->main);
		vhi_req_info->main = NULL;
	}
	if (vhi_req_info->transtype) {
		free(vhi_req_info->transtype);
		vhi_req_info->transtype = NULL;
	}

	if (vhi_req_info->basegroup) {
		free(vhi_req_info->basegroup);
		vhi_req_info->basegroup = NULL;
	}
}


char* composeSaleRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* ptr = NULL;
	char* pPRESENT_FLAG = getXmlTagValue(ePRESENT_FLAG, req_xml_info);
	char* pPAYMENT_TYPE = getXmlTagValue(ePAYMENT_TYPE, req_xml_info);
	char* pCARD_TOKEN   = getXmlTagValue(eCARD_TOKEN, req_xml_info);

	if (pPAYMENT_TYPE == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE should not be NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	if (vhi_req_info->transtype) {
		free(vhi_req_info->transtype);
		vhi_req_info->transtype = NULL;
	}

	/*
	 * pPRESENT_FLAG
	 * 1 - card not present
	 * 2 - card present
	 * 3 - card swiped
	 * 4 - Transaction is being passed by RFID
	 */

	if (strncmp( pPAYMENT_TYPE, COMMAND.CREDIT, strlen(COMMAND.CREDIT)) == COMPARE_OK)
    {
		/*
		 * CREDIT TRANSACTIONS
		 * optional group 9, 26
		 */
		// CREDIT payment type OR EMV
		char* pCUSTOMER_STREET = getXmlTagValue(eCUSTOMER_STREET, req_xml_info);
		char* pCUSTOMER_ZIP = getXmlTagValue(eCUSTOMER_ZIP, req_xml_info);


		if (pPRESENT_FLAG == NULL ) {
			debug_sprintf(szDbgMsg, "%s: ERROR! PRESENT_FLAG is NULL during SALE", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(pCARD_TOKEN != NULL)
			{
				if( (pCUSTOMER_STREET != NULL) || (pCUSTOMER_ZIP != NULL) ) //Praveen_P1: 26 September: Need to send AVS details for card token based transaction
				{
					debug_sprintf(szDbgMsg, "%s: Card Token is present and Customer Street/Customer Zip is present, so setting to manual entry mode", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					pPRESENT_FLAG = MANUAL_ENTRY;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Card Token is present, so setting to swiped mode", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					pPRESENT_FLAG = SWIPED_ENTRY; //TODO Please check if this is fine
				}
			}
			else
			{
				GLOBAL->http.status_code = eHTTP_OK;
				GLOBAL->appErrCode = eERR_REQUEST_ERROR;

				return NULL;
			}
		}

		if ((strncmp(pPRESENT_FLAG, MANUAL_ENTRY, 1) == COMPARE_OK )
		&& (pCUSTOMER_STREET != NULL || pCUSTOMER_ZIP != NULL)) {
			//manual entry with AVS
			ptr = compose610CreditSaleAVS(GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);

			if (ptr == NULL) {
				debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return NULL;
			}

			uint len = strlen(ptr) + 1 + 1; //1=RS, 1=NULL
			vhi_req_info->transtype = (char*)calloc(len, sizeof(char));
			strcpy(vhi_req_info->transtype, ptr);
			appendHexRs(vhi_req_info->transtype);
			append610Group(eGROUP001, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
			append610Group(eGROUP004, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
			append610Group(eGROUP009, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
			append610Group(eGROUP015, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
			append610Group(eGROUP023, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
			append610Group(eGROUP026, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
			append610Group(eGROUP028, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
			append610Group(eGROUP034, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
			append610Group(eGROUP038, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		}
		else {
			debug_sprintf(szDbgMsg, "Swiped/inserted card");
			APP_TRACE(szDbgMsg);
			//swiped or manual entry with no AVS
			ptr = compose610CreditSale(&vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);
			if (ptr == NULL) {
				debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return NULL;
			}
		}

	}
	else if (strncmp( pPAYMENT_TYPE, "DEBIT", 5) == COMPARE_OK) {
		/*
		 * DEBIT TRANSACTIONS
		 * optional group 9, 26
		 */
		ptr = compose610DebitSale(GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);

		if (ptr == NULL) {
			debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NULL;
		}

		uint len = strlen(ptr) + 1 + 1; //1=RS, 1=NULL
		vhi_req_info->transtype = (char*)calloc(len, sizeof(char));
		strcpy(vhi_req_info->transtype, ptr);
		appendHexRs(vhi_req_info->transtype);
		append610Group(eGROUP001, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP004, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP009, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP023, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP026, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
        append610Group(eGROUP028, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
        append610Group(eGROUP034, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
        append610Group(eGROUP035, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
        append610Group(eGROUP038, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
	}
	else if (strncmp( pPAYMENT_TYPE, "GIFT", 4) == COMPARE_OK) {
		/*
		 * GIFT TRANSACTIONS
		 * optional group 9, 26
		 */
		ptr = compose610GiftSale(GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);

		if (ptr == NULL) {
			debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NULL;
		}

		uint len = strlen(ptr) + 1 + 1; //1=RS, 1=NULL
		vhi_req_info->transtype = (char*)calloc(len, sizeof(char));
		strcpy(vhi_req_info->transtype, ptr);
		appendHexRs(vhi_req_info->transtype);
		append610Group(eGROUP001, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP004, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP009, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
        append610Group(eGROUP018, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP023, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP026, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP028, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP034, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP038, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
	}
    
    // EBT sale
    else if (strncmp( pPAYMENT_TYPE, "EBT", 3) == COMPARE_OK)
    {
        ptr = compose610EBTSale(&vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);
        if (ptr == NULL)
        {
            debug_sprintf(szDbgMsg, "%s: ERROR! 610 EBT sale message is NULL", __FUNCTION__);
            APP_TRACE(szDbgMsg);
            
            return NULL;
        }
    }
    
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE invalid", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INVALID_PAYMENTTYPE;

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi_req_info->transtype;
}

char* composePostAuthRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* ptr = NULL;
	char* pPAYMENT_TYPE = getXmlTagValue(ePAYMENT_TYPE, req_xml_info);

	if (pPAYMENT_TYPE == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE should not be NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	if (vhi_req_info->transtype) {
		free(vhi_req_info->transtype);
		vhi_req_info->transtype = NULL;
	}

	/*
	 * pPRESENT_FLAG
	 * 1 - card not present
	 * 2 - card present
	 * 3 - card swiped
	 * 4 - Transaction is being passed by RFID
	 */

	if (strncmp(pPAYMENT_TYPE, COMMAND.CREDIT, strlen(COMMAND.CREDIT)) == COMPARE_OK) {
		/*
		 * CREDIT TRANSACTIONS ONLY
		 * POST AUTH TXN
		 * optional group 9, 26, 28
		 */
        ptr= compose_610CreditPriorAuthAdj(&vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);;
        if (ptr == NULL) {
            debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
            APP_TRACE(szDbgMsg);
            
            return NULL;
        }
	}
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE invalid", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INVALID_PAYMENTTYPE;

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi_req_info->transtype;
}

char* composeCreditRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* ptr = NULL;
	char* pPAYMENT_TYPE = getXmlTagValue(ePAYMENT_TYPE, req_xml_info);

	if (pPAYMENT_TYPE == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE should not be NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	if (vhi_req_info->transtype) {
		free(vhi_req_info->transtype);
		vhi_req_info->transtype = NULL;
	}

	/*
	 * CARD TRANSACTIONS
	 * optional group 9, 26, 28
	 */
	if (strncmp(pPAYMENT_TYPE, COMMAND.CREDIT, strlen(COMMAND.CREDIT))==COMPARE_OK)
    {
        // CREDIT payment type OR EMV
        ptr= compose610CreditReturn(&vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo );
        if (ptr == NULL) {
            debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
            APP_TRACE(szDbgMsg);
            
            return NULL;
        }
	}
	/*
	 * DEBIT TRANSACTIONS
	 * optional group 9, 26, 28
	 */
	else if (strncmp(pPAYMENT_TYPE, "DEBIT", 5)==COMPARE_OK) {
		ptr = compose610DebitReturn(GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);

		if (ptr==NULL) {
			debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NULL;
		}

		uint len = strlen(ptr) + 1 + 1; //1=RS, 1=NULL
		vhi_req_info->transtype = (char*)calloc(len, sizeof(char));
		strcpy(vhi_req_info->transtype, ptr);
		appendHexRs(vhi_req_info->transtype);
		append610Group(eGROUP001, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP004, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP009, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP023, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP026, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP028, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
        append610Group(eGROUP034, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
        append610Group(eGROUP035, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
        append610Group(eGROUP038, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
	}
	/*
	 * GIFT TRANSACTIONS
	 * optional group 9, 26, 28
	 */
	else if (strncmp( pPAYMENT_TYPE, "GIFT", 4) == COMPARE_OK) {
		ptr = compose610GiftReturn(GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);

		if (ptr==NULL) {
			debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NULL;
		}

		uint len = strlen(ptr) + 1 + 1; //1=RS, 1=NULL
		vhi_req_info->transtype = (char*)calloc(len, sizeof(char));
		strcpy(vhi_req_info->transtype, ptr);
		appendHexRs(vhi_req_info->transtype);
		append610Group(eGROUP001, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP004, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP009, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
        append610Group(eGROUP018, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP023, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP026, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP028, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP034, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP038, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
	}
    
    // EBT return
    else if (strncmp( pPAYMENT_TYPE, "EBT", 3) == COMPARE_OK)
    {
        ptr = compose610EBTReturn(&vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);
        if (ptr == NULL)
        {
            debug_sprintf(szDbgMsg, "%s: ERROR! 610 EBT return message is NULL", __FUNCTION__);
            APP_TRACE(szDbgMsg);
            
            return NULL;
        }
    }
    
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE invalid", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INVALID_PAYMENTTYPE;

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi_req_info->transtype;
}

char* composeVoidRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* 	ptr = NULL;
	int		iAppLogEnabled		= 0;
	char	szAppLogData[300]	= "";

	char* pPAYMENT_TYPE = getXmlTagValue(ePAYMENT_TYPE, req_xml_info);
	char* pCTROUTD = getXmlTagValue(eCTROUTD, req_xml_info);
	char* pCARD_TOKEN = getXmlTagValue(eCARD_TOKEN, req_xml_info);

	if (pPAYMENT_TYPE == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE should not be NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	iAppLogEnabled = isAppLogEnabled();

	if (vhi_req_info->transtype) {
		free(vhi_req_info->transtype);
		vhi_req_info->transtype = NULL;
	}

	if (pCARD_TOKEN == NULL && GCONFIG->eTokenMode == eTOKEN_TRT) {

		char cPAN[76+1] = {0};
		getVoidData(pCTROUTD, cPAN, NULL, NULL, GLOBAL);
		if('C' == cPAN[0])
		{
			GCONFIG->eTokenMode = eTOKEN_TUT;

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Changing TokenMode to TUT");
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData,NULL);
			}
		}
	}

	if (strncmp(pPAYMENT_TYPE, COMMAND.CREDIT, strlen(COMMAND.CREDIT)) == COMPARE_OK) {
		/*
		 * CREDIT TRANSACTIONS
		 * optional group 9, 26, 28
		 */
        ptr= compose610CreditVoid(&vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);;
        if (ptr == NULL) {
            debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
            APP_TRACE(szDbgMsg);
            
            return NULL;
        }
	}
	else if (strncmp(pPAYMENT_TYPE, "DEBIT", 5)==COMPARE_OK) {
		/*
		 * DEBIT TRANSACTIONS
		 * optional group 9, 26, 28
		 */
        ptr= compose610DebitVoid(&vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);;
        if (ptr == NULL) {
            debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
            APP_TRACE(szDbgMsg);
            
            return NULL;
        }
	}
	else if (strncmp( pPAYMENT_TYPE, "GIFT", 4) == COMPARE_OK) {
		/*
		 * GIFT TRANSACTIONS
		 * optional group 9, 26, 27, 28
		 */
		ptr = compose610GiftVoid(GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);

		if (ptr == NULL) {
			debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NULL;
		}

		uint len = strlen(ptr) + 1 + 1; //1=RS, 1=NULL
		vhi_req_info->transtype = (char*)calloc(len, sizeof(char));
		strcpy(vhi_req_info->transtype, ptr);
		appendHexRs(vhi_req_info->transtype);
		//append610Group(eGROUP001, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info); //Praveen_P1: As per Vantiv's Analyst G001 should not be present for Voids
		append610Group(eGROUP004, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP009, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP014, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
        append610Group(eGROUP018, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP023, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP026, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP027, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP028, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP034, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP038, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
	}
    
    // EBT void/reversal
    else if (strncmp( pPAYMENT_TYPE, "EBT", 3) == COMPARE_OK)
    {
        ptr = compose610EBTVoid(&vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);
        if (ptr == NULL)
        {
            debug_sprintf(szDbgMsg, "%s: ERROR! 610 EBT void message is NULL", __FUNCTION__);
            APP_TRACE(szDbgMsg);
            
            return NULL;
        }
    }
    
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE invalid", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INVALID_PAYMENTTYPE;

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi_req_info->transtype;
}

char* composeActivateRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* ptr = NULL;
	char* pPAYMENT_TYPE = getXmlTagValue(ePAYMENT_TYPE, req_xml_info);

	if (pPAYMENT_TYPE == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE should not be NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	if (vhi_req_info->transtype) {
		free(vhi_req_info->transtype);
		vhi_req_info->transtype = NULL;
	}

	if (strncmp( pPAYMENT_TYPE, "GIFT", 4) == COMPARE_OK) { //optional group 9, 26
		/*
		 * GIFT TRANSACTIONS ONLY
		 * optional group 9, 26
		 */
		ptr = compose610GiftActivation(GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);

		if (ptr == NULL) {
			debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NULL;
		}

		uint len = strlen(ptr) + 1 + 1; //1=RS, 1=NULL
		vhi_req_info->transtype = (char*)calloc(len, sizeof(char));
		strcpy(vhi_req_info->transtype, ptr);
		appendHexRs(vhi_req_info->transtype);
		append610Group(eGROUP001, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP004, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP009, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
        append610Group(eGROUP018, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP023, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP026, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP034, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP038, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
	}
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE invalid", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INVALID_PAYMENTTYPE;

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi_req_info->transtype;
}

char* composeReloadRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* ptr = NULL;
	char* pPAYMENT_TYPE = getXmlTagValue(ePAYMENT_TYPE, req_xml_info);

	if (pPAYMENT_TYPE == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE should not be NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	if (vhi_req_info->transtype) {
		free(vhi_req_info->transtype);
		vhi_req_info->transtype = NULL;
	}

	if (strncmp( pPAYMENT_TYPE, "GIFT", 4) == COMPARE_OK) {
		/*
		 * GIFT TRANSACTIONS ONLY
		 * optional group 9, 26, 28
		 */
		ptr = compose610GiftReload(GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);

		if (ptr == NULL) {
			debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NULL;
		}

		uint len = strlen(ptr) + 1 + 1; //1=RS, 1=NULL
		vhi_req_info->transtype = (char*)calloc(len, sizeof(char));
		strcpy(vhi_req_info->transtype, ptr);
		appendHexRs(vhi_req_info->transtype);
		append610Group(eGROUP001, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP004, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP009, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
        append610Group(eGROUP018, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP023, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP026, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP028, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP034, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP038, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
	}
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE invalid", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INVALID_PAYMENTTYPE;

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi_req_info->transtype;
}

char* composeUnloadRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* ptr = NULL;
	char* pPAYMENT_TYPE = getXmlTagValue(ePAYMENT_TYPE, req_xml_info);

	if (pPAYMENT_TYPE == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE should not be NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	if (vhi_req_info->transtype) {
		free(vhi_req_info->transtype);
		vhi_req_info->transtype = NULL;
	}

	if (strncmp( pPAYMENT_TYPE, "GIFT", 4) == COMPARE_OK) {
		/*
		 * GIFT TRANSACTIONS ONLY
		 * optional group 9, 26, 28
		 */
		ptr = compose610GiftUnload(GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);

		if (ptr == NULL) {
			debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NULL;
		}

		uint len = strlen(ptr) + 1 + 1; //1=RS, 1=NULL
		vhi_req_info->transtype = (char*)calloc(len, sizeof(char));
		strcpy(vhi_req_info->transtype, ptr);
		appendHexRs(vhi_req_info->transtype);
		append610Group(eGROUP001, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP004, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP009, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
        append610Group(eGROUP018, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP023, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP026, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP028, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP034, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP038, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
	}
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE invalid", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INVALID_PAYMENTTYPE;

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi_req_info->transtype;
}

char* composeGiftCloseRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* ptr = NULL;
	char* pPAYMENT_TYPE = getXmlTagValue(ePAYMENT_TYPE, req_xml_info);

	if (pPAYMENT_TYPE == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE should not be NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	if (vhi_req_info->transtype) {
		free(vhi_req_info->transtype);
		vhi_req_info->transtype = NULL;
	}

	if (strncmp( pPAYMENT_TYPE, "GIFT", 4) == COMPARE_OK) {
		/*
		 * GIFT TRANSACTIONS
		 * optional group 9, 26, 28
		 */
		ptr = compose610GiftClose(GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);

		if (ptr == NULL) {
			debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NULL;
		}

		uint len = strlen(ptr) + 1 + 1; //1=RS, 1=NULL
		vhi_req_info->transtype = (char*)calloc(len, sizeof(char));
		strcpy(vhi_req_info->transtype, ptr);
		appendHexRs(vhi_req_info->transtype);
		append610Group(eGROUP001, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP004, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP009, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
        append610Group(eGROUP018, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP023, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP026, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP028, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP034, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP038, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
	}
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE invalid", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INVALID_PAYMENTTYPE;

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi_req_info->transtype;
}

char* composeBalanceRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* ptr = NULL;
	char* pPAYMENT_TYPE = getXmlTagValue(ePAYMENT_TYPE, req_xml_info);

	if (pPAYMENT_TYPE == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE should not be NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	if (vhi_req_info->transtype) {
		free(vhi_req_info->transtype);
		vhi_req_info->transtype = NULL;
	}

	if (strncmp( pPAYMENT_TYPE, "GIFT", 4) == COMPARE_OK) {
		/*
		 * GIFT
		 * optional group 9, 26, 28
		 */
		ptr = compose610GiftBalance(GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);

		if (ptr == NULL) {
			debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NULL;
		}

		uint len = strlen(ptr) + 1 + 1; //1=RS, 1=NULL
		vhi_req_info->transtype = (char*)calloc(len, sizeof(char));
		strcpy(vhi_req_info->transtype, ptr);
		appendHexRs(vhi_req_info->transtype);
		append610Group(eGROUP001, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP004, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP009, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
        append610Group(eGROUP018, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP023, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP026, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP028, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP034, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP038, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
	}
	else if (strncmp(pPAYMENT_TYPE, COMMAND.CREDIT, strlen(COMMAND.CREDIT)) == COMPARE_OK
	|| strncmp(pPAYMENT_TYPE, "DEBIT", 5) == COMPARE_OK)
	{
		/*
		 * DEBIT, CREDIT
		 * optional group 9, 26, 28
		 */
		ptr = compose610DebitCreditBalance(GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);

		if (ptr == NULL) {
			debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NULL;
		}

		uint len = strlen(ptr) + 1 + 1; //1=RS, 1=NULL
		vhi_req_info->transtype = (char*)calloc(len, sizeof(char));
		strcpy(vhi_req_info->transtype, ptr);
		appendHexRs(vhi_req_info->transtype);
		append610Group(eGROUP001, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP004, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP009, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP023, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP026, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP028, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP034, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP038, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
	}
    
    // EBT void/reversal
    else if (strncmp( pPAYMENT_TYPE, "EBT", 3) == COMPARE_OK)
    {
        ptr = compose610EBTBalance(&vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);
        if (ptr == NULL)
        {
            debug_sprintf(szDbgMsg, "%s: ERROR! 610 EBT balance message is NULL", __FUNCTION__);
            APP_TRACE(szDbgMsg);
            
            return NULL;
        }
    }
    
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE invalid", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INVALID_PAYMENTTYPE;

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi_req_info->transtype;
}

char* composePreAuthRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* ptr = NULL;
	char* pPAYMENT_TYPE = getXmlTagValue(ePAYMENT_TYPE, req_xml_info);

	if (pPAYMENT_TYPE == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE should not be NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	if (vhi_req_info->transtype) {
		free(vhi_req_info->transtype);
		vhi_req_info->transtype = NULL;
	}

	if (strncmp( pPAYMENT_TYPE, "GIFT", 4) == COMPARE_OK)
	{
		/*
		 * GIFT TRANSACTIONS
		 * optional group 9, 26
		 */
		ptr = compose610GiftPreauthorization(GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);

		if (ptr == NULL) {
			debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NULL;
		}

		uint len = strlen(ptr) + 1 + 1; //1=RS, 1=NULL
		vhi_req_info->transtype = (char*)calloc(len, sizeof(char));
		strcpy(vhi_req_info->transtype, ptr);
		appendHexRs(vhi_req_info->transtype);
		append610Group(eGROUP001, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP004, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP009, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
        append610Group(eGROUP018, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP023, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP026, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP034, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP038, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
	}
	else if (strncmp( pPAYMENT_TYPE, "CREDIT", 6) == COMPARE_OK)
	{
		char* pCUSTOMER_STREET 	= getXmlTagValue(eCUSTOMER_STREET, req_xml_info);
		char* pCUSTOMER_ZIP 	= getXmlTagValue(eCUSTOMER_ZIP, req_xml_info);
		char* pPRESENT_FLAG 	= getXmlTagValue(ePRESENT_FLAG, req_xml_info);
		char* pCARD_TOKEN 		=   getXmlTagValue(eCARD_TOKEN, req_xml_info);

		if (pPRESENT_FLAG == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: ERROR! PRESENT_FLAG is NULL during AUTH", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(pCARD_TOKEN != NULL)
			{
				if( (pCUSTOMER_STREET != NULL) || (pCUSTOMER_ZIP != NULL) ) //Praveen_P1: 26 September: Need to send AVS details for card token based transaction
				{
					debug_sprintf(szDbgMsg, "%s: Card Token is present and Customer Street/Customer Zip is present, so setting to manual entry mode", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					pPRESENT_FLAG = MANUAL_ENTRY;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Card Token is present, so setting to swiped mode", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					pPRESENT_FLAG = SWIPED_ENTRY; //TODO Please check if this is fine
				}
			}
			else
			{
				GLOBAL->http.status_code = eHTTP_OK;
				GLOBAL->appErrCode = eERR_REQUEST_ERROR;

				return NULL;
			}
		}

		if ((strncmp(pPRESENT_FLAG, MANUAL_ENTRY, 1) == COMPARE_OK )
		&& (pCUSTOMER_STREET != NULL || pCUSTOMER_ZIP != NULL))
		{
			/*
			 * CREDIT TRANSACTIONS
			 * optional group 9, 26
			 */
			ptr = compose610CreditPreauthorizationAVS(&vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);

			if (ptr == NULL) {
				debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return NULL;
			}
		}
		else
		{
			/*
			 * CREDIT TRANSACTIONS
			 * optional group 9, 26
			 */
			ptr = compose610CreditPreauthorization(&vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);

			if (ptr == NULL) {
				debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return NULL;
			}
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE invalid", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INVALID_PAYMENTTYPE;

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi_req_info->transtype;
}

char* composeCompletionRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* ptr = NULL;
	char* pPAYMENT_TYPE = getXmlTagValue(ePAYMENT_TYPE, req_xml_info);

	if (pPAYMENT_TYPE == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE should not be NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	if (vhi_req_info->transtype) {
		free(vhi_req_info->transtype);
		vhi_req_info->transtype = NULL;
	}

	if (strncmp( pPAYMENT_TYPE, "GIFT", 4) == COMPARE_OK) {

		/*
		 * GIFT TRANSACTIONS
		 * optional group 9, 26, 28
		 */
		ptr = compose610GiftCompletion(GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);

		if (ptr == NULL) {
			debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			GLOBAL->http.status_code = eHTTP_OK;
			GLOBAL->appErrCode = eERR_REQUEST_ERROR; //currently sending as request error, need to send exact reason for failing

			return NULL;
		}

		uint len = strlen(ptr) + 1 + 1; //1=RS, 1=NULL
		vhi_req_info->transtype = (char*)calloc(len, sizeof(char));
		strcpy(vhi_req_info->transtype, ptr);
		appendHexRs(vhi_req_info->transtype);
		append610Group(eGROUP001, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP004, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP009, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP014, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
        append610Group(eGROUP018, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP023, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP026, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP028, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP034, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP038, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);

	}
	else if (strncmp( pPAYMENT_TYPE, "CREDIT", 4) == COMPARE_OK)
	{
		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INVALID_PAYMENTTYPE;

		return NULL;
#if 0
		/*
		 * CREDIT TRANSACTIONS
		 * optional group 9, 26, 28
		 */
		ptr = compose610GiftCompletion(GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);

		if (ptr == NULL) {
			debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			GLOBAL->http.status_code = eHTTP_OK;
			GLOBAL->appErrCode = eERR_REQUEST_ERROR; //currently sending as request error, need to send exact reason for failing

			return NULL;
		}

		uint len = strlen(ptr) + 1 + 1; //1=RS, 1=NULL
		vhi_req_info->transtype = (char*)calloc(len, sizeof(char));
		strcpy(vhi_req_info->transtype, ptr);
		appendHexRs(vhi_req_info->transtype);
		append610Group(eGROUP001, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP004, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP009, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP014, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP018, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP023, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP026, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP028, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP034, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP038, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
#endif
	}
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE invalid", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INVALID_PAYMENTTYPE;

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi_req_info->transtype;
}

char* composeBatchInquiryRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* ptr = NULL;

	if (vhi_req_info->transtype) {
		free(vhi_req_info->transtype);
		vhi_req_info->transtype = NULL;
	}

	ptr = compose610BatchInquiry(GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);

	if (ptr == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	uint len = strlen(ptr) + 1 + 1; //1=RS, 1=NULL
	vhi_req_info->transtype = (char*)calloc(len, sizeof(char));
	strcpy(vhi_req_info->transtype, ptr);
	appendHexRs(vhi_req_info->transtype);
	append610Group(eGROUP009, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi_req_info->transtype;
}

char* composeBatchReleaseRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* ptr = NULL;

	if (vhi_req_info->transtype) {
		free(vhi_req_info->transtype);
		vhi_req_info->transtype = NULL;
	}

	ptr = compose610BatchRelease(GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);

	if (ptr == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	uint len = strlen(ptr) + 1 + 1; //1=RS, 1=NULL
	vhi_req_info->transtype = (char*)calloc(len, sizeof(char));
	strcpy(vhi_req_info->transtype, ptr);
	appendHexRs(vhi_req_info->transtype);
	append610Group(eGROUP009, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi_req_info->transtype;
}

char* composeTokenizationRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* ptr = NULL;
	char* pPAYMENT_TYPE = getXmlTagValue(ePAYMENT_TYPE, req_xml_info);

	if (pPAYMENT_TYPE == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE should not be NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	if (vhi_req_info->transtype) {
		free(vhi_req_info->transtype);
		vhi_req_info->transtype = NULL;
	}

	if (strncmp(pPAYMENT_TYPE, "ADMIN", 5) == COMPARE_OK)
	{
		/*
		 * optional group 9, 28
		 */
		ptr = compose610Tokenization(GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);

		if (ptr == NULL) {
			debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NULL;
		}

		uint len = strlen(ptr) + 1 + 1; //1=RS, 1=NULL
		vhi_req_info->transtype = (char*)calloc(len, sizeof(char));
		strcpy(vhi_req_info->transtype, ptr);
		appendHexRs(vhi_req_info->transtype);
		append610Group(eGROUP001, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP004, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP009, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP023, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP026, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info); //if E2EE is enabled
		append610Group(eGROUP028, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP034, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
		append610Group(eGROUP038, &vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info);
	}
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE invalid", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INVALID_PAYMENTTYPE;

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi_req_info->transtype;
}

// composeEMVOfflineAdvice
//
// Used to send advice to host about an offline EMV sale/return
//
char *composeEMVOfflineAdvice(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
    char* ptr = NULL;
    
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    ptr= compose610EMVOfflineAdvice(&vhi_req_info->transtype, GLOBAL, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);;
    if (ptr == NULL) {
        debug_sprintf(szDbgMsg, "%s: ERROR! 610 base message is NULL", __FUNCTION__);
        APP_TRACE(szDbgMsg);
        
        return NULL;
    }
    
    return vhi_req_info->transtype;
}


char* composeVhiRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	int		iAppLogEnabled		= 0;
	char	szAppLogData[300]	= "";
	//char	szAppLogDiag[256]	= "";

	iAppLogEnabled = isAppLogEnabled();

	char* pFUNCTION_TYPE = getXmlTagValue(eFUNCTION_TYPE, req_xml_info);
	char* pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);

	char* ptr = NULL;

	if (pFUNCTION_TYPE == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! FUNCTION_TYPE is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	if (pCOMMAND == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! COMMAND is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	/*TUT*/
	char* pCARD_TOKEN = getXmlTagValue(eCARD_TOKEN, req_xml_info);

	if (pCARD_TOKEN != NULL && GCONFIG->eTokenMode == eTOKEN_TRT) {

		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "Received Card Token, Changing TokenMode to TUT");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData,NULL);
		}
		GCONFIG->eTokenMode = eTOKEN_TUT;
	}
	/*end*/

	/*TOR case*/
	if (GLOBAL->http.status_code == eHTTP_GATEWAY_TIMEOUT) {
		pCOMMAND = NULL;
		pCOMMAND = "VOID";

		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "Internally Changing the Command to [VOID]");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData,NULL);
		}
	}
	/*end*/

	if (vhi_req_info->basegroup) {
		free(vhi_req_info->basegroup);
		vhi_req_info->basegroup = NULL;
	}

	if(iAppLogEnabled)
	{
		sprintf(szAppLogData, "Framing %s Command Request to Vantiv Host", pCOMMAND);
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
	}

	if (strncmp(pFUNCTION_TYPE, FUNCTION_TYPE.PAYMENT, strlen(FUNCTION_TYPE.PAYMENT)) == COMPARE_OK) {

		if (strncmp(pCOMMAND, COMMAND.PRE_AUTH, strlen(COMMAND.PRE_AUTH)) == COMPARE_OK) {
			ptr = composePreAuthRequest(GLOBAL, vhi_req_info, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);
		}
		else if (strncmp(pCOMMAND, COMMAND.SALE, strlen(COMMAND.SALE)) == COMPARE_OK) {
			ptr = composeSaleRequest(GLOBAL, vhi_req_info, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);
		}
		else if (strncmp(pCOMMAND, COMMAND.CREDIT, strlen(COMMAND.CREDIT)) == COMPARE_OK) {
			ptr = composeCreditRequest(GLOBAL, vhi_req_info, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);
		}
		else if (strncmp(pCOMMAND, COMMAND.VOID, strlen(COMMAND.VOID)) == COMPARE_OK) {
			ptr = composeVoidRequest(GLOBAL, vhi_req_info, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);
		}
		else if (strncmp(pCOMMAND, COMMAND.POST_AUTH, strlen(COMMAND.POST_AUTH)) == COMPARE_OK) {
			ptr = composePostAuthRequest(GLOBAL, vhi_req_info, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);
		}
		else if (strncmp(pCOMMAND, COMMAND.ADD_TIP, strlen(COMMAND.ADD_TIP)) == COMPARE_OK) {
			ptr = composePostAuthRequest(GLOBAL, vhi_req_info, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);
		}
		else if (strncmp(pCOMMAND, COMMAND.COMPLETION, strlen(COMMAND.COMPLETION)) == COMPARE_OK) {
			ptr = composeCompletionRequest(GLOBAL, vhi_req_info, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);
		}
		else if (strncmp(pCOMMAND, COMMAND.ADD_VALUE, strlen(COMMAND.ADD_VALUE)) == COMPARE_OK) {
			ptr = composeReloadRequest(GLOBAL, vhi_req_info, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);
		}
		else if (strncmp(pCOMMAND, COMMAND.REMOVE_VALUE, strlen(COMMAND.REMOVE_VALUE)) == COMPARE_OK) {
			ptr = composeUnloadRequest(GLOBAL, vhi_req_info, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);
		}
		else if (strncmp(pCOMMAND, COMMAND.ACTIVATE, strlen(COMMAND.ACTIVATE)) == COMPARE_OK) {
			ptr = composeActivateRequest(GLOBAL, vhi_req_info, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);
		}
		else if (strncmp(pCOMMAND, COMMAND.DEACTIVATE, strlen(COMMAND.DEACTIVATE)) == COMPARE_OK) {
			ptr = composeGiftCloseRequest(GLOBAL, vhi_req_info, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);
		}
		else if (strncmp(pCOMMAND, COMMAND.BALANCE, strlen(COMMAND.BALANCE)) == COMPARE_OK) {
			ptr = composeBalanceRequest(GLOBAL, vhi_req_info, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);
		}
        
        // EMV offline approval advice
        else if (strncmp(pCOMMAND, COMMAND.EMV_ADVICE, strlen(COMMAND.EMV_ADVICE)) == COMPARE_OK) {
            ptr = composeEMVOfflineAdvice(GLOBAL, vhi_req_info, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);
        }
        
        /*temporary signature request*/
		else if (strncmp(pCOMMAND, COMMAND.SIGNATURE, strlen(COMMAND.SIGNATURE)) == COMPARE_OK) {
			ptr = "No signature Vantiv request";
		}
		/*end*/
		else if (strncmp(pCOMMAND, "PROCESSOR", 9) == COMPARE_OK) {
			ptr = "PROCESSOR CHECKING REQUEST";
		}
		else if (strncmp(pCOMMAND, COMMAND.TOKEN_QUERY, strlen(COMMAND.TOKEN_QUERY)) == COMPARE_OK) {
			ptr = composeTokenizationRequest(GLOBAL, vhi_req_info, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);
		}
		else {
			debug_sprintf(szDbgMsg, "%s: ERROR! COMMAND not supported", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			GLOBAL->http.status_code = eHTTP_OK;
			GLOBAL->appErrCode = eERR_INVALID_COMMAND;

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Failed to Frame Vantiv request, [%s] Command Not Supported", pCOMMAND);
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, NULL);
			}

			return NULL;
		}
	}
	else if (strncmp(pFUNCTION_TYPE, FUNCTION_TYPE.BATCH, strlen(FUNCTION_TYPE.BATCH)) == COMPARE_OK) {

		if (strncmp(pCOMMAND, COMMAND.SETTLE, strlen(COMMAND.SETTLE)) == COMPARE_OK) {
			ptr = composeBatchReleaseRequest(GLOBAL, vhi_req_info, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);
		}
		else {
			debug_sprintf(szDbgMsg, "%s: ERROR! pCOMMAND not supported", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NULL;
		}
	}
	else if (strncmp(pFUNCTION_TYPE, FUNCTION_TYPE.REPORT, strlen(FUNCTION_TYPE.REPORT))==COMPARE_OK) {
#if 0 //Praveen_P1: Currently we are not supporting DAYSUMMARY command
		if (strncmp(pCOMMAND, COMMAND.DAYSUMMARY, strlen(COMMAND.DAYSUMMARY)) == COMPARE_OK) {
			ptr = composeBatchInquiryRequest(GLOBAL, vhi_req_info, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);
		}
		else {
			debug_sprintf(szDbgMsg, "%s: ERROR! COMMAND not supported", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			GLOBAL->http.status_code = eHTTP_OK;
			GLOBAL->appErrCode = eERR_INVALID_COMMAND;

			return NULL;
		}
#endif
		debug_sprintf(szDbgMsg, "%s: ERROR! COMMAND not supported", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INVALID_COMMAND;

		return NULL;
	}
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! not supported", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	if (ptr == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! ptr is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	if (vhi_req_info->basegroup) {
		free(vhi_req_info->basegroup);
		vhi_req_info->basegroup = NULL;
	}

	vhi_req_info->basegroup = (char*)calloc(strlen(ptr)+1, sizeof(vhi_req_info->basegroup));
	strncpy(vhi_req_info->basegroup, ptr, strlen(ptr));

	if (strlen(vhi_req_info->basegroup) <= 0) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Compose failed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	if(iAppLogEnabled)
	{
		sprintf(szAppLogData, "Successfully Framed %s Command Request For Vantiv Host", pCOMMAND);
		addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_PROCESSED, szAppLogData, NULL);
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi_req_info->basegroup;
}

char* composeTpsHeader(sVHI_REQ * vhi_req_info)
{
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[300]	= "";

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	_sTPS_HEADER info;
	char* basegroup = vhi_req_info->basegroup;

	if (basegroup==NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! basegroup should not be NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	memset(&info, 0x00, sizeof(info));
	memset(vhi_req_info->tps, 0x00, sizeof(vhi_req_info->tps));

	strcpy(info.REQUEST, "REQUEST=");
	strcpy(info.f1, "BT");

	char p[4+1] = {0};
	int baselen = strlen(basegroup);

	sprintf(p, "%d", baselen); //length of message starting field 4
	precedeZero(sizeof(p), p);
	strcpy(info.f2, p);

	fillSpace(sizeof(info.f3), info.f3);

	strcpy(vhi_req_info->tps, info.REQUEST);
	strcat(vhi_req_info->tps, info.f1);
	strcat(vhi_req_info->tps, info.f2);
	strcat(vhi_req_info->tps, info.f3);

	if (strlen(vhi_req_info->tps) != sizeof(vhi_req_info->tps)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! TPS header not exact in bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	if (vhi_req_info->tps==NULL || strlen(vhi_req_info->tps)<=0) {
		debug_sprintf(szDbgMsg, "%s: ERROR! TPS is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	if(iAppLogEnabled)
	{
		strcpy(szAppLogData, "Successfully Framed TPS Header For the Request");
		addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_PROCESSED, szAppLogData,NULL);
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi_req_info->tps;
}

int composeVantivRequest(sGLOBALS* GLOBAL, sVHI_REQ * pstvhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{

	sVHI_REQ vhi_req_info;

	memset(&vhi_req_info, 0x00, sizeof(sVHI_REQ));

	memcpy(&vhi_req_info, pstvhi_req_info, sizeof(sVHI_REQ));

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/*
	 * This is the request to VANTIV
	 * Not including HTTP headers
	 * Including "REQUEST="
	 * Including TPS Header
	 */
	char* basegroup = composeVhiRequest(GLOBAL, &vhi_req_info, GCONFIG, req_xml_info, vhi610info, vhi_fieldinfo);

	if (basegroup == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Base/Group should not be NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
		//GLOBAL->http.status_code = eHTTP_OK;
		//GLOBAL->appErrCode = eERR_INTERNAL_ERROR;

		memcpy(pstvhi_req_info, &vhi_req_info, sizeof(sVHI_REQ));

		return EXIT_FAILURE;
	}

	char* tpsheader = composeTpsHeader(&vhi_req_info);

	if (tpsheader == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! TPS should not be NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INTERNAL_ERROR;

		memcpy(pstvhi_req_info, &vhi_req_info, sizeof(sVHI_REQ));

		return EXIT_FAILURE;
	}

	uint bglen = strlen(basegroup);
	uint tpslen = strlen(tpsheader);

	if (vhi_req_info.main != NULL) {
		free(vhi_req_info.main);
		vhi_req_info.main = NULL;
	}

	vhi_req_info.main = (char*)calloc(bglen+tpslen+1, sizeof(char));
	strcpy(vhi_req_info.main, tpsheader);
	strcat(vhi_req_info.main, basegroup);

	if (vhi_req_info.main == NULL || strlen(vhi_req_info.main)<=0) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Vantiv request should not be NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INTERNAL_ERROR;

		memcpy(pstvhi_req_info, &vhi_req_info, sizeof(sVHI_REQ));

		return EXIT_FAILURE;
	}

	memcpy(pstvhi_req_info, &vhi_req_info, sizeof(sVHI_REQ));

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}
#if 0
char* getVantivRequest()
{
	return vhi_req_info.main;
}
#endif
