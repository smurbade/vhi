/*
 * tcpip_server.c
 *
 *  Created on: Nov 20, 2013
 *      Author: Dexter M. Alberto
 */


#include <sys/socket.h>       /*  socket definitions        */
#include <sys/types.h>        /*  socket types              */
#include <sys/wait.h>         /*  for waitpid()             */
#include <arpa/inet.h>        /*  inet (3) funtions         */
#include <unistd.h>           /*  misc. UNIX functions      */
#include <sys/ioctl.h>
#include <pthread.h>
#include <svc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "logger.h"
#include "tcpip_server.h"
#include "http_handler.h"
#include "service_request.h"
#include "config_data.h"
#include "appLog.h"

extern int addJobToThreadPool(void *(*function_p)(int ), int arg_p);

int mainServerThreaded()
{
#ifdef LOGGING_ENABLED
    char szDbgMsg[128] = {0};
#endif
    int 	status;
	int		iAppLogEnabled		= 0;
	char	szAppLogData[256]	= "";
	char	szAppLogDiag[256]	= "";
	char	szIPPrt[50]			= "";
    int 	socket_desc , client_sock , c;
    int		iOn					= 1;
    int		rv					= 0;
    struct sockaddr_in server , client;

    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);

    iAppLogEnabled = isAppLogEnabled();

    //Create socket
    if ((socket_desc = socket(AF_INET , SOCK_STREAM , 0)) < 0) {
    	debug_sprintf(szDbgMsg, "%s: ERROR! Could not create socket", __FUNCTION__);
    	APP_TRACE(szDbgMsg);

    	if(iAppLogEnabled)
    	{
    		strcpy(szAppLogData, "Socket Creation Failed");
    		strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
    		addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
    	}

    	return EXIT_FAILURE;
    }

    debug_sprintf(szDbgMsg, "%s: Socket created", __FUNCTION__);
    APP_TRACE(szDbgMsg);

    /* Socket Option: Set socket as reusable */
	rv = setsockopt(socket_desc, SOL_SOCKET, SO_REUSEADDR, (char *)&iOn,
															sizeof(iOn));
	if(rv != EXIT_SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to make socket reusable",
															__FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Failed To Set Socket Options(REUSABLE), Return Value[%d]", rv);
			strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
			addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
		}

		return EXIT_FAILURE;
	}
#if 0
	/* Socket Option: Set socket as non blocking */
	rv = ioctl(socket_desc, FIONBIO, &iOn);
	if(rv != EXIT_SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to make socket non-blocking",
															__FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}
#endif
    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr(LOCAL_HOST);
    
    // CAN BE USED TO ALLOW EXTERNAL CONNECTION FOR DEBUG
    //server.sin_addr.s_addr = INADDR_ANY;
    
    server.sin_port = htons(SERVER_PORT);

    //Bind
    if ((status=bind(socket_desc,(struct sockaddr *)&server , sizeof(server))) < 0) {
        debug_sprintf(szDbgMsg, "%s: ERROR! bind failed. Error %d", __FUNCTION__, status);
        APP_TRACE(szDbgMsg);

        if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Failed To Bind the Server, Return Value[%d]", rv);
			strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
			addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
		}

        return EXIT_FAILURE;
    }

    debug_sprintf(szDbgMsg, "%s: Bind done", __FUNCTION__);
    APP_TRACE(szDbgMsg);

    //Listen
    if (listen(socket_desc , 1) < 0) {
    	debug_sprintf(szDbgMsg, "%s: ERROR! listen error", __FUNCTION__);
    	APP_TRACE(szDbgMsg);

    	if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Failed To Listen To Connections, Return Value[%d]", rv);
			strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
			addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
		}

    	return EXIT_FAILURE;
    }

    //Accept and incoming connection
    debug_sprintf(szDbgMsg, "%s: Waiting for incoming connections...", __FUNCTION__);
    APP_TRACE(szDbgMsg);

    if(iAppLogEnabled)
    {
    	strcpy(szAppLogData, "VHI Server Started. Waiting for Incoming Connections...");
    	addAppEventLog(APP_NAME, ENTRYTYPE_INFO,ENTRYID_START_UP , szAppLogData, NULL);
    }
    c = sizeof(struct sockaddr_in);

//    pthread_t sniffer_thread;
//    void *ret;

    while(1)
    {
    	client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
    	if(client_sock < 0)
		{
			if((errno != EAGAIN) && (errno !=  EWOULDBLOCK))
			{
				debug_sprintf(szDbgMsg, "%s: accept FAILED", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				svcWait(500);
				continue;
			}
		}
    	else
		{
    		/* ------- Got the new connection -------- */
			/* Get the connection information string for the new connection. The
			 * connection information string consists of IP and the port number
			 * of the POS client in the format specified below
			 * xxx.xxx.xxx.xxx:yyyy */
			sprintf(szIPPrt, "%s:%d", inet_ntoa(client.sin_addr), htons(client.sin_port));

			debug_sprintf(szDbgMsg, "%s: Connection accepted from [%s] on %d",__FUNCTION__, szIPPrt, client_sock);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, START_INDICATOR);
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START, szAppLogData, NULL);

				strcpy(szAppLogData, "Accepted New Connection From Client, Adding Job to the Service thread");
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START, szAppLogData, NULL);
			}

#if 0
			new_sock = malloc(1*sizeof(int));
			*new_sock = client_sock;
#endif

			/* Need to assign service request to thread */
			rv = addJobToThreadPool((void *)onServiceRequestThread, client_sock);
			if(rv != EXIT_SUCCESS)
			{
				debug_sprintf(szDbgMsg, "%s: Error while adding the job to the threadpool", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Error While Adding Job to the Queue");
					strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
				}
				//TODO What to do here?? Should we retry to add the job again??
			}
#if 0
			if (pthread_create( &sniffer_thread , NULL ,  onServiceRequestThread , (void*) new_sock) < 0){
				debug_sprintf(szDbgMsg, "%s: ERROR! pthread_create() error", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					strcpy(szAppLogData, "Error While Adding Job to the Service thread");
					strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
					addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_START_UP, szAppLogData, szAppLogDiag);
				}

			}
#endif
	#if 0
			/*Praveen_P1: Commenting the pthead_join call so that
			multiple requests from SCA can be honoured by VHI module
			This is done to fix the response timed out error reported from production
			when SAF reocrds are present on the terminal, it would check the Host connection status
			at the same time if online request comes, it will be waited till host connection status
			is done. In the mean time SCA is getting timed out
			*/
			/*
			 * Praveen_P1: We will wait for this thread to finish here
			 * we are allowing only one transaction at a time either
			 * SAF or Online transaction
			 */
			if (pthread_join(sniffer_thread, &ret) != 0) {
				debug_sprintf(szDbgMsg, "%s: ERROR! pthread_join() error", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
	#endif
		}
		svcWait(15);
    }

    //We shouldnt get here
    if (client_sock < 0) {
        debug_sprintf(szDbgMsg, "%s: accept failed", __FUNCTION__);
        APP_TRACE(szDbgMsg);
        return EXIT_FAILURE;
    }

    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);

    return EXIT_FAILURE;
}
