/*
 * vantiv_response.c
 *
 *  Created on: Nov 5, 2013
 *      Author: Dexter M. Alberto
 */


#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "vantiv_response.h"
#include "define_data.h"
#include "logger.h"
#include "config_data.h"
#include "sca_xml_request.h"
#include "appLog.h"

#ifdef LOGGING_ENABLED
static char    szDbgMsg[1024];
#endif

static void saveRany(sVANTIV_RESPONSE * vrinfo, char *destination, int max_length, char *group_name);

void freeVantivResponseData(sVANTIV_RESPONSE * vrinfo)
{
	if (vrinfo->base) {
		free(vrinfo->base);
		vrinfo->base = NULL;
	}

	if (vrinfo->buffer) {
		free(vrinfo->buffer);
		vrinfo->buffer = NULL;
	}

	if (vrinfo->group) {
		free(vrinfo->group);
		vrinfo->group = NULL;
	}

	//Reset the whole structure
	memset(vrinfo, 0x00, sizeof(sVANTIV_RESPONSE));
}


int parseVantivResponse(char* ptr, sGLOBALS* GLOBAL, sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo, sVANTIV_BN *svtvbn, _sSCA_XML_REQUEST *req_xml_info)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);
	int		iAppLogEnabled		= 0;
	char	szAppLogData[2560]	= "";

	iAppLogEnabled = isAppLogEnabled();

	/*Processor checking*/
	if (pCOMMAND != NULL) {
		if (strncmp(pCOMMAND, "PROCESSOR", 9) == COMPARE_OK) {
			debug_sprintf(szDbgMsg, "%s: NOTE! Processor checking no need to parse", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return EXIT_SUCCESS;
		}
	}
	/*end*/

	/*For SIGNATURE pCOMMAND*/
	if (pCOMMAND != NULL) {
		if (strncmp( pCOMMAND, COMMAND.SIGNATURE, strlen(COMMAND.SIGNATURE) ) == COMPARE_OK) {
			debug_sprintf(szDbgMsg, "%s: NOTE! No SIGNATURE Vantiv request", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return EXIT_SUCCESS;
		}
	}
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! COMMAND is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INTERNAL_ERROR;

		return EXIT_FAILURE;
	}
	/*end*/

	if(iAppLogEnabled)
	{
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_VANTIV_RESPONSE, ptr, NULL);

		strcpy(szAppLogData, "Parsing the Response Received From Vantiv");
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
	}

	char* endptr = NULL;
	int itps_len = 0;
	char ctps_len[4+1] = {0};
	char response_code[3+1] = {0};

	if (ptr == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! ptr is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INTERNAL_ERROR;
		return EXIT_FAILURE;
	}

	if ( strlen(ptr) <= 0 ) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 0", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INTERNAL_ERROR;
		return EXIT_FAILURE;
	}

	memset(response_code, 0x00, sizeof(response_code));

	strncpy(response_code, ptr, 3);

	debug_sprintf(szDbgMsg, "%s: Response Code is %s", __FUNCTION__, response_code);
	APP_TRACE(szDbgMsg);

	if(strcmp(response_code, "000") == 0)
	{
		debug_sprintf(szDbgMsg, "%s: Normal response. Request was submitted and received by the Tandem host then sent off for authorization. Host returned authorization response", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}
	else if(strcmp(response_code, "100") == 0)
	{
		debug_sprintf(szDbgMsg, "%s: A request was received by Tandem host but packet contained no data", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_RESP_ERR_100;

		return EXIT_SUCCESS;
	}
	else if(strcmp(response_code, "101") == 0)
	{
		debug_sprintf(szDbgMsg, "%s: TCP/IP Message Header was not formatted properly. Request was not submitted for authorization.", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_RESP_ERR_101;

		return EXIT_SUCCESS;
	}
	else if(strcmp(response_code, "110") == 0)
	{
		debug_sprintf(szDbgMsg, "%s: Unable to connect to Vantiv financial processors. Request was not submitted to Vantiv financial processors.", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		GLOBAL->isProcessorAvailable = false;
		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_NOERROR;

		return EXIT_SUCCESS;
	}
	else if(strcmp(response_code, "121") == 0)
	{
		debug_sprintf(szDbgMsg, "%s: Request was submitted, but the connection to the Vantiv financial processors was unexpectedly shutdown before the response was received", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_RESP_ERR_121;

		return EXIT_SUCCESS;
	}

	//temporary
	endptr = strstr(ptr, "BT"); //search first occurence of BT

	if (endptr == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! No such TPS BT header", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INTERNAL_ERROR;
		return EXIT_FAILURE;
	}

	ptr += (endptr-ptr); //skip the first "000 "

	if (strncmp(ptr, "BT", 2) != COMPARE_OK) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Skip of TPS error", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INTERNAL_ERROR;
		return EXIT_FAILURE;
	}

	ptr+=2; //skip BT
    strcpy(ctps_len, "");
	strncat(ctps_len, ptr, 4); //copy field 2
	itps_len = atoi(ctps_len); //convert to int
	ptr+=19; //skip field 2 and 3 of tps

	debug_sprintf(szDbgMsg, "%s: itps_len is %d", __FUNCTION__, itps_len);
	APP_TRACE(szDbgMsg);

	uint len = strlen(ptr);

	if (len != itps_len) {
		debug_sprintf(szDbgMsg, "%s: ERROR! TPS length no match with field 4", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INTERNAL_ERROR;
		return EXIT_FAILURE;
	}

	/* save total length of payload */
	vrinfo->bufferlen = len;

	if (vrinfo->buffer) {
		free(vrinfo->buffer);
		vrinfo->buffer = NULL;
	}

	vrinfo->buffer = (char*)calloc(len+1, sizeof(char));
	strncpy(vrinfo->buffer, ptr, len);

	if (saveVantivBaseResponse(vrinfo) != EXIT_SUCCESS) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Response Base", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	if (saveVantivGroupResponse(vrinfo) != EXIT_SUCCESS) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Response Group", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	if (saveVantivBitNumberResponse(vrinfo, svtvbn) != EXIT_SUCCESS) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Saving Bit Number", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	if (identifyVantivResponse(vrinfo, svtvbn) != EXIT_SUCCESS) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Identifying response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	if (parseIdentifiedVantivResponse(vrinfo, svbninfo) != EXIT_SUCCESS) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Parsing response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

int saveVantivBaseResponse(sVANTIV_RESPONSE * vrinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* endptr = NULL;

	endptr = strchr(vrinfo->buffer, '\x1E'); //find RS in base

	if (endptr == NULL) {
		vrinfo->baselen = strlen(vrinfo->buffer);
	}
	else {
		vrinfo->baselen = endptr - vrinfo->buffer;
	}


	if (vrinfo->baselen <= 0) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 0 bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	if (vrinfo->base) {
		free(vrinfo->base);
		vrinfo->base = NULL;
	}

	vrinfo->base = (char*)calloc(vrinfo->baselen+1, sizeof(char));

	int i;
	for (i=0; i<vrinfo->baselen; i++) {
		if (vrinfo->buffer[i] != '\x1E' || vrinfo->buffer[i] != '\x1D')
			vrinfo->base[i] = vrinfo->buffer[i];
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

int saveVantivGroupResponse(sVANTIV_RESPONSE * vrinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	/* compute for total number of group */
	char* endptr = NULL;
	vrinfo->totalgrp = 0;

	int i = 0;
	for(i=0; i<vrinfo->bufferlen; i++) {
		if (vrinfo->buffer[i] == '\x1D') { //GS
			vrinfo->totalgrp++;
		}
	}

	if (vrinfo->totalgrp <= 0) {
		if (vrinfo->group) {
			free(vrinfo->group);
		}
		vrinfo->grouplen = 0;
		vrinfo->group = NULL;

		return EXIT_SUCCESS;
	}

	endptr = strchr(vrinfo->buffer, '\x1E'); //RS

	if (endptr != NULL) {
		endptr++; //eliminate RS in offset 0
		vrinfo->grouplen = strlen(endptr);

		if (vrinfo->group) {
			free(vrinfo->group);
			vrinfo->group = NULL;
		}

		vrinfo->group = (char*)calloc(vrinfo->grouplen+1, sizeof(char));
		strncpy(vrinfo->group, endptr, vrinfo->grouplen);
	}
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! No RS found", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	saveR007(vrinfo);
	saveR008(vrinfo);
	saveR011(vrinfo);
    saveR017(vrinfo);
    saveR023(vrinfo);
    saveR029(vrinfo);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

int saveVantivBitNumberResponse(sVANTIV_RESPONSE * vrinfo, sVANTIV_BN *svtvbn)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* ptr = vrinfo->base;

	if (ptr == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	memset(svtvbn, 0x00, sizeof(sVANTIV_BN));

	int i;
	for(i=0; i<(sizeof(svtvbn->bn00) - 1); i++) {
		svtvbn->bn00[i] = *ptr++;
	}

	for(i=0; i<(sizeof(svtvbn->bnbmt) - 1); i++) {
		svtvbn->bnbmt[i] = *ptr++;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

int identifyVantivResponse(sVANTIV_RESPONSE * vrinfo, sVANTIV_BN *svtvbn)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if (strncmp(svtvbn->bn00, "0110", 4) == COMPARE_OK) {
		if (strncmp(svtvbn->bnbmt, "61", 2) == COMPARE_OK) {
			vrinfo->RESPONSEID = e0110_61;
		}
		else if (strncmp(svtvbn->bnbmt, "62", 2) == COMPARE_OK) {
			vrinfo->RESPONSEID = e0110_62;
		}
		else if (strncmp(svtvbn->bnbmt, "90", 2) == COMPARE_OK) {
			if (strlen(vrinfo->base) == 107) {
				vrinfo->RESPONSEID = e0110_90; //Approval only
			}
			else if (strlen(vrinfo->base) == 116) {
				vrinfo->RESPONSEID = e0110_90_GC; //GC Balance Inquiry
			}
			else {
				debug_sprintf(szDbgMsg, "%s: ERROR! base must be 107 or 116 in bytes", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}
		}
		else if (strncmp(svtvbn->bnbmt, "99", 2) == COMPARE_OK) {
			vrinfo->RESPONSEID = e0110_99;
		}
		else if (strncmp(svtvbn->bnbmt, "53", 2) == COMPARE_OK) {
			vrinfo->RESPONSEID = e0110_53;
		}
		else {
			debug_sprintf(szDbgMsg, "%s: ERROR! Vantiv response not supported", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return EXIT_FAILURE;
		}
	}
	else if (strncmp(svtvbn->bn00, "0210", sizeof(svtvbn->bn00)-1) == COMPARE_OK) {
		if (strncmp(svtvbn->bnbmt, "61", 2) == COMPARE_OK) {
			vrinfo->RESPONSEID = e0210_61;
		}
		else if (strncmp(svtvbn->bnbmt, "62", 2) == COMPARE_OK) {
			vrinfo->RESPONSEID = e0210_62;
		}
		else if (strncmp(svtvbn->bnbmt, "63", 2) == COMPARE_OK) {
			vrinfo->RESPONSEID = e0210_63;
		}
		else if (strncmp(svtvbn->bnbmt, "91", 2) == COMPARE_OK) {
			vrinfo->RESPONSEID = e0210_91;
		}
		else if (strncmp(svtvbn->bnbmt, "99", 2) == COMPARE_OK) {
			vrinfo->RESPONSEID = e0210_99;
		}
		else {
			debug_sprintf(szDbgMsg, "%s: ERROR! Vantiv response not supported", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return EXIT_FAILURE;
		}
	}
	else if (strncmp(svtvbn->bn00, "0230", sizeof(svtvbn->bn00)-1) == COMPARE_OK) {
		if (strncmp(svtvbn->bnbmt, "62", 2) == COMPARE_OK) {
			vrinfo->RESPONSEID = e0230_62;
		}
		else if (strncmp(svtvbn->bnbmt, "91", 2) == COMPARE_OK) {
			vrinfo->RESPONSEID = e0230_91;
		}
		else if (strncmp(svtvbn->bnbmt, "99", 2) == COMPARE_OK) {
			vrinfo->RESPONSEID = e0230_99;
		}
		else {
			debug_sprintf(szDbgMsg, "%s: ERROR! Vantiv response not supported", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return EXIT_FAILURE;
		}
	}
	else if (strncmp(svtvbn->bn00, "0330", sizeof(svtvbn->bn00)-1) == COMPARE_OK) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Vantiv response not supported", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}
	else if (strncmp(svtvbn->bn00, "0410", sizeof(svtvbn->bn00)-1) == COMPARE_OK) {
		if (strncmp(svtvbn->bnbmt, "61", 2) == COMPARE_OK) {
			vrinfo->RESPONSEID = e0410_61;
		}
		else if (strncmp(svtvbn->bnbmt, "62", 2) == COMPARE_OK) {
			vrinfo->RESPONSEID = e0410_62;
		}
		else if (strncmp(svtvbn->bnbmt, "63", 2) == COMPARE_OK) {
			vrinfo->RESPONSEID = e0410_63;
		}
		else if (strncmp(svtvbn->bnbmt, "91", 2) == COMPARE_OK) {
			vrinfo->RESPONSEID = e0410_91;
		}
		else if (strncmp(svtvbn->bnbmt, "99", 2) == COMPARE_OK) {
			vrinfo->RESPONSEID = e0410_99;
		}
		else {
			debug_sprintf(szDbgMsg, "%s: ERROR! Vantiv response not supported", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return EXIT_FAILURE;
		}
	}
	else if (strncmp(svtvbn->bn00, "0510", sizeof(svtvbn->bn00)-1) == COMPARE_OK) {
		if (strncmp(svtvbn->bnbmt, "92", 2) == COMPARE_OK) {
			vrinfo->RESPONSEID = e0510_92;
		}
		else if (strncmp(svtvbn->bnbmt, "99", 2) == COMPARE_OK) {
			vrinfo->RESPONSEID = e0510_99;
		}
		else {
			debug_sprintf(szDbgMsg, "%s: ERROR! Vantiv response not supported", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return EXIT_FAILURE;
		}
	}
	else if (strncmp(svtvbn->bn00, "0810", sizeof(svtvbn->bn00)-1) == COMPARE_OK) {
        if (strncmp(svtvbn->bnbmt, "99", 2) == COMPARE_OK)
        {
            vrinfo->RESPONSEID = e0810_99;
        }
        else
        {
            debug_sprintf(szDbgMsg, "%s: ERROR! Vantiv response not supported", __FUNCTION__);
            APP_TRACE(szDbgMsg);

            return EXIT_FAILURE;
        }
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

int parseIdentifiedVantivResponse(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	switch (vrinfo->RESPONSEID)
	{
	case e0110_61: //GC Mass Transaction/EBT Balance Inquiry Approval
		{
			if (VTV_Parse_0110_61(vrinfo, svbninfo) != EXIT_SUCCESS) {
				debug_sprintf(szDbgMsg, "%s: ERROR! Parsing response", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}
			break;
		}
	case e0110_62: //Gift Card/EBT Error
		{
			if (VTV_Parse_0110_62(vrinfo, svbninfo) != EXIT_SUCCESS) {
				debug_sprintf(szDbgMsg, "%s: ERROR! Parsing response", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}
			break;
		}
	case e0110_90: //Authorization Approval
		{
			if (VTV_Parse_0110_90(vrinfo, svbninfo) != EXIT_SUCCESS) {
				debug_sprintf(szDbgMsg, "%s: ERROR! Parsing response", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}
			break;
		}
	case e0110_90_GC: { //GC GC/POSA/Debit/Credit Balance Inquiry Approval
		if (VTV_Parse_0110_90_GC(vrinfo, svbninfo) != EXIT_SUCCESS) {
			debug_sprintf(szDbgMsg, "%s: ERROR! Parsing response", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return EXIT_FAILURE;
		}
		break;
	}
	case e0110_99: //Credit/Debit Error
		{
			if (VTV_Parse_0110_99(vrinfo, svbninfo) != EXIT_SUCCESS) {
				debug_sprintf(szDbgMsg, "%s: ERROR! Parsing response", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}
			break;
		}
	case e0110_53: //Token/De-token Conversion Approval
		{
			if (VTV_Parse_0110_53(vrinfo, svbninfo) != EXIT_SUCCESS) {
				debug_sprintf(szDbgMsg, "%s: ERROR! Parsing response", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}
			break;
		}
	/* Financial transaction response */
	case e0210_61: //Gift Card Approval
		{
			if (VTV_Parse_0210_61(vrinfo, svbninfo) != EXIT_SUCCESS) {
				debug_sprintf(szDbgMsg, "%s: ERROR! Parsing response", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}
			break;
		}
	case e0210_62: //Gift Card/EBT Error
		{
			if (VTV_Parse_0210_62(vrinfo, svbninfo) != EXIT_SUCCESS) {
				debug_sprintf(szDbgMsg, "%s: ERROR! Parsing response", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}
			break;
		}
	case e0210_63: //Gift Card Preauthorization Approval
		{
			if (VTV_Parse_0210_63(vrinfo, svbninfo) != EXIT_SUCCESS) {
				debug_sprintf(szDbgMsg, "%s: ERROR! Parsing response", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}
			break;
		}
	case e0210_91: //Credit/Debit Approval
		{
			if (VTV_Parse_0210_91(vrinfo, svbninfo) != EXIT_SUCCESS) {
				debug_sprintf(szDbgMsg, "%s: ERROR! Parsing response", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}
			break;
		}
	case e0210_99: //Credit/Debit Error
		{
			if (VTV_Parse_0210_99(vrinfo, svbninfo) != EXIT_SUCCESS) {
				debug_sprintf(szDbgMsg, "%s: ERROR! Parsing response", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}
			break;
		}
	/* Financial transaction response (prior authorization sale) */
	case e0230_62: //Gift Card/EBT Error
		{
			if (VTV_Parse_0230_62(vrinfo, svbninfo) != EXIT_SUCCESS) {
				debug_sprintf(szDbgMsg, "%s: ERROR! Parsing response", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}
			break;
		}
	case e0230_91: //Credit/Debit Approval
		{
			if (VTV_Parse_0230_91(vrinfo, svbninfo) != EXIT_SUCCESS) {
				debug_sprintf(szDbgMsg, "%s: ERROR! Parsing response", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}
			break;
		}
	case e0230_99: //Credit/Debit Error
		{
			if (VTV_Parse_0230_99(vrinfo, svbninfo) != EXIT_SUCCESS) {
				debug_sprintf(szDbgMsg, "%s: ERROR! Parsing response", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}
			break;
		}
	/* Acquirer reversal request response */
	case e0410_61: //Gift Card Reversal (Void) Approval
		{
			if (VTV_Parse_0410_61(vrinfo, svbninfo) != EXIT_SUCCESS) {
				debug_sprintf(szDbgMsg, "%s: ERROR! Parsing response", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}
			break;
		}
	case e0410_62: //Gift Card Reversal (Void) Error
		{
			if (VTV_Parse_0410_62(vrinfo, svbninfo) != EXIT_SUCCESS) {
				debug_sprintf(szDbgMsg, "%s: ERROR! Parsing response", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}
			break;
		}
	case e0410_63: //Gift Card Preauthorization Reversal (Void) Approval
		{
			if (VTV_Parse_0410_63(vrinfo, svbninfo) != EXIT_SUCCESS) {
				debug_sprintf(szDbgMsg, "%s: ERROR! Parsing response", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}
			break;
		}
	case e0410_91: //Credit/Debit Reversal (Void) Approval
		{
			if (VTV_Parse_0410_91(vrinfo, svbninfo) != EXIT_SUCCESS) {
				debug_sprintf(szDbgMsg, "%s: ERROR! Parsing response", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}
			break;
		}
	case e0410_99: //Credit/Debit/Check Reversal (Void) Error
		{
			if (VTV_Parse_0410_99(vrinfo, svbninfo) != EXIT_SUCCESS) {
				debug_sprintf(szDbgMsg, "%s: ERROR! Parsing response", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}
			break;
		}
	/* Acquirer reconciliation request response */
	case e0510_92: //Approval "or" Gift Card Batch Inquiry Response
		{
			if (VTV_Parse_0510_92(vrinfo, svbninfo) != EXIT_SUCCESS) {
				debug_sprintf(szDbgMsg, "%s: ERROR! Parsing response", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}
			break;
		}
    case e0510_99: //Reconciliation Error
    case e0810_99: //Network Error
		{
			if (VTV_Parse_0510_99(vrinfo, svbninfo) != EXIT_SUCCESS) {
				debug_sprintf(szDbgMsg, "%s: ERROR! Parsing response", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}
			break;
		}
	default:
		{
			debug_sprintf(szDbgMsg, "%s: ERROR! Vantiv response not identified", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return EXIT_FAILURE;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

void saveR007(sVANTIV_RESPONSE * vrinfo)
{
    saveRany(vrinfo, vrinfo->r007, sizeof(vrinfo->r007)- 1, "R007");
}

void saveR008(sVANTIV_RESPONSE * vrinfo)
{
    saveRany(vrinfo, vrinfo->r008, sizeof(vrinfo->r008)- 1, "R008");
}

void saveR011(sVANTIV_RESPONSE * vrinfo)
{
    saveRany(vrinfo, vrinfo->r011, sizeof(vrinfo->r011)- 1, "R011");
}

void saveR017(sVANTIV_RESPONSE * vrinfo)
{
    saveRany(vrinfo, vrinfo->r017, sizeof(vrinfo->r017)- 1, "R017");
}

void saveR023(sVANTIV_RESPONSE * vrinfo)
{
    saveRany(vrinfo, vrinfo->r023, sizeof(vrinfo->r023)- 1, "R023");
}

void saveR029(sVANTIV_RESPONSE * vrinfo)
{
    saveRany(vrinfo, vrinfo->r029, sizeof(vrinfo->r029)- 1, "R029");
}

// max_length is the max length of string in destination
static void saveRany(sVANTIV_RESPONSE * vrinfo, char *destination, int max_length, char *group_name)
{
    char    *ptr = NULL;
    char    *end = NULL;
    int     len;
    
    strcpy(destination, "");
    if (vrinfo->group == NULL)
        return;
    
    //search for first occurence of response group and save to ptr
    ptr = strstr(vrinfo->group, group_name);
    if (ptr == NULL)
        return;             // response does not exist
    
    debug_sprintf(szDbgMsg, "Found group %s", group_name);
    APP_TRACE(szDbgMsg);
    //search the GS in ptr and save to end, this is the end of the R0nn
    end = strchr(ptr, '\x1D'); //\x1D = GS
    if (end == NULL)
        return;             // response is not terminated
    
    len = end - ptr;
    if (len > max_length)
        len= max_length;    // response is too long

    // strncpy does not guarantee a '\0' at end of string
    strncat(destination, ptr, len);
	
#ifdef DEVDEBUG
    debug_sprintf(szDbgMsg, "saveRany for %s, data %s. len=%d", group_name, destination, len);
    APP_TRACE(szDbgMsg);
#endif
}

int VTV_Parse_0110_61(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: VANTIV RESPONSE: GC Mass Transaction/EBT Balance Inquiry Approval", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char buffer[vrinfo->baselen+1];
	char* ptr;
	int i;

	memset(svbninfo, 0x00, sizeof(_sVTV_FIELDS));
	memset(buffer, 0x00, sizeof(buffer));
	strcpy(buffer, vrinfo->base);

	ptr = buffer;

	if (strlen(buffer) != 219) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	for(i=0; i<(sizeof(svbninfo->bn00)-1); i++)
		svbninfo->bn00[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bnbmt)-1); i++)
		svbninfo->bnbmt[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn03)-1); i++)
		svbninfo->bn03[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn07)-1); i++)
		svbninfo->bn07[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn11)-1); i++)
		svbninfo->bn11[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn65)-1); i++)
		svbninfo->bn65[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_1)-1); i++)
		svbninfo->bn105_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_2)-1); i++)
		svbninfo->bn105_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_3)-1); i++)
		svbninfo->bn105_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_4)-1); i++)
		svbninfo->bn105_4[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn115)-1); i++)
		svbninfo->bn115[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_1)-1); i++)
		svbninfo->bn120_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_2)-1); i++)
		svbninfo->bn120_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_3)-1); i++)
		svbninfo->bn120_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn124_1)-1); i++)
		svbninfo->bn124_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn128)-1); i++)
		svbninfo->bn128[i] = *ptr++;

	if (*ptr) {
		debug_sprintf(szDbgMsg, "%s: ERROR! failed parsing vantiv response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

int VTV_Parse_0110_62(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: VANTIV RESPONSE: Gift Card/EBT Error", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char buffer[vrinfo->baselen+1];
	char* ptr;
	int i;

	memset(buffer, 0x00, sizeof(buffer));
	strcpy(buffer, vrinfo->base);

	ptr = buffer;

	if (strlen(buffer) != 209) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	for(i=0; i<(sizeof(svbninfo->bn00)-1); i++)
		svbninfo->bn00[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bnbmt)-1); i++)
		svbninfo->bnbmt[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn11)-1); i++)
		svbninfo->bn11[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_1)-1); i++)
		svbninfo->bn105_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_2)-1); i++)
		svbninfo->bn105_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_3)-1); i++)
		svbninfo->bn105_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_4)-1); i++)
		svbninfo->bn105_4[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn115)-1); i++)
		svbninfo->bn115[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn123_1)-1); i++)
		svbninfo->bn123_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn123_2)-1); i++)
		svbninfo->bn123_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn124_1)-1); i++)
		svbninfo->bn124_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn128)-1); i++)
		svbninfo->bn128[i] = *ptr++;

	if (*ptr) {
		debug_sprintf(szDbgMsg, "%s: ERROR! failed parsing vantiv response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

int VTV_Parse_0110_90(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: VANTIV RESPONSE: Authorization Approval", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char buffer[vrinfo->baselen+1];
	char* ptr;
	int i;

	memset(buffer, 0x00, sizeof(buffer));
	strcpy(buffer, vrinfo->base);

	ptr = buffer;

	if (strlen(buffer) != 107) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	for(i=0; i<(sizeof(svbninfo->bn00)-1); i++)
		svbninfo->bn00[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bnbmt)-1); i++)
		svbninfo->bnbmt[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn03)-1); i++)
		svbninfo->bn03[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn07)-1); i++)
		svbninfo->bn07[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn11)-1); i++)
		svbninfo->bn11[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn37)-1); i++)
		svbninfo->bn37[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn65)-1); i++)
		svbninfo->bn65[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_1)-1); i++)
		svbninfo->bn105_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_2)-1); i++)
		svbninfo->bn105_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_3)-1); i++)
		svbninfo->bn105_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_4)-1); i++)
		svbninfo->bn105_4[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn115)-1); i++)
		svbninfo->bn115[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_1)-1); i++)
		svbninfo->bn120_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_2)-1); i++)
		svbninfo->bn120_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_3)-1); i++)
		svbninfo->bn120_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn124_1)-1); i++)
		svbninfo->bn124_1[i] = *ptr++;

	if (*ptr) {
		debug_sprintf(szDbgMsg, "%s: ERROR! failed parsing vantiv response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

int VTV_Parse_0110_90_GC(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: VANTIV RESPONSE: GC GC/POSA/Debit/Credit Balance Inquiry Approval", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char buffer[vrinfo->baselen+1];
	char* ptr;
	int i;

	memset(svbninfo, 0x00, sizeof(_sVTV_FIELDS));
	memset(buffer, 0x00, sizeof(buffer));
	strcpy(buffer, vrinfo->base);

	ptr = buffer;

	if (strlen(buffer) != 116) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	for(i=0; i<(sizeof(svbninfo->bn00)-1); i++)
		svbninfo->bn00[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bnbmt)-1); i++)
		svbninfo->bnbmt[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn03)-1); i++)
		svbninfo->bn03[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn04)-1); i++)
		svbninfo->bn04[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn07)-1); i++)
		svbninfo->bn07[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn11)-1); i++)
		svbninfo->bn11[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn37)-1); i++)
		svbninfo->bn37[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn65)-1); i++)
		svbninfo->bn65[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_1)-1); i++)
		svbninfo->bn105_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_2)-1); i++)
		svbninfo->bn105_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_3)-1); i++)
		svbninfo->bn105_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_4)-1); i++)
		svbninfo->bn105_4[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn115)-1); i++)
		svbninfo->bn115[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_1)-1); i++)
		svbninfo->bn120_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_2)-1); i++)
		svbninfo->bn120_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_3)-1); i++)
		svbninfo->bn120_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn124_1)-1); i++)
		svbninfo->bn124_1[i] = *ptr++;

	if (*ptr) {
		debug_sprintf(szDbgMsg, "%s: ERROR! failed parsing vantiv response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

int VTV_Parse_0110_99(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: VANTIV RESPONSE: Debit, Credit and Check Error", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char buffer[vrinfo->baselen+1];
	char* ptr;
	int i;

	memset(svbninfo, 0x00, sizeof(_sVTV_FIELDS));
	memset(buffer, 0x00, sizeof(buffer));
	strcpy(buffer, vrinfo->base);

	ptr = buffer;

	if (strlen(buffer) != 89) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	for(i=0; i<(sizeof(svbninfo->bn00)-1); i++)
		svbninfo->bn00[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bnbmt)-1); i++)
		svbninfo->bnbmt[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn11)-1); i++)
		svbninfo->bn11[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_1)-1); i++)
		svbninfo->bn105_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_2)-1); i++)
		svbninfo->bn105_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_3)-1); i++)
		svbninfo->bn105_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_4)-1); i++)
		svbninfo->bn105_4[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn115)-1); i++)
		svbninfo->bn115[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn123_1)-1); i++)
		svbninfo->bn123_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn123_2)-1); i++)
		svbninfo->bn123_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn124_1)-1); i++)
		svbninfo->bn124_1[i] = *ptr++;

	if (*ptr) {
		debug_sprintf(szDbgMsg, "%s: ERROR! failed parsing vantiv response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

int VTV_Parse_0110_53(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: VANTIV RESPONSE: Token/De-token Conversion Approval", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char buffer[vrinfo->baselen+1];
	char* ptr;
	int i;

	memset(svbninfo, 0x00, sizeof(_sVTV_FIELDS));
	memset(buffer, 0x00, sizeof(buffer));
	strcpy(buffer, vrinfo->base);

	ptr = buffer;

	if (strlen(buffer) != 51) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	for(i=0; i<(sizeof(svbninfo->bn00)-1); i++)
		svbninfo->bn00[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bnbmt)-1); i++)
		svbninfo->bnbmt[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn03)-1); i++)
		svbninfo->bn03[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn07)-1); i++)
		svbninfo->bn07[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn11)-1); i++)
		svbninfo->bn11[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn115)-1); i++)
		svbninfo->bn115[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_1)-1); i++)
		svbninfo->bn120_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_2)-1); i++)
		svbninfo->bn120_2[i] = *ptr++;

	if (*ptr) {
		debug_sprintf(szDbgMsg, "%s: ERROR! failed parsing vantiv response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

int VTV_Parse_0210_61(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: VANTIV RESPONSE: Gift Card Approval", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char buffer[vrinfo->baselen+1];
	char* ptr;
	int i;

	memset(svbninfo, 0x00, sizeof(_sVTV_FIELDS));
	memset(buffer, 0x00, sizeof(buffer));
	strcpy(buffer, vrinfo->base);

	ptr = buffer;

	if (strlen(buffer) != 227) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	for(i=0; i<(sizeof(svbninfo->bn00)-1); i++)
		svbninfo->bn00[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bnbmt)-1); i++)
		svbninfo->bnbmt[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn03)-1); i++)
		svbninfo->bn03[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn07)-1); i++)
		svbninfo->bn07[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn11)-1); i++)
		svbninfo->bn11[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn37)-1); i++)
		svbninfo->bn37[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn65)-1); i++)
		svbninfo->bn65[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_1)-1); i++)
		svbninfo->bn105_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_2)-1); i++)
		svbninfo->bn105_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_3)-1); i++)
		svbninfo->bn105_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_4)-1); i++)
		svbninfo->bn105_4[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn115)-1); i++)
		svbninfo->bn115[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_1)-1); i++)
		svbninfo->bn120_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_2)-1); i++)
		svbninfo->bn120_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_3)-1); i++)
		svbninfo->bn120_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn124_1)-1); i++)
		svbninfo->bn124_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn128)-1); i++)
		svbninfo->bn128[i] = *ptr++;

	if (*ptr) {
		debug_sprintf(szDbgMsg, "%s: ERROR! failed parsing vantiv response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

int VTV_Parse_0210_62(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: VANTIV RESPONSE: Gift Card/EBT Error", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char buffer[vrinfo->baselen+1];
	char* ptr;
	int i;

	memset(svbninfo, 0x00, sizeof(_sVTV_FIELDS));
	memset(buffer, 0x00, sizeof(buffer));
	strcpy(buffer, vrinfo->base);

	ptr = buffer;

	if (strlen(buffer) != 209) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	for(i=0; i<(sizeof(svbninfo->bn00)-1); i++)
		svbninfo->bn00[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bnbmt)-1); i++)
		svbninfo->bnbmt[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn11)-1); i++)
		svbninfo->bn11[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_1)-1); i++)
		svbninfo->bn105_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_2)-1); i++)
		svbninfo->bn105_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_3)-1); i++)
		svbninfo->bn105_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_4)-1); i++)
		svbninfo->bn105_4[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn115)-1); i++)
		svbninfo->bn115[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn123_1)-1); i++)
		svbninfo->bn123_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn123_2)-1); i++)
		svbninfo->bn123_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn124_1)-1); i++)
		svbninfo->bn124_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn128)-1); i++)
		svbninfo->bn128[i] = *ptr++;

	if (*ptr) {
		debug_sprintf(szDbgMsg, "%s: ERROR! failed parsing vantiv response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

int VTV_Parse_0210_63(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: VANTIV RESPONSE: Gift Card Preauthorization Approval", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char buffer[vrinfo->baselen+1];
	char* ptr;
	int i;

	memset(svbninfo, 0x00, sizeof(_sVTV_FIELDS));
	memset(buffer, 0x00, sizeof(buffer));
	strcpy(buffer, vrinfo->base);

	ptr = buffer;

	if (strlen(buffer) != 229) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	for(i=0; i<(sizeof(svbninfo->bn00)-1); i++)
		svbninfo->bn00[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bnbmt)-1); i++)
		svbninfo->bnbmt[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn03)-1); i++)
		svbninfo->bn03[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn12)-1); i++)
		svbninfo->bn12[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn13)-1); i++)
		svbninfo->bn13[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn11)-1); i++)
		svbninfo->bn11[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn37)-1); i++)
		svbninfo->bn37[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn65)-1); i++)
		svbninfo->bn65[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_1)-1); i++)
		svbninfo->bn105_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_2)-1); i++)
		svbninfo->bn105_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_3)-1); i++)
		svbninfo->bn105_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_4)-1); i++)
		svbninfo->bn105_4[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn115)-1); i++)
		svbninfo->bn115[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_1)-1); i++)
		svbninfo->bn120_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_2)-1); i++)
		svbninfo->bn120_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_3)-1); i++)
		svbninfo->bn120_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn124_1)-1); i++)
		svbninfo->bn124_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn128)-1); i++)
		svbninfo->bn128[i] = *ptr++;

	if (*ptr) {
		debug_sprintf(szDbgMsg, "%s: ERROR! failed parsing vantiv response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

int VTV_Parse_0210_91(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: VANTIV RESPONSE: Credit/Debit Approval", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char buffer[vrinfo->baselen+1];
	char* ptr;
	int i;

	memset(svbninfo, 0x00, sizeof(_sVTV_FIELDS));
	memset(buffer, 0x00, sizeof(buffer));
	strcpy(buffer, vrinfo->base);

	ptr = buffer;

	if (strlen(buffer) != 107) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	for(i=0; i<(sizeof(svbninfo->bn00)-1); i++)
		svbninfo->bn00[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bnbmt)-1); i++)
		svbninfo->bnbmt[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn03)-1); i++)
		svbninfo->bn03[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn07)-1); i++)
		svbninfo->bn07[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn11)-1); i++)
		svbninfo->bn11[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn37)-1); i++)
		svbninfo->bn37[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn65)-1); i++)
		svbninfo->bn65[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_1)-1); i++)
		svbninfo->bn105_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_2)-1); i++)
		svbninfo->bn105_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_3)-1); i++)
		svbninfo->bn105_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_4)-1); i++)
		svbninfo->bn105_4[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn115)-1); i++)
		svbninfo->bn115[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_1)-1); i++)
		svbninfo->bn120_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_2)-1); i++)
		svbninfo->bn120_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_3)-1); i++)
		svbninfo->bn120_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn124_1)-1); i++)
		svbninfo->bn124_1[i] = *ptr++;

	if (*ptr) {
		debug_sprintf(szDbgMsg, "%s: ERROR! failed parsing vantiv response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

int VTV_Parse_0210_99(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: VANTIV RESPONSE: Credit/Debit Error", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char buffer[vrinfo->baselen+1];
	char* ptr;
	int i;

	memset(svbninfo, 0x00, sizeof(_sVTV_FIELDS));
	memset(buffer, 0x00, sizeof(buffer));
	strcpy(buffer, vrinfo->base);

	ptr = buffer;

	if (strlen(buffer) != 89) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	for(i=0; i<(sizeof(svbninfo->bn00)-1); i++)
		svbninfo->bn00[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bnbmt)-1); i++)
		svbninfo->bnbmt[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn11)-1); i++)
		svbninfo->bn11[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_1)-1); i++)
		svbninfo->bn105_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_2)-1); i++)
		svbninfo->bn105_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_3)-1); i++)
		svbninfo->bn105_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_4)-1); i++)
		svbninfo->bn105_4[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn115)-1); i++)
		svbninfo->bn115[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn123_1)-1); i++)
		svbninfo->bn123_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn123_2)-1); i++)
		svbninfo->bn123_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn124_1)-1); i++)
		svbninfo->bn124_1[i] = *ptr++;

	if (*ptr) {
		debug_sprintf(szDbgMsg, "%s: ERROR! failed parsing vantiv response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

int VTV_Parse_0230_62(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: VANTIV RESPONSE: Gift Card/EBT Error", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char buffer[vrinfo->baselen+1];
	char* ptr;
	int i;

	memset(svbninfo, 0x00, sizeof(_sVTV_FIELDS));
	memset(buffer, 0x00, sizeof(buffer));
	strcpy(buffer, vrinfo->base);

	ptr = buffer;

	if (strlen(buffer) != 209) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	for(i=0; i<(sizeof(svbninfo->bn00)-1); i++)
		svbninfo->bn00[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bnbmt)-1); i++)
		svbninfo->bnbmt[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn11)-1); i++)
		svbninfo->bn11[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_1)-1); i++)
		svbninfo->bn105_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_2)-1); i++)
		svbninfo->bn105_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_3)-1); i++)
		svbninfo->bn105_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_4)-1); i++)
		svbninfo->bn105_4[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn115)-1); i++)
		svbninfo->bn115[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn123_1)-1); i++)
		svbninfo->bn123_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn123_2)-1); i++)
		svbninfo->bn123_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn124_1)-1); i++)
		svbninfo->bn124_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn128)-1); i++)
		svbninfo->bn128[i] = *ptr++;

	if (*ptr) {
		debug_sprintf(szDbgMsg, "%s: ERROR! failed parsing vantiv response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

int VTV_Parse_0230_91(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: VANTIV RESPONSE: Credit/Debit Approval", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char buffer[vrinfo->baselen+1];
	char* ptr;
	int i;

	memset(svbninfo, 0x00, sizeof(_sVTV_FIELDS));
	memset(buffer, 0x00, sizeof(buffer));
	strcpy(buffer, vrinfo->base);

	ptr = buffer;

	if (strlen(buffer) != 107) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	for(i=0; i<(sizeof(svbninfo->bn00)-1); i++)
		svbninfo->bn00[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bnbmt)-1); i++)
		svbninfo->bnbmt[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn03)-1); i++)
		svbninfo->bn03[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn07)-1); i++)
		svbninfo->bn07[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn11)-1); i++)
		svbninfo->bn11[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn37)-1); i++)
		svbninfo->bn37[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn65)-1); i++)
		svbninfo->bn65[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_1)-1); i++)
		svbninfo->bn105_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_2)-1); i++)
		svbninfo->bn105_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_3)-1); i++)
		svbninfo->bn105_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_4)-1); i++)
		svbninfo->bn105_4[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn115)-1); i++)
		svbninfo->bn115[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_1)-1); i++)
		svbninfo->bn120_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_2)-1); i++)
		svbninfo->bn120_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_3)-1); i++)
		svbninfo->bn120_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn124_1)-1); i++)
		svbninfo->bn124_1[i] = *ptr++;

	if (*ptr) {
		debug_sprintf(szDbgMsg, "%s: ERROR! failed parsing vantiv response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

int VTV_Parse_0230_99(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: VANTIV RESPONSE: Credit/Debit Error", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char buffer[vrinfo->baselen+1];
	char* ptr;
	int i;

	memset(svbninfo, 0x00, sizeof(_sVTV_FIELDS));
	memset(buffer, 0x00, sizeof(buffer));
	strcpy(buffer, vrinfo->base);

	ptr = buffer;

	if (strlen(buffer) != 89) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	for(i=0; i<(sizeof(svbninfo->bn00)-1); i++)
		svbninfo->bn00[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bnbmt)-1); i++)
		svbninfo->bnbmt[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn11)-1); i++)
		svbninfo->bn11[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_1)-1); i++)
		svbninfo->bn105_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_2)-1); i++)
		svbninfo->bn105_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_3)-1); i++)
		svbninfo->bn105_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_4)-1); i++)
		svbninfo->bn105_4[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn115)-1); i++)
		svbninfo->bn115[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn123_1)-1); i++)
		svbninfo->bn123_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn123_2)-1); i++)
		svbninfo->bn123_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn124_1)-1); i++)
		svbninfo->bn124_1[i] = *ptr++;

	if (*ptr) {
		debug_sprintf(szDbgMsg, "%s: ERROR! failed parsing vantiv response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}


int VTV_Parse_0410_61(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: VANTIV RESPONSE: Gift Card Reversal (Void) Approval", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char buffer[vrinfo->baselen+1];
	char* ptr;
	int i;

	memset(svbninfo, 0x00, sizeof(_sVTV_FIELDS));
	memset(buffer, 0x00, sizeof(buffer));
	strcpy(buffer, vrinfo->base);

	ptr = buffer;

	if (strlen(buffer) != 227) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	for(i=0; i<(sizeof(svbninfo->bn00)-1); i++)
		svbninfo->bn00[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bnbmt)-1); i++)
		svbninfo->bnbmt[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn03)-1); i++)
		svbninfo->bn03[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn07)-1); i++)
		svbninfo->bn07[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn11)-1); i++)
		svbninfo->bn11[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn37)-1); i++)
		svbninfo->bn37[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn65)-1); i++)
		svbninfo->bn65[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_1)-1); i++)
		svbninfo->bn105_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_2)-1); i++)
		svbninfo->bn105_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_3)-1); i++)
		svbninfo->bn105_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_4)-1); i++)
		svbninfo->bn105_4[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn115)-1); i++)
		svbninfo->bn115[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_1)-1); i++)
		svbninfo->bn120_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_2)-1); i++)
		svbninfo->bn120_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_3)-1); i++)
		svbninfo->bn120_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn124_1)-1); i++)
		svbninfo->bn124_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn128)-1); i++)
		svbninfo->bn128[i] = *ptr++;

	if (*ptr) {
		debug_sprintf(szDbgMsg, "%s: ERROR! failed parsing vantiv response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

int VTV_Parse_0410_62(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: VANTIV RESPONSE: Gift Card Reversal (Void) Error", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char buffer[vrinfo->baselen+1];
	char* ptr;
	int i;

	memset(svbninfo, 0x00, sizeof(_sVTV_FIELDS));
	memset(buffer, 0x00, sizeof(buffer));
	strcpy(buffer, vrinfo->base);

	ptr = buffer;

	if (strlen(buffer) != 209) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	for(i=0; i<(sizeof(svbninfo->bn00)-1); i++)
		svbninfo->bn00[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bnbmt)-1); i++)
		svbninfo->bnbmt[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn11)-1); i++)
		svbninfo->bn11[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_1)-1); i++)
		svbninfo->bn105_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_2)-1); i++)
		svbninfo->bn105_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_3)-1); i++)
		svbninfo->bn105_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_4)-1); i++)
		svbninfo->bn105_4[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn115)-1); i++)
		svbninfo->bn115[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn123_1)-1); i++)
		svbninfo->bn123_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn123_2)-1); i++)
		svbninfo->bn123_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn124_1)-1); i++)
		svbninfo->bn124_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn128)-1); i++)
		svbninfo->bn128[i] = *ptr++;

	if (*ptr) {
		debug_sprintf(szDbgMsg, "%s: ERROR! failed parsing vantiv response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

int VTV_Parse_0410_63(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: VANTIV RESPONSE: Gift Card Preauthorization Reversal (Void) Approval", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char buffer[vrinfo->baselen+1];
	char* ptr;
	int i;

	memset(svbninfo, 0x00, sizeof(_sVTV_FIELDS));
	memset(buffer, 0x00, sizeof(buffer));
	strcpy(buffer, vrinfo->base);

	ptr = buffer;

	if (strlen(buffer) != 229) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	for(i=0; i<(sizeof(svbninfo->bn00)-1); i++)
		svbninfo->bn00[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bnbmt)-1); i++)
		svbninfo->bnbmt[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn03)-1); i++)
		svbninfo->bn03[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn12)-1); i++)
		svbninfo->bn12[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn13)-1); i++)
		svbninfo->bn13[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn11)-1); i++)
		svbninfo->bn11[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn37)-1); i++)
		svbninfo->bn37[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn65)-1); i++)
		svbninfo->bn65[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_1)-1); i++)
		svbninfo->bn105_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_2)-1); i++)
		svbninfo->bn105_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_3)-1); i++)
		svbninfo->bn105_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_4)-1); i++)
		svbninfo->bn105_4[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn115)-1); i++)
		svbninfo->bn115[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_1)-1); i++)
		svbninfo->bn120_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_2)-1); i++)
		svbninfo->bn120_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_3)-1); i++)
		svbninfo->bn120_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn124_1)-1); i++)
		svbninfo->bn124_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn128)-1); i++)
		svbninfo->bn128[i] = *ptr++;

	if (*ptr) {
		debug_sprintf(szDbgMsg, "%s: ERROR! failed parsing vantiv response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

int VTV_Parse_0410_91(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: VANTIV RESPONSE: Credit/Debit Reversal (Void) Approval", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char buffer[vrinfo->baselen+1];
	char* ptr;
	int i;

	memset(svbninfo, 0x00, sizeof(_sVTV_FIELDS));
	memset(buffer, 0x00, sizeof(buffer));
	strcpy(buffer, vrinfo->base);

	ptr = buffer;

	if (strlen(buffer) != 107) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	for(i=0; i<(sizeof(svbninfo->bn00)-1); i++)
		svbninfo->bn00[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bnbmt)-1); i++)
		svbninfo->bnbmt[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn03)-1); i++)
		svbninfo->bn03[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn07)-1); i++)
		svbninfo->bn07[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn11)-1); i++)
		svbninfo->bn11[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn37)-1); i++)
		svbninfo->bn37[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn65)-1); i++)
		svbninfo->bn65[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_1)-1); i++)
		svbninfo->bn105_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_2)-1); i++)
		svbninfo->bn105_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_3)-1); i++)
		svbninfo->bn105_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_4)-1); i++)
		svbninfo->bn105_4[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn115)-1); i++)
		svbninfo->bn115[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_1)-1); i++)
		svbninfo->bn120_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_2)-1); i++)
		svbninfo->bn120_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn120_3)-1); i++)
		svbninfo->bn120_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn124_1)-1); i++)
		svbninfo->bn124_1[i] = *ptr++;

	if (*ptr) {
		debug_sprintf(szDbgMsg, "%s: ERROR! failed parsing vantiv response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

int VTV_Parse_0410_99(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: VANTIV RESPONSE: Credit/Debit/Check Reversal (Void) Error", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char buffer[vrinfo->baselen+1];
	char* ptr;
	int i;

	memset(svbninfo, 0x00, sizeof(_sVTV_FIELDS));
	memset(buffer, 0x00, sizeof(buffer));
	strcpy(buffer, vrinfo->base);

	ptr = buffer;

	if (strlen(buffer) != 89) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	for(i=0; i<(sizeof(svbninfo->bn00)-1); i++)
		svbninfo->bn00[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bnbmt)-1); i++)
		svbninfo->bnbmt[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn11)-1); i++)
		svbninfo->bn11[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_1)-1); i++)
		svbninfo->bn105_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_2)-1); i++)
		svbninfo->bn105_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_3)-1); i++)
		svbninfo->bn105_3[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn105_4)-1); i++)
		svbninfo->bn105_4[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn115)-1); i++)
		svbninfo->bn115[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn123_1)-1); i++)
		svbninfo->bn123_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn123_2)-1); i++)
		svbninfo->bn123_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn124_1)-1); i++)
		svbninfo->bn124_1[i] = *ptr++;

	if (*ptr) {
		debug_sprintf(szDbgMsg, "%s: ERROR! failed parsing vantiv response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}


int VTV_Parse_0510_92(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char buffer[vrinfo->baselen+1];
	char* ptr;
	int i;

	memset(svbninfo, 0x00, sizeof(_sVTV_FIELDS));
	memset(buffer, 0x00, sizeof(buffer));
	strcpy(buffer, vrinfo->base);

	ptr = buffer;

	if (strlen(buffer) == RECON_APPROVAL_LENGTH) {
		debug_sprintf(szDbgMsg, "%s: Approval", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if (strlen(buffer) != RECON_APPROVAL_LENGTH) {
			debug_sprintf(szDbgMsg, "%s: ERROR! Not exact bytes", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			return EXIT_FAILURE;
		}

		for(i=0; i<(sizeof(svbninfo->bn00)-1); i++)
			svbninfo->bn00[i] = *ptr++;
		for(i=0; i<(sizeof(svbninfo->bnbmt)-1); i++)
			svbninfo->bnbmt[i] = *ptr++;
		for(i=0; i<(sizeof(svbninfo->bn11)-1); i++)
			svbninfo->bn11[i] = *ptr++;
		for(i=0; i<(sizeof(svbninfo->bn66)-1); i++)
			svbninfo->bn66[i] = *ptr++;
		for(i=0; i<(sizeof(svbninfo->bn74)-1); i++)
			svbninfo->bn74[i] = *ptr++;
		for(i=0; i<(sizeof(svbninfo->bn76)-1); i++)
			svbninfo->bn76[i] = *ptr++;
		for(i=0; i<(sizeof(svbninfo->bn86)-1); i++)
			svbninfo->bn86[i] = *ptr++;
		for(i=0; i<(sizeof(svbninfo->bn88)-1); i++)
			svbninfo->bn88[i] = *ptr++;
		for(i=0; i<(sizeof(svbninfo->bn115)-1); i++)
			svbninfo->bn115[i] = *ptr++;
		for(i=0; i<(sizeof(svbninfo->bn124_1)-1); i++)
			svbninfo->bn124_1[i] = *ptr++;

		if (*ptr) {
			debug_sprintf(szDbgMsg, "%s: ERROR! failed parsing vantiv response", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return EXIT_FAILURE;
		}
	}
	else {
		debug_sprintf(szDbgMsg, "%s: Gift Card Batch Inquiry Response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if (strlen(buffer) != RECON_GBATCH_INQ_LENGTH) {
			debug_sprintf(szDbgMsg, "%s: ERROR! Not exact bytes", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return EXIT_FAILURE;
		}

		for(i=0; i<(sizeof(svbninfo->bn00)-1); i++)
			svbninfo->bn00[i] = *ptr++;
		for(i=0; i<(sizeof(svbninfo->bnbmt)-1); i++)
			svbninfo->bnbmt[i] = *ptr++;
		for(i=0; i<(sizeof(svbninfo->bn11)-1); i++)
			svbninfo->bn11[i] = *ptr++;
		for(i=0; i<(sizeof(svbninfo->bn66)-1); i++)
			svbninfo->bn66[i] = *ptr++;
		for(i=0; i<(sizeof(svbninfo->bn74)-1); i++)
			svbninfo->bn74[i] = *ptr++;
		for(i=0; i<(sizeof(svbninfo->bn76)-1); i++)
			svbninfo->bn76[i] = *ptr++;
		for(i=0; i<(sizeof(svbninfo->bn86)-1); i++)
			svbninfo->bn86[i] = *ptr++;
		for(i=0; i<(sizeof(svbninfo->bn88)-1); i++)
			svbninfo->bn88[i] = *ptr++;
		for(i=0; i<(sizeof(svbninfo->bn115)-1); i++)
			svbninfo->bn115[i] = *ptr++;
		for(i=0; i<(sizeof(svbninfo->bn124_1)-1); i++)
			svbninfo->bn124_1[i] = *ptr++;
		for(i=0; i<(sizeof(svbninfo->bn127_1)-1); i++)
			svbninfo->bn127_1[i] = *ptr++;
		for(i=0; i<(sizeof(svbninfo->bn127_2)-1); i++)
			svbninfo->bn127_2[i] = *ptr++;
		for(i=0; i<(sizeof(svbninfo->bn127_3)-1); i++)
			svbninfo->bn127_3[i] = *ptr++;
		for(i=0; i<(sizeof(svbninfo->bn127_4)-1); i++)
			svbninfo->bn127_4[i] = *ptr++;
		for(i=0; i<(sizeof(svbninfo->bn127_5)-1); i++)
			svbninfo->bn127_5[i] = *ptr++;

		if (*ptr) {
			debug_sprintf(szDbgMsg, "%s: ERROR! failed parsing vantiv response", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return EXIT_FAILURE;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

// Also deals with 0810_99, network error
int VTV_Parse_0510_99(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: VANTIV RESPONSE: Reconciliation/network Error", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char buffer[vrinfo->baselen+1];
	char* ptr;
	int i;

	memset(svbninfo, 0x00, sizeof(_sVTV_FIELDS));
	memset(buffer, 0x00, sizeof(buffer));
	strcpy(buffer, vrinfo->base);

	ptr = buffer;

	if (strlen(buffer) != 67) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Not exact bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	for(i=0; i<(sizeof(svbninfo->bn00)-1); i++)
		svbninfo->bn00[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bnbmt)-1); i++)
		svbninfo->bnbmt[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn11)-1); i++)
		svbninfo->bn11[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn115)-1); i++)
		svbninfo->bn115[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn123_1)-1); i++)
		svbninfo->bn123_1[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn123_2)-1); i++)
		svbninfo->bn123_2[i] = *ptr++;
	for(i=0; i<(sizeof(svbninfo->bn124_1)-1); i++)
		svbninfo->bn124_1[i] = *ptr++;

	if (*ptr) {
		debug_sprintf(szDbgMsg, "%s: ERROR! failed parsing vantiv response", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}


char* getVantivBNResponse(enum eBN_INDEX ID, _sVTV_FIELDS *svbninfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter --- (BN= %d)", __FUNCTION__, ID);
	APP_TRACE(szDbgMsg);

	switch(ID)
	{
	case eBN00: {
		if (strlen(svbninfo->bn00)<=0) return NULL;
		return svbninfo->bn00; break;
	}
	case eBN03: {
		if (strlen(svbninfo->bn03)<=0) return NULL;
		return svbninfo->bn03; break;
	}
	case eBNBMT: {
		if (strlen(svbninfo->bnbmt)<=0) return NULL;
		return svbninfo->bnbmt; break;
	}
	case eBN04: {
		if (strlen(svbninfo->bn04)<=0) return NULL;
		return svbninfo->bn04; break;
	}
	case eBN07: {
		if (strlen(svbninfo->bn07)<=0) return NULL;
		return svbninfo->bn07; break;
	}
	case eBN11: {
		if (strlen(svbninfo->bn11)<=0) return NULL;
		return svbninfo->bn11; break;
	}
	case eBN12: {
		if (strlen(svbninfo->bn12)<=0) return NULL;
		return svbninfo->bn12; break;
	}
	case eBN13: {
		if (strlen(svbninfo->bn13)<=0) return NULL;
		return svbninfo->bn13; break;
	}
	case eBN37: {
		if (strlen(svbninfo->bn37)<=0) return NULL;
		return svbninfo->bn37; break;
	}
	case eBN65: {
		if (strlen(svbninfo->bn65)<=0) return NULL;
		return svbninfo->bn65; break;
	}
	case eBN105_1: { //Praveen_P1: 03 Dec 2015 - This is to pass the CVV2_CODE and AVS_CODE in the SSI response to SCA
		if (strlen(svbninfo->bn105_1)<=0) return NULL;
		return svbninfo->bn105_1; break;
	}
	case eBN105_3: {
		if (strlen(svbninfo->bn105_3)<=0) return NULL;
		return svbninfo->bn105_3; break;
	}
	case eBN120_1: {
		if (strlen(svbninfo->bn120_1)<=0) return NULL;
		return svbninfo->bn120_1; break;
	}
	case eBN120_3: {
		if (strlen(svbninfo->bn120_3)<=0) return NULL;
		return svbninfo->bn120_3; break;
	}
	case eBN123_1: {
		if (strlen(svbninfo->bn123_1)<=0) return NULL;
		return svbninfo->bn123_1; break;
	}
	case eBN123_2: {
		if (strlen(svbninfo->bn123_2)<=0) return NULL;
		return svbninfo->bn123_2; break;
	}
	case eBN128: {
		if (strlen(svbninfo->bn128)<=0) return NULL;
		return svbninfo->bn128; break;
	}
	default: {
		debug_sprintf(szDbgMsg, "%s: ERROR! Error", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		break;
	}
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return NULL;
}

char* getR007(sVANTIV_RESPONSE * vrinfo)
{
	if ( strlen(vrinfo->r007) <= 0 ) {
		return NULL;
	}

	return vrinfo->r007;
}

char* getR008(sVANTIV_RESPONSE * vrinfo)
{
	if ( strlen(vrinfo->r008) <= 0 ) {
		return NULL;
	}

	return vrinfo->r008;
}

char* getR011(sVANTIV_RESPONSE * vrinfo)
{
	if ( strlen(vrinfo->r011) <= 0 ) {
		return NULL;
	}

	return vrinfo->r011;
}

char* getR017(sVANTIV_RESPONSE * vrinfo)
{
	if ( strlen(vrinfo->r017) <= 0 ) {
		return NULL;
	}

	return vrinfo->r017;
}

char* getR023(sVANTIV_RESPONSE * vrinfo)
{
    if ( strlen(vrinfo->r023) <= 0 ) {
        return NULL;
    }
    
    return vrinfo->r023;
}

char* getR029(sVANTIV_RESPONSE * vrinfo)
{
    if ( strlen(vrinfo->r029) <= 0 ) {
        return NULL;
    }

    return vrinfo->r029;
}
