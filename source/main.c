/*
 * main.c
 *
 *  Created on: Sept 25, 2013
 *      Author: Dexter M. ALberto
 */


#include <stdlib.h>
#include "tcpip_server.h"
#include "config_data.h"
#include "logger.h"
#include "define_data.h"
#include "string.h"
#include "svc.h"

#include "tcpipdbg.h"
#include <sys/timeb.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <curl/curl.h>
#include "main.h"
#include "sca_xml_request.h"
#include "help_function.h"
#include "service_request.h"
#include "curl_connect.h"
#include "appLog.h"
#include "configusr1applib.h"

extern int createThreadPool(int );

static int setDebugParamsToEnv();
static void * TORProcessor(void *);
void tlv_test_code(void);

pthread_mutex_t gptTORRecordFileMutex;
static pthread_t ptTORProcId = 0;

int main()
{
	int rv = EXIT_SUCCESS;
	int iAppLogEnabled = 0;
	char szAppLogData[256] = "";
	char szAppLogDiag[256] = "";
	char szErrMsg[256] = "";

	/*
	 * STEP 1: Initialize the ConfigUsr1 App.
	 */

	rv = initConfigUsr1AppLib();

	/*
	 * STEP 2: Initialize the debugger.
	 */

	//Reading Debug parameters from the config file and setting them to Environment
	setDebugParamsToEnv();

#ifdef LOGGING_ENABLED
	setupDebug();
	char szDbgMsg[512] = {0};
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: App Ver:[%s]", __FUNCTION__, VHI_APP_VERSION);
	APP_TRACE(szDbgMsg);

	debug_sprintf(szDbgMsg, "%s: App Build Number:[%s]", __FUNCTION__, VHI_BUILD_NUMBER);
	APP_TRACE(szDbgMsg);

#ifdef TEST_TLV
	tlv_test_code();
#endif

	/*
	 * Praveen_P1: Please see following note: So we call at the starting of the application before any thread gets created
	 * The basic rule for constructing a program that uses libcurl is this:
	 * Call curl_global_init(), with a CURL_GLOBAL_ALL argument,
	 * immediately after the program starts, while it is still only one thread and before it uses libcurl at all.
	 */

	/* Initialize the CURL library */
	curl_global_init(CURL_GLOBAL_ALL);

	/*
	 * ---------------------------------
	 * STEP 3: Initialize App Logging
	 * ---------------------------------
	 */
	rv = initializeAppLog();
	if (rv != EXIT_SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to initialize Application logging scheme",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(szErrMsg, "Failed to Initialize VHI Application Logging");
		syslog(LOG_ERR | LOG_USER, szErrMsg); //Logging to syslog

		return EXIT_FAILURE;
	}

	iAppLogEnabled = isAppLogEnabled();

	if(iAppLogEnabled)
	{

		strcpy(szAppLogData, STARTUP_INDICATOR);
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData,
				NULL);

		strcpy(szAppLogData,"VHI Application Logging Initiated Successfully");
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData,
						NULL);

		sprintf(
				szAppLogData,
				"VHI Application Version:[%s], VHI Application Build Number:[%s]",
				VHI_APP_VERSION, VHI_BUILD_NUMBER);
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData,NULL);
	}

	/*
	 * STEP 4: Initial setting of configurable data
	 */
	rv = setConfigData();

	if (rv != EXIT_SUCCESS) {
		debug_sprintf(szDbgMsg, "%s: FAILED to setup config data", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "Failed To Load VHI Config Parameters");
			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP, szAppLogData, NULL);

			strcpy(szAppLogData, "VHI Application Halted!!!");
			strcpy(szAppLogDiag, "Please Check The VHI Configuration");
			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP, szAppLogData, szAppLogDiag);

			strcpy(szAppLogData, END_INDICATOR);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData, NULL);
		}

		return EXIT_FAILURE;
	}

	if (iAppLogEnabled) {
		strcpy(szAppLogData,"Successfully Setup Config Data");
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP, szAppLogData,
				NULL);
	}
	/*
	 * STEP 5: Create the TOR thread to retry the
	 * TOR requests in background
	 */

	/*
	 * TOR Records file consists of TOR requests which needs to be retried on background
	 */
	if (pthread_mutex_init(&gptTORRecordFileMutex, NULL)) //Initialize the SAF Records File Mutex
	{
		debug_sprintf(szDbgMsg, "%s - Failed to create TOR Records File mutex!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "Failed To Create TOR Record File Mutex");
			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP,szAppLogData, NULL);

			strcpy(szAppLogData, "VHI Application Stopped!!!");
			strcpy(szAppLogDiag, "Please Check The VHI Configuration");
			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP,szAppLogData, szAppLogDiag);

			strcpy(szAppLogData, END_INDICATOR);
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP,szAppLogData, NULL);
		}

		while(1)
		{
			svcWait(100);
		}
	}

	/*-------- Create the TOR Retry Thread ---------- */
	rv = pthread_create(&ptTORProcId, NULL, TORProcessor, NULL);
	if (rv == EXIT_SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: TOR Processing Thread Created Successfully", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if (iAppLogEnabled) {
			strcpy(szAppLogData, "TOR Processing Thread Created Successfully");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_START_UP,
					szAppLogData, NULL);
		}
	}
	else 
		{
		debug_sprintf(szDbgMsg, "%s: Error while creating thread for TOR Processing", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "Failed To Create TOR Processing Thread");
			strcpy(szAppLogDiag, "Error while creating thread for TOR Processing");
			addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_START_UP,szAppLogData,szAppLogDiag);
		}
	}

	/*
	 * ---------------------------------
	 * STEP 6: Create Thread Pool
	 * ---------------------------------
	 */
	rv = createThreadPool(4); //Currently creating pool of 4 threads
	if(rv != EXIT_SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: FAILED to create Thread Pool to service requests",
																__FUNCTION__);
		APP_TRACE(szDbgMsg);

		strcpy(szErrMsg, "FAILED to create Thread Pool to service requests");
		syslog(LOG_ERR|LOG_USER, szErrMsg); //Logging to syslog

		if(iAppLogEnabled)
		{
			strcpy(szAppLogData, "Failed To Create Thread Pool");
			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP, szAppLogData, NULL);

			strcpy(szAppLogData, "VHI Application Halted!!!");
			strcpy(szAppLogDiag, "Internal Application Error, Please Contact Verifone");
			addAppEventLog(APP_NAME, ENTRYTYPE_ERROR, ENTRYID_START_UP, szAppLogData, szAppLogDiag);

			strcpy(szAppLogData, END_INDICATOR);
			addAppEventLog(APP_NAME, ENTRYTYPE_WARNING, ENTRYID_START_UP, szAppLogData, NULL);
		}

		while(1)
		{
			svcWait(10);
		}
	}

	if(iAppLogEnabled)
	{
		strcpy(szAppLogData, "Created Thread Pool For Service Requests Successfully");
		addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_START_UP, szAppLogData, NULL);
	}

	/*
	 * STEP 7: Start VHI server
	 * This is all where to start
	 * Loop infinitely to accept and service connections/commands
	 */
	if (iAppLogEnabled) {
		strcpy(szAppLogData, "Starting the VHI Server...");
		addAppEventLog(APP_NAME,ENTRYTYPE_INFO, ENTRYID_START_UP,szAppLogData,NULL);
	}

	rv = mainServerThreaded();
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return 0;
}

/*
 * ============================================================================
 * Function Name: setDebugParamsToEnv
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static int setDebugParamsToEnv()
{
	int rv = EXIT_SUCCESS;
	char szParamValue[20] = "";
	int iLen;
#if 0
#ifdef DEBUG
	char szDbgMsg[256] = "";
#endif
#endif

#if 0
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
#endif
	iLen = sizeof(szParamValue);

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "VHI_DEBUG", szParamValue, iLen);
	if(rv > 0)
	{
#if 0
		debug_sprintf(szDbgMsg, "%s: Setting VHI_DEBUG variable[%s=%s]", __FUNCTION__, "VHI_DEBUG", szParamValue);
		APP_TRACE(szDbgMsg);
#endif
		rv = setenv("VHI_DEBUG", szParamValue, 1);
		if (rv != EXIT_SUCCESS)
		{
#if 0
			debug_sprintf(szDbgMsg, "%s: Error while setting VHI_DEBUG to env", __FUNCTION__);
			APP_TRACE(szDbgMsg);
#endif
		}

	}

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "DBGIP", szParamValue, iLen);
	if(rv > 0)
	{
#if 0
		debug_sprintf(szDbgMsg, "%s: Setting DBGIP variable[%s=%s]", __FUNCTION__, "DBGIP", szParamValue);
		APP_TRACE(szDbgMsg);
#endif
		rv = setenv("DBGIP", szParamValue, 1);
		if (rv != EXIT_SUCCESS)
		{
#if 0
			debug_sprintf(szDbgMsg, "%s: Error while setting DBGIP to env", __FUNCTION__);
			APP_TRACE(szDbgMsg);
#endif
		}

	}

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "DBGPRT", szParamValue, iLen);
	if(rv > 0)
	{
#if 0
		debug_sprintf(szDbgMsg, "%s: Setting DBGPRT variable[%s=%s]", __FUNCTION__, "DBGPRT", szParamValue);
		APP_TRACE(szDbgMsg);
#endif
		rv = setenv("DBGPRT", szParamValue, 1);
		if (rv != EXIT_SUCCESS)
		{
#if 0
			debug_sprintf(szDbgMsg, "%s: Error while setting DBGPRT to env", __FUNCTION__);
			APP_TRACE(szDbgMsg);
#endif
		}

	}

	memset(szParamValue, 0x00, iLen);
	rv = getEnvFile("reg", "DEBUGOPT", szParamValue, iLen);
	if(rv > 0)
	{
#if 0
		debug_sprintf(szDbgMsg, "%s: Setting DEBUGOPT variable[%s=%s]", __FUNCTION__, "DEBUGOPT", szParamValue);
		APP_TRACE(szDbgMsg);
#endif
		rv = setenv("DEBUGOPT", szParamValue, 1);
		if (rv != EXIT_SUCCESS)
		{
#if 0
			debug_sprintf(szDbgMsg, "%s: Error while setting DEBUGOPT to env", __FUNCTION__);
			APP_TRACE(szDbgMsg);
#endif
		}

	}

	rv = EXIT_SUCCESS;
#if 0
	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);
#endif
	return rv;
}

/*
 * ============================================================================
 * Function Name: TORProcessor
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
static void * TORProcessor(void * arg)
{
	char 		szTORRecord[2048] 				= ""; //Need to check whether this much data is enough to hold each TOR request
	char 		pVresponse[MAX_DATA_TO_RECEIVE] = { 0 };
	char 		szTempBuf[256] 					= "";
	char		szAppLogData[2560]				= "";
	sCONFIG LocGCONFIG; //To Store the Config variables
	sGLOBALS *GLOBAL;
	sGLOBALS sObj;
	_sSCA_XML_REQUEST req_xml_info[eREQ_ELEMENT_MAX];
	FILE *FhPerm;
	FILE *FhTemp;
	int					iSkipFirstRec 		= 0;
	int 				iRecordLength 		= 0;
	int 				iRetVal       		= 0;
	int					iTotalTORRetries	= 5; //Vantiv suggested to send totally 5 times
	int					iTORRetriesLeft		= 0;
	int					iAppLogEnabled		= isAppLogEnabled();

#ifdef DEBUG
	char szDbgMsg[4096] = "";
#endif

	debug_sprintf(szDbgMsg, "%s: TOR Processor thread begins execution", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	GLOBAL = &sObj;
	memset(&LocGCONFIG, 0x00, sizeof(sCONFIG));
	memset(GLOBAL, 0x00, sizeof(sGLOBALS));

	/* Copy the config parameter values to the local config structure */
	memcpy(&LocGCONFIG, &GCONFIG, sizeof(sCONFIG));

	memset(&req_xml_info, 0x00, sizeof(_sSCA_XML_REQUEST) * eREQ_ELEMENT_MAX);

	//Set function type and command name here
	req_xml_info[eFUNCTION_TYPE].element = (char*)calloc(strlen("PAYMENT")+1,sizeof(char));
	strcpy(req_xml_info[eFUNCTION_TYPE].element, "PAYMENT");

	req_xml_info[eCOMMAND].element = (char*)calloc(strlen("TOR_REQ")+1,sizeof(char));
	strcpy(req_xml_info[eCOMMAND].element, "TOR_REQ");


	while (1)
	{
		/*
		 * STEP - 1: Check whether FILE exists
		 */
		iRetVal = getTxtFileBufferLength(TOR_RECORD_FILE_NAME);

		if (iRetVal <= 0)
		{
			//debug_sprintf(szDbgMsg, "%s: No TOR Records to process", __FUNCTION__);
			//APP_TRACE(szDbgMsg);

			svcWait(10000);
			continue;
		}

		debug_sprintf(szDbgMsg, "%s: TOR Record(s) present", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		/*
		 * STEP - 2: Read the TOR record
		 */

		debug_sprintf(szDbgMsg, "%s: Acquiring the TOR file[%s] to read the TOR request", __FUNCTION__, TOR_RECORD_FILE_NAME);
		APP_TRACE(szDbgMsg);

		acquireMutexLock(&gptTORRecordFileMutex, "TOR Record File");

		FhPerm = fopen(TOR_RECORD_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
		if (FhPerm == NULL)
		{
			debug_sprintf(szDbgMsg, "%s: Error while opening the TOR record file!!!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			debug_sprintf(szDbgMsg, "%s: Releasing the TOR file[%s]", __FUNCTION__, TOR_RECORD_FILE_NAME);
			APP_TRACE(szDbgMsg);			
			
			releaseMutexLock(&gptTORRecordFileMutex, "TOR Record File");
			svcWait(10000);
			continue;
		}

		memset(szTORRecord, 0x00, sizeof(szTORRecord));

		if (fgets(szTORRecord, sizeof(szTORRecord), FhPerm) == NULL) //Wanted it to terminate by the newline character
				{
			if(feof(FhPerm))
			{
				debug_sprintf(szDbgMsg, "%s: End of file reached, no more lines", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Error while getting the TOR record from the file", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(FhPerm); //Closing the file before returning
				debug_sprintf(szDbgMsg, "%s: Releasing the TOR file[%s] ", __FUNCTION__, TOR_RECORD_FILE_NAME);
				APP_TRACE(szDbgMsg);
				releaseMutexLock(&gptTORRecordFileMutex, "TOR Record File");
				svcWait(10000);
				continue;
			}
		}

		debug_sprintf(szDbgMsg, "%s: Read the TOR Record", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		szTORRecord[strlen(szTORRecord)] = 0x00; ////Nullyfying the LF

#ifdef DEVDEBUG
		if(strlen(szTORRecord) < 4050)
		{
			debug_sprintf(szDbgMsg, "%s: TOR Record [%s]", __FUNCTION__, szTORRecord);
			APP_TRACE(szDbgMsg);
		}
#endif

		fclose(FhPerm); //Close the file and release the mutex since we read the TOR record
		debug_sprintf(szDbgMsg, "%s: Releasing the TOR file[%s] after reading the TOR record", __FUNCTION__, TOR_RECORD_FILE_NAME);
		APP_TRACE(szDbgMsg);
		releaseMutexLock(&gptTORRecordFileMutex, "TOR Record File");

		/*
		 * STEP - 3: Upload the TOR request
		 */
		initializeServiceData(GLOBAL); //To initialize the GLOBAL structure with default values

		iTORRetriesLeft = 0;

		iTORRetriesLeft = iTotalTORRetries - GCONFIG.torRetry;

		if(iTORRetriesLeft > 0)
		{
			debug_sprintf(szDbgMsg, "%s: Need to try TOR retries for %d times", __FUNCTION__, iTORRetriesLeft);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Need to Try Below TOR Request For %d Times", iTORRetriesLeft);
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);

				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_VANTIV_REQUEST, szTORRecord, NULL);
			}
		}
		else
		{
			iTORRetriesLeft = 0;
		}

		while(1)
		{
			if(iTORRetriesLeft == 0)
			{
				debug_sprintf(szDbgMsg, "%s: Maximum TOR retries reached!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Maximum TOR Retries(%d) Reached", iTotalTORRetries);
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSED, szAppLogData, NULL);
				}

				break;
			}

			debug_sprintf(szDbgMsg, "%s: Trying TOR for %d time", __FUNCTION__, ((iTotalTORRetries - iTORRetriesLeft) + 1));
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Trying TOR Request For %d Time", ((iTotalTORRetries - iTORRetriesLeft) + 1));
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
			}

			memset(pVresponse, 0x00, sizeof(pVresponse));
			iRetVal = sendRecvViaCurl(szTORRecord, pVresponse, GLOBAL,&LocGCONFIG, req_xml_info);

			if (iRetVal == EXIT_SUCCESS)
			{
				//We have received some response, so breaking from this loop

				debug_sprintf(szDbgMsg, "%s: Received response for TOR request", __FUNCTION__);
				APP_TRACE(szDbgMsg);
#ifdef DEVDEBUG
				if(strlen(pVresponse) < 4050)
				{
					debug_sprintf(szDbgMsg, "%s: TOR Response [%s]", __FUNCTION__, pVresponse);
					APP_TRACE(szDbgMsg);
				}
#endif
				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Received Below Response For The TOR Request");
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);

					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_VANTIV_RESPONSE, pVresponse, NULL);
				}

				break;
			}
			else
			{
				/*
				 * Praveen_P1: 11 November 2016
				 * Fix for PTMX-1657
				 * Need to consider Host Not Available case when trying TOR
				 * If Host is not available, then we should not decrement the TOR retry count left
				 */
				if(GLOBAL->isProcessorAvailable == true || GLOBAL->http.status_code == eHTTP_GATEWAY_TIMEOUT)
				{
					debug_sprintf(szDbgMsg, "%s: Host is Available, Reducing the TOR Retry Count", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					iTORRetriesLeft--;
				}
				else
				{
					debug_sprintf(szDbgMsg, "%s: Host is NOT Available, NOT Reducing the TOR Retry Count", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					if(iAppLogEnabled)
					{
						sprintf(szAppLogData, "Host is NOT Available, NOT Reducing the TOR Retry Count");
						addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
					}
				}
			}

			debug_sprintf(szDbgMsg, "%s: Waiting for 10 secs before retrying TOR", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			svcWait(10000); //try reposting it after sometime
		}

		debug_sprintf(szDbgMsg, "%s: Successfully processed the TOR Record", __FUNCTION__);
		APP_TRACE(szDbgMsg); //We dont bother about the what the response..any response is consider as successful

		/*
		 * STEP - 4: Remove the processed TOR record from the file
		 */

		debug_sprintf(szDbgMsg, "%s: Acquiring the TOR file[%s] to remove the TOR request", __FUNCTION__, TOR_RECORD_FILE_NAME);
		APP_TRACE(szDbgMsg);

		acquireMutexLock(&gptTORRecordFileMutex, "TOR Record File");

		while(1)
		{
			//Open the TOR File
			FhPerm = fopen(TOR_RECORD_FILE_NAME, "r+"); //Open a file for update (both for input and output). The file must exist.
			if (FhPerm == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while opening the TOR original record file!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				break;
			}

			//Open the Temp file to write the remaining TOR records
			FhTemp = fopen(TOR_RECORD_FILE_NAME_TEMP, "a+"); //Open file for output at the end of a file. Output operations always write data at the end of the file, expanding it. Repositioning operations (fseek, fsetpos, rewind) are ignored. The file is created if it does not exist
			if (FhTemp == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: Error while opening the SAF Temporary record file!!!", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				fclose(FhPerm);
				break;
			}

			memset(szTORRecord, 0x00, sizeof(szTORRecord));
			iSkipFirstRec = 0;

			while (fgets(szTORRecord, sizeof(szTORRecord), FhPerm) != NULL) /* read a line */
			{
				if (iSkipFirstRec == 0) //Skip the first TOR record
				{
					debug_sprintf(szDbgMsg, "%s: Need not copy first TOR record, since it is processed", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					iSkipFirstRec = 1;
				}
				else//Copy the TOR record to temp file
				{					
					iRecordLength = strlen(szTORRecord);

					iRetVal = fwrite(szTORRecord, sizeof(char), iRecordLength,FhTemp);

					if(iRetVal == iRecordLength)
					{
						debug_sprintf(szDbgMsg, "%s: Successfully written TOR record to the Temp file ", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
					else
					{
						debug_sprintf(szDbgMsg, "%s: Failure while writing TOR record to the file, Actual count[%d], Written bytes [%d] ", __FUNCTION__, iRecordLength, iRetVal);
						APP_TRACE(szDbgMsg);
					}

					memset(szTORRecord, 0x00, sizeof(szTORRecord));
				}
			}

			fclose(FhPerm);
			fclose(FhTemp);

			//Moving the temporary TOR record to original file
			memset(szTempBuf, 0x00, sizeof(szTempBuf));
			sprintf(szTempBuf, "mv -f %s %s", TOR_RECORD_FILE_NAME_TEMP,TOR_RECORD_FILE_NAME); //copying the file
			debug_sprintf(szDbgMsg,"%s - The command to be executed is [%s]", __FUNCTION__,szTempBuf);
			APP_TRACE(szDbgMsg);
			iRetVal = local_svcSystem(szTempBuf);

			if(iRetVal == 0)
			{
				debug_sprintf(szDbgMsg,"%s - Successfully executed the command", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
			else
			{
				debug_sprintf(szDbgMsg,"%s - Error in execution of the command", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			break;
		}

		debug_sprintf(szDbgMsg, "%s: Releasing the TOR file[%s] after removing the TOR record", __FUNCTION__, TOR_RECORD_FILE_NAME);
		APP_TRACE(szDbgMsg);
		releaseMutexLock(&gptTORRecordFileMutex, "TOR Record File");

		svcWait(10000); //Wait for sometime and then check for the file
	}

	debug_sprintf(szDbgMsg, "%s: TOR Processing Thread Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}
