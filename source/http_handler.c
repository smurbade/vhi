/*
 * http_handler.c
 *
 *  Created on: Nov 21, 2013
 *      Author: Dexter M. Alberto
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include <unistd.h>
#include <errno.h>

#include "http_handler.h"
#include "help_function.h"
#include "logger.h"
#include "config_data.h"
#include "appLog.h"


static ssize_t readn(int fd, void *vptr, size_t n);/* Read "n" bytes from a descriptor. */

int parseHttpRequest(int conn, sGLOBALS* GLOBAL)
{
#ifdef LOGGING_ENABLED
	char szDbgMsg[2048] = {0};
#endif
	int		iAppLogEnabled		= isAppLogEnabled();
	char	szAppLogData[5120]	= "";
	char	szAppLogDiag[300]	= "";


	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	int iRead  = 0;
	char buffer[MAX_LINE_TO_RECEIVE]={0};
	int status = 0;

	while(1) {
		memset(buffer, 0x00, sizeof(buffer));
		readHeaderLine(conn, buffer); //receive data by lines

		if (buffer[0]=='\0' || buffer[0]=='\r') { //if reach to the payload
			break;
		}
#ifdef DEVDEBUG
		debug_sprintf(szDbgMsg, "%s: Read Header Line [%s]", __FUNCTION__, buffer);
		APP_TRACE(szDbgMsg);
#endif
		status = parseHttpHeader(buffer, GLOBAL); //parse the headers
		if ( status != 0 ) {
			debug_sprintf(szDbgMsg, "%s: ERROR!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return EXIT_FAILURE;
		}

		if(GLOBAL->http.method == eHEAD){
			debug_sprintf(szDbgMsg, "%s: HTTP Method is HEAD", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			// This one we will get when SCA is checking if SSI is online
			GLOBAL->http.status_code = eHTTP_OK;
			return EXIT_FAILURE; //Sending failure here so that we will send the xml response by skipping the other steps
		}
	}

	if (GLOBAL->http.content_type==NULL) { //accept only text/xml format
		GLOBAL->http.status_code = eHTTP_BAD_REQUEST;
		debug_sprintf(szDbgMsg, "%s: ERROR! CONTENT-TYPE required", __FUNCTION__);
		APP_TRACE(szDbgMsg);

    	if(iAppLogEnabled)
    	{
    		strcpy(szAppLogData, "Failed To Parse the HTTP Header Received From Client");
    		strcpy(szAppLogDiag, "Content Type Required.");
    		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RECEIVE, szAppLogData, szAppLogDiag);
    	}

		return EXIT_FAILURE;
	}

	if(iAppLogEnabled)
	{
		strcpy(szAppLogData, "Successfully Read HTTP Header");
		addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_RECEIVE, szAppLogData, NULL);
	}

	if (GLOBAL->http.requestLength <= 0) { //if payload length is <= 0

		GLOBAL->http.status_code = eHTTP_LENGTH_REQUIRED;
#if 0
		/* Currently we are setting this to success(eHTTP_OK) because when SCA sends host connectivity packet, it will not have length.
		  * So returning success result code in that case  */
		GLOBAL->http.status_code = eHTTP_OK;
#endif
		debug_sprintf(szDbgMsg, "%s: ERROR! CONTENT-LENGTH required", __FUNCTION__);
		APP_TRACE(szDbgMsg);

    	if(iAppLogEnabled)
    	{
    		strcpy(szAppLogData, "Failed To Parse the HTTP Request Received From Client.");
    		strcpy(szAppLogDiag, "Content Length Required.");
    		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RECEIVE, szAppLogData, szAppLogDiag);
    	}

		return EXIT_FAILURE;
	}

	if (GLOBAL->http.expect != NULL) { //Expect header
		/* Expect a limit
		if (GLOBAL->http.requestLength < MAX_DATA_TO_RECEIVE) {
			if (strncmp(GLOBAL->http.expect, "100-continue", 12) == 0) {
				continueHttpResponse(conn); //response 100-continue
			}
		}
		else {
			GLOBAL->http.status_code = eHTTP_REQUEST_ENTITY_TOO_LARGE;
			debug_sprintf(szDbgMsg, "%s: ERROR! Request to large", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return EXIT_FAILURE;
		}
		*/

		if (strncmp(GLOBAL->http.expect, "100-continue", 12) == COMPARE_OK) {
			continueHttpResponse(conn); //response 100-continue
		}
		else {
			debug_sprintf(szDbgMsg, "%s: ERROR! unexpected", __FUNCTION__);
			APP_TRACE(szDbgMsg);

	    	if(iAppLogEnabled)
	    	{
	    		strcpy(szAppLogData, "Failed To Parse the HTTP Request Received From Client");
	    		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RECEIVE, szAppLogData, NULL);
	    	}

			return EXIT_FAILURE;
		}
	}

	if (GLOBAL->http.xml_request) {
		free(GLOBAL->http.xml_request);
		GLOBAL->http.xml_request = NULL;
	}

	GLOBAL->http.xml_request = (char*)calloc(GLOBAL->http.requestLength+1, sizeof(char));
    debug_sprintf(szDbgMsg, "Size of incoming message %d", GLOBAL->http.requestLength);
    APP_TRACE(szDbgMsg);

    iRead = GLOBAL->http.requestLength;

    /*Changed the read to readn as the previous one was a non-blocking one*/
    readn(conn, GLOBAL->http.xml_request, iRead);


#if 0
	while(1) {
		int rcv=0;
		memset(buffer, 0x00, MAX_LINE_TO_RECEIVE);

		if ((rcv = read(conn, buffer, MAX_LINE_TO_RECEIVE-1)) > 0) { //read the payload by 1024 lines
			strncat(GLOBAL->http.xml_request, buffer, strlen(buffer));
		}

		if (rcv < (MAX_LINE_TO_RECEIVE-1)) { //break if rcv is less than max
			break;
		}
	}
#endif

	if (GLOBAL->http.requestLength != strlen(GLOBAL->http.xml_request)) { //if payload length is <= 0
		GLOBAL->http.status_code = eHTTP_BAD_REQUEST;
		debug_sprintf(szDbgMsg, "%s: ERROR! length not match %d, %d", __FUNCTION__, GLOBAL->http.requestLength, strlen(GLOBAL->http.xml_request));
		APP_TRACE(szDbgMsg);
        debug_sprintf(szDbgMsg, "Request is =>%s<=", GLOBAL->http.xml_request);
        APP_TRACE(szDbgMsg);

    	if(iAppLogEnabled)
    	{
    		strcpy(szAppLogData, "Failed To Read the HTTP Request Received From Client");
    		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_RECEIVE, szAppLogData, NULL);
    	}

		return EXIT_FAILURE;
	}

	if(iAppLogEnabled)
	{
		strcpy(szAppLogData, "Successfully Read HTTP Body");
		addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_RECEIVE, szAppLogData, NULL);

		sprintf(szAppLogData, "Successfully Received the Request from the Client");
		addAppEventLog(APP_NAME, ENTRYTYPE_SUCCESS, ENTRYID_RECEIVE, szAppLogData, NULL);
	}

	if(iAppLogEnabled)
	{
		//strcpy(szAppLogData, GLOBAL->http.xml_request); //Praveen_P1: 29 Dec 2015
		addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_DHI_REQUEST, GLOBAL->http.xml_request, NULL);
	}


	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

    return EXIT_SUCCESS;
}

void readHeaderLine(int conn, void* ptr)
{
	int rcv=0;
	char c, *p;
	p = ptr;

	while(1) {
		if ( (rcv = read(conn, &c, 1)) == 1 ) {
			*p++ = c;
			if (c == '\n')
				break;
		}
		else {
			perror("read");
		}
	}
	*p = 0;
}

int parseHttpHeader(char* ptr, sGLOBALS* GLOBAL)
{
#ifdef LOGGING_ENABLED
	char szDbgMsg[512] = {0};
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* endptr = NULL;
	char* header = NULL;
	char* value = NULL;

	if ( ptr == NULL ) { //if line is NULL
		//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INTERNAL_ERROR;
		debug_sprintf(szDbgMsg, "%s: ERROR! ptr is null", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	if ( GLOBAL->http.firstHeader == 0 ) { //first header
		GLOBAL->http.firstHeader = 1; //indicate that 1st header is checked

		if (!strncmp(ptr, "POST ", 5)) { //support only POST request
			GLOBAL->http.method = ePOST;
			GLOBAL->http.status_code = eHTTP_OK;
			ptr+=5;
		}
		else if (!strncmp(ptr, "HEAD ", 5)) {
			GLOBAL->http.method = eHEAD;
			GLOBAL->http.status_code = eHTTP_OK;
			ptr+=5;
		}
		else { //return if not POST request
			GLOBAL->http.method = eUNSUPPORTED;
			GLOBAL->http.status_code = eHTTP_METHOD_NOT_ALLOWED;
			debug_sprintf(szDbgMsg, "%s: ERROR! HTTP Method unsupported", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return EXIT_FAILURE;
		}

		if (strncmp(ptr, "/ ", 2)) { //return if error
			GLOBAL->http.status_code = eHTTP_NOT_FOUND;
			debug_sprintf(szDbgMsg, "%s: ERROR! resource should be", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return EXIT_FAILURE;
		}
		ptr += 2;

		if (strncmp(ptr, "HTTP/1.1", 8)) { //return if error
			GLOBAL->http.status_code = eHTTP_HTTP_VERSION_NOT_SUPPORTED;
			debug_sprintf(szDbgMsg, "%s: ERROR! HTTP/1.1 is supported", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return EXIT_FAILURE;
		}
	}
	else { //next header
		if ( (GLOBAL->http.method == ePOST  || GLOBAL->http.method == eHEAD ) && GLOBAL->http.status_code == eHTTP_OK ) { //1st header is success

			endptr = strchr(ptr, ':');
			if (endptr==NULL) { //no : in header
				GLOBAL->http.status_code = eHTTP_BAD_REQUEST;
				debug_sprintf(szDbgMsg, "%s: ERROR! no colon found", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}

			header = (char*)calloc((endptr-ptr)+1, sizeof(char));
			strncpy(header, ptr, endptr-ptr ); //copy the header only
//			tmp_trim(header); //trim
			capitalize(header); //capitalized
			ptr += (endptr - ptr)+1;
#if 0//ignore if there is no space after ":"
			if ( *ptr != ' ' ) { //check if there is space after :
				GLOBAL->http.status_code = eHTTP_BAD_REQUEST;
				return EXIT_FAILURE;
			}

			ptr+=1;
#endif
			if ( *ptr == ' ' ) { //delete if space before value
				ptr+=1;
			}

			if (strlen(ptr)<=0) { //if no header's value
				GLOBAL->http.status_code = eHTTP_BAD_REQUEST;
				debug_sprintf(szDbgMsg, "%s: ERROR!", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}

			value = (char*)calloc(strlen(ptr)+1, sizeof(char));
			strcpy(value, ptr); //copy header's value
//			tmp_trim(value); //trim

			if (value==NULL || strlen(value)<=0) {
				//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
				GLOBAL->http.status_code = eHTTP_OK;
				GLOBAL->appErrCode = eERR_INTERNAL_ERROR;

				debug_sprintf(szDbgMsg, "%s: ERROR!", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return EXIT_FAILURE;
			}

			if (!strncmp(header, "CONTENT-LENGTH", 14)) {
				if (GLOBAL->http.content_length)
					free(GLOBAL->http.content_length);

				GLOBAL->http.content_length = (char*)calloc(strlen(value)+1, sizeof(char));
				strcpy(GLOBAL->http.content_length, value); //copy the header's value

				GLOBAL->http.requestLength = atoi(GLOBAL->http.content_length);
				if ( GLOBAL->http.requestLength <= 0 ) { //if no length value
					GLOBAL->http.status_code = eHTTP_LENGTH_REQUIRED;
					debug_sprintf(szDbgMsg, "%s: ERROR! LENGTH REQUIRED", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					return EXIT_FAILURE;
				}
			}
			else if (!strncmp(header, "CONTENT-TYPE", 12)) {
				if (GLOBAL->http.content_type)
					free(GLOBAL->http.content_type);

				GLOBAL->http.content_type = (char*)calloc(strlen(value)+1, sizeof(char));
				strcpy(GLOBAL->http.content_type, value); //copy the header's value

				if (strncmp(GLOBAL->http.content_type, "text/xml", 8)) {
					GLOBAL->http.status_code = eHTTP_UNSUPPORTED_MEDIA_TYPE;
					debug_sprintf(szDbgMsg, "%s: ERROR! UNSUPPORTED MEDIA TYPE", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					return EXIT_FAILURE;
				}
			}
			else if (!strncmp(header, "EXPECT", 6)) {
				if (GLOBAL->http.expect)
					free(GLOBAL->http.expect);

				GLOBAL->http.expect = (char*)calloc(strlen(value)+1, sizeof(char));
				strcpy(GLOBAL->http.expect, value); //copy the header's value
			}

			//free data
			if (header)
				free(header);
			if (value)
				free(value);

		}
		else { //there is error in 1st header
			debug_sprintf(szDbgMsg, "%s: ERROR! Method: %d, Status code: %d", __FUNCTION__, GLOBAL->http.method, GLOBAL->http.status_code);
			APP_TRACE(szDbgMsg);

			return EXIT_FAILURE;
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

    return EXIT_SUCCESS;
}

void continueHttpResponse(int conn)
{
#ifdef LOGGING_ENABLED
	char szDbgMsg[512] = {0};
#endif
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char * buffer = "HTTP/1.1 100 Continue\r\n"
			"\r\n";

	if ( write(conn, buffer, strlen(buffer) ) < 0) {
		debug_sprintf(szDbgMsg, "%s: ERROR! write error", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		exit(EXIT_FAILURE);
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

/*
 * ============================================================================
 * Function Name: readn
 *
 * Description	: Standard readn function which can be used to read n bytes
 *
 * Input Params	: Client FD, buffer to fill
 *
 * Output Params: NONE
 * ============================================================================
 */
static ssize_t readn(int fd, void *vptr, size_t n) /* Read "n" bytes from a descriptor. */
{
	size_t	nleft;
	ssize_t	nread;
	char	*ptr;

	ptr = vptr;
	nleft = n;
	while (nleft > 0)
	{
		if ( (nread = read(fd, ptr, nleft)) < 0)
		{
			if (errno == EINTR)
				nread = 0;		/* and call read() again */
			else
				return(-1);
		}
		else if (nread == 0)
			break;				/* EOF */

		nleft -= nread;
		ptr   += nread;
	}
	return(n - nleft);		/* return >= 0 */
}
