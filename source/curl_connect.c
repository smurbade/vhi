/*
 * curl_connect.c
 *
 *  Created on: Feb 20, 2014
 *      Author: Dexter M. Alberto
 */
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "curl_connect.h"
#include "logger.h"
#include <ctype.h>
#include <curl/curl.h>
#include "config_data.h"
#include "sca_xml_request.h"
#include "help_function.h"
#include "appLog.h"
#include "svc.h"

#ifdef LOGGING_ENABLED
char szDbgMsg[1024] = {0};
#endif

static void get800request(char *basegroup, char *tpsheader, sCONFIG * GCONFIG);
static int chkHostConn(CURL * curlHandle);

#ifdef LOGGING_CURL_ENABLED
// Can trace CURL packets using this callback
int debug_callback(CURL *handle,
                   curl_infotype type,
                   char *data,
                   size_t size,
                   void *userptr)
{
    int i;
    
    debug_sprintf(szDbgMsg, "CURL DEBUG type=%d, size=%d", type, size);
    APP_TRACE(szDbgMsg);
    for (i=0; i<size; i += 8)
    {
        debug_sprintf(szDbgMsg, "CURL DATA %04X: %02x, %02x, %02x, %02x, %02x, %02x, %02x, %02x", i, *(data+0+i), *(data+1+i), *(data+2+i), *(data+3+i), *(data+4+i), *(data+5+i), *(data+6+i), *(data+7+i));
        APP_TRACE(szDbgMsg);
    }
    for (i=0; i<size; i += 8)
    {
        debug_sprintf(szDbgMsg, "CURL TEXT %04X: %c%c%c%c%c%c%c%c", i, *(data+0+i), *(data+1+i), *(data+2+i), *(data+3+i), *(data+4+i), *(data+5+i), *(data+6+i), *(data+7+i));
        APP_TRACE(szDbgMsg);
    }
    
    return 0;
}
#endif

bool isProcessorOnline(sCONFIG * GCONFIG) {

	CURL 			*curlHandle;
	CURLcode 		res;
	char 			szCurlErrBuf[4096] = {0};
	char			*request;
	char			basegroup[68]	    = {0};
	char			tpsheader[8+21+1]	= {0};
	char 			response_code[3+1]  = {0};
	bool 			bResult = false;
	static int		host 		 = PRIMARY_URL;
	int				iCnt 		 = 0;
	int				iNumHostURLs = 1;
	char 			cSTAN[6+1] = {0};
	unsigned long long iSTAN=0;
	struct string 	sRecvData;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if((GCONFIG->vantivSecdURL != NULL) && (strlen(GCONFIG->vantivSecdURL) > 0))
	{
		debug_sprintf(szDbgMsg, "%s: Vantiv Secondary URL is set, setting iNumHostURLs to 2", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		iNumHostURLs = 2;
	}

	struct 		curl_slist *headers = NULL;

	while(iCnt < iNumHostURLs)
	{
		curlHandle = NULL;
		headers    = NULL;

		memset(&sRecvData, 0x00, sizeof(sRecvData));
		initializeString(&sRecvData);

		headers = curl_slist_append(headers, "Accept:");
		headers = curl_slist_append(headers, "Host:");
		headers = curl_slist_append(headers, "Content-Type: application/x-www-form-urlencoded");

		curlHandle = curl_easy_init();

		if (curlHandle) {
			if(host == PRIMARY_URL)
			{
				debug_sprintf(szDbgMsg, "%s: Posting to Primary URL [%s]", __FUNCTION__, GCONFIG->vantivURL);
				APP_TRACE(szDbgMsg);
				curl_easy_setopt(curlHandle, CURLOPT_URL, GCONFIG->vantivURL);
				/*
				 * Praveen_P1: 10 Dec 2015
				 * Fix for PTMX-496
				 * If URL contains the IPadress, With 2L setting CURL was returning 35 error code
				 * i.e. error:14077410:SSL routines:SSL23_GET_SERVER_HELLO:sslv3 alert handshake failure
				 * With 0L setting, URL and Common name mismatch error is ignored by CURL
				 * With this the URL contain the IPADDRESS
				 */
				if(GCONFIG->verifyHost == true)
				{
					curl_easy_setopt(curlHandle, CURLOPT_SSL_VERIFYHOST, 2L);
				}
				else
				{
					curl_easy_setopt(curlHandle, CURLOPT_SSL_VERIFYHOST, 0L);
				}
			}
			else if(host == SECONDARY_URL)
			{
				debug_sprintf(szDbgMsg, "%s: Posting to Secondary URL [%s]", __FUNCTION__, GCONFIG->vantivSecdURL);
				APP_TRACE(szDbgMsg);
				curl_easy_setopt(curlHandle, CURLOPT_URL, GCONFIG->vantivSecdURL);
				/*
				 * Praveen_P1: 10 Dec 2015
				 * Fix for PTMX-496
				 * If URL contains the IPadress, With 2L setting CURL was returning 35 error code
				 * i.e. error:14077410:SSL routines:SSL23_GET_SERVER_HELLO:sslv3 alert handshake failure
				 * With 0L setting, URL and Common name mismatch error is ignored by CURL
				 * With this the URL contain the IPADDRESS
				 */
				if(GCONFIG->verifyHost2 == true)
				{
					curl_easy_setopt(curlHandle, CURLOPT_SSL_VERIFYHOST, 2L);
				}
				else
				{
					curl_easy_setopt(curlHandle, CURLOPT_SSL_VERIFYHOST, 0L);
				}
			}
			else //Should not come here...
			{
				debug_sprintf(szDbgMsg, "%s: [Should Not Come] Posting to Primary URL [%s]", __FUNCTION__, GCONFIG->vantivURL);
				APP_TRACE(szDbgMsg);
				curl_easy_setopt(curlHandle, CURLOPT_URL, GCONFIG->vantivURL);

				if(GCONFIG->verifyHost == true)
				{
					curl_easy_setopt(curlHandle, CURLOPT_SSL_VERIFYHOST, 2L);
				}
				else
				{
					curl_easy_setopt(curlHandle, CURLOPT_SSL_VERIFYHOST, 0L);
				}
			}

			curl_easy_setopt(curlHandle, CURLOPT_CAINFO, CA_CERT_FILE);
			curl_easy_setopt(curlHandle, CURLOPT_USERPWD, GCONFIG->hostUsrPwd);
			curl_easy_setopt(curlHandle, CURLOPT_HTTPHEADER, headers);

			/*connection timeout*/
			if( GCONFIG->connTimeout > 0 )
			{
				curl_easy_setopt(curlHandle, CURLOPT_CONNECTTIMEOUT, GCONFIG->connTimeout); //Praveen_P1: Setting connection time out value
			}

			/*timeout setting*/
			if( GCONFIG->torTimeout > 0 )
			{
				curl_easy_setopt(curlHandle, CURLOPT_TIMEOUT, GCONFIG->torTimeout);
			}
			/*end*/

			curl_easy_setopt(curlHandle, CURLOPT_NOSIGNAL, 1L);
			curl_easy_setopt(curlHandle, CURLOPT_POST, 1L);
			curl_easy_setopt(curlHandle, CURLOPT_SSL_VERIFYPEER, 1L);

			curl_easy_setopt(curlHandle, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
			curl_easy_setopt(curlHandle, CURLOPT_FAILONERROR, true);
			//curl_easy_setopt(curlHandle, CURLOPT_CONNECT_ONLY, 1L);

			memset(basegroup, 0x00, sizeof(basegroup));
			memset(tpsheader, 0x00, sizeof(tpsheader));

			get800request(basegroup, tpsheader, GCONFIG);

			uint bglen = strlen(basegroup);
			uint tpslen = strlen(tpsheader);

			request = (char*)calloc(bglen+tpslen+1, sizeof(char));
			strcpy(request, tpsheader);
			strcat(request, basegroup);

			debug_sprintf(szDbgMsg, "%s: Health Message Request = [%s]", __FUNCTION__, request);
			APP_TRACE(szDbgMsg);

			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_VANTIV_REQUEST, request, NULL);

			// B2 URL encode the request message
			char *encoded_request = curl_easy_escape(curlHandle, request, strlen(request));
			if(encoded_request)
			{
				curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDS, (void *) encoded_request);
				curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDSIZE, strlen(encoded_request));
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: ERROR! Error encoding data", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(sRecvData.ptr)
					free(sRecvData.ptr);
				return bResult;
			}

			curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, writeFunc);
			curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, &sRecvData);

			curl_easy_setopt(curlHandle, CURLOPT_ERRORBUFFER, szCurlErrBuf);

			res = curl_easy_perform(curlHandle);

			if(res == CURLE_OK)
			{
				debug_sprintf(szDbgMsg, "%s: Perform SUCCESS", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				debug_sprintf(szDbgMsg, "%s: Health Message Response [%s]", __FUNCTION__, sRecvData.ptr);
				APP_TRACE(szDbgMsg);

				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_VANTIV_RESPONSE, sRecvData.ptr, NULL);

				memset(response_code, 0x00, sizeof(response_code));

				strncpy(response_code, sRecvData.ptr, 3);

				debug_sprintf(szDbgMsg, "%s: Response Code is %s", __FUNCTION__, response_code);
				APP_TRACE(szDbgMsg);

				if(strcmp(response_code, "110") == 0)
				{
					bResult = false;
				}
				else
				{
					bResult = true;
				}

				memset(cSTAN, 0x00, sizeof(cSTAN));
				getEnvFile("DHI", "STAN", cSTAN, (sizeof(cSTAN) - 1) );

				if (strlen(cSTAN) >= 0 )
				{
					iSTAN = atoi(cSTAN);
					if(iSTAN >=0) {
						iSTAN++;
						if(iSTAN>999999) {
							iSTAN=1;
						}
						memset(cSTAN, 0x00, sizeof(cSTAN));
						sprintf(cSTAN,"%llu", iSTAN);
						putEnvFile("DHI", "STAN", cSTAN);
					}
				}

			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: Return Value from PERFORM = [%d]",
																__FUNCTION__, res);
				APP_TRACE(szDbgMsg);
			}

			if (res != CURLE_OK)
			{
				switch (res)
				{
					case CURLE_COULDNT_CONNECT:
					case CURLE_COULDNT_RESOLVE_HOST:
					case CURLE_COULDNT_RESOLVE_PROXY:
					{
						bResult = false;
						break;
					}
					case CURLE_OPERATION_TIMEDOUT:
						//bResult = true;
						bResult = false; //Praveen_P1: Not considering response timeout as host available scenario
						break;
					case CURLE_GOT_NOTHING:
						bResult = false;
						break;
					default:
					{
						debug_sprintf(szDbgMsg, "%s: ERROR! Unhandled error", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						bResult = false;
						break;
					}
				}
			}

			/* always cleanup */
			curl_easy_cleanup(curlHandle);

			/* Cleaning up headers */
			curl_slist_free_all(headers);

			if(request != NULL)
			{
				free(request);
			}

			if(sRecvData.ptr != NULL)
			{
				free(sRecvData.ptr);
			}

			if(bResult == true)
			{
				break;
			}
		}

		if(iNumHostURLs == 2) //Praveen_P1: Will check with next URL if two URLs are present
		{
			/* If the first host is not working then try the next host */
			host ^= 1;
		}
		iCnt++;
	}

	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, bResult);
	APP_TRACE(szDbgMsg);

	return bResult;
}

void freeCurlData()
{

}

void initializeString(struct string *s)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	s->nLen = 0;

	if(s->ptr) {
		free(s->ptr);
		s->ptr = NULL;
	}

	s->ptr = malloc(s->nLen+1);
	if (s->ptr == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! malloc() failed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		exit(EXIT_FAILURE);
	}

	s->ptr[0] = '\0';

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

size_t writeFunc(void *ptr, size_t size, size_t nmemb, struct string *s)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	size_t new_len = s->nLen + size*nmemb;
	s->ptr = realloc(s->ptr, new_len+1);

	if (s->ptr == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! realloc() failed", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		exit(EXIT_FAILURE);
	}

	memcpy(s->ptr+s->nLen, ptr, size*nmemb);
	s->ptr[new_len] = '\0';
	s->nLen = new_len;

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return size*nmemb;
}

int sendRecvViaCurl( char* request, char* getdata, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	int		iAppLogEnabled		= 0;
	char	szAppLogData[300]	= "";
	char	szAppLogDiag[300]	= "";

	iAppLogEnabled = isAppLogEnabled();

	/*For SIGNATURE pCOMMAND*/
	char* pCOMMAND = getXmlTagValue( eCOMMAND , req_xml_info);
	if( pCOMMAND != NULL ) {
		if (strncmp( pCOMMAND, COMMAND.SIGNATURE, strlen(COMMAND.SIGNATURE) ) == COMPARE_OK ) {
			debug_sprintf(szDbgMsg, "%s: NOTE! No SIGNATURE Vantiv request", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				sprintf(szAppLogData, "Signature Request Received, No Need to Post This Request to Vantiv Host");
				addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
			}
			return EXIT_SUCCESS;
		}
	}
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! COMMAND is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;

		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INTERNAL_ERROR;
		return EXIT_FAILURE;
	}
	/*end*/

	/*Processor checking*/
	if (strncmp(pCOMMAND, "PROCESSOR", 9) == COMPARE_OK) {

		if(iAppLogEnabled)
		{
			sprintf(szAppLogData, "Host Connection Status Request Received, Need to Check the Current Connection Status With Vantiv Host");
			addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
		}
		//if (isProcessorOnline(VANTIV_TEST_URL) == false) {
		if (isProcessorOnline(GCONFIG) == false) {
			GLOBAL->isProcessorAvailable = false;
			strcpy(getdata, "PROCESSOR is unavailable!");
		}
		else {
			GLOBAL->isProcessorAvailable = true;
			strcpy(getdata, "PROCESSOR is available!");
		}

		return EXIT_SUCCESS;
	}
	/*end*/

	int nStatus = EXIT_SUCCESS;
	CURL *curl;
	CURLcode resp_code;
	char szCurlErrBuf[4096] = {0};
	struct string sRecvData;
	static int	host 		 = PRIMARY_URL;
	int			iCnt 		 = 0;
	int			iNumHostURLs = 1;
	/*
	 * Setting up chunked headers
	 */
	struct curl_slist *headers = NULL;

	if((GCONFIG->vantivSecdURL != NULL) && (strlen(GCONFIG->vantivSecdURL) > 0))
	{
		debug_sprintf(szDbgMsg, "%s: Vantiv Secondary URL is set, setting iNumHostURLs to 2", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		iNumHostURLs = 2;
	}

	while(iCnt < iNumHostURLs)
	{
		curl = NULL;
		headers = NULL;
		nStatus = EXIT_SUCCESS;
		memset(&sRecvData, 0x00, sizeof(sRecvData));
		initializeString(&sRecvData);

		headers = curl_slist_append(headers, "Accept:");
		headers = curl_slist_append(headers, "Host:");
		headers = curl_slist_append(headers, "Content-Type: application/x-www-form-urlencoded");

	//	curl_global_init(CURL_GLOBAL_DEFAULT); Praveen_P1: We are initializing only once at the starting of the application

		curl = curl_easy_init();

		if(curl == NULL) {
			debug_sprintf(szDbgMsg, "%s: ERROR! Error while initializing CURL handler", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogDiag, "Error! while initializing CURL handler");
				sprintf(szAppLogData, "CURL handler failed to Initialize");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSING, szAppLogData, szAppLogDiag);
			}

			//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
			GLOBAL->http.status_code = eHTTP_OK;
			GLOBAL->appErrCode = eERR_INTERNAL_ERROR;
			return EXIT_FAILURE;
		}

		//curl_easy_setopt(curl, CURLOPT_URL, VANTIV_TEST_URL);
		if(host == PRIMARY_URL)
		{
			debug_sprintf(szDbgMsg, "%s: Posting to Primary URL [%s]", __FUNCTION__, GCONFIG->vantivURL);
			APP_TRACE(szDbgMsg);
			curl_easy_setopt(curl, CURLOPT_URL, GCONFIG->vantivURL);
			/*
			 * Praveen_P1: 10 Dec 2015
			 * Fix for PTMX-496
			 * If URL contains the IPadress, With 2L setting CURL was returning 35 error code
			 * i.e. error:14077410:SSL routines:SSL23_GET_SERVER_HELLO:sslv3 alert handshake failure
			 * With 0L setting, URL and Common name mismatch error is ignored by CURL
			 * With this the URL contain the IPADDRESS
			 */
			if(GCONFIG->verifyHost == true)
			{
				curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 2L);
			}
			else
			{
				curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
			}
		}
		else if(host == SECONDARY_URL)
		{
			debug_sprintf(szDbgMsg, "%s: Posting to Secondary URL [%s]", __FUNCTION__, GCONFIG->vantivSecdURL);
			APP_TRACE(szDbgMsg);
			curl_easy_setopt(curl, CURLOPT_URL, GCONFIG->vantivSecdURL);
			/*
			 * Praveen_P1: 10 Dec 2015
			 * Fix for PTMX-496
			 * If URL contains the IPadress, With 2L setting CURL was returning 35 error code
			 * i.e. error:14077410:SSL routines:SSL23_GET_SERVER_HELLO:sslv3 alert handshake failure
			 * With 0L setting, URL and Common name mismatch error is ignored by CURL
			 * With this the URL contain the IPADDRESS
			 */
			if(GCONFIG->verifyHost2 == true)
			{
				curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 2L);
			}
			else
			{
				curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
			}
		}
		else //Should not come here...
		{
			debug_sprintf(szDbgMsg, "%s: [Should Not Come] Posting to Primary URL [%s]", __FUNCTION__, GCONFIG->vantivURL);
			APP_TRACE(szDbgMsg);
			curl_easy_setopt(curl, CURLOPT_URL, GCONFIG->vantivURL);

			if(GCONFIG->verifyHost == true)
			{
				curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 2L);
			}
			else
			{
				curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
			}
		}

		curl_easy_setopt(curl, CURLOPT_CAINFO, CA_CERT_FILE);
		curl_easy_setopt(curl, CURLOPT_USERPWD, GCONFIG->hostUsrPwd);
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

	#ifdef LOGGING_CURL_ENABLED
		curl_easy_setopt(curl, CURLOPT_VERBOSE, true);
		curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, debug_callback);
	#endif

		/*connection timeout*/
		if( GCONFIG->connTimeout > 0 )
		{
			curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, GCONFIG->connTimeout); //Praveen_P1: Setting connection time out value
		}

		/*timeout setting*/
		if( GCONFIG->torTimeout > 0 )
		{
			curl_easy_setopt(curl, CURLOPT_TIMEOUT, GCONFIG->torTimeout);
		}
		/*end*/

		curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1L);
		curl_easy_setopt(curl, CURLOPT_POST, 1L);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1L);


		curl_easy_setopt(curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);

		if(chkHostConn(curl) != EXIT_SUCCESS)
		{
			/*
			 * Failed to connect() to host or proxy.
			 */
			GLOBAL->isProcessorAvailable = false;
			GLOBAL->http.status_code = eHTTP_OK;
			nStatus = EXIT_FAILURE;

			GLOBAL->appErrCode = eERR_NOERROR;

			debug_sprintf(szDbgMsg, "%s: ERROR! Connect Only Got Failed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if(iAppLogEnabled)
			{
				strcpy(szAppLogData, "Failed To Connect to Vantiv Host, Error[CURLOPT_CONNECT_ONLY FAILED]. Error[CURLE_COULDNT_CONNECT].");
				strcpy(szAppLogDiag, "Please Check The Vantiv Host URLs, Otherwise Contact Verifone");
				addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, szAppLogDiag);
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Connect Only Got Succeeded, Posting the Request now", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			// B2 URL encode the request message
			char *encoded_request = curl_easy_escape(curl, request, strlen(request));
			if(encoded_request)
			{
				curl_easy_setopt(curl, CURLOPT_POSTFIELDS, (void *) encoded_request);
				curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, strlen(encoded_request));
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: ERROR! Error encoding data", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(sRecvData.ptr)
					free(sRecvData.ptr);
				return EXIT_FAILURE;
			}

			/* Set the detection as TRUE for HTTP errors */
			curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1);

			/* Set the write function */
			// B2 NOTE:
			//        If you get a warning like "_curl_easy_setopt_err_write_callback
			//        declared with attribute warning" then it comes from the source
			//        code of libcurl, which did not finalize its 64-bit port.

			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeFunc);
			curl_easy_setopt(curl, CURLOPT_WRITEDATA, &sRecvData);

			curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, szCurlErrBuf);

			/* Perform the request, res will get the return code */
			resp_code = curl_easy_perform(curl);

			if (resp_code == CURLE_OK)
			{
				/*increment of STAN*/
				GLOBAL->http.status_code = eHTTP_OK;
				GLOBAL->appErrCode = eERR_NOERROR;
				incrementStan(GLOBAL, req_xml_info);

				debug_sprintf(szDbgMsg, "%s: CURLE OK", __FUNCTION__);
				APP_TRACE(szDbgMsg);


				if(iAppLogEnabled)
				{
					sprintf(szAppLogData, "Request Successfully Posted and Received Response Successfully From VANTIV Host");
					addAppEventLog(APP_NAME, ENTRYTYPE_INFO, ENTRYID_PROCESSING, szAppLogData, NULL);
				}

				GLOBAL->isProcessorAvailable = true;
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: CURLE ERROR [%d]", __FUNCTION__, (int )resp_code);
				APP_TRACE(szDbgMsg);

				switch(resp_code)
				{
					case CURLE_UNSUPPORTED_PROTOCOL: /* 1 */
						/*
						 * The URL you passed to libcurl used a protocol that this libcurl does not support.
						 * The support might be a compile-time option that you didn't use,
						 * it can be a misspelled protocol string or just a protocol libcurl has no code for.
						 */
						//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
						GLOBAL->http.status_code = eHTTP_OK;
						nStatus = EXIT_FAILURE;
						GLOBAL->appErrCode = eERR_UNSUPPORT_PROTOCOL;

						debug_sprintf(szDbgMsg, "%s: ERROR! protocol not supported by library", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						if(iAppLogEnabled)
						{
							strcpy(szAppLogData, "Failed To Post Data to Vantiv, Error[CURLE_UNSUPPORTED_PROTOCOL].");
							strcpy(szAppLogDiag, "Please Check The Vantiv Host URLs, Otherwise Contact Verifone");
							addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, szAppLogDiag);
						}

						break;

					case CURLE_URL_MALFORMAT: /* 3 */
						/*
						 * The URL was not properly formatted.
						 */
						//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
						GLOBAL->http.status_code = eHTTP_OK;
						nStatus = EXIT_FAILURE;

						GLOBAL->appErrCode = eERR_HOST_URL_MALFORMAT;

						debug_sprintf(szDbgMsg, "%s: ERROR! URL not properly formatted", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						if(iAppLogEnabled)
						{
							strcpy(szAppLogData, "Failed To Post Data to Vantiv, Error[CURLE_URL_MALFORMAT].");
							strcpy(szAppLogDiag, "Please Check The Vantiv Host URLs, Otherwise Contact Verifone");
							addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, szAppLogDiag);
						}

						break;

					case CURLE_COULDNT_RESOLVE_PROXY: /* 5 */
					case CURLE_COULDNT_RESOLVE_HOST: /* 6 */
					case CURLE_COULDNT_CONNECT: /* 7 */
						/*
						 * Failed to connect() to host or proxy.
						 */
						GLOBAL->isProcessorAvailable = false;
						GLOBAL->http.status_code = eHTTP_OK;
						nStatus = EXIT_FAILURE;

						GLOBAL->appErrCode = eERR_NOERROR;

						debug_sprintf(szDbgMsg, "%s: ERROR! Failed to connect server", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						if(iAppLogEnabled)
						{
							strcpy(szAppLogData, "Failed To Post Data to Vantiv, Error[CURLE_COULDNT_RESOLVE_HOST]. Error[CURLE_COULDNT_CONNECT].");
							strcpy(szAppLogDiag, "Please Check The Vantiv Host URLs, Otherwise Contact Verifone");
							addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, szAppLogDiag);
						}

						break;

					case CURLE_HTTP_RETURNED_ERROR: /* 22 */
						/*
						 * This is returned if CURLOPT_FAILONERROR is set TRUE
						 * and the HTTP server returns an error code that is >= 400.
						 */
						//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
						GLOBAL->http.status_code = eHTTP_OK;
						nStatus = EXIT_FAILURE;

						GLOBAL->appErrCode = eERR_HOST_HTTP_ERROR;

						debug_sprintf(szDbgMsg, "%s: ERROR! Got HTTP error while posting", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						if(iAppLogEnabled)
						{
							strcpy(szAppLogData, "Failed To Post Data to Vantiv, Error[CURLE_HTTP_RETURNED_ERROR].");
							strcpy(szAppLogDiag, "Please Check The Vantiv Host URLs, Otherwise Contact Verifone");
							addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, szAppLogDiag);
						}

						break;

					case CURLE_WRITE_ERROR: /* 23 */
						/*
						 * An error occurred when writing received data to a local file,
						 * or an error was returned to libcurl from a write callback.
						 */
						//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
						GLOBAL->http.status_code = eHTTP_OK;
						nStatus = EXIT_FAILURE;

						GLOBAL->appErrCode = eERR_HOST_HTTP_ERROR;

						debug_sprintf(szDbgMsg, "%s: ERROR! Failed to save the response data", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						if(iAppLogEnabled)
						{
							strcpy(szAppLogData, "Failed To Post Data to Vantiv, Error[CURLE_WRITE_ERROR].");
							strcpy(szAppLogDiag, "Please Check The Vantiv Host URLs, Otherwise Contact Verifone");
							addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, szAppLogDiag);
						}

						break;

					case CURLE_OUT_OF_MEMORY: /* 27 */
						/*
						 * A memory allocation request failed.
						 * This is serious badness and things are severely screwed up if this ever occurs.
						 */
						//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
						GLOBAL->http.status_code = eHTTP_OK;
						nStatus = EXIT_FAILURE;

						GLOBAL->appErrCode = eERR_HOST_HTTP_ERROR;

						debug_sprintf(szDbgMsg, "%s: ERROR! Facing memory shortage", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						if(iAppLogEnabled)
						{
							strcpy(szAppLogData, "Failed To Post Data to Vantiv, Error[CURLE_OUT_OF_MEMORY].");
							strcpy(szAppLogDiag, "Please Check The Vantiv Host URLs, Otherwise Contact Verifone");
							addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, szAppLogDiag);
						}

						break;

					case CURLE_OPERATION_TIMEDOUT: /* 28 */
						/*
						 * Operation timeout.
						 * The specified time-out period was reached according to the conditions.
						 */
						GLOBAL->http.status_code = eHTTP_GATEWAY_TIMEOUT;
						//GLOBAL->http.status_code = eHTTP_OK;
						//GLOBAL->appErrCode = eERR_HOST_HTTP_ERROR;
						nStatus = EXIT_FAILURE;

						debug_sprintf(szDbgMsg, "%s: ERROR! Timeout happened while receiving", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						if(iAppLogEnabled)
						{
							strcpy(szAppLogData, "Failed To Post Data to Vantiv, Error[CURLE_OPERATION_TIMEDOUT].");
							strcpy(szAppLogDiag, "Please Check The Vantiv Host URLs, Otherwise Contact Verifone");
							addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, szAppLogDiag);
						}

						break;

					case CURLE_SSL_CONNECT_ERROR: /* 35 */
						/*
						 * A problem occurred somewhere in the SSL/TLS handshake.
						 *
						 */
						//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
						GLOBAL->http.status_code = eHTTP_OK;
						GLOBAL->appErrCode = eERR_HOST_HTTP_ERROR;
						nStatus = EXIT_FAILURE;

						debug_sprintf(szDbgMsg, "%s: ERROR! SSL/TLS Handshake FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						if(iAppLogEnabled)
						{
							strcpy(szAppLogData, "Failed To Post Data to Vantiv, Error[CURLE_SSL_CONNECT_ERROR].");
							strcpy(szAppLogDiag, "Please Check The Vantiv Host URLs, Otherwise Contact Verifone");
							addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, szAppLogDiag);
						}

						break;

					case CURLE_PEER_FAILED_VERIFICATION: /* 51 */
						/*
						 * The remote server's SSL certificate or SSH md5 fingerprint was deemed not OK.
						 */
						//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
						GLOBAL->http.status_code = eHTTP_OK;
						GLOBAL->appErrCode = eERR_HOST_HTTP_ERROR;
						nStatus = EXIT_FAILURE;

						debug_sprintf(szDbgMsg, "%s: ERROR! Server's SSL certificate verification FAILED", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						if(iAppLogEnabled)
						{
							strcpy(szAppLogData, "Failed To Post Data to Vantiv, Error[CURLE_PEER_FAILED_VERIFICATION].");
							strcpy(szAppLogDiag, "Please Check The Vantiv Host URLs, Otherwise Contact Verifone");
							addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, szAppLogDiag);
						}

						break;

					case CURLE_SEND_ERROR: /* 55 */
						/*
						 * Failed sending network data.
						 */
						//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
						GLOBAL->http.status_code = eHTTP_OK;
						GLOBAL->appErrCode = eERR_HOST_HTTP_ERROR;
						nStatus = EXIT_FAILURE;

						debug_sprintf(szDbgMsg, "%s: ERROR! Failed to post the request data", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						if(iAppLogEnabled)
						{
							strcpy(szAppLogData, "Failed To Post Data to Vantiv, Error[CURLE_SEND_ERROR].");
							strcpy(szAppLogDiag, "Please Check The Vantiv Host URLs, Otherwise Contact Verifone");
							addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, szAppLogDiag);
						}

						break;

					case CURLE_RECV_ERROR: /* 56 */

						/*
						 * Praveen_P1: 5 Oct 2016
						 * Amdocs Case#
						 */
						GLOBAL->http.status_code = eHTTP_GATEWAY_TIMEOUT;
						nStatus = EXIT_FAILURE;

						debug_sprintf(szDbgMsg, "%s: ERROR! Failed to receive the response, considering it as response timeout", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						if(iAppLogEnabled)
						{
							strcpy(szAppLogData, "Failed To Post Data to Vantiv, Error[CURLE_RECV_ERROR].");
							strcpy(szAppLogDiag, "Please Check The Vantiv Host URLs, Otherwise Contact Verifone");
							addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, szAppLogDiag);
						}
						resp_code = CURLE_OPERATION_TIMEDOUT;
#if 0
						/*
						 * Failure with receiving network data.
						 */
						//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
						GLOBAL->http.status_code = eHTTP_OK;
						GLOBAL->appErrCode = eERR_HOST_HTTP_ERROR;
						nStatus = EXIT_FAILURE;

						debug_sprintf(szDbgMsg, "%s: ERROR! Failed to receive the response", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						if(iAppLogEnabled)
						{
							strcpy(szAppLogData, "Failed To Post Data to Vantiv, Error[CURLE_RECV_ERROR].");
							strcpy(szAppLogDiag, "Please Check The Vantiv Host URLs, Otherwise Contact Verifone");
							addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, szAppLogDiag);
						}
#endif
						break;

					case CURLE_SSL_CACERT: /* 60 */
						/*
						 * Peer certificate cannot be authenticated with known CA certificates.
						 */
						//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
						GLOBAL->http.status_code = eHTTP_OK;
						GLOBAL->appErrCode = eERR_HOST_HTTP_ERROR;
						nStatus = EXIT_FAILURE;

						debug_sprintf(szDbgMsg, "%s: ERROR! Couldnt authenticate peer certificate", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						if(iAppLogEnabled)
						{
							strcpy(szAppLogData, "Failed To Post Data to Vantiv, Error[CURLE_SSL_CACERT].");
							strcpy(szAppLogDiag, "Please Check The Vantiv Host URLs, Otherwise Contact Verifone");
							addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, szAppLogDiag);
						}

						break;

					case CURLE_SSL_CACERT_BADFILE: /* 77 */
						/*
						 * Problem with reading the SSL CA cert (path? access rights?)
						 */
						//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
						GLOBAL->http.status_code = eHTTP_OK;
						GLOBAL->appErrCode = eERR_HOST_HTTP_ERROR;
						nStatus = EXIT_FAILURE;

						debug_sprintf(szDbgMsg, "%s: ERROR! Cant find SSL CA sertificate", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						if(iAppLogEnabled)
						{
							strcpy(szAppLogData, "Failed To Post Data to Vantiv, Error[CURLE_SSL_CACERT_BADFILE].");
							strcpy(szAppLogDiag, "Please Check The Vantiv Host URLs, Otherwise Contact Verifone");
							addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, szAppLogDiag);
						}

						break;

					default:
						//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
						GLOBAL->http.status_code = eHTTP_OK;
						GLOBAL->appErrCode = eERR_INTERNAL_ERROR;
						nStatus = EXIT_FAILURE;

						debug_sprintf(szDbgMsg, "%s: ERROR! Error occured while posting", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						if(iAppLogEnabled)
						{
							strcpy(szAppLogData, "Failed To Post Data to Vantiv.");
							strcpy(szAppLogDiag, "Please Check The Vantiv Host URLs, Otherwise Contact Verifone");
							addAppEventLog(APP_NAME, ENTRYTYPE_FAILURE, ENTRYID_PROCESSED, szAppLogData, szAppLogDiag);
						}

						break;
				}

				debug_sprintf(szDbgMsg, "%s: ERROR! Lib Curl Error [%s]", __FUNCTION__,szCurlErrBuf);
				APP_TRACE(szDbgMsg);
			}
		}

		strcpy(getdata, "");
		strncat(getdata, sRecvData.ptr, sRecvData.nLen);

		/* always cleanup */
		curl_easy_cleanup(curl);

		/* Cleaning up headers */
		curl_slist_free_all(headers);

		if(sRecvData.ptr) {
			free(sRecvData.ptr);
		}

		if ( (resp_code == CURLE_OK) || (resp_code == CURLE_OPERATION_TIMEDOUT) )
		{
			debug_sprintf(szDbgMsg, "%s: Breaking from the loop", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			break;
		}

		if(iNumHostURLs == 2) //Praveen_P1: Will check with next URL if two URLs are present
		{
			/* If the first host is not working then try the next host */
			host ^= 1;
		}
		iCnt++;

	}
	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, nStatus);
	APP_TRACE(szDbgMsg);

	return nStatus;
}

/*
 * ============================================================================
 * Function Name: chkHostConn
 *
 * Description	:
 *
 * Input Params	:
 *
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */

static int chkHostConn(CURL * curlHandle)
{
	int		rv		= EXIT_SUCCESS;

#ifdef DEBUG
	char			szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	while(1)
	{
		curl_easy_setopt(curlHandle, CURLOPT_CONNECT_ONLY, 1L);

		rv = curl_easy_perform(curlHandle);
		if(rv == CURLE_OK)
		{
			debug_sprintf(szDbgMsg, "%s: Perform SUCCESS", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Return Value from PERFORM = [%d]",__FUNCTION__, rv);
			APP_TRACE(szDbgMsg);
			rv = EXIT_FAILURE;
		}

		/* Unsetting the connect only option */
		curl_easy_setopt(curlHandle, CURLOPT_CONNECT_ONLY, 0L);

		break;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);

	return rv;
}

static void get800request(char *basegroup, char *tpsheader, sCONFIG * GCONFIG)
{
	char			stan[6+1]			= {0};
	char			bankid[4+1]			= {0};
	char			tid[3+1]			= {0};
	char			mid[12+1]			= {0};

	debug_sprintf(szDbgMsg, "%s: Entering ", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	strcpy(basegroup, "I2.E3    080001");

	memset(stan, 0x00, sizeof(stan));
	getEnvFile("DHI", "STAN", stan, (sizeof(stan) - 1) );

	if (strlen(stan) <= 0)
	{
		debug_sprintf(szDbgMsg, "%s: ERROR! STAN", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	precedeZero(sizeof(stan), stan);

	debug_sprintf(szDbgMsg, "%s: stan [%s]", __FUNCTION__, stan);
	APP_TRACE(szDbgMsg);

	strcat(basegroup, stan);

	memset(bankid, 0x00, sizeof(bankid));

	strcpy(bankid, "");
	strncat(bankid, GCONFIG->bankID, sizeof(bankid)-1);

	if (strlen(bankid) > 1)
	{
		precedeZero(sizeof(bankid), bankid);
	}

	strcat(basegroup, bankid);

	memset(tid, 0x00, sizeof(tid));

	strcpy(tid, "");
	strncat(tid, GCONFIG->tid, sizeof(tid)-1);

	if (strlen(tid) > 1)
	{
		precedeZero(sizeof(tid), tid);
	}

	strcat(basegroup, tid);

	memset(mid, 0x00, sizeof(mid));

	strcpy(mid, "");
	strncat(mid, GCONFIG->mid, sizeof(mid)-1);

	if (strlen(mid) > 1)
	{
		precedeZero(sizeof(mid), mid);
	}

	strcat(basegroup, mid);

	strcat(basegroup, "00000000");

	strcat(basegroup, "360");

	strcat(basegroup, "PING_TEST_ECHO  ");

	debug_sprintf(szDbgMsg, "%s: basegroup = %s", __FUNCTION__, basegroup);
	APP_TRACE(szDbgMsg);




	char p[4+1] = {0};
	int baselen = strlen(basegroup);

	sprintf(p, "%d", baselen); //length of message starting field 4
	precedeZero(sizeof(p), p);

	strcpy(tpsheader, "REQUEST=");
	strcat(tpsheader, "BT");
	strcat(tpsheader, p);
	strcat(tpsheader, "               ");

	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}
