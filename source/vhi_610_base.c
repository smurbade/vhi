/*
 * vhi_610_base.c
 *
 *  Created on: Nov 5, 2013
 *      Author: Dexter M. Alberto
 */


#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "vhi_request.h"
#include "sca_xml_request.h"
#include "help_function.h"
#include "logger.h"
#include "vhi_610_fields.h"
#include "vhi_610_group.h"
#include "define_data.h"
#include "vhi_610_base.h"
#include "config_data.h"

#ifdef LOGGING_ENABLED
static char    szDbgMsg[512];
#endif

static char *message_type[]=
{
    "0100",     //eAUTHORIZATION_00, //01XX
    "0200",     //eFINANCIAL_TRANS_00, //02XX
    "0220",     //eFINANCIAL_TRANS_0220, //0220
    "0300",     //eFILE_UPDATE_00, //03XX
    "0400",     //eREVERSAL_00, //04XX
    "0500",     //eRECON_CONTROL_00, //05XX
    "0800",     //eNETWORK_MANAGEMENT, //08XX
};

static char *getFieldX(int field_number, int processing_code, sGLOBALS *GLOBAL, sCONFIG *GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo);
static void first_four_fields(char *buffer, enum e00 msg_type, char *bm_type);

// This is for TYPE= 0200, BM= 10, DEBIT SALE
char* compose610DebitSale(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	char date_time[11+1] = "";

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	_sVHI_FIELDS_INFO sinfo;

	memset(vhi610info->dbtsale, 0x00, sizeof(vhi610info->dbtsale));
	memset(&sinfo, 0, sizeof(sinfo));

	strcpy(sinfo.bnpr, getFieldBNPR(vhi_fieldinfo));
	strcpy(sinfo.bnnr, getFieldBNNR(vhi_fieldinfo));
	strcpy(sinfo.bn00, getFieldBN00(eFINANCIAL_TRANS_00, vhi_fieldinfo));
	strcpy(sinfo.bnbmt, "10");
	strcpy(sinfo.bn03, getFieldBN03(eSALE_PROCCODE, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn04, getFieldBN04(GLOBAL, req_xml_info, vhi_fieldinfo));
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn07, getDateTime(eMMDDYYhhmm, date_time));
	strcpy(sinfo.bn11, getFieldBN11(vhi_fieldinfo));
#if 0
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn12, getDateTime(eMMDDYY, date_time));
	memset(date_time, 0x00, sizeof(date_time));
	//strcpy(sinfo.bn13, getDateTime(ehhmmss, date_time));
#endif
	strcpy(sinfo.bn12, getFieldBN12(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn13, getFieldBN13(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn22, getFieldBN22(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn25, getFieldBN25(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn32, getFieldBN32(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn41, getFieldBN41(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn42, getFieldBN42(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn43, getFieldBN43(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn45, getFieldBN45(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn48, getFieldBN48(vhi_fieldinfo));
	strcpy(sinfo.bn52, getFieldBN52(req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn55, getFieldBN55(req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn60, getFieldBN60(req_xml_info, vhi_fieldinfo));
	fillZero(sizeof(sinfo.bn70), sinfo.bn70);
	strcpy(sinfo.bn107, getFieldBN107(GCONFIG, req_xml_info, vhi_fieldinfo, GLOBAL));
	strcpy(sinfo.bn109, getFieldBN109(req_xml_info, vhi_fieldinfo));
	//fillSpace(sizeof(sinfo.bn109), sinfo.bn109);
	strcpy(sinfo.bn115, getFieldBN115(req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn117, getFieldBN117(req_xml_info, vhi_fieldinfo));

	/* add all in buffer */
	strcpy(vhi610info->dbtsale, sinfo.bnpr);
	strcat(vhi610info->dbtsale, sinfo.bnnr);
	strcat(vhi610info->dbtsale, sinfo.bn00);
	strcat(vhi610info->dbtsale, sinfo.bnbmt);
	strcat(vhi610info->dbtsale, sinfo.bn03);
	strcat(vhi610info->dbtsale, sinfo.bn04);
	strcat(vhi610info->dbtsale, sinfo.bn07);
	strcat(vhi610info->dbtsale, sinfo.bn11);
	strcat(vhi610info->dbtsale, sinfo.bn12);
	strcat(vhi610info->dbtsale, sinfo.bn13);
	strcat(vhi610info->dbtsale, sinfo.bn22);
	strcat(vhi610info->dbtsale, sinfo.bn25);
	strcat(vhi610info->dbtsale, sinfo.bn32);
	strcat(vhi610info->dbtsale, sinfo.bn41);
	strcat(vhi610info->dbtsale, sinfo.bn42);
	strcat(vhi610info->dbtsale, sinfo.bn43);
	strcat(vhi610info->dbtsale, sinfo.bn45);
	strcat(vhi610info->dbtsale, sinfo.bn48);
	strcat(vhi610info->dbtsale, sinfo.bn52);
	strcat(vhi610info->dbtsale, sinfo.bn55);
	strcat(vhi610info->dbtsale, sinfo.bn60);
	strcat(vhi610info->dbtsale, sinfo.bn70);
	strcat(vhi610info->dbtsale, sinfo.bn107);
	strcat(vhi610info->dbtsale, sinfo.bn115);
	strcat(vhi610info->dbtsale, sinfo.bn117);

	debug_sprintf(szDbgMsg, "%s: VHI REQUEST: Debit Card Sale (DUKPT Key)", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(strlen(vhi610info->dbtsale) != sizeof(vhi610info->dbtsale)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 610 base length not exact", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi610info->dbtsale;
}

// This is for TYPE= 0200, BM= 10, DEBIT RETURN
char* compose610DebitReturn(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	char date_time[11+1] = "";
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	_sVHI_FIELDS_INFO sinfo;

	memset(vhi610info->dbtrefnd, 0x00, sizeof(vhi610info->dbtrefnd));
	memset(&sinfo, 0, sizeof(sinfo));

	strcpy(sinfo.bnpr, getFieldBNPR(vhi_fieldinfo));
	strcpy(sinfo.bnnr, getFieldBNNR(vhi_fieldinfo));
	strcpy(sinfo.bn00, getFieldBN00(eFINANCIAL_TRANS_00, vhi_fieldinfo));
	strcpy(sinfo.bnbmt, "10");
	strcpy(sinfo.bn03, getFieldBN03(eREFUND_PROCCODE, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn04, getFieldBN04(GLOBAL, req_xml_info, vhi_fieldinfo));
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn07, getDateTime(eMMDDYYhhmm, date_time));
	strcpy(sinfo.bn11, getFieldBN11(vhi_fieldinfo));
#if 0
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn12, getDateTime(eMMDDYY, date_time));
	memset(date_time, 0x00, sizeof(date_time));
#endif
	strcpy(sinfo.bn12, getFieldBN12(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn13, getFieldBN13(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn22, getFieldBN22(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn25, getFieldBN25(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn32, getFieldBN32(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn41, getFieldBN41(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn42, getFieldBN42(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn43, getFieldBN43(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn45, getFieldBN45(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn48, getFieldBN48(vhi_fieldinfo));
	strcpy(sinfo.bn52, getFieldBN52(req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn55, getFieldBN55(req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn60, getFieldBN60(req_xml_info, vhi_fieldinfo));
	fillZero(sizeof(sinfo.bn70), sinfo.bn70);
	strcpy(sinfo.bn107, getFieldBN107(GCONFIG, req_xml_info, vhi_fieldinfo, GLOBAL));
	strcpy(sinfo.bn115, getFieldBN115(req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn117, getFieldBN117(req_xml_info, vhi_fieldinfo));

	/* add all in buffer */
	strcpy(vhi610info->dbtrefnd, sinfo.bnpr);
	strcat(vhi610info->dbtrefnd, sinfo.bnnr);
	strcat(vhi610info->dbtrefnd, sinfo.bn00);
	strcat(vhi610info->dbtrefnd, sinfo.bnbmt);
	strcat(vhi610info->dbtrefnd, sinfo.bn03);
	strcat(vhi610info->dbtrefnd, sinfo.bn04);
	strcat(vhi610info->dbtrefnd, sinfo.bn07);
	strcat(vhi610info->dbtrefnd, sinfo.bn11);
	strcat(vhi610info->dbtrefnd, sinfo.bn12);
	strcat(vhi610info->dbtrefnd, sinfo.bn13);
	strcat(vhi610info->dbtrefnd, sinfo.bn22);
	strcat(vhi610info->dbtrefnd, sinfo.bn25);
	strcat(vhi610info->dbtrefnd, sinfo.bn32);
	strcat(vhi610info->dbtrefnd, sinfo.bn41);
	strcat(vhi610info->dbtrefnd, sinfo.bn42);
	strcat(vhi610info->dbtrefnd, sinfo.bn43);
	strcat(vhi610info->dbtrefnd, sinfo.bn45);
	strcat(vhi610info->dbtrefnd, sinfo.bn48);
	strcat(vhi610info->dbtrefnd, sinfo.bn52);
	strcat(vhi610info->dbtrefnd, sinfo.bn55);
	strcat(vhi610info->dbtrefnd, sinfo.bn60);
	strcat(vhi610info->dbtrefnd, sinfo.bn70);
	strcat(vhi610info->dbtrefnd, sinfo.bn107);
	strcat(vhi610info->dbtrefnd, sinfo.bn115);
	strcat(vhi610info->dbtrefnd, sinfo.bn117);

	debug_sprintf(szDbgMsg, "%s: VHI REQUEST: Debit Card Return (DUKPT Key)", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(strlen(vhi610info->dbtrefnd) != sizeof(vhi610info->dbtrefnd)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 610 base length not exact", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi610info->dbtrefnd;
}

// This is for TYPE= 0400, BM= 04, DEBIT VOID --- EMV
char* compose610DebitVoid(char **transtype, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
    // Indicate which fields and groups required
    int     fields[]= {2,7,11,12,13,32,41,42,43,48,52,55,70,90,107,115,117, -1}; // 0 and "bit map" are done before
    int     groups[]= {eGROUP004, eGROUP009, eGROUP014, eGROUP023, eGROUP026, eGROUP027, eGROUP028, eGROUP034, eGROUP035, eGROUP038, -1}; //Praveen_P1: As per Vantiv analyst G001 should not be sent for Voids
    
    int     index;
    char    *buffer;
    
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    memset(vhi610info->crdsale, 0x00, sizeof(vhi610info->crdsale));
    buffer= vhi610info->crdsale;
    first_four_fields(buffer, eREVERSAL_00, "04");       // set correct message identifier and "bit map"
    
    index= 0;
    while (fields[index] >= 0)
    {
        // Include message identifier and processing code
        strcat(buffer, getFieldX(fields[index], -1, GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
        index++;
    }
    
    strcat(buffer, RS);
    *transtype = (char*)calloc(strlen(buffer)+ 1, sizeof(char));   // extra 1 for '\0'
    strcpy(*transtype, buffer);
    
    index= 0;
    while (groups[index] >= 0)
    {
        append610Group(groups[index], transtype, GLOBAL, GCONFIG, req_xml_info);
        index++;
    }
    
    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);

    return *transtype;
}


// This is for TYPE= 0200, BM= 22, SALE --- EMV
// transtype is allocate in this routine
char* compose610CreditSale(char **transtype, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
    
#ifdef COLIN_TEST
    // changed his network routing from "2XN000" to "E3    "
    char temp[]= "I2.E3    020022004000000001000041515141900000304151514193502000000000001340001000012775031003                                       4502244713161718=1607101000000000000100000001000000010000000000000005000005              0000000000000000000000000.G00100000000003000005.G009000NNYYNNNNN0NYNNNN";
    *transtype = (char*)calloc(strlen(temp)+ 1, sizeof(char));   // extra 1 for '\0'
    strcpy(*transtype, temp);
    return *transtype;
#endif
 
    // Indicate which fields and groups required
    int     fields[]= {3,4,7,11,12,13,22,25,32,41,42,43,45,48,55,60,67,70,107,109,110,115, -1}; // 0 and "bit map" are done before
    int     groups[]= {eGROUP001, eGROUP004, eGROUP009, eGROUP015, eGROUP023, eGROUP026, eGROUP028, eGROUP034, eGROUP035, eGROUP036, eGROUP038, eGROUP043, eGROUP045, -1};
    
    int     index;
    char    *buffer;
    
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    memset(vhi610info->crdsale, 0x00, sizeof(vhi610info->crdsale));
    buffer= vhi610info->crdsale;
    first_four_fields(buffer, eFINANCIAL_TRANS_00, "22");       // set correct message identifier and "bit map"

    index= 0;
    while (fields[index] >= 0)
    {
        // Include message identifier and processing code
        strcat(buffer, getFieldX(fields[index], eSALE_PROCCODE, GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
        index++;
    }
    
    strcat(buffer, RS);
    *transtype = (char*)calloc(strlen(buffer)+ 1, sizeof(char));   // extra 1 for '\0'
    strcpy(*transtype, buffer);
    
    index= 0;
    while (groups[index] >= 0)
    {
        append610Group(groups[index], transtype, GLOBAL, GCONFIG, req_xml_info);
        index++;
    }
    
    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    return *transtype;
}

// first_four_fields
//
//  Adds first four fields
//  processor routing ans3, network routing an6, message identifier n4 & "bit map" n2
//
static void first_four_fields(char *buffer, enum e00 msg_type, char *bm_type)
{
    strcat(buffer, "I2.");
    strcat(buffer, "E3    ");
    strcat(buffer, message_type[msg_type]);
    strcat(buffer, bm_type);
}

// getFieldX
//
//  Generic field generator
//  Uses field_number to gebnerate correct field
//  Other parameters can make variations on the field
//  processing_code only required if field 3 used
//
static char *getFieldX(int field_number, int processing_code, sGLOBALS *GLOBAL, sCONFIG *GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
    static char temp[100];
    
    switch (field_number)
    {
        case 2:
            return getFieldBN02(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo);
            
        case 3:
            if (processing_code < 0)
            {
                debug_sprintf(szDbgMsg, "*** NO processing code ***");
                APP_TRACE(szDbgMsg);
                break;
            }
            return getFieldBN03(processing_code, req_xml_info, vhi_fieldinfo);
            
        case 4:
            return getFieldBN04(GLOBAL, req_xml_info, vhi_fieldinfo);
            
        case 7:
            return getDateTime(eMMDDYYhhmm, temp);
            
        case 11:
            return getFieldBN11(vhi_fieldinfo);
            
        case 12:
            //return getDateTime(eMMDDYY, temp);
        	return getFieldBN12(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo);
            
        case 13:
            //return getDateTime(ehhmmss, temp);
        	return getFieldBN13(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo);
            
        case 22:
            return getFieldBN22(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo);
            
        case 25:
            return getFieldBN25(GCONFIG, req_xml_info, vhi_fieldinfo);
            
        case 32:
            return getFieldBN32(GCONFIG, vhi_fieldinfo);
            
        case 41:
            return getFieldBN41(GCONFIG, vhi_fieldinfo);
            
        case 42:
            return getFieldBN42(GCONFIG, vhi_fieldinfo);
            
        case 43:
            return getFieldBN43(GCONFIG, req_xml_info, vhi_fieldinfo);
            
        case 45:
            return getFieldBN45(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo);
            
        case 48:
            return getFieldBN48(vhi_fieldinfo);
            
        case 52:
            return getFieldBN52(req_xml_info, vhi_fieldinfo);
            
        case 55:
            return getFieldBN55(req_xml_info, vhi_fieldinfo);
            
        case 60:
            return getFieldBN60(req_xml_info, vhi_fieldinfo);
            
        case 65:
            return getFieldBN65(GLOBAL, req_xml_info, vhi_fieldinfo);
            
        case 67:
            strcpy(temp, "00");
            return temp;
            
        case 70:
            // Required for EMV reversals
            return getFieldBN70(req_xml_info, vhi_fieldinfo);
            
        case 74:
            strcpy(temp, "000000");
            return temp;
            
        case 76:
            strcpy(temp, "000000");
            return temp;
            
        case 86:
            strcpy(temp, "000000000000");
            return temp;
            
        case 88:
            strcpy(temp, "000000000000");
            return temp;
            
        case 90:
            return getFieldBN90(vhi_fieldinfo);
            
        case 105:
            strcpy(temp, "  ");                 // for 105.1 - 2 spaces
            strcat(temp, " ");                  // for 105.2 - 1 space
            strcat(temp, "               ");    // for 105.3 - 15 spaces
            strcat(temp, "    ");               // for 105.4 - 4 spaces
            return temp;
            
        case 106:
            return getFieldBN106(req_xml_info, vhi_fieldinfo);
            
        case 107:
            return getFieldBN107(GCONFIG, req_xml_info, vhi_fieldinfo, GLOBAL);
            
        case 109:
            strcpy(temp, getFieldBN109(req_xml_info, vhi_fieldinfo));
            return temp;
            
        case 110:
            return getFieldBN110(req_xml_info, vhi_fieldinfo);
            
        case 111:
            strcpy(temp, "               ");
            return temp;
            
        case 112:
            return getFieldBN112(req_xml_info, vhi_fieldinfo);
            
        case 115:
            return getFieldBN115(req_xml_info, vhi_fieldinfo);
            
        case 117:
            return getFieldBN117(req_xml_info, vhi_fieldinfo);
            
        case 129:
            strcpy(temp, "0000");
            return temp;
            
        case 139:
            strcpy(temp, "99999999");
            return temp;
            
        case 140:
            strcpy(temp, "999999");
            return temp;
            
        default:
            debug_sprintf(szDbgMsg, "*** INCORRECT field %d ***", field_number);
            APP_TRACE(szDbgMsg);
            break;
    }
    return "";
}


// This is for TYPE= 0200, BM= 25, AVS SALE
char* compose610CreditSaleAVS(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	char date_time[11+1] = "";


	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	_sVHI_FIELDS_INFO sinfo;

	char* pBILLPAY =  getXmlTagValue( eBILLPAY, req_xml_info );

	if (pBILLPAY == NULL) {
		pBILLPAY = "FALSE";
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: BILLPAY flag is present in the request", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	memset(vhi610info->crdsaleavs, 0x00, sizeof(vhi610info->crdsaleavs));
	memset(&sinfo, 0, sizeof(sinfo));

	strcpy(sinfo.bnpr, getFieldBNPR(vhi_fieldinfo));
	strcpy(sinfo.bnnr, getFieldBNNR(vhi_fieldinfo));
	strcpy(sinfo.bn00, getFieldBN00(eFINANCIAL_TRANS_00, vhi_fieldinfo));
	strcpy(sinfo.bnbmt, "25");

	if( (strncmp( pBILLPAY, "TRUE", strlen("TRUE") ) == COMPARE_OK) )
	{
		debug_sprintf(szDbgMsg, "%s: BILLPAY flag is present, its CREDIT AVS SALE transaction so need to change processing code to 50", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		strcpy(sinfo.bn03, "504000");
	}
	else
	{
		strcpy(sinfo.bn03, "524000");
	}


	strcpy(sinfo.bn04 ,getFieldBN04(GLOBAL, req_xml_info, vhi_fieldinfo));

	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn07, getDateTime(eMMDDYYhhmm, date_time));
	strcpy(sinfo.bn11, getFieldBN11(vhi_fieldinfo));
#if 0
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn12, getDateTime(eMMDDYY, date_time));
	memset(date_time, 0x00, sizeof(date_time));
#endif
	strcpy(sinfo.bn12, getFieldBN12(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn13, getFieldBN13(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn22, getFieldBN22(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn25, getFieldBN25(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn32, getFieldBN32(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn41, getFieldBN41(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn42, getFieldBN42(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn43, getFieldBN43(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn45, getFieldBN45(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn48, getFieldBN48(vhi_fieldinfo));
	strcpy(sinfo.bn55, getFieldBN55(req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn60, getFieldBN60(req_xml_info, vhi_fieldinfo));
	fillZero(sizeof(sinfo.bn67), sinfo.bn67);
	fillZero(sizeof(sinfo.bn70), sinfo.bn70);
	strcpy(sinfo.bn106, getFieldBN106(req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn107, getFieldBN107(GCONFIG, req_xml_info, vhi_fieldinfo, GLOBAL));
	strcpy(sinfo.bn109, getFieldBN109(req_xml_info, vhi_fieldinfo));
	//fillSpace(sizeof(sinfo.bn109), sinfo.bn109);
	strcpy(sinfo.bn110, getFieldBN110(req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn115, getFieldBN115(req_xml_info, vhi_fieldinfo));

	/* add all in buffer */
	strcpy(vhi610info->crdsaleavs, sinfo.bnpr);
	strcat(vhi610info->crdsaleavs, sinfo.bnnr);
	strcat(vhi610info->crdsaleavs, sinfo.bn00);
	strcat(vhi610info->crdsaleavs, sinfo.bnbmt);
	strcat(vhi610info->crdsaleavs, sinfo.bn03);
	strcat(vhi610info->crdsaleavs, sinfo.bn04);
	strcat(vhi610info->crdsaleavs, sinfo.bn07);
	strcat(vhi610info->crdsaleavs, sinfo.bn11);
	strcat(vhi610info->crdsaleavs, sinfo.bn12);
	strcat(vhi610info->crdsaleavs, sinfo.bn13);
	strcat(vhi610info->crdsaleavs, sinfo.bn22);
	strcat(vhi610info->crdsaleavs, sinfo.bn25);
	strcat(vhi610info->crdsaleavs, sinfo.bn32);
	strcat(vhi610info->crdsaleavs, sinfo.bn41);
	strcat(vhi610info->crdsaleavs, sinfo.bn42);
	strcat(vhi610info->crdsaleavs, sinfo.bn43);
	strcat(vhi610info->crdsaleavs, sinfo.bn45);
	strcat(vhi610info->crdsaleavs, sinfo.bn48);
	strcat(vhi610info->crdsaleavs, sinfo.bn55);
	strcat(vhi610info->crdsaleavs, sinfo.bn60);
	strcat(vhi610info->crdsaleavs, sinfo.bn67);
	strcat(vhi610info->crdsaleavs, sinfo.bn70);
	strcat(vhi610info->crdsaleavs, sinfo.bn106);
	strcat(vhi610info->crdsaleavs, sinfo.bn107);
	strcat(vhi610info->crdsaleavs, sinfo.bn109);
	strcat(vhi610info->crdsaleavs, sinfo.bn110);
	strcat(vhi610info->crdsaleavs, sinfo.bn115);

	debug_sprintf(szDbgMsg, "%s: VHI REQUEST: Credit Card Sale w/ AVS", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(strlen(vhi610info->crdsaleavs) != sizeof(vhi610info->crdsaleavs)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 610 base length not exact", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi610info->crdsaleavs;
}

// This is for TYPE= 0200, BM= 22, REFUND --- EMV
// transtype is allocate in this routine
char* compose610CreditReturn(char **transtype, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
    // Indicate which fields and groups required
    int     fields[]= {3,4,7,11,12,13,22,25,32,41,42,43,45,48,55,60,67,70,107,109,110,115, -1}; // 0 and "bit map" are done before
    int     groups[]= {eGROUP001, eGROUP004, eGROUP009, eGROUP015, eGROUP023, eGROUP026, eGROUP028, eGROUP034, eGROUP035, eGROUP036, eGROUP038, eGROUP043, eGROUP045, -1};
    
    int     index;
    char    *buffer;
    
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    memset(vhi610info->crdsale, 0x00, sizeof(vhi610info->crdsale));
    buffer= vhi610info->crdsale;
    first_four_fields(buffer, eFINANCIAL_TRANS_00, "22");       // set correct message identifier and "bit map"
    
    index= 0;
    while (fields[index] >= 0)
    {
        // Include message identifier and processing code
        strcat(buffer, getFieldX(fields[index], eREFUND_PROCCODE, GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
        index++;
    }
    
    strcat(buffer, RS);
    *transtype = (char*)calloc(strlen(buffer)+ 1, sizeof(char));   // extra 1 for '\0'
    strcpy(*transtype, buffer);
    
    index= 0;
    while (groups[index] >= 0)
    {
        append610Group(groups[index], transtype, GLOBAL, GCONFIG, req_xml_info);
        index++;
    }
    
    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    return *transtype;
    
}


// This is for TYPE= 0400, BM= 01, VOID --- EMV
// transtype is allocate in this routine
char* compose610CreditVoid(char **transtype, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
    // Indicate which fields and groups required
    int     fields[]= {2,7,11,12,13,32,41,42,43,48,55,70,90,107,115, -1}; // 0 and "bit map" are done before
    int     groups[]= {eGROUP004, eGROUP009, eGROUP014, eGROUP023, eGROUP026, eGROUP027, eGROUP028, eGROUP034, eGROUP035, eGROUP036, eGROUP038, -1}; //Praveen_P1: As per Vantiv analyst G001 should not be sent for Voids
    
    int     index;
    char    *buffer;
    
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    memset(vhi610info->crdsale, 0x00, sizeof(vhi610info->crdsale));
    buffer= vhi610info->crdsale;
    first_four_fields(buffer, eREVERSAL_00, "01");       // set correct message identifier and "bit map"
    
    index= 0;
    while (fields[index] >= 0)
    {
        // Include message identifier and processing code
        strcat(buffer, getFieldX(fields[index], -1, GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
        index++;
    }
    
    strcat(buffer, RS);
    *transtype = (char*)calloc(strlen(buffer)+ 1, sizeof(char));   // extra 1 for '\0'
    strcpy(*transtype, buffer);
    
    index= 0;
    while (groups[index] >= 0)
    {
        append610Group(groups[index], transtype, GLOBAL, GCONFIG, req_xml_info);
        index++;
    }
    
    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    return *transtype;
    
}


// This is for TYPE= 0200, BM= 60, SALE
char* compose610GiftSale(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	char date_time[11+1] = "";

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	_sVHI_FIELDS_INFO sinfo;

	memset(vhi610info->gftsale, 0x00, sizeof(vhi610info->gftsale));
	memset(&sinfo, 0, sizeof(sinfo));

	strcpy(sinfo.bnpr, getFieldBNPR(vhi_fieldinfo));
	strcpy(sinfo.bnnr, getFieldBNNR(vhi_fieldinfo));
	strcpy(sinfo.bn00, getFieldBN00(eFINANCIAL_TRANS_00, vhi_fieldinfo));
	strcpy(sinfo.bnbmt, "60");
	strcpy(sinfo.bn03, getFieldBN03(eSALE_PROCCODE, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn04, getFieldBN04(GLOBAL, req_xml_info, vhi_fieldinfo));
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn07, getDateTime(eMMDDYYhhmm, date_time));
	strcpy(sinfo.bn11, getFieldBN11(vhi_fieldinfo));
#if 0
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn12, getDateTime(eMMDDYY, date_time));
	memset(date_time, 0x00, sizeof(date_time));
	//strcpy(sinfo.bn13, getDateTime(ehhmmss, date_time));
#endif
	strcpy(sinfo.bn12, getFieldBN12(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn13, getFieldBN13(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn22, getFieldBN22(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn25, getFieldBN25(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn32, getFieldBN32(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn41, getFieldBN41(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn42, getFieldBN42(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn43, getFieldBN43(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn45, getFieldBN45(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn48, getFieldBN48(vhi_fieldinfo));
	strcpy(sinfo.bn55, getFieldBN55(req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn107, getFieldBN107(GCONFIG, req_xml_info, vhi_fieldinfo, GLOBAL));
	strcpy(sinfo.bn115, getFieldBN115(req_xml_info, vhi_fieldinfo));

	/* add all in vhi610info->gftsale */
	strcpy(vhi610info->gftsale, sinfo.bnpr);
	strcat(vhi610info->gftsale, sinfo.bnnr);
	strcat(vhi610info->gftsale, sinfo.bn00);
	strcat(vhi610info->gftsale, sinfo.bnbmt);
	strcat(vhi610info->gftsale, sinfo.bn03);
	strcat(vhi610info->gftsale, sinfo.bn04);
	strcat(vhi610info->gftsale, sinfo.bn07);
	strcat(vhi610info->gftsale, sinfo.bn11);
	strcat(vhi610info->gftsale, sinfo.bn12);
	strcat(vhi610info->gftsale, sinfo.bn13);
	strcat(vhi610info->gftsale, sinfo.bn22);
	strcat(vhi610info->gftsale, sinfo.bn25);
	strcat(vhi610info->gftsale, sinfo.bn32);
	strcat(vhi610info->gftsale, sinfo.bn41);
	strcat(vhi610info->gftsale, sinfo.bn42);
	strcat(vhi610info->gftsale, sinfo.bn43);
	strcat(vhi610info->gftsale, sinfo.bn45);
	strcat(vhi610info->gftsale, sinfo.bn48);
	strcat(vhi610info->gftsale, sinfo.bn55);
	strcat(vhi610info->gftsale, sinfo.bn107);
	strcat(vhi610info->gftsale, sinfo.bn115);

	debug_sprintf(szDbgMsg, "%s: VHI REQUEST: Gift Card Purchase", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(strlen(vhi610info->gftsale) != sizeof(vhi610info->gftsale)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 610 base length not exact", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi610info->gftsale;
}

// This is for TYPE= 0200, BM= 60, REFUND
char* compose610GiftReturn(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	char date_time[11+1] = "";

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	_sVHI_FIELDS_INFO sinfo;

	memset(vhi610info->gftrefnd, 0x00, sizeof(vhi610info->gftrefnd));
	memset(&sinfo, 0, sizeof(sinfo));
	memset(date_time, 0x00, sizeof(date_time));

	strcpy(sinfo.bnpr, getFieldBNPR(vhi_fieldinfo));
	strcpy(sinfo.bnnr, getFieldBNNR(vhi_fieldinfo));
	strcpy(sinfo.bn00, getFieldBN00(eFINANCIAL_TRANS_00, vhi_fieldinfo));
	strcpy(sinfo.bnbmt, "60");
	strcpy(sinfo.bn03, getFieldBN03(eREFUND_PROCCODE, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn04, getFieldBN04(GLOBAL, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn07, getDateTime(eMMDDYYhhmm, date_time));
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn11, getFieldBN11(vhi_fieldinfo));
#if 0
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn12, getDateTime(eMMDDYY, date_time));
	memset(date_time, 0x00, sizeof(date_time));
#endif
	strcpy(sinfo.bn12, getFieldBN12(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn13, getFieldBN13(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn22, getFieldBN22(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn25, getFieldBN25(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn32, getFieldBN32(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn41, getFieldBN41(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn42, getFieldBN42(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn43, getFieldBN43(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn45, getFieldBN45(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn48, getFieldBN48(vhi_fieldinfo));
	strcpy(sinfo.bn55, getFieldBN55(req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn107, getFieldBN107(GCONFIG, req_xml_info, vhi_fieldinfo, GLOBAL));
	strcpy(sinfo.bn115, getFieldBN115(req_xml_info, vhi_fieldinfo));

	/* add all in vhi610info->gftrefnd */
	strcpy(vhi610info->gftrefnd, sinfo.bnpr);
	strcat(vhi610info->gftrefnd, sinfo.bnnr);
	strcat(vhi610info->gftrefnd, sinfo.bn00);
	strcat(vhi610info->gftrefnd, sinfo.bnbmt);
	strcat(vhi610info->gftrefnd, sinfo.bn03);
	strcat(vhi610info->gftrefnd, sinfo.bn04);
	strcat(vhi610info->gftrefnd, sinfo.bn07);
	strcat(vhi610info->gftrefnd, sinfo.bn11);
	strcat(vhi610info->gftrefnd, sinfo.bn12);
	strcat(vhi610info->gftrefnd, sinfo.bn13);
	strcat(vhi610info->gftrefnd, sinfo.bn22);
	strcat(vhi610info->gftrefnd, sinfo.bn25);
	strcat(vhi610info->gftrefnd, sinfo.bn32);
	strcat(vhi610info->gftrefnd, sinfo.bn41);
	strcat(vhi610info->gftrefnd, sinfo.bn42);
	strcat(vhi610info->gftrefnd, sinfo.bn43);
	strcat(vhi610info->gftrefnd, sinfo.bn45);
	strcat(vhi610info->gftrefnd, sinfo.bn48);
	strcat(vhi610info->gftrefnd, sinfo.bn55);
	strcat(vhi610info->gftrefnd, sinfo.bn107);
	strcat(vhi610info->gftrefnd, sinfo.bn115);

	debug_sprintf(szDbgMsg, "%s: VHI REQUEST: Gift Card Refund", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(strlen(vhi610info->gftrefnd) != sizeof(vhi610info->gftrefnd)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 610 base length not exact", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi610info->gftrefnd;
}

// This is for TYPE= 0400, BM= 60, VOID
char* compose610GiftVoid(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	char date_time[11+1] = "";

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	_sVHI_FIELDS_INFO sinfo;

	memset(vhi610info->gftvoid, 0x00, sizeof(vhi610info->gftvoid));
	memset(&sinfo, 0, sizeof(sinfo));

	strcpy(sinfo.bnpr, getFieldBNPR(vhi_fieldinfo));
	strcpy(sinfo.bnnr, getFieldBNNR(vhi_fieldinfo));
	strcpy(sinfo.bn00, getFieldBN00(eREVERSAL_00, vhi_fieldinfo));
	strcpy(sinfo.bnbmt, "60"); //what is 60/80 bit map type?
	strcpy(sinfo.bn02, getFieldBN02(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn07, getDateTime(eMMDDYYhhmm, date_time));
	strcpy(sinfo.bn11, getFieldBN11(vhi_fieldinfo));
#if 0
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn12, getDateTime(eMMDDYY, date_time));
	memset(date_time, 0x00, sizeof(date_time));
#endif
	strcpy(sinfo.bn12, getFieldBN12(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn13, getFieldBN13(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn32, getFieldBN32(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn41, getFieldBN41(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn42, getFieldBN42(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn43, getFieldBN43(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn48, getFieldBN48(vhi_fieldinfo));
	strcpy(sinfo.bn55, getFieldBN55(req_xml_info, vhi_fieldinfo));
	fillZero(sizeof(sinfo.bn70), sinfo.bn70);
	strcpy(sinfo.bn90, getFieldBN90(vhi_fieldinfo));
	strcpy(sinfo.bn107, getFieldBN107(GCONFIG, req_xml_info, vhi_fieldinfo, GLOBAL));
	strcpy(sinfo.bn115, getFieldBN115(req_xml_info, vhi_fieldinfo));

	/* add all in vhi610info->gftvoid */
	strcpy(vhi610info->gftvoid, sinfo.bnpr);
	strcat(vhi610info->gftvoid, sinfo.bnnr);
	strcat(vhi610info->gftvoid, sinfo.bn00);
	strcat(vhi610info->gftvoid, sinfo.bnbmt);
	strcat(vhi610info->gftvoid, sinfo.bn02);
	strcat(vhi610info->gftvoid, sinfo.bn07);
	strcat(vhi610info->gftvoid, sinfo.bn11);
	strcat(vhi610info->gftvoid, sinfo.bn12);
	strcat(vhi610info->gftvoid, sinfo.bn13);
	strcat(vhi610info->gftvoid, sinfo.bn32);
	strcat(vhi610info->gftvoid, sinfo.bn41);
	strcat(vhi610info->gftvoid, sinfo.bn42);
	strcat(vhi610info->gftvoid, sinfo.bn43);
	strcat(vhi610info->gftvoid, sinfo.bn48);
	strcat(vhi610info->gftvoid, sinfo.bn55);
	strcat(vhi610info->gftvoid, sinfo.bn70);
	strcat(vhi610info->gftvoid, sinfo.bn90);
	strcat(vhi610info->gftvoid, sinfo.bn107);
	strcat(vhi610info->gftvoid, sinfo.bn115);

	debug_sprintf(szDbgMsg, "%s: VHI REQUEST: Gift Card Reversal (Void)", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(strlen(vhi610info->gftvoid) != sizeof(vhi610info->gftvoid)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 610 base length not exact", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi610info->gftvoid;
}

// This is for TYPE= 0200, BM= 60, GIFT ACTIVATION
char* compose610GiftActivation(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	char date_time[11+1] = "";

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	_sVHI_FIELDS_INFO sinfo;

	memset(vhi610info->gftactv, 0x00, sizeof(vhi610info->gftactv));
	memset(&sinfo, 0, sizeof(sinfo));

	strcpy(sinfo.bnpr, getFieldBNPR(vhi_fieldinfo));
	strcpy(sinfo.bnnr, getFieldBNNR(vhi_fieldinfo));
	strcpy(sinfo.bn00, getFieldBN00(eFINANCIAL_TRANS_00, vhi_fieldinfo));
	strcpy(sinfo.bnbmt, "60");
	strcpy(sinfo.bn03, getFieldBN03(eGIFT_ACTIVATION, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn04, getFieldBN04(GLOBAL, req_xml_info, vhi_fieldinfo));
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn07, getDateTime(eMMDDYYhhmm, date_time));
	strcpy(sinfo.bn11, getFieldBN11(vhi_fieldinfo));
#if 0
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn12, getDateTime(eMMDDYY, date_time));
	memset(date_time, 0x00, sizeof(date_time));
#endif
	strcpy(sinfo.bn12, getFieldBN12(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn13, getFieldBN13(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn22, getFieldBN22(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn25, getFieldBN25(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn32, getFieldBN32(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn41, getFieldBN41(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn42, getFieldBN42(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn43, getFieldBN43(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn45, getFieldBN45(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn48, getFieldBN48(vhi_fieldinfo));
	strcpy(sinfo.bn55, getFieldBN55(req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn107, getFieldBN107(GCONFIG, req_xml_info, vhi_fieldinfo, GLOBAL));
	strcpy(sinfo.bn115, getFieldBN115(req_xml_info, vhi_fieldinfo));

	/* add all in vhi610info->gftactv */
	strcpy(vhi610info->gftactv, sinfo.bnpr);
	strcat(vhi610info->gftactv, sinfo.bnnr);
	strcat(vhi610info->gftactv, sinfo.bn00);
	strcat(vhi610info->gftactv, sinfo.bnbmt);
	strcat(vhi610info->gftactv, sinfo.bn03);
	strcat(vhi610info->gftactv, sinfo.bn04);
	strcat(vhi610info->gftactv, sinfo.bn07);
	strcat(vhi610info->gftactv, sinfo.bn11);
	strcat(vhi610info->gftactv, sinfo.bn12);
	strcat(vhi610info->gftactv, sinfo.bn13);
	strcat(vhi610info->gftactv, sinfo.bn22);
	strcat(vhi610info->gftactv, sinfo.bn25);
	strcat(vhi610info->gftactv, sinfo.bn32);
	strcat(vhi610info->gftactv, sinfo.bn41);
	strcat(vhi610info->gftactv, sinfo.bn42);
	strcat(vhi610info->gftactv, sinfo.bn43);
	strcat(vhi610info->gftactv, sinfo.bn45);
	strcat(vhi610info->gftactv, sinfo.bn48);
	strcat(vhi610info->gftactv, sinfo.bn55);
	strcat(vhi610info->gftactv, sinfo.bn107);
	strcat(vhi610info->gftactv, sinfo.bn115);

	debug_sprintf(szDbgMsg, "%s: VHI REQUEST: Gift Card Activation", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(strlen(vhi610info->gftactv) != sizeof(vhi610info->gftactv)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 610 base length not exact", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi610info->gftactv;
}

// This is for TYPE= 0200, BM= 60, GIFT RELOAD
char* compose610GiftReload(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	char date_time[11+1] = "";

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	_sVHI_FIELDS_INFO sinfo;

	memset(vhi610info->gftrload, 0x00, sizeof(vhi610info->gftrload));
	memset(&sinfo, 0, sizeof(sinfo));

	strcpy(sinfo.bnpr, getFieldBNPR(vhi_fieldinfo));
	strcpy(sinfo.bnnr, getFieldBNNR(vhi_fieldinfo));
	strcpy(sinfo.bn00, getFieldBN00(eFINANCIAL_TRANS_00, vhi_fieldinfo));
	strcpy(sinfo.bnbmt, "60");
	strcpy(sinfo.bn03, getFieldBN03(eGIFT_RELOAD, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn04, getFieldBN04(GLOBAL, req_xml_info, vhi_fieldinfo));
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn07, getDateTime(eMMDDYYhhmm, date_time));
	strcpy(sinfo.bn11, getFieldBN11(vhi_fieldinfo));
#if 0
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn12, getDateTime(eMMDDYY, date_time));
	memset(date_time, 0x00, sizeof(date_time));
#endif
	strcpy(sinfo.bn12, getFieldBN12(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn13, getFieldBN13(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn22, getFieldBN22(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn25, getFieldBN25(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn32, getFieldBN32(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn41, getFieldBN41(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn42, getFieldBN42(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn43, getFieldBN43(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn45, getFieldBN45(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn48, getFieldBN48(vhi_fieldinfo));
	strcpy(sinfo.bn55, getFieldBN55(req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn107, getFieldBN107(GCONFIG, req_xml_info, vhi_fieldinfo, GLOBAL));
	strcpy(sinfo.bn115, getFieldBN115(req_xml_info, vhi_fieldinfo));

	/* add all in vhi610info->gftrload */
	strcpy(vhi610info->gftrload, sinfo.bnpr);
	strcat(vhi610info->gftrload, sinfo.bnnr);
	strcat(vhi610info->gftrload, sinfo.bn00);
	strcat(vhi610info->gftrload, sinfo.bnbmt);
	strcat(vhi610info->gftrload, sinfo.bn03);
	strcat(vhi610info->gftrload, sinfo.bn04);
	strcat(vhi610info->gftrload, sinfo.bn07);
	strcat(vhi610info->gftrload, sinfo.bn11);
	strcat(vhi610info->gftrload, sinfo.bn12);
	strcat(vhi610info->gftrload, sinfo.bn13);
	strcat(vhi610info->gftrload, sinfo.bn22);
	strcat(vhi610info->gftrload, sinfo.bn25);
	strcat(vhi610info->gftrload, sinfo.bn32);
	strcat(vhi610info->gftrload, sinfo.bn41);
	strcat(vhi610info->gftrload, sinfo.bn42);
	strcat(vhi610info->gftrload, sinfo.bn43);
	strcat(vhi610info->gftrload, sinfo.bn45);
	strcat(vhi610info->gftrload, sinfo.bn48);
	strcat(vhi610info->gftrload, sinfo.bn55);
	strcat(vhi610info->gftrload, sinfo.bn107);
	strcat(vhi610info->gftrload, sinfo.bn115);

	debug_sprintf(szDbgMsg, "%s: VHI REQUEST: Gift Card Reload", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(strlen(vhi610info->gftrload) != sizeof(vhi610info->gftrload)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 610 base length not exact", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi610info->gftrload;
}

// This is for TYPE= 0200, BM= 60, GIFT UNLOAD
char* compose610GiftUnload(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	char date_time[11+1] = "";

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	_sVHI_FIELDS_INFO sinfo;

	memset(vhi610info->gftunload, 0x00, sizeof(vhi610info->gftunload));
	memset(&sinfo, 0, sizeof(sinfo));

	strcpy(sinfo.bnpr, getFieldBNPR(vhi_fieldinfo));
	strcpy(sinfo.bnnr, getFieldBNNR(vhi_fieldinfo));
	strcpy(sinfo.bn00, getFieldBN00(eFINANCIAL_TRANS_00, vhi_fieldinfo));
	strcpy(sinfo.bnbmt, "60");
	strcpy(sinfo.bn03, getFieldBN03(eGIFT_UNLOAD, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn04, getFieldBN04(GLOBAL, req_xml_info, vhi_fieldinfo));
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn07, getDateTime(eMMDDYYhhmm, date_time));
	strcpy(sinfo.bn11, getFieldBN11(vhi_fieldinfo));
#if 0
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn12, getDateTime(eMMDDYY, date_time));
	memset(date_time, 0x00, sizeof(date_time));
#endif
	strcpy(sinfo.bn12, getFieldBN12(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn13, getFieldBN13(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn22, getFieldBN22(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn25, getFieldBN25(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn32, getFieldBN32(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn41, getFieldBN41(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn42, getFieldBN42(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn43, getFieldBN43(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn45, getFieldBN45(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn48, getFieldBN48(vhi_fieldinfo));
	strcpy(sinfo.bn55, getFieldBN55(req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn107, getFieldBN107(GCONFIG, req_xml_info, vhi_fieldinfo, GLOBAL));
	strcpy(sinfo.bn115, getFieldBN115(req_xml_info, vhi_fieldinfo));

	/* add all in vhi610info->gftunload */
	strcpy(vhi610info->gftunload, sinfo.bnpr);
	strcat(vhi610info->gftunload, sinfo.bnnr);
	strcat(vhi610info->gftunload, sinfo.bn00);
	strcat(vhi610info->gftunload, sinfo.bnbmt);
	strcat(vhi610info->gftunload, sinfo.bn03);
	strcat(vhi610info->gftunload, sinfo.bn04);
	strcat(vhi610info->gftunload, sinfo.bn07);
	strcat(vhi610info->gftunload, sinfo.bn11);
	strcat(vhi610info->gftunload, sinfo.bn12);
	strcat(vhi610info->gftunload, sinfo.bn13);
	strcat(vhi610info->gftunload, sinfo.bn22);
	strcat(vhi610info->gftunload, sinfo.bn25);
	strcat(vhi610info->gftunload, sinfo.bn32);
	strcat(vhi610info->gftunload, sinfo.bn41);
	strcat(vhi610info->gftunload, sinfo.bn42);
	strcat(vhi610info->gftunload, sinfo.bn43);
	strcat(vhi610info->gftunload, sinfo.bn45);
	strcat(vhi610info->gftunload, sinfo.bn48);
	strcat(vhi610info->gftunload, sinfo.bn55);
	strcat(vhi610info->gftunload, sinfo.bn107);
	strcat(vhi610info->gftunload, sinfo.bn115);

	debug_sprintf(szDbgMsg, "%s: VHI REQUEST: Gift Card Unload", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(strlen(vhi610info->gftunload) != sizeof(vhi610info->gftunload)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 610 base length not exact", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi610info->gftunload;
}

// This is for TYPE= 0200, BM= 60, GIFT CLOSE
char* compose610GiftClose(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	char date_time[11+1] = "";

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	_sVHI_FIELDS_INFO sinfo;

	memset(vhi610info->gftclose, 0x00, sizeof(vhi610info->gftclose));
	memset(&sinfo, 0, sizeof(sinfo));

	strcpy(sinfo.bnpr, getFieldBNPR(vhi_fieldinfo));
	strcpy(sinfo.bnnr, getFieldBNNR(vhi_fieldinfo));
	strcpy(sinfo.bn00, getFieldBN00(eFINANCIAL_TRANS_00, vhi_fieldinfo));
	strcpy(sinfo.bnbmt, "60");
	strcpy(sinfo.bn03, getFieldBN03(eGIFT_CLOSE, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn04, getFieldBN04(GLOBAL, req_xml_info, vhi_fieldinfo));
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn07, getDateTime(eMMDDYYhhmm, date_time));
	strcpy(sinfo.bn11, getFieldBN11(vhi_fieldinfo));
#if 0
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn12, getDateTime(eMMDDYY, date_time));
	memset(date_time, 0x00, sizeof(date_time));
#endif
	strcpy(sinfo.bn12, getFieldBN12(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn13, getFieldBN13(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn22, getFieldBN22(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn25, getFieldBN25(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn32, getFieldBN32(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn41, getFieldBN41(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn42, getFieldBN42(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn43, getFieldBN43(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn45, getFieldBN45(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn48, getFieldBN48(vhi_fieldinfo));
	strcpy(sinfo.bn55, getFieldBN55(req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn107, getFieldBN107(GCONFIG, req_xml_info, vhi_fieldinfo, GLOBAL));
	strcpy(sinfo.bn115, getFieldBN115(req_xml_info, vhi_fieldinfo));

	/* add all in vhi610info->gftclose */
	strcpy(vhi610info->gftclose, sinfo.bnpr);
	strcat(vhi610info->gftclose, sinfo.bnnr);
	strcat(vhi610info->gftclose, sinfo.bn00);
	strcat(vhi610info->gftclose, sinfo.bnbmt);
	strcat(vhi610info->gftclose, sinfo.bn03);
	strcat(vhi610info->gftclose, sinfo.bn04);
	strcat(vhi610info->gftclose, sinfo.bn07);
	strcat(vhi610info->gftclose, sinfo.bn11);
	strcat(vhi610info->gftclose, sinfo.bn12);
	strcat(vhi610info->gftclose, sinfo.bn13);
	strcat(vhi610info->gftclose, sinfo.bn22);
	strcat(vhi610info->gftclose, sinfo.bn25);
	strcat(vhi610info->gftclose, sinfo.bn32);
	strcat(vhi610info->gftclose, sinfo.bn41);
	strcat(vhi610info->gftclose, sinfo.bn42);
	strcat(vhi610info->gftclose, sinfo.bn43);
	strcat(vhi610info->gftclose, sinfo.bn45);
	strcat(vhi610info->gftclose, sinfo.bn48);
	strcat(vhi610info->gftclose, sinfo.bn55);
	strcat(vhi610info->gftclose, sinfo.bn107);
	strcat(vhi610info->gftclose, sinfo.bn115);

	debug_sprintf(szDbgMsg, "%s: VHI REQUEST: Gift Card Close", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(strlen(vhi610info->gftclose) != sizeof(vhi610info->gftclose)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 610 base length not exact", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi610info->gftclose;
}

// This is for TYPE= 0100, BM= 60, GIFT BALANCE
char* compose610GiftBalance(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	char date_time[11+1] = "";

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	_sVHI_FIELDS_INFO sinfo;

	memset(vhi610info->gftblnce, 0x00, sizeof(vhi610info->gftblnce));
	memset(&sinfo, 0, sizeof(sinfo));

	strcpy(sinfo.bnpr, getFieldBNPR(vhi_fieldinfo));
	strcpy(sinfo.bnnr, getFieldBNNR(vhi_fieldinfo));
	strcpy(sinfo.bn00, getFieldBN00(eAUTHORIZATION_00, vhi_fieldinfo));
	strcpy(sinfo.bnbmt, "60");
	strcpy(sinfo.bn03, getFieldBN03(eGIFT_BALINQ, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn04, getFieldBN04(GLOBAL, req_xml_info, vhi_fieldinfo));
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn07, getDateTime(eMMDDYYhhmm, date_time));
	strcpy(sinfo.bn11, getFieldBN11(vhi_fieldinfo));
#if 0
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn12, getDateTime(eMMDDYY, date_time));
	memset(date_time, 0x00, sizeof(date_time));
#endif
	strcpy(sinfo.bn12, getFieldBN12(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn13, getFieldBN13(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn22, getFieldBN22(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn25, getFieldBN25(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn32, getFieldBN32(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn41, getFieldBN41(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn42, getFieldBN42(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn43, getFieldBN43(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn45, getFieldBN45(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn48, getFieldBN48(vhi_fieldinfo));
	strcpy(sinfo.bn55, getFieldBN55(req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn107, getFieldBN107(GCONFIG, req_xml_info, vhi_fieldinfo, GLOBAL));
	strcpy(sinfo.bn115, getFieldBN115(req_xml_info, vhi_fieldinfo));

	/* add all in vhi610info->gftblnce */
	strcpy(vhi610info->gftblnce, sinfo.bnpr);
	strcat(vhi610info->gftblnce, sinfo.bnnr);
	strcat(vhi610info->gftblnce, sinfo.bn00);
	strcat(vhi610info->gftblnce, sinfo.bnbmt);
	strcat(vhi610info->gftblnce, sinfo.bn03);
	strcat(vhi610info->gftblnce, sinfo.bn04);
	strcat(vhi610info->gftblnce, sinfo.bn07);
	strcat(vhi610info->gftblnce, sinfo.bn11);
	strcat(vhi610info->gftblnce, sinfo.bn12);
	strcat(vhi610info->gftblnce, sinfo.bn13);
	strcat(vhi610info->gftblnce, sinfo.bn22);
	strcat(vhi610info->gftblnce, sinfo.bn25);
	strcat(vhi610info->gftblnce, sinfo.bn32);
	strcat(vhi610info->gftblnce, sinfo.bn41);
	strcat(vhi610info->gftblnce, sinfo.bn42);
	strcat(vhi610info->gftblnce, sinfo.bn43);
	strcat(vhi610info->gftblnce, sinfo.bn45);
	strcat(vhi610info->gftblnce, sinfo.bn48);
	strcat(vhi610info->gftblnce, sinfo.bn55);
	strcat(vhi610info->gftblnce, sinfo.bn107);
	strcat(vhi610info->gftblnce, sinfo.bn115);

	debug_sprintf(szDbgMsg, "%s: VHI REQUEST: Gift Card Balance Inquiry/Mini-Statement", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(strlen(vhi610info->gftblnce) != sizeof(vhi610info->gftblnce)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 610 base length not exact", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi610info->gftblnce;
}

// This is for TYPE= 0200, BM= 60, GIFT PREAUTH
char* compose610GiftPreauthorization(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	char date_time[11+1] = "";

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	_sVHI_FIELDS_INFO sinfo;

	memset(vhi610info->gftpreauth, 0x00, sizeof(vhi610info->gftpreauth));
	memset(&sinfo, 0, sizeof(sinfo));

	strcpy(sinfo.bnpr, getFieldBNPR(vhi_fieldinfo));
	strcpy(sinfo.bnnr, getFieldBNNR(vhi_fieldinfo));
	strcpy(sinfo.bn00, getFieldBN00(eFINANCIAL_TRANS_00, vhi_fieldinfo));
	strcpy(sinfo.bnbmt, "60");
	strcpy(sinfo.bn03, getFieldBN03(eGIFT_PREAUTH, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn04, getFieldBN04(GLOBAL, req_xml_info, vhi_fieldinfo));
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn07, getDateTime(eMMDDYYhhmm, date_time));
	strcpy(sinfo.bn11, getFieldBN11(vhi_fieldinfo));
#if 0
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn12, getDateTime(eMMDDYY, date_time));
	memset(date_time, 0x00, sizeof(date_time));
#endif
	strcpy(sinfo.bn12, getFieldBN12(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn13, getFieldBN13(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn22, getFieldBN22(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn25, getFieldBN25(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn32, getFieldBN32(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn41, getFieldBN41(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn42, getFieldBN42(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn43, getFieldBN43(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn45, getFieldBN45(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn48, getFieldBN48(vhi_fieldinfo));
	strcpy(sinfo.bn55, getFieldBN55(req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn107, getFieldBN107(GCONFIG, req_xml_info, vhi_fieldinfo, GLOBAL));
	strcpy(sinfo.bn115, getFieldBN115(req_xml_info, vhi_fieldinfo));
	fillZero(sizeof(sinfo.bn129), sinfo.bn129);

	/* add all in vhi610info->gftpreauth */
	strcpy(vhi610info->gftpreauth, sinfo.bnpr);
	strcat(vhi610info->gftpreauth, sinfo.bnnr);
	strcat(vhi610info->gftpreauth, sinfo.bn00);
	strcat(vhi610info->gftpreauth, sinfo.bnbmt);
	strcat(vhi610info->gftpreauth, sinfo.bn03);
	strcat(vhi610info->gftpreauth, sinfo.bn04);
	strcat(vhi610info->gftpreauth, sinfo.bn07);
	strcat(vhi610info->gftpreauth, sinfo.bn11);
	strcat(vhi610info->gftpreauth, sinfo.bn12);
	strcat(vhi610info->gftpreauth, sinfo.bn13);
	strcat(vhi610info->gftpreauth, sinfo.bn22);
	strcat(vhi610info->gftpreauth, sinfo.bn25);
	strcat(vhi610info->gftpreauth, sinfo.bn32);
	strcat(vhi610info->gftpreauth, sinfo.bn41);
	strcat(vhi610info->gftpreauth, sinfo.bn42);
	strcat(vhi610info->gftpreauth, sinfo.bn43);
	strcat(vhi610info->gftpreauth, sinfo.bn45);
	strcat(vhi610info->gftpreauth, sinfo.bn48);
	strcat(vhi610info->gftpreauth, sinfo.bn55);
	strcat(vhi610info->gftpreauth, sinfo.bn107);
	strcat(vhi610info->gftpreauth, sinfo.bn115);
	strcat(vhi610info->gftpreauth, sinfo.bn129);

	debug_sprintf(szDbgMsg, "%s: VHI REQUEST: Gift Card Preauthorization", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(strlen(vhi610info->gftpreauth) != sizeof(vhi610info->gftpreauth)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 610 base length not exact", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi610info->gftpreauth;
}

// This is for TYPE= 0200, BM= 60, GIFT COMPLETION
char* compose610GiftCompletion(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	char date_time[11+1] = "";

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	_sVHI_FIELDS_INFO sinfo;

	memset(vhi610info->gftcmplte, 0x00, sizeof(vhi610info->gftcmplte));
	memset(&sinfo, 0, sizeof(sinfo));

	strcpy(sinfo.bnpr, getFieldBNPR(vhi_fieldinfo));
	strcpy(sinfo.bnnr, getFieldBNNR(vhi_fieldinfo));
	strcpy(sinfo.bn00, getFieldBN00(eFINANCIAL_TRANS_00, vhi_fieldinfo));
	strcpy(sinfo.bnbmt, "60");
	strcpy(sinfo.bn03, getFieldBN03(eGIFT_COMPLETION, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn04, getFieldBN04(GLOBAL, req_xml_info, vhi_fieldinfo));
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn07, getDateTime(eMMDDYYhhmm, date_time));
	strcpy(sinfo.bn11, getFieldBN11(vhi_fieldinfo));
	strcpy(sinfo.bn12, getFieldBN12(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn13, getFieldBN13(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn22, getFieldBN22(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn25, getFieldBN25(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn32, getFieldBN32(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn41, getFieldBN41(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn42, getFieldBN42(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn43, getFieldBN43(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn45, getFieldBN45(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn48, getFieldBN48(vhi_fieldinfo));
	strcpy(sinfo.bn55, getFieldBN55(req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn107, getFieldBN107(GCONFIG, req_xml_info, vhi_fieldinfo, GLOBAL));
	strcpy(sinfo.bn115, getFieldBN115(req_xml_info, vhi_fieldinfo));


	/* add all in vhi610info->gftcmplte */
	strcpy(vhi610info->gftcmplte, sinfo.bnpr);
	strcat(vhi610info->gftcmplte, sinfo.bnnr);
	strcat(vhi610info->gftcmplte, sinfo.bn00);
	strcat(vhi610info->gftcmplte, sinfo.bnbmt);
	strcat(vhi610info->gftcmplte, sinfo.bn03);
	strcat(vhi610info->gftcmplte, sinfo.bn04);
	strcat(vhi610info->gftcmplte, sinfo.bn07);
	strcat(vhi610info->gftcmplte, sinfo.bn11);
	strcat(vhi610info->gftcmplte, sinfo.bn12);
	strcat(vhi610info->gftcmplte, sinfo.bn13);
	strcat(vhi610info->gftcmplte, sinfo.bn22);
	strcat(vhi610info->gftcmplte, sinfo.bn25);
	strcat(vhi610info->gftcmplte, sinfo.bn32);
	strcat(vhi610info->gftcmplte, sinfo.bn41);
	strcat(vhi610info->gftcmplte, sinfo.bn42);
	strcat(vhi610info->gftcmplte, sinfo.bn43);
	strcat(vhi610info->gftcmplte, sinfo.bn45);
	strcat(vhi610info->gftcmplte, sinfo.bn48);
	strcat(vhi610info->gftcmplte, sinfo.bn55);
	strcat(vhi610info->gftcmplte, sinfo.bn107);
	strcat(vhi610info->gftcmplte, sinfo.bn115);

	debug_sprintf(szDbgMsg, "%s: VHI REQUEST: Gift Card Completion", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(strlen(vhi610info->gftcmplte) != sizeof(vhi610info->gftcmplte)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 610 base length not exact", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi610info->gftcmplte;
}

// This is for TYPE= 0100, BM= 21, CREDIT PREAUTH
char* compose610CreditPreauthorization(char **transtype,sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	// Indicate which fields and groups required
	int     fields[]= {3,4,7,11,12,13,22,25,32,41,42,43,45,48,55,60,70,107,109,110,115, -1}; // 0 and "bit map" are done before
	int     groups[]= {eGROUP001, eGROUP004, eGROUP009, eGROUP015, eGROUP023, eGROUP026, eGROUP028, eGROUP034, eGROUP035, eGROUP036, eGROUP038, eGROUP043, eGROUP045, -1};

	int     index;
	char    *buffer;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(vhi610info->crdsale, 0x00, sizeof(vhi610info->crdsale));
	buffer= vhi610info->crdsale;
	first_four_fields(buffer, eAUTHORIZATION_00, "21");       // set correct message identifier and "bit map"

	index= 0;
	while (fields[index] >= 0)
	{
		// Include message identifier and processing code
		strcat(buffer, getFieldX(fields[index], eCREDIT_PREAUTH, GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
		index++;
	}

	strcat(buffer, RS);
	*transtype = (char*)calloc(strlen(buffer)+ 1, sizeof(char));   // extra 1 for '\0'
	strcpy(*transtype, buffer);

	index= 0;
	while (groups[index] >= 0)
	{
		append610Group(groups[index], transtype, GLOBAL, GCONFIG, req_xml_info);
		index++;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return *transtype;
}

// This is for TYPE= 0100, BM= 21, CREDIT PREAUTH
char* compose610CreditPreauthorizationAVS(char **transtype,sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	// Indicate which fields and groups required
	int     fields[]= {3,4,7,11,12,13,22,25,32,41,42,43,45,48,55,60,70,106,107,109,110,115, -1}; // 0 and "bit map" are done before
	int     groups[]= {eGROUP001, eGROUP004, eGROUP009, eGROUP015, eGROUP023, eGROUP026, eGROUP028, eGROUP034, eGROUP035, eGROUP036, eGROUP038, eGROUP043, eGROUP045, -1};

	int     index;
	char    *buffer;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(vhi610info->crdsale, 0x00, sizeof(vhi610info->crdsale));
	buffer= vhi610info->crdsale;
	first_four_fields(buffer, eAUTHORIZATION_00, "25");       // set correct message identifier and "bit map"

	index= 0;
	while (fields[index] >= 0)
	{
		// Include message identifier and processing code
		strcat(buffer, getFieldX(fields[index], eCREDIT_PREAUTHAVS, GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
		index++;
	}

	strcat(buffer, RS);
	*transtype = (char*)calloc(strlen(buffer)+ 1, sizeof(char));   // extra 1 for '\0'
	strcpy(*transtype, buffer);

	index= 0;
	while (groups[index] >= 0)
	{
		append610Group(groups[index], transtype, GLOBAL, GCONFIG, req_xml_info);
		index++;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return *transtype;
}

// This is for TYPE= 0500, BM= 01, BATCH INQUIRY
char* compose610BatchInquiry(sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	_sVHI_FIELDS_INFO sinfo;

	memset(vhi610info->batchinquiry, 0x00, sizeof(vhi610info->batchinquiry));
	memset(&sinfo, 0, sizeof(sinfo));

	strcpy(sinfo.bnpr, getFieldBNPR(vhi_fieldinfo));
	strcpy(sinfo.bnnr, getFieldBNNR(vhi_fieldinfo));
	strcpy(sinfo.bn00, getFieldBN00(eRECON_CONTROL_00, vhi_fieldinfo));
	strcpy(sinfo.bnbmt, "01");
	strcpy(sinfo.bn03, getFieldBN03(eBATCH_INQUIRY, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn11, getFieldBN11(vhi_fieldinfo));
	strcpy(sinfo.bn32, getFieldBN32(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn41, getFieldBN41(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn42, getFieldBN42(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn48, getFieldBN48(vhi_fieldinfo));
	fillZero(sizeof(sinfo.bn70), sinfo.bn70);
	fillZero(sizeof(sinfo.bn74), sinfo.bn74);
	fillZero(sizeof(sinfo.bn76), sinfo.bn76);
	fillZero(sizeof(sinfo.bn86), sinfo.bn86);
	fillZero(sizeof(sinfo.bn88), sinfo.bn88);
	strcpy(sinfo.bn115, getFieldBN115(req_xml_info, vhi_fieldinfo));

	/* add all in vhi610info->gftbtchtotl */
	strcpy(vhi610info->batchinquiry, sinfo.bnpr);
	strcat(vhi610info->batchinquiry, sinfo.bnnr);
	strcat(vhi610info->batchinquiry, sinfo.bn00);
	strcat(vhi610info->batchinquiry, sinfo.bnbmt);
	strcat(vhi610info->batchinquiry, sinfo.bn03);
	strcat(vhi610info->batchinquiry, sinfo.bn11);
	strcat(vhi610info->batchinquiry, sinfo.bn32);
	strcat(vhi610info->batchinquiry, sinfo.bn41);
	strcat(vhi610info->batchinquiry, sinfo.bn42);
	strcat(vhi610info->batchinquiry, sinfo.bn48);
	strcat(vhi610info->batchinquiry, sinfo.bn70);
	strcat(vhi610info->batchinquiry, sinfo.bn74);
	strcat(vhi610info->batchinquiry, sinfo.bn76);
	strcat(vhi610info->batchinquiry, sinfo.bn86);
	strcat(vhi610info->batchinquiry, sinfo.bn88);
	strcat(vhi610info->batchinquiry, sinfo.bn115);

	debug_sprintf(szDbgMsg, "%s: VHI REQUEST: Batch Inquiry", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(strlen(vhi610info->batchinquiry) != sizeof(vhi610info->batchinquiry)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 610 base length not exact", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi610info->batchinquiry;
}

// This is for TYPE= 0500, BM= 01, BATCH RELEASE
char* compose610BatchRelease(sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	_sVHI_FIELDS_INFO sinfo;

	memset(vhi610info->batchrelease, 0x00, sizeof(vhi610info->batchrelease));
	memset(&sinfo, 0, sizeof(sinfo));

	strcpy(sinfo.bnpr, getFieldBNPR(vhi_fieldinfo));
	strcpy(sinfo.bnnr, getFieldBNNR(vhi_fieldinfo));
	strcpy(sinfo.bn00, getFieldBN00(eRECON_CONTROL_00, vhi_fieldinfo));
	strcpy(sinfo.bnbmt, "01");
	strcpy(sinfo.bn03, getFieldBN03(eBATCH_RELEASE, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn11, getFieldBN11(vhi_fieldinfo));
	strcpy(sinfo.bn32, getFieldBN32(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn41, getFieldBN41(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn42, getFieldBN42(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn48, getFieldBN48(vhi_fieldinfo));
	fillZero(sizeof(sinfo.bn70), sinfo.bn70);
	fillZero(sizeof(sinfo.bn74), sinfo.bn74);
	fillZero(sizeof(sinfo.bn76), sinfo.bn76);
	fillZero(sizeof(sinfo.bn86), sinfo.bn86);
	fillZero(sizeof(sinfo.bn88), sinfo.bn88);
	strcpy(sinfo.bn115, getFieldBN115(req_xml_info, vhi_fieldinfo));

	/* add all in vhi610info->gftbtchtotl */
	strcpy(vhi610info->batchrelease, sinfo.bnpr);
	strcat(vhi610info->batchrelease, sinfo.bnnr);
	strcat(vhi610info->batchrelease, sinfo.bn00);
	strcat(vhi610info->batchrelease, sinfo.bnbmt);
	strcat(vhi610info->batchrelease, sinfo.bn03);
	strcat(vhi610info->batchrelease, sinfo.bn11);
	strcat(vhi610info->batchrelease, sinfo.bn32);
	strcat(vhi610info->batchrelease, sinfo.bn41);
	strcat(vhi610info->batchrelease, sinfo.bn42);
	strcat(vhi610info->batchrelease, sinfo.bn48);
	strcat(vhi610info->batchrelease, sinfo.bn70);
	strcat(vhi610info->batchrelease, sinfo.bn74);
	strcat(vhi610info->batchrelease, sinfo.bn76);
	strcat(vhi610info->batchrelease, sinfo.bn86);
	strcat(vhi610info->batchrelease, sinfo.bn88);
	strcat(vhi610info->batchrelease, sinfo.bn115);

	debug_sprintf(szDbgMsg, "%s: VHI REQUEST: Batch Release", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(strlen(vhi610info->batchrelease) != sizeof(vhi610info->batchrelease)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 610 base length not exact", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi610info->batchrelease;
}

// This is for TYPE= 0500, BM= 60, GIFT BATCH TOTALS
char* compose610GiftBatchTotals(sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	_sVHI_FIELDS_INFO sinfo;

	memset(vhi610info->gftbtchtotl, 0x00, sizeof(vhi610info->gftbtchtotl));
	memset(&sinfo, 0, sizeof(sinfo));

	strcpy(sinfo.bnpr, getFieldBNPR(vhi_fieldinfo));
	strcpy(sinfo.bnnr, getFieldBNNR(vhi_fieldinfo));
	strcpy(sinfo.bn00, getFieldBN00(eRECON_CONTROL_00, vhi_fieldinfo));
	strcpy(sinfo.bnbmt, "60");
	strcpy(sinfo.bn03, getFieldBN03(eGIFT_BATCH_TOTAL, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn11, getFieldBN11(vhi_fieldinfo));
	strcpy(sinfo.bn32, getFieldBN32(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn41, getFieldBN41(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn42, getFieldBN42(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn48, getFieldBN48(vhi_fieldinfo));
	fillZero(sizeof(sinfo.bn70), sinfo.bn70);
	fillZero(sizeof(sinfo.bn74), sinfo.bn74);
	fillZero(sizeof(sinfo.bn76), sinfo.bn76);
	fillZero(sizeof(sinfo.bn86), sinfo.bn86);
	fillZero(sizeof(sinfo.bn88), sinfo.bn88);
	strcpy(sinfo.bn115, getFieldBN115(req_xml_info, vhi_fieldinfo));

	/* add all in vhi610info->gftbtchtotl */
	strcpy(vhi610info->gftbtchtotl, sinfo.bnpr);
	strcat(vhi610info->gftbtchtotl, sinfo.bnnr);
	strcat(vhi610info->gftbtchtotl, sinfo.bn00);
	strcat(vhi610info->gftbtchtotl, sinfo.bnbmt);
	strcat(vhi610info->gftbtchtotl, sinfo.bn03);
	strcat(vhi610info->gftbtchtotl, sinfo.bn11);
	strcat(vhi610info->gftbtchtotl, sinfo.bn32);
	strcat(vhi610info->gftbtchtotl, sinfo.bn41);
	strcat(vhi610info->gftbtchtotl, sinfo.bn42);
	strcat(vhi610info->gftbtchtotl, sinfo.bn48);
	strcat(vhi610info->gftbtchtotl, sinfo.bn70);
	strcat(vhi610info->gftbtchtotl, sinfo.bn74);
	strcat(vhi610info->gftbtchtotl, sinfo.bn76);
	strcat(vhi610info->gftbtchtotl, sinfo.bn86);
	strcat(vhi610info->gftbtchtotl, sinfo.bn88);
	strcat(vhi610info->gftbtchtotl, sinfo.bn115);

	debug_sprintf(szDbgMsg, "%s: VHI REQUEST: Gift Card Batch Totals", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(strlen(vhi610info->gftbtchtotl) != sizeof(vhi610info->gftbtchtotl)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 610 base length not exact", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi610info->gftbtchtotl;
}

// This is for TYPE= 0220, BM= 24, CREDIT ADJUST --- EMV
// transtype is allocate in this routine
char* compose_610CreditPriorAuthAdj(char **transtype, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
    // Indicate which fields and groups required
    int     fields[]= {3,4,7,11,12,13,22,25,32,41,42,43,45,48,55,65,67,70,105,107,109,110,115, -1}; // 0 and "bit map" are done before
    int     groups[]= {eGROUP001, eGROUP004, eGROUP009, eGROUP014, eGROUP023, eGROUP026, eGROUP028, eGROUP034, eGROUP035, eGROUP036, eGROUP038, eGROUP043, eGROUP045, -1};
    
    int     index;
    char    *buffer;
    enum _ePROC_CODE    processing_code;
    
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    memset(vhi610info->crdsale, 0x00, sizeof(vhi610info->crdsale));
    buffer= vhi610info->crdsale;
    first_four_fields(buffer, eFINANCIAL_TRANS_0220, "24");       // set correct message identifier and "bit map"
    
    /*temporary*/
    char* pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);
    
    if (pCOMMAND != NULL)
    {
        if (strncmp(pCOMMAND, COMMAND.ADD_TIP, strlen(COMMAND.ADD_TIP)) == COMPARE_OK)
            processing_code= eCDT_PRIORADJUSTMENT;
        else
            processing_code= eCDT_PRIORAUTH;
    }
    else
    {
        debug_sprintf(szDbgMsg, "%s: WARNING!", __FUNCTION__);
        APP_TRACE(szDbgMsg);
        
        return NULL;
    }
    /*end*/
   
    index= 0;
    while (fields[index] >= 0)
    {
        // Include message identifier and processing code
        strcat(buffer, getFieldX(fields[index], processing_code, GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
        index++;
    }
    
    strcat(buffer, RS);
    *transtype = (char*)calloc(strlen(buffer)+ 1, sizeof(char));   // extra 1 for '\0'
    strcpy(*transtype, buffer);
    
    index= 0;
    while (groups[index] >= 0)
    {
        if (groups[index] == eGROUP014)
        {
            // group 14 only if COMMAND is ADD_TIP
            char* pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);
            
            if (pCOMMAND != NULL)
            {
                if (strncmp(pCOMMAND, COMMAND.ADD_TIP, strlen(COMMAND.ADD_TIP)) == COMPARE_OK)
                {
                    append610Group(groups[index], transtype, GLOBAL, GCONFIG, req_xml_info);
                }
            }
        }
        else
        {
            append610Group(groups[index], transtype, GLOBAL, GCONFIG, req_xml_info);
        }
        index++;
    }
    
    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    return *transtype;
    
}


// This is for TYPE= 0100, BM= 10, DEBIT/CREDIT BALANCE
char* compose610DebitCreditBalance(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	char date_time[11+1] = "";

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	_sVHI_FIELDS_INFO sinfo;

	memset(vhi610info->debcrebalinq, 0x00, sizeof(vhi610info->debcrebalinq));
	memset(&sinfo, 0, sizeof(sinfo));

	strcpy(sinfo.bnpr, getFieldBNPR(vhi_fieldinfo));
	strcpy(sinfo.bnnr, getFieldBNNR(vhi_fieldinfo));
	strcpy(sinfo.bn00, getFieldBN00(eAUTHORIZATION_00, vhi_fieldinfo));
	strcpy(sinfo.bnbmt, "10");
	strcpy(sinfo.bn03, getFieldBN03(eCRED_DEBT_BAL_INQ, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn04, getFieldBN04(GLOBAL, req_xml_info, vhi_fieldinfo));
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn07, getDateTime(eMMDDYYhhmm, date_time));
	strcpy(sinfo.bn11, getFieldBN11(vhi_fieldinfo));
#if 0
	//memset(date_time, 0x00, sizeof(date_time));
	//strcpy(sinfo.bn12, getDateTime(eMMDDYY, date_time));
	//memset(date_time, 0x00, sizeof(date_time));
#endif
	strcpy(sinfo.bn12, getFieldBN12(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn13, getFieldBN13(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn22, getFieldBN22(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn25, getFieldBN25(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn32, getFieldBN32(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn41, getFieldBN41(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn42, getFieldBN42(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn43, getFieldBN43(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn45, getFieldBN45(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn48, getFieldBN48(vhi_fieldinfo));
	strcpy(sinfo.bn52, getFieldBN52(req_xml_info, vhi_fieldinfo));
	fillZero(sizeof(sinfo.bn70), sinfo.bn70);
	strcpy(sinfo.bn107, getFieldBN107(GCONFIG, req_xml_info, vhi_fieldinfo, GLOBAL));
	strcpy(sinfo.bn115, getFieldBN115(req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn117, getFieldBN117(req_xml_info, vhi_fieldinfo));

	/* add all in vhi610info->debcrebalinq */
	strcpy(vhi610info->debcrebalinq, sinfo.bnpr);
	strcat(vhi610info->debcrebalinq, sinfo.bnnr);
	strcat(vhi610info->debcrebalinq, sinfo.bn00);
	strcat(vhi610info->debcrebalinq, sinfo.bnbmt);
	strcat(vhi610info->debcrebalinq, sinfo.bn03);
	strcat(vhi610info->debcrebalinq, sinfo.bn04);
	strcat(vhi610info->debcrebalinq, sinfo.bn07);
	strcat(vhi610info->debcrebalinq, sinfo.bn11);
	strcat(vhi610info->debcrebalinq, sinfo.bn12);
	strcat(vhi610info->debcrebalinq, sinfo.bn13);
	strcat(vhi610info->debcrebalinq, sinfo.bn22);
	strcat(vhi610info->debcrebalinq, sinfo.bn25);
	strcat(vhi610info->debcrebalinq, sinfo.bn32);
	strcat(vhi610info->debcrebalinq, sinfo.bn41);
	strcat(vhi610info->debcrebalinq, sinfo.bn42);
	strcat(vhi610info->debcrebalinq, sinfo.bn43);
	strcat(vhi610info->debcrebalinq, sinfo.bn45);
	strcat(vhi610info->debcrebalinq, sinfo.bn48);
	strcat(vhi610info->debcrebalinq, sinfo.bn52);
	strcat(vhi610info->debcrebalinq, sinfo.bn70);
	strcat(vhi610info->debcrebalinq, sinfo.bn107);
	strcat(vhi610info->debcrebalinq, sinfo.bn115);
	strcat(vhi610info->debcrebalinq, sinfo.bn117);

	debug_sprintf(szDbgMsg, "%s: VHI REQUEST: Debit/Credit Card Balance Inquiry (DUKPT Key)", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(strlen(vhi610info->debcrebalinq) != sizeof(vhi610info->debcrebalinq)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 610 base length not exact", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi610info->debcrebalinq;
}

// This is for TYPE= 0100, BM= 50, TOKENIZE
char* compose610Tokenization(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	char date_time[11+1] = "";
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	_sVHI_FIELDS_INFO sinfo;

	memset(vhi610info->tokenconv, 0x00, sizeof(vhi610info->tokenconv));
	memset(&sinfo, 0, sizeof(sinfo));

	strcpy(sinfo.bnpr, getFieldBNPR(vhi_fieldinfo));
	strcpy(sinfo.bnnr, getFieldBNNR(vhi_fieldinfo));
	strcpy(sinfo.bn00, getFieldBN00(eAUTHORIZATION_00, vhi_fieldinfo));
	strcpy(sinfo.bnbmt, "50");
	strcpy(sinfo.bn03, getFieldBN03(eTOKENIZATION, req_xml_info, vhi_fieldinfo));
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn07, getDateTime(eMMDDYYhhmm, date_time));
	strcpy(sinfo.bn11, getFieldBN11(vhi_fieldinfo));
#if 0
	memset(date_time, 0x00, sizeof(date_time));
	strcpy(sinfo.bn12, getDateTime(eMMDDYY, date_time));
	memset(date_time, 0x00, sizeof(date_time));
#endif
	strcpy(sinfo.bn12, getFieldBN12(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn13, getFieldBN13(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn22, getFieldBN22(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn25, getFieldBN25(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn32, getFieldBN32(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn41, getFieldBN41(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn42, getFieldBN42(GCONFIG, vhi_fieldinfo));
	strcpy(sinfo.bn43, getFieldBN43(GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn45, getFieldBN45(GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
	strcpy(sinfo.bn48, getFieldBN48(vhi_fieldinfo));
	strcpy(sinfo.bn55, getFieldBN55(req_xml_info, vhi_fieldinfo));
	fillZero(sizeof(sinfo.bn70), sinfo.bn70);
	strcpy(sinfo.bn107, getFieldBN107(GCONFIG, req_xml_info, vhi_fieldinfo, GLOBAL));
	strcpy(sinfo.bn115, getFieldBN115(req_xml_info, vhi_fieldinfo));
	fillChar9(sizeof(sinfo.bn139), sinfo.bn139);
	fillChar9(sizeof(sinfo.bn140), sinfo.bn140);

	/* add all in vhi610info->tokenconv */
	strcpy(vhi610info->tokenconv, sinfo.bnpr);
	strcat(vhi610info->tokenconv, sinfo.bnnr);
	strcat(vhi610info->tokenconv, sinfo.bn00);
	strcat(vhi610info->tokenconv, sinfo.bnbmt);
	strcat(vhi610info->tokenconv, sinfo.bn03);
	strcat(vhi610info->tokenconv, sinfo.bn07);
	strcat(vhi610info->tokenconv, sinfo.bn11);
	strcat(vhi610info->tokenconv, sinfo.bn12);
	strcat(vhi610info->tokenconv, sinfo.bn13);
	strcat(vhi610info->tokenconv, sinfo.bn22);
	strcat(vhi610info->tokenconv, sinfo.bn25);
	strcat(vhi610info->tokenconv, sinfo.bn32);
	strcat(vhi610info->tokenconv, sinfo.bn41);
	strcat(vhi610info->tokenconv, sinfo.bn42);
	strcat(vhi610info->tokenconv, sinfo.bn43);
	strcat(vhi610info->tokenconv, sinfo.bn45);
	strcat(vhi610info->tokenconv, sinfo.bn48);
	strcat(vhi610info->tokenconv, sinfo.bn55);
	strcat(vhi610info->tokenconv, sinfo.bn70);
	strcat(vhi610info->tokenconv, sinfo.bn107);
	strcat(vhi610info->tokenconv, sinfo.bn115);
	strcat(vhi610info->tokenconv, sinfo.bn139);
	strcat(vhi610info->tokenconv, sinfo.bn140);

	debug_sprintf(szDbgMsg, "%s: VHI REQUEST: Tokenization Conversion", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(strlen(vhi610info->tokenconv) != sizeof(vhi610info->tokenconv)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 610 base length not exact", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NULL;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return vhi610info->tokenconv;
}

// This is for TYPE= 0220, BM= 34, EMV OFFLINE APPROVAL ADVICE
// transtype is allocate in this routine
char *compose610EMVOfflineAdvice(char **transtype, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
    // Indicate which fields and groups required
    int     fields[]= {3,4,7,11,12,13,22,25,32,41,42,43,45,48,55,60,67,70,105,107,109,110,115, -1}; // 0 and "bit map" are done before
    int     groups[]= {eGROUP014, eGROUP023, eGROUP026, eGROUP028, eGROUP034, eGROUP035, eGROUP036, eGROUP038, -1};
    
    int     index;
    char    *buffer;
    
    enum _ePROC_CODE    processing_code;
    
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    memset(vhi610info->emvadvice, 0x00, sizeof(vhi610info->emvadvice));
    buffer= vhi610info->emvadvice;
    first_four_fields(buffer, eFINANCIAL_TRANS_0220, "34");       // set correct message identifier and "bit map"
    
    char* pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);
    if (strncmp(pCOMMAND, COMMAND.SALE, strlen(COMMAND.SALE)) == COMPARE_OK)
        processing_code= eSALE_PROCCODE;
    else if (strncmp(pCOMMAND, COMMAND.CREDIT, strlen(COMMAND.CREDIT)) == COMPARE_OK)
        processing_code= eREFUND_PROCCODE;
    else
        return NULL;    // this combination is not allowed
    
    index= 0;
    while (fields[index] >= 0)
    {
        // Include message identifier and processing code
        strcat(buffer, getFieldX(fields[index], processing_code, GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
        index++;
    }
    
    strcat(buffer, RS);
    *transtype = (char*)calloc(strlen(buffer)+ 1, sizeof(char));   // extra 1 for '\0'
    strcpy(*transtype, buffer);
    
    index= 0;
    while (groups[index] >= 0)
    {
        append610Group(groups[index], transtype, GLOBAL, GCONFIG, req_xml_info);
        index++;
    }
    
    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    return *transtype;
}

// This is for TYPE= 0200, BM= 14, EBT sale
// EBT vouchers not supported
// transtype is allocate in this routine
char *compose610EBTSale(char **transtype, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
    // Indicate which fields and groups required
    int     fields[]= {3,4,7,11,12,13,22,25,32,41,42,43,45,48,52,55,60,70,107,112,115,117, -1}; // 0 and "bit map" are done before
    int     groups[]= {eGROUP001, eGROUP004, eGROUP009, eGROUP015, eGROUP026, eGROUP029, eGROUP030, eGROUP031, eGROUP034, eGROUP036, eGROUP038, -1};
    
    int     index;
    char    *buffer;
    
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    memset(vhi610info->ebtsale, 0x00, sizeof(vhi610info->ebtsale));
    buffer= vhi610info->ebtsale;
    first_four_fields(buffer, eFINANCIAL_TRANS_00, "14");       // set correct message identifier and "bit map"
    
    index= 0;
    while (fields[index] >= 0)
    {
        // Include message identifier and processing code
        strcat(buffer, getFieldX(fields[index], eSALE_PROCCODE, GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
        index++;
    }
    
    strcat(buffer, RS);
    *transtype = (char*)calloc(strlen(buffer)+ 1, sizeof(char));   // extra 1 for '\0'
    strcpy(*transtype, buffer);
    
    index= 0;
    while (groups[index] >= 0)
    {
        append610Group(groups[index], transtype, GLOBAL, GCONFIG, req_xml_info);
        index++;
    }
    
    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    return *transtype;
}

// This is for TYPE= 0200, BM= 14, EBT return
// transtype is allocate in this routine
char *compose610EBTReturn(char **transtype, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
    // Indicate which fields and groups required
    int     fields[]= {3,4,7,11,12,13,22,25,32,41,42,43,45,48,52,55,60,70,107,112,115,117, -1}; // 0 and "bit map" are done before
    int     groups[]= {eGROUP001, eGROUP004, eGROUP009, eGROUP026, eGROUP028, eGROUP034, eGROUP036, eGROUP038, -1};
    
    int     index;
    char    *buffer;
    
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    memset(vhi610info->ebtrefnd, 0x00, sizeof(vhi610info->ebtrefnd));
    buffer= vhi610info->ebtrefnd;
    first_four_fields(buffer, eFINANCIAL_TRANS_00, "14");       // set correct message identifier and "bit map"
    
    index= 0;
    while (fields[index] >= 0)
    {
        // Include message identifier and processing code
        strcat(buffer, getFieldX(fields[index], eREFUND_PROCCODE, GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
        index++;
    }
    
    strcat(buffer, RS);
    *transtype = (char*)calloc(strlen(buffer)+ 1, sizeof(char));   // extra 1 for '\0'
    strcpy(*transtype, buffer);
    
    index= 0;
    while (groups[index] >= 0)
    {
        append610Group(groups[index], transtype, GLOBAL, GCONFIG, req_xml_info);
        index++;
    }
    
    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    return *transtype;
}

// This is for TYPE= 0400, BM= 04, EBT void (DUKPT)
// transtype is allocate in this routine
char *compose610EBTVoid(char **transtype, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
    // EBT vouchers not supported
    
    // Indicate which fields and groups required
    int     fields[]= {2,7,11,12,13,32,41,42,43,48,52,55,70,90,107,115,117, -1}; // 0 and "bit map" are done before
    int     groups[]= {eGROUP004, eGROUP009, eGROUP014, eGROUP023, eGROUP026, eGROUP027, eGROUP028, eGROUP034, eGROUP035, eGROUP038, -1}; //Praveen_P1: As per Vantiv's Analyst G001 should not be present for Voids
    
    int     index;
    char    *buffer;
    enum _ePROC_CODE    processing_code;
    
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    memset(vhi610info->ebtvoid, 0x00, sizeof(vhi610info->ebtvoid));
    buffer= vhi610info->ebtvoid;
    first_four_fields(buffer, eREVERSAL_00, "04");       // set correct message identifier and "bit map"
    
    processing_code= eEBT_VOID;
    
    index= 0;
    while (fields[index] >= 0)
    {
        // Include message identifier and processing code
        strcat(buffer, getFieldX(fields[index], processing_code, GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
        index++;
    }
    
    strcat(buffer, RS);
    *transtype = (char*)calloc(strlen(buffer)+ 1, sizeof(char));   // extra 1 for '\0'
    strcpy(*transtype, buffer);
    
    index= 0;
    while (groups[index] >= 0)
    {
        append610Group(groups[index], transtype, GLOBAL, GCONFIG, req_xml_info);
        index++;
    }
    
    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    return *transtype;
}

// This is for TYPE= 0100, BM= 14, EBT balance
// transtype is allocate in this routine
char *compose610EBTBalance(char **transtype, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
    // EBT vouchers not supported
    
    // Indicate which fields and groups required
    int     fields[]= {3,7,11,12,13,22,25,32,41,42,43,45,48,52,70,107,112,115,117, -1}; // 0 and "bit map" are done before
    int     groups[]= {eGROUP026, eGROUP028, eGROUP034, eGROUP036, eGROUP038, -1};
    
    int     index;
    char    *buffer;
    enum _ePROC_CODE    processing_code;
    
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    memset(vhi610info->ebtbalance, 0x00, sizeof(vhi610info->ebtbalance));
    buffer= vhi610info->ebtbalance;
    first_four_fields(buffer, eAUTHORIZATION_00, "14");       // set correct message identifier and "bit map"
    
    processing_code= eEBT_BALANCE;      // when WIC supported need another processing code
    
    index= 0;
    while (fields[index] >= 0)
    {
        // Include message identifier and processing code
        strcat(buffer, getFieldX(fields[index], processing_code, GLOBAL, GCONFIG, req_xml_info, vhi_fieldinfo));
        index++;
    }
    
    strcat(buffer, RS);
    *transtype = (char*)calloc(strlen(buffer)+ 1, sizeof(char));   // extra 1 for '\0'
    strcpy(*transtype, buffer);
    
    index= 0;
    while (groups[index] >= 0)
    {
        append610Group(groups[index], transtype, GLOBAL, GCONFIG, req_xml_info);
        index++;
    }
    
    debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    return *transtype;
}

