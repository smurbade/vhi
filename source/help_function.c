/*
 * help_function.c
 *
 *  Created on: Oct 16, 2013
 *      Author: Dexter M. Alberto
 */


#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <semaphore.h>
#include <pthread.h>
#include "help_function.h"
#include "logger.h"
#include "define_data.h"
#include <time.h>       /* time_t, time, ctime */
#include <ctype.h>
#include <sys/stat.h>
#include <curl/curl.h>
#include <dlfcn.h>
#include "sca_xml_request.h"
#include "config_data.h"
#include "vhi_response.h"
#include "vantiv_response.h"
#include "svc.h"

#ifdef LOGGING_ENABLED
static char    szDbgMsg[512];
#endif

void LogData()
{
	/* Kept for compilation to be possible with libtcpip library */
}
void freeHelpData()
{
#if 0
	if(shelp_info.date_time) {
		free(shelp_info.date_time);
		shelp_info.date_time = NULL;
	}
#endif
}

char* getDateTime(enum e_DATE_TIME_ format, char *date_time)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(date_time == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input parameter NULL!!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return NULL;
	}

#if 0
	if(shelp_info.date_time) {
		free(shelp_info.date_time);
		shelp_info.date_time = NULL;
	}
#endif

	time_t rawtime;

	time (&rawtime);
	char pp[25+1] = {0};
	strcpy(pp, ctime (&rawtime));

	char MM[2+1] = {0};
	char DD[2+1] = {0};
	char YYYY[4+1] = {0};
	char YY[2+1] = {0};
	char hh[2+1] = {0};
	char mm[2+1] = {0};
	char ss[2+1] = {0};

	char* buffer = "";

	if(pp[4] == 'J' && pp[5] == 'a') {
		strcpy(MM, "01");
	}
	if(pp[4] == 'F') {
		strcpy(MM, "02");
	}
	if(pp[4] == 'M' && pp[5] == 'a' && pp[6] == 'r') {
		strcpy(MM, "03");
	}
	if(pp[4] == 'A' && pp[5] == 'p') {
		strcpy(MM, "04");
	}
	if(pp[4] == 'M' && pp[5] == 'a' && pp[6] == 'y') {
		strcpy(MM, "05");
	}
	if(pp[4] == 'J' && pp[5] == 'u' && pp[6] == 'n') {
		strcpy(MM, "06");
	}
	if(pp[4] == 'J' && pp[5] == 'u' && pp[6] == 'l') {
		strcpy(MM, "07");
	}
	if(pp[4] == 'A' && pp[5] == 'u') {
		strcpy(MM, "08");
	}
	if(pp[4] == 'S') {
		strcpy(MM, "09");
	}
	if(pp[4] == 'O') {
		strcpy(MM, "10");
	}
	if(pp[4] == 'N') {
		strcpy(MM, "11");
	}
	if(pp[4] == 'D') {
		strcpy(MM, "12");
	}

	sprintf(DD, "%c%c", pp[8], pp[9]); //DATE
	if(DD[0] == ' ')
		DD[0] = '0';

	sprintf(YY, "%c%c", pp[22], pp[23]); //YEAR
	sprintf(YYYY, "%c%c%c%c", pp[20], pp[21], pp[22], pp[23]); //YEAR

	sprintf(hh, "%c%c", pp[11], pp[12]); //HOURS
	sprintf(mm, "%c%c", pp[14], pp[15]); //MINUTES
	sprintf(ss, "%c%c", pp[17], pp[18]); //SECONDS

	switch(format)
	{
	case eMMDDYYhhmm:
	{
		char tmp[10+1];
		sprintf(tmp, "%s%s%s%s%s", MM, DD, YY, hh, mm);
		buffer = tmp;
		break;
	}
	case eMMDDYY:
	{
		char tmp[6+1];
		sprintf(tmp, "%s%s%s", MM, DD, YY);
		buffer = tmp;
		break;
	}
	case ehhmmss:
	{
		char tmp[6+1];
		sprintf(tmp, "%s%s%s", hh, mm, ss);
		buffer = tmp;
		break;
	}
	case eYYYY_MM_DD:
	{
		char tmp[10+1];
		sprintf(tmp, "%s.%s.%s", YYYY, MM, DD);
		buffer = tmp;
		break;
	}
	case eHH_MM_SS:
	{
		char tmp[8+1] = {0};
		sprintf(tmp, "%s:%s:%s", hh, mm, ss);
		buffer = tmp;
		break;
	}
	default:
		debug_sprintf(szDbgMsg, "%s: ERROR! Invalid enum", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		break;
	}

	//shelp_info.date_time = (char*)calloc(strlen(buffer)+1, sizeof(char));
	//memset(shelp_info->date_time, 0x00, sizeof(shelp_info->date_time));
	strcpy(date_time, buffer);

	if(date_time == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return date_time;
}

void appendHexRs(char* ptr)
{
	strcat(ptr, RS);
}

void appendHexGs(char* ptr)
{
	strcat(ptr, GS);
}

void capitalize(char* ptr)
{
	while(*ptr) {
		*ptr = toupper(*ptr);
		++ptr;
	}
}

int removeDecimalPoint(char* buffer)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if( buffer == NULL ) {
		debug_sprintf(szDbgMsg, "%s: ERROR! buffer is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	if( strchr(buffer, '.') == NULL ) {
		debug_sprintf(szDbgMsg, "%s: ERROR! no decimal point found", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	if( strlen(buffer) < 4 ) {
		debug_sprintf(szDbgMsg, "%s: ERROR! buffer less that 4", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	int len = strlen(buffer);
	char dup[len+1], newbuffer[len+1];
	char* ptr = dup;

	memset( dup, 0x00, sizeof(dup) );
	memset( newbuffer, 0x00, sizeof(newbuffer) );
	strcpy(dup, buffer);

	while( strlen(ptr) ) {
		if( *ptr != '.' ) {
			strncat(newbuffer, ptr, 1);
			ptr++;
		}
		else {
			ptr++;
		}
	}

	ptr = newbuffer;
	while(strlen(ptr)) {
		if(!isdigit(*ptr)) {
			debug_sprintf(szDbgMsg, "%s: ERROR! not a digit", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return EXIT_FAILURE;
		}
		ptr++;
	}

	memset(buffer, 0x00, strlen(buffer));
	strcpy(buffer, newbuffer);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

int addDecimalPoint( char* buffer )
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	double i;
	char c[50+1]={0};
	int len = strlen(buffer);
	char dup[len+1];
	char* ptr = dup;
	memset(dup, 0x00, sizeof(dup));

	if( buffer == NULL ) {
		debug_sprintf(szDbgMsg, "%s: ERROR! buffer is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	strcpy(dup, buffer);

	/*check for non digit*/
	while(*ptr) {
		if(!isdigit(*ptr)) {
			debug_sprintf(szDbgMsg, "%s: ERROR! not all digits", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return EXIT_FAILURE;
		}
		ptr++;
	}
	/*end*/

	if( strlen(dup) > sizeof(c) ) {
		debug_sprintf(szDbgMsg, "%s: ERROR! this function only support up to 50 bytes char's", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return EXIT_FAILURE;
	}

	ptr = dup;

	if( strlen(dup) < 11 ) {
		i = atoi(dup);
		i = i/100;
		sprintf(c,"%0.2f", i);
		ptr = c;
	}
	else {
		int plen = strlen(ptr);
		int j=0;

		for(j=0; j < plen-2; j++) {
			c[j] = *ptr++;
		}

		strncat(c, ".", 1);
		strncat(c, ptr, 2);
		ptr = c;

		while(*ptr=='0' && strlen(ptr) != 4) {
			ptr++;
		}
	}

	len = strlen(ptr);

	buffer = (char*)realloc(buffer, len + 1);
	memset(buffer, 0x00, len+1);
	strcpy(buffer, ptr);

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	return EXIT_SUCCESS;
}

void fillSpace(int size, char* buffer)
{
	memset(buffer, 0x00, (size-1));
	memset(buffer, ' ', (size-1));
}

void fillZero(int size, char* buffer)
{
	memset(buffer, 0x00, (size-1));
	memset(buffer, '0', (size-1));
}

void fillChar9(int size, char* buffer)
{
	memset(buffer, 0x00, (size-1));
	memset(buffer, '9', (size-1));
}

void fillChar8(int size, char* buffer)
{
	memset(buffer, 0x00, (size-1));
	memset(buffer, '8', (size-1));
}

void appendSpace(int size, char* buffer)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(buffer == NULL || strlen(buffer) == 0) {
		fillSpace(size, buffer);
		return;
	}

	if( strlen(buffer) == (size - 1) ) {
		return;
	}
	else if( strlen(buffer) >= size ) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Excess characters", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		exit(EXIT_FAILURE);
	}
	else {
		int len = 0, rem = 0;

		len = strlen(buffer);
		rem = (size) - len;
		char spc[rem];
		memset(spc, 0x00, sizeof(spc));
		memset(spc, ' ', (rem - 1));
		strncat(buffer, spc, (rem - 1));
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

void precedeSpace(int size, char* buffer)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if( buffer == NULL || strlen(buffer) == 0) {
		fillSpace(size, buffer);
		return;
	}

	if( strlen(buffer) == (size - 1) ) {
		return;
	}
	else if( strlen(buffer) >= size ) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Excess characters", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		exit(EXIT_FAILURE);
	}
	else {
		int len = 0, rem = 0;

		len = strlen(buffer);
		rem = (size) - len;
		char tmp[size];
		memset(tmp, 0x00, sizeof(tmp));
		memset(tmp, ' ', (rem-1));
		strncat(tmp, buffer, len);
		memset(buffer, 0x00, strlen(buffer));
		strncat(buffer, tmp, (size-1));
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

void precedeZero(int size, char* buffer)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(buffer == NULL || strlen(buffer) == 0) {
		fillZero(size, buffer);
		return;
	}

	if( strlen(buffer) == (size - 1) ) {
		return;
	}
	else if( strlen(buffer) >= size ) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Excess characters", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		exit(EXIT_FAILURE);
	}
	else {
		int len = 0, rem = 0;

		len = strlen(buffer);
		rem = (size) - len;
		char tmp[size];
		memset(tmp, 0x00, sizeof(tmp));
		memset(tmp, '0', (rem-1));
		strncat(tmp, buffer, len);
		memset(buffer, 0x00, strlen(buffer));
		strncat(buffer, tmp, (size-1));
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

void printToFile(char* filename, char* buffer)
{
	FILE * pFile;
	pFile = fopen (filename,"w");

	if (pFile!=NULL) {
		fputs (buffer, pFile);
		fclose (pFile);
	}
}

void incrementStan(sGLOBALS* GLOBAL, _sSCA_XML_REQUEST *req_xml_info)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);
	char* pFUNCTION_TYPE = getXmlTagValue(eFUNCTION_TYPE, req_xml_info);

	if (pFUNCTION_TYPE == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! FUNCTION_TYPE is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INTERNAL_ERROR;
		return;
	}

	if( strncmp(pFUNCTION_TYPE, FUNCTION_TYPE.PAYMENT, strlen(FUNCTION_TYPE.PAYMENT)) == COMPARE_OK ) {

		if (pCOMMAND == NULL) {
			debug_sprintf(szDbgMsg, "%s: ERROR! COMMAND is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
			GLOBAL->http.status_code = eHTTP_OK;
			GLOBAL->appErrCode = eERR_INTERNAL_ERROR;
			return;
		}

		if( strncmp(pCOMMAND, COMMAND.SIGNATURE, strlen(COMMAND.SIGNATURE)) != COMPARE_OK &&
			strncmp(pCOMMAND, COMMAND.TOR_REQ,   strlen(COMMAND.TOR_REQ))   != COMPARE_OK	    ) {
			char cSTAN[6+1] = {0};
			unsigned long long iSTAN=0;

			/*
			 * Praveen_P1: 17 Feb 2016
			 * Calling getEnvFile call instead of local
			 * call getEnv, in the getEnv function, we have
			 * local buffer which is getting returned. if two calls
			 * are made at same time, may get empty value from this function
			 */
			//strcpy(cSTAN, getEnv("DHI", "STAN"));

			getEnvFile("DHI", "STAN", cSTAN, (sizeof(cSTAN) - 1) );

			if (strlen(cSTAN) <= 0 || strlen(cSTAN)>sizeof(cSTAN)-1) {
				debug_sprintf(szDbgMsg, "%s: ERROR!", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
				GLOBAL->http.status_code = eHTTP_OK;
				GLOBAL->appErrCode = eERR_INTERNAL_ERROR;
				return;
			}

			iSTAN = atoi(cSTAN);
			if(iSTAN<=0) {
				debug_sprintf(szDbgMsg, "%s: ERROR!", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				//GLOBAL->http.status_code = eHTTP_INTERNAL_SERVER_ERROR;
				GLOBAL->http.status_code = eHTTP_OK;
				GLOBAL->appErrCode = eERR_INTERNAL_ERROR;
				return;
			}

			iSTAN++;
			if(iSTAN>999999) {
				iSTAN=1;
			}

			memset(cSTAN, 0x00, sizeof(cSTAN));
			sprintf(cSTAN,"%llu", iSTAN);
			setEnv("DHI", "STAN", cSTAN);
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

void maskPanTrack(char* pan)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if (pan == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

	if (strlen(pan) <= 0) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 0", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

	if (strlen(pan) <= 4) {
		debug_sprintf(szDbgMsg, "%s: ERROR! must be greater than 4 digit", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

	char* endptr = NULL;
	int len = 0, i = 0;

	if ((endptr = strchr(pan, '=')) != NULL) {
		len = endptr - pan;

		for (i=0; i<len-4; i++) {
			*pan++ = 'X';
		}
	}
	else {
		len = strlen(pan);
		for (i=0; i<len-4; i++) {
			*pan++ = 'X';
		}
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

void createTextFile(char* filename, bool bNewFile)
{
	FILE * pFile;

	if (bNewFile == true)
		pFile = fopen (filename,"w");
	else
		pFile = fopen (filename,"a");

	if (pFile!=NULL) {
		fclose (pFile);
	}
}

bool isFileExist(char* filename)
{
#if 0
	FILE *pFile = fopen(filename, "r");

	if (pFile != NULL) {

		fclose(pFile);
		return true;
	}

	return false;
#endif
	int			rv			= 0;
	bool		brv			= true;
	struct stat	fileStatus;

	/*	debug_sprintf(szDbgMsg, "%s: Enter", __FUNCTION__);
	APP_TRACE(szDbgMsg);*/

	memset(&fileStatus, 0x00, sizeof(fileStatus));

	rv = stat(filename, &fileStatus);
	if(rv < 0)
	{
		if(errno == EACCES)
		{
			debug_sprintf(szDbgMsg, "%s: Not enough search permissions for %s",__FUNCTION__, filename);
		}
		else if (errno == ENAMETOOLONG)
		{
			debug_sprintf(szDbgMsg, "%s: Name %s is too long", __FUNCTION__,filename);
		}
		else
		{
			/* For ENOENT/ENOTDIR/ELOOP */
			debug_sprintf(szDbgMsg,"%s: File - [%s] (Name not correct)/(file doesnt exist)", __FUNCTION__, filename);
		}
		APP_TRACE(szDbgMsg);
		brv = false;
	}
	else
	{
		/* Check if the file is a regular file */
		if(S_ISREG(fileStatus.st_mode))
		{
			/* Check if the file is not empty */
			if(fileStatus.st_size <= 0)
			{
				debug_sprintf(szDbgMsg, "%s: File [%s] is empty", __FUNCTION__,filename);
				APP_TRACE(szDbgMsg);
				brv = false;
			}
		}
		else
		{
			debug_sprintf(szDbgMsg, "%s: Error: File [%s] is not a regular file", __FUNCTION__, filename);
			APP_TRACE(szDbgMsg);
			brv = false;
		}
	}

/*	debug_sprintf(szDbgMsg, "%s: Returning %d", __FUNCTION__, rv);
	APP_TRACE(szDbgMsg);*/

	return brv;
}

void removeTextFile(char* filename)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if( remove( filename ) != 0 ) {
		debug_sprintf(szDbgMsg, "%s: WARNING! file not deleted", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

int getTxtFileBufferLength(char* fileName)
{
	//debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	int iTextLen = 0;

	FILE* pTmp;
	pTmp = fopen(fileName, "r");

	if (pTmp != NULL) {

		while (fgetc(pTmp)!= EOF) {

			iTextLen++;

		}

		if (feof(pTmp)){
			//debug_sprintf(szDbgMsg, "%s: End of file", __FUNCTION__);
			//APP_TRACE(szDbgMsg);
		}
		else {
			//debug_sprintf(szDbgMsg, "%s: ERROR! EOF not reached", __FUNCTION__);
			//APP_TRACE(szDbgMsg);

			fclose(pTmp);
			return 0;
		}
		fclose(pTmp);
	} else {
        //printf("ERROR! %s not found!", fileName);
        //debug_sprintf(szDbgMsg, "ERROR! %s not found!", fileName);
        //APP_TRACE(szDbgMsg);
		return 0;
	}

	if (iTextLen <= 0) {
		//debug_sprintf(szDbgMsg, "%s: ERROR! text length is 0", __FUNCTION__);
		//APP_TRACE(szDbgMsg);

		return 0;
	}

	//debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	//APP_TRACE(szDbgMsg);

	return iTextLen;
}

// Amount field is formatted as "SMMDDYYhhmmss0.00" where S is the entry type (S,M,E,C,F) followed by date & time + amount.
void saveVoidData(char* pCtroutd, sGLOBALS* GLOBAL, _sVTV_FIELDS *svbninfo, _sSCA_XML_REQUEST *req_xml_info)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* 	fileName 			= TEXTFILE_VOIDS;
	char	szAcctNum[30]		= "";

	if (pCtroutd == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! CTROUTD is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

	if (strlen(pCtroutd) <= 0 || strlen(pCtroutd) > 9) {
		debug_sprintf(szDbgMsg, "%s: ERROR! CTROUTD bytes 0", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

	if (strlen(pCtroutd) > 9) {
		debug_sprintf(szDbgMsg, "%s: ERROR! CTROUTD bytes over", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}
    
    debug_sprintf(szDbgMsg, "save void data CTROUTD is %s.", pCtroutd);
    APP_TRACE(szDbgMsg);

	char* pFUNCTION_TYPE = getXmlTagValue(eFUNCTION_TYPE, req_xml_info);
	char* pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);
	char* pTRACK_DATA = getXmlTagValue(eTRACK_DATA, req_xml_info);
	char* pTRACKIND = getXmlTagValue(eTRACK_IND, req_xml_info);
	char* pACCT_NUM = getXmlTagValue(eACCT_NUM, req_xml_info);
	char* pTRANS_AMOUNT = getXmlTagValue(eTRANS_AMOUNT, req_xml_info);
	char *pCARD_TOKEN = getXmlTagValue(eCARD_TOKEN, req_xml_info);

	char cCtroutd[9+1] = {0};
	char cPAN[76+1] = {0};
	char cAmount[15+1] = {0};
	char cPaymentMedia[20+1] = {0};

    strcpy(cCtroutd, "");
	strncat(cCtroutd, pCtroutd, sizeof(cCtroutd)-1);
	precedeZero(sizeof(cCtroutd), cCtroutd);

	if (pFUNCTION_TYPE == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! FUNCTION_TYPE is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

	if (strncmp(pFUNCTION_TYPE, FUNCTION_TYPE.PAYMENT, strlen(FUNCTION_TYPE.PAYMENT)) == COMPARE_OK) {
		if (pCOMMAND == NULL) {
			debug_sprintf(szDbgMsg, "%s: ERROR! COMMAND is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return;
		}

		if (strncmp(pCOMMAND, "SALE", 4) == COMPARE_OK
		|| strncmp(pCOMMAND, COMMAND.POST_AUTH, strlen(COMMAND.POST_AUTH)) == COMPARE_OK
		|| strncmp(pCOMMAND, COMMAND.CREDIT, strlen(COMMAND.CREDIT)) == COMPARE_OK
		|| strncmp(pCOMMAND, COMMAND.ACTIVATE, strlen(COMMAND.ACTIVATE)) == COMPARE_OK
		|| strncmp(pCOMMAND, COMMAND.ADD_VALUE, strlen(COMMAND.ADD_VALUE)) == COMPARE_OK
		|| strncmp(pCOMMAND, COMMAND.REMOVE_VALUE, strlen(COMMAND.REMOVE_VALUE)) == COMPARE_OK
		|| strncmp(pCOMMAND, COMMAND.COMPLETION, strlen(COMMAND.COMPLETION)) == COMPARE_OK
		|| strncmp(pCOMMAND, COMMAND.DEACTIVATE, strlen(COMMAND.DEACTIVATE)) == COMPARE_OK)
		{
			if (pTRACK_DATA != NULL) {
				strcpy(cPAN, "");

				/*
				 * Praveen_P1: 01 Dec 2015
				 * While pikcing up the void data to post the transaction
				 * application looking for = symbol to parse for PAN number, so
				 * if Track1 only card used, parsing will fail and account number will not
				 * contain = seperator
				 * so when we have Track1 data we will store Account number taken from Track1
				 */
				if(pTRACKIND != NULL && (strcmp(pTRACKIND, "1") == 0))
				{
					memset(szAcctNum, 0x00, sizeof(szAcctNum));
					getPANFromTrackData(pTRACK_DATA, atoi(pTRACKIND), szAcctNum, (sizeof(szAcctNum) - 1));
					strcpy(cPAN, szAcctNum);
					strcat(cPAN, "=");
					strcat(cPAN, "49"); //Praveen_P1: Hardcoding expiry data and year since it does not get picked
					strcat(cPAN, "12");
				}
				else//For Track2, we will store as it is
				{
					strncat(cPAN, pTRACK_DATA, strlen(pTRACK_DATA));
				}

				/*
				char* endptr;
				endptr = strchr(pTRACK_DATA, '=');
				int len = endptr - pTRACK_DATA;
				strncpy(cPAN, pTRACK_DATA, len);
				 */
			}
			else{
				if (pACCT_NUM != NULL) {
					char* pEXP_MONTH = getXmlTagValue(eEXP_MONTH, req_xml_info);
					char* pEXP_YEAR = getXmlTagValue(eEXP_YEAR, req_xml_info);

					if (pEXP_MONTH == NULL && pEXP_YEAR == NULL) {
						debug_sprintf(szDbgMsg, "%s: ERROR!", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						return;
					}

					strcpy(cPAN, pACCT_NUM);
					strcat(cPAN, "=");
					strcat(cPAN, pEXP_YEAR);
					strcat(cPAN, pEXP_MONTH);

					char* pCVV2 = getXmlTagValue(eCVV2, req_xml_info);
					if (pCVV2 != NULL) {
						strcat(cPAN, pCVV2);
					}
				}

				else if (pCARD_TOKEN != NULL)
				{
					char* pEXP_MONTH = getXmlTagValue(eEXP_MONTH, req_xml_info);
					char* pEXP_YEAR = getXmlTagValue(eEXP_YEAR, req_xml_info);

					if (pEXP_MONTH == NULL && pEXP_YEAR == NULL) {
						debug_sprintf(szDbgMsg, "%s: ERROR!", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						return;
					}

					strcpy(cPAN, "C");
					strcat(cPAN,pCARD_TOKEN);
					strcat(cPAN, "=");
					strcat(cPAN, pEXP_YEAR);
					strcat(cPAN, pEXP_MONTH);
				}
				else {
					debug_sprintf(szDbgMsg, "%s: ERROR! ACCT_NUM ,TRACK_DATA ,CARD_TOKEN is NULL", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					debug_sprintf(szDbgMsg, "%s: ERROR! No Data To be stored in [%s] File", __FUNCTION__,TEXTFILE_VOIDS);
					APP_TRACE(szDbgMsg);
					return;
				}
			}

			// All transaction have the amount encoded with "SMMDDYYhhmmss0.00".
			// If PRESENT_FLAG not available then assume swiped.
			// If date or time not available then use "000000".
			char *pPRESENT_FLAG = getXmlTagValue(ePRESENT_FLAG, req_xml_info);

			if (pPRESENT_FLAG == NULL)
			{
				debug_sprintf(szDbgMsg, "%s: ERROR! PRESENT_FLAG is NULL", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				strcpy(cAmount, "S");       // set something as default
			}
			else
			{
				if (strncmp(pPRESENT_FLAG, SWIPED_ENTRY, 1) == COMPARE_OK)
				{
					strcpy(cAmount, "S");
				}
				else if (strncmp(pPRESENT_FLAG, MANUAL_ENTRY, 1) == COMPARE_OK)
				{
					strcpy(cAmount, "M");
				}
				else if (strncmp(pPRESENT_FLAG, PROXIMITY_ENTRY, 1) == COMPARE_OK)
				{
					strcpy(cAmount, "R");
				}
				else if (strncmp(pPRESENT_FLAG, EMV_CHIP_ENTRY, 1) == COMPARE_OK)
				{
					strcpy(cAmount, "E");
				}
				else if (strncmp(pPRESENT_FLAG, EMV_CTLS_ENTRY, 1) == COMPARE_OK)
				{
					strcpy(cAmount, "C");
				}
				else if (strncmp(pPRESENT_FLAG, EMV_FALLBACK_ENTRY, 1) == COMPARE_OK)
				{
					strcpy(cAmount, "F");
				}
			}

			//append with local date and time
			char* pLocalTransDate = getVantivBNResponse(eBN12, svbninfo);
			char* pLocalTransTime = getVantivBNResponse(eBN13, svbninfo);

			if (pLocalTransDate != NULL)
			{
				if (strlen(pLocalTransDate) != 6)
				{
					debug_sprintf(szDbgMsg, "Error! Local Transaction Date not 6 bytes!");
					APP_TRACE(szDbgMsg);
				}

				strncat(cAmount, pLocalTransDate, 6);
			}
			else
			{
				debug_sprintf(szDbgMsg, "Error! Local Transaction Date is NULL");
				APP_TRACE(szDbgMsg);
				strncat(cAmount, "000000", 6);      // set up a dummy value
			}

			if (pLocalTransTime != NULL)
			{
				if (strlen(pLocalTransTime) != 6)
				{
					debug_sprintf(szDbgMsg, "Error! Local Time not 6 bytes!");
					APP_TRACE(szDbgMsg);
				}

				strncat(cAmount, pLocalTransTime, 6);
			}
			else
			{
				debug_sprintf(szDbgMsg, "Error! Local Time is NULL");
				APP_TRACE(szDbgMsg);
				strncat(cAmount, "000000", 6);      // set up a dummy value
			}

			if (pTRANS_AMOUNT == NULL)
				strncat(cAmount, "0.00", 4);
			else
				strncat(cAmount, pTRANS_AMOUNT, strlen(pTRANS_AMOUNT)); //append the amount
			debug_sprintf(szDbgMsg, "Save void amount is %s.", cAmount);
			APP_TRACE(szDbgMsg);

			char* pPaymentMedia = VHI_PAYMENT_MEDIA(svbninfo, req_xml_info);

			memset(cPaymentMedia, 0x00, sizeof(cPaymentMedia));

			if(pPaymentMedia != NULL)
			{
				if(strcasecmp(pPaymentMedia, "DEBIT") == COMPARE_OK)
				{
					debug_sprintf(szDbgMsg, "Payment Media is DEBIT, so storing based on PAN");
					APP_TRACE(szDbgMsg);

					if(cPAN[0] == '4')
					{
						strcpy(cPaymentMedia, "VISA");
					}
					if(cPAN[0] == '5')
					{
						strcpy(cPaymentMedia, "MC");
					}
					if(cPAN[0] == '6')
					{
						strcpy(cPaymentMedia, "DISC");
					}
					if(cPAN[0] == '3')
					{
						strcpy(cPaymentMedia, "AMEX");
					}
				}
				else
				{
					strcpy(cPaymentMedia, pPaymentMedia);
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "Did not get Payment media, storing based on PAN");
				APP_TRACE(szDbgMsg);

				if(cPAN[0] == '4')
				{
					strcpy(cPaymentMedia, "VISA");
				}
				if(cPAN[0] == '5')
				{
					strcpy(cPaymentMedia, "MC");
				}
				if(cPAN[0] == '6')
				{
					strcpy(cPaymentMedia, "DISC");
				}
				if(cPAN[0] == '3')
				{
					strcpy(cPaymentMedia, "AMEX");
				}

			}

			debug_sprintf(szDbgMsg, "Save payment media is %s.", cPaymentMedia);
			APP_TRACE(szDbgMsg);

			//createTextFile(fileName, false); //Praveen_P1: I think it is not required, anyhow fopen with an option creates the file if it does not exist

			FILE* pFile;
			pFile = fopen(fileName, "a");

			if (pFile != NULL) {
				fputs(cCtroutd, pFile);
				fputs(" ", pFile);
				fputs(cPAN, pFile);
				fputs(" ", pFile);
				fputs(cAmount, pFile);
				fputs(" ", pFile);
				fputs(cPaymentMedia, pFile);
				fputs("|", pFile);

				fclose(pFile);
			}
			else {
				debug_sprintf(szDbgMsg, "%s: ERROR! file not exist", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return;
			}
		}
        
        // ++++++++++ SPECIAL CASE FOR PRE_AUTH ++++++++++
		else if (strncmp(pCOMMAND, COMMAND.PRE_AUTH, strlen(COMMAND.PRE_AUTH)) == COMPARE_OK) {
			if (pTRACK_DATA != NULL) {
				strcpy(cPAN, pTRACK_DATA);
			}
			else {
				if (pACCT_NUM != NULL) {
					char* pEXP_YEAR = getXmlTagValue(eEXP_YEAR, req_xml_info);
					char* pEXP_MONTH = getXmlTagValue(eEXP_MONTH, req_xml_info);
					char* pCVV2 = getXmlTagValue(eCVV2, req_xml_info);

					if (pEXP_YEAR == NULL || pEXP_MONTH == NULL) {
						debug_sprintf(szDbgMsg, "%s: ERROR! EXP_YEAR, EXP_MONTH is NULL", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						return;
					}

					strcpy(cPAN, pACCT_NUM);
					strcat(cPAN, "=");
					strcat(cPAN, pEXP_YEAR);
					strcat(cPAN, pEXP_MONTH);

					if (pCVV2 != NULL) {
						strncat(cPAN, pCVV2, strlen(pCVV2));
					}
				}
				else {
					debug_sprintf(szDbgMsg, "%s: ERROR! TRACK_DATA, ACCT_NUM is NULL", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					return;
				}
			}

			if (pTRANS_AMOUNT == NULL) {
				debug_sprintf(szDbgMsg, "%s: WARNING! TRANS_AMOUNT is NULL", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				/* save present flag for pre auth */
				char *pPRESENT_FLAG = getXmlTagValue(ePRESENT_FLAG, req_xml_info);

				if (pPRESENT_FLAG == NULL) {
					debug_sprintf(szDbgMsg, "%s: ERROR! PRESENT_FLAG is NULL", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else {
					if (strncmp(pPRESENT_FLAG, SWIPED_ENTRY, 1) == COMPARE_OK) {
						strcpy(cAmount, "S");
					}
					else if (strncmp(pPRESENT_FLAG, MANUAL_ENTRY, 1) == COMPARE_OK) {
						strcpy(cAmount, "M");
					}
                    else if (strncmp(pPRESENT_FLAG, PROXIMITY_ENTRY, 1) == COMPARE_OK) {
                        strcpy(cAmount, "R");
                    }
                    else if (strncmp(pPRESENT_FLAG, EMV_CHIP_ENTRY, 1) == COMPARE_OK) {
                        strcpy(cAmount, "E");
                    }
                    else if (strncmp(pPRESENT_FLAG, EMV_CTLS_ENTRY, 1) == COMPARE_OK) {
                        strcpy(cAmount, "C");
                    }
                    else if (strncmp(pPRESENT_FLAG, EMV_FALLBACK_ENTRY, 1) == COMPARE_OK) {
                        strcpy(cAmount, "F");
                    }
				}
				/* end */

				//append with local date and time
				char* pLocalTransDate = getVantivBNResponse(eBN12, svbninfo);
				char* pLocalTransTime = getVantivBNResponse(eBN13, svbninfo);

				if (pLocalTransDate != NULL || pLocalTransTime != NULL) {
					if (strlen(pLocalTransDate) != 6 || strlen(pLocalTransTime) != 6) {
						debug_sprintf(szDbgMsg, "Error! Local Transaction Date/Time not 6 bytes!");
						APP_TRACE(szDbgMsg);
					}

					strncat(cAmount, pLocalTransDate, 6);
					strncat(cAmount, pLocalTransTime, 6);
				} else {
					debug_sprintf(szDbgMsg, "Error! Local Transaction Date/Time is NULL");
					APP_TRACE(szDbgMsg);
				}
				//end

				strncat(cAmount, "0.00", 4);
			}
			else {
				/* save present flag for pre auth */
				char *pPRESENT_FLAG = getXmlTagValue(ePRESENT_FLAG, req_xml_info);

				if (pPRESENT_FLAG == NULL) {
					debug_sprintf(szDbgMsg, "%s: ERROR! PRESENT_FLAG is NULL", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				else {
					//copy S or M or R in cAmount before putting amount
					if (strncmp(pPRESENT_FLAG, SWIPED_ENTRY, 1) == COMPARE_OK) {
						strcpy(cAmount, "S");
					}
					else if (strncmp(pPRESENT_FLAG, MANUAL_ENTRY, 1) == COMPARE_OK) {
						strcpy(cAmount, "M");
					}
					else if (strncmp(pPRESENT_FLAG, PROXIMITY_ENTRY, 1) == COMPARE_OK) {
						strcpy(cAmount, "R");
					}
                    else if (strncmp(pPRESENT_FLAG, EMV_CHIP_ENTRY, 1) == COMPARE_OK) {
                        strcpy(cAmount, "E");
                    }
                    else if (strncmp(pPRESENT_FLAG, EMV_CTLS_ENTRY, 1) == COMPARE_OK) {
                        strcpy(cAmount, "C");
                    }
                    else if (strncmp(pPRESENT_FLAG, EMV_FALLBACK_ENTRY, 1) == COMPARE_OK) {
                        strcpy(cAmount, "F");
                    }
					//end
				}
				/* end */

				//append with local date and time
				char* pLocalTransDate = getVantivBNResponse(eBN12, svbninfo);
				char* pLocalTransTime = getVantivBNResponse(eBN13, svbninfo);

				if (pLocalTransDate != NULL || pLocalTransTime != NULL) {
					if (strlen(pLocalTransDate) != 6 || strlen(pLocalTransTime) != 6) {
						debug_sprintf(szDbgMsg, "Error! Local Transaction Date/Time not 6 bytes!");
						APP_TRACE(szDbgMsg);
					}

					strncat(cAmount, pLocalTransDate, 6);
					strncat(cAmount, pLocalTransTime, 6);
				} else {
					debug_sprintf(szDbgMsg, "Error! Local Transaction Date/Time is NULL");
					APP_TRACE(szDbgMsg);
				}
				//end

				strncat(cAmount, pTRANS_AMOUNT, strlen(pTRANS_AMOUNT)); //append the amount
			}

			char* pPaymentMedia = VHI_PAYMENT_MEDIA(svbninfo, req_xml_info);

			if(pPaymentMedia != NULL)
			{
				memset(cPaymentMedia, 0x00, sizeof(cPaymentMedia));
				strcpy(cPaymentMedia, pPaymentMedia);
			}
			else
			{
				debug_sprintf(szDbgMsg, "Did not get Payment media, storing based on PAN");
				APP_TRACE(szDbgMsg);

				if(cPAN[0] == '4')
				{
					strcpy(cPaymentMedia, "VISA");
				}
				if(cPAN[0] == '5')
				{
					strcpy(cPaymentMedia, "MC");
				}
				if(cPAN[0] == '6')
				{
					strcpy(cPaymentMedia, "DISC");
				}
				if(cPAN[0] == '3')
				{
					strcpy(cPaymentMedia, "AMEX");
				}

			}

			//createTextFile(fileName, false); //Praveen_P1: I think it is not required, anyhow fopen with an option creates the file if it does not exist

			FILE* pFile;
			pFile = fopen(fileName, "a");

			if (pFile != NULL) {
				fputs(cCtroutd, pFile);
				fputs(" ", pFile);
				fputs(cPAN, pFile);
				fputs(" ", pFile);
				fputs(cAmount, pFile);
				fputs(" ", pFile);
				fputs(cPaymentMedia, pFile);
				fputs("|", pFile);

				fclose(pFile);
			}
			else {
				debug_sprintf(szDbgMsg, "%s: ERROR! file not exist", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return;
			}
		}
	
        // ++++++++++ SPECIAL CASE FOR ADD TIP ++++++++++
        else if (strncmp(pCOMMAND, COMMAND.ADD_TIP, strlen(COMMAND.ADD_TIP)) == COMPARE_OK ) {
			char* pCTROUTD = getXmlTagValue(eCTROUTD, req_xml_info);
			char* pTIP_AMOUNT = getXmlTagValue(eTIP_AMOUNT, req_xml_info);

			if (pCTROUTD == NULL) {
				debug_sprintf(szDbgMsg, "%s: ERROR! CTROUTD is NULL", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return;
			}

			if (pTIP_AMOUNT == NULL) {
				debug_sprintf(szDbgMsg, "%s: ERROR! TIP_AMOUNT is NULL", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return;
			}

			memset(cPAN, 0x00, sizeof(cPAN));
			memset(cAmount, 0x00, sizeof(cAmount));

			getVoidData(pCTROUTD, cPAN, cAmount, NULL, GLOBAL);

			double dtip = atof(pTIP_AMOUNT);
			double damount = atof(cAmount);
			char total[9+1] = {0};
			sprintf(total, "%0.2f", damount + dtip);

			createTextFile(fileName, false);

			FILE* pFile;
			pFile = fopen(fileName, "a");

			if (pFile != NULL) {
				fputs(cCtroutd, pFile);
				fputs(" ", pFile);
				fputs(cPAN, pFile);
				fputs(" ", pFile);
				fputs(total, pFile);
				fputs("|", pFile);

				fclose(pFile);
			}
			else {
				debug_sprintf(szDbgMsg, "%s: ERROR! file not exist", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return;
			}
		}
	}
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! for PAYMENT only", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

void getVoidData(char* key, char* getPan, char* getAmount, char* getPaymentMedia, sGLOBALS* GLOBAL)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if (key == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! key is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

	if (strlen(key) <= 0) {
		debug_sprintf(szDbgMsg, "%s: ERROR! key is 0", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}
    
    debug_sprintf(szDbgMsg, "get void data CTROUTD is %s.", key);
    APP_TRACE(szDbgMsg);

	char* fileName = TEXTFILE_VOIDS;

	int iTextLen = 0;
	char ctroutd[9+1] = {0};
	char result[128] = {0};

    strcpy(ctroutd, key);
	precedeZero(sizeof(ctroutd), ctroutd);
	createTextFile(fileName, false);
	iTextLen = getTxtFileBufferLength(fileName);

	if (iTextLen <= 0) {
		debug_sprintf(szDbgMsg, "%s: ERROR! Text length 0", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

	FILE* pFile;
	pFile = fopen(fileName, "r");

	if (pFile != NULL) {
		char buffer[iTextLen+1];
		memset(buffer, 0x00, sizeof(buffer));

		if (fgets(buffer, sizeof(buffer), pFile) != NULL){
			char* ptr = buffer;
			char* endptr = NULL;
			int i = 0;

			while (strlen(ptr)) {
				endptr = strchr(ptr, '|');

				i = endptr - ptr;

				if (strncmp(ptr, ctroutd, strlen(ctroutd)) == COMPARE_OK) {
                    strcpy(result, "");
					strncat(result, ptr, i);
					break;
				}

				ptr += i+1;
			}
		}
		else {
			debug_sprintf(szDbgMsg, "%s: ERROR!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		fclose(pFile);
	} else {
		debug_sprintf(szDbgMsg, "%s: ERROR! opening file", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return;
	}

	if (strlen(result) <= 0) {
		debug_sprintf(szDbgMsg, "%s: ERROR! result is 0", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INVALID_CTROUTD;

		return;
	}

	char* ptr = result;
	//skip ctroutd
	while(*ptr) {
		if (*ptr != ' ')
			ptr++;
		else
			break;
	}

	if( *ptr == ' ')
		ptr++;

	//copy pan
	if (getPan != NULL) {
		while (*ptr) {
			if (*ptr != ' ')
				*getPan++ = *ptr++;
			else
				break;
		}
	}
	else {
		while (*ptr++ != ' ') {}
	}

	if (*ptr == ' ')
		ptr++;

	//copy amount
	if (getAmount != NULL) {
        char *temp= getAmount;
		while (*ptr) {
			if (*ptr != ' ')
				*getAmount++ = *ptr++;
			else
				break;
		}
        debug_sprintf(szDbgMsg, "amount = %s.", temp);
        APP_TRACE(szDbgMsg);
	}
	else {
		while (*ptr++ != ' ') {}
	}

	debug_sprintf(szDbgMsg, "ptr = [%s]", ptr);
	APP_TRACE(szDbgMsg); //Praveen_P1: Remove this later

	/*
	 * Praveen_P1: we are storing payment media in to the void records
	 * we need this field to determine inclusion of G035 group in the request
	 * this field wont be present prior to 1.00.04 B5 release
	 * so if space is there after amount field then we have payment media
	 * otherwise not
	 */
	if (*ptr == ' ') {
		ptr++;
	}

	//copy Payment Media
	if (getPaymentMedia != NULL) {
		char *temp= getPaymentMedia;
		while (*ptr) {
			if (*ptr != ' ')
				*getPaymentMedia++ = *ptr++;
			else
				break;
		}
		debug_sprintf(szDbgMsg, "payment media = %s.", temp);
		APP_TRACE(szDbgMsg);
	}
	else {
		while (*ptr++ != ' ') {}
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}


void saveAdjustmentData(char* CTROUTD, char* approvedAmount, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if (GCONFIG->atype1 == eRESTAURANT_TYPE || GCONFIG->adjcapable == 'Y') {
		char ctroutd[9+1] = {0};
		strcpy(ctroutd, CTROUTD);
		precedeZero(sizeof(ctroutd), ctroutd);

		char* fileName = TEXTFILE_TIPADJUST;

		char* pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);
		char* pPAYMENT_TYPE = getXmlTagValue(ePAYMENT_TYPE, req_xml_info);

		if (strncmp(pPAYMENT_TYPE, COMMAND.CREDIT, strlen(COMMAND.CREDIT)) == COMPARE_OK) {
			if (strncmp(pCOMMAND, "SALE", 4) == COMPARE_OK) {
				char* pTRACK_DATA = getXmlTagValue(eTRACK_DATA, req_xml_info);
				char* pACCT_NUM = getXmlTagValue(eACCT_NUM, req_xml_info);
				char* pEXP_YEAR = getXmlTagValue(eEXP_YEAR, req_xml_info);
				char* pEXP_MONTH = getXmlTagValue(eEXP_MONTH, req_xml_info);
				char* pCVV2 = getXmlTagValue(eCVV2, req_xml_info);
				char* pTRANS_AMOUNT = getXmlTagValue(eTRANS_AMOUNT, req_xml_info);
				char* pPRESENT_FLAG = getXmlTagValue(ePRESENT_FLAG, req_xml_info);

				char newPAN[76+1] = {0};
				char amount[15+1] = {0};

				if (pTRANS_AMOUNT != NULL) {
					if (approvedAmount != NULL) {
						strcpy(amount, approvedAmount);
					}
					else {
						strcpy(amount, pTRANS_AMOUNT);
					}
				}
				else {
					debug_sprintf(szDbgMsg, "%s: ERROR! TRANS_AMOUNT is NULL", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					return;
				}

				if (pTRACK_DATA != NULL) {
					strcpy(newPAN, pTRACK_DATA);
				}
				else {
					if (pACCT_NUM != NULL) {
						strcpy(newPAN, pACCT_NUM);

						if (pEXP_YEAR == NULL) {
							debug_sprintf(szDbgMsg, "%s: ERROR! EXP_YEAR is NULL", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							return;
						}

						if (pEXP_MONTH == NULL) {
							debug_sprintf(szDbgMsg, "%s: ERROR! EXP_MONTH is NULL", __FUNCTION__);
							APP_TRACE(szDbgMsg);

							return;
						}

						strncat (newPAN, "=", 1);
						strncat (newPAN, pEXP_YEAR, strlen(pEXP_YEAR));
						strncat (newPAN, pEXP_MONTH, strlen(pEXP_MONTH));

						if (pCVV2 != NULL) {
							strncat (newPAN, pCVV2, strlen(pCVV2));
						}
					}
					else {
						debug_sprintf(szDbgMsg, "%s: ERROR! ACCT_NUM is NULL", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						return;
					}
				}

				createTextFile(fileName, false);

				FILE* pFile;
				pFile = fopen(fileName, "a");

				if (pPRESENT_FLAG == NULL) {
					debug_sprintf(szDbgMsg, "%s: ERROR! PRESENT_FLAG is NULL", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}

				if (pFile != NULL) {
					fputs(ctroutd, pFile);
					fputs(" ", pFile);
					fputs(pPRESENT_FLAG, pFile);
					fputs(" ", pFile);
					fputs(newPAN, pFile);
					fputs(" ", pFile);
					fputs(amount, pFile);
					fputs("|", pFile);
					fclose(pFile);
				} else {
					debug_sprintf(szDbgMsg, "%s: ERROR! file not exist", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					return;
				}
			}
			else if (strncmp(pCOMMAND, COMMAND.ADD_TIP, strlen(COMMAND.ADD_TIP)) == COMPARE_OK) {
				char* pCTROUTD = getXmlTagValue(eCTROUTD, req_xml_info);
				char pFlag[1+1] = {0};
				char newPAN[76+1] = {0};
				char amount[15+1] = {0};

				if (pCTROUTD != NULL) {
					getAdjustmentData(pFlag, newPAN, amount, GLOBAL, req_xml_info);

					if (strlen(newPAN) <= 0 || strlen(amount) <= 0) {
						debug_sprintf(szDbgMsg, "%s: WARNING!", __FUNCTION__);
						APP_TRACE(szDbgMsg);
					}
				}
				else {
					debug_sprintf(szDbgMsg, "%s: WARNING!", __FUNCTION__);
					APP_TRACE(szDbgMsg);
				}
				/*end*/

				char* pTIP_AMOUNT = getXmlTagValue(eTIP_AMOUNT, req_xml_info);
				if (pTIP_AMOUNT == NULL) {
					debug_sprintf(szDbgMsg, "%s: ERROR! TIP_AMOUNT is NULL", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					return;
				}

				char ctip[9+1] = {0};
                strcpy(ctip, "");
				strncat(ctip, pTIP_AMOUNT, sizeof(ctip)-1);

				double dtip = atof(pTIP_AMOUNT);
				double damount = atof(amount);
				char total[9+1] = {0};
				sprintf(total, "%0.2f", damount + dtip);

				createTextFile(fileName, false);

				FILE* pFile;
				pFile = fopen(fileName, "a");

				if (pFile != NULL) {
					fputs(CTROUTD, pFile);
					fputs(" ", pFile);
					fputs(pFlag, pFile);
					fputs(" ", pFile);
					fputs(newPAN, pFile);
					fputs(" ", pFile);
					fputs(total, pFile);
					fputs("|", pFile);
					fclose(pFile);
				} else {
					debug_sprintf(szDbgMsg, "%s: ERROR! file not exist", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					return;
				}
			}
		}
	}
	/*end*/

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}

void getAdjustmentData(char* getPFlag, char* getPAN, char* getAmount, sGLOBALS* GLOBAL, _sSCA_XML_REQUEST *req_xml_info)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	char* fileName = TEXTFILE_TIPADJUST;

	int iTextLen = 0;
	char ctroutd[9+1] = {0};
	char result[128+1] = {0};
	char* pCTROUTD = getXmlTagValue(eCTROUTD, req_xml_info);

	if (pCTROUTD == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! CTROUTD is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return;
	}

	strcpy(ctroutd, pCTROUTD);
	precedeZero(sizeof(ctroutd), ctroutd);

	iTextLen = getTxtFileBufferLength(fileName);

	FILE* pFile;
	pFile = fopen(fileName, "r");

	if (pFile != NULL) {
		char buffer[iTextLen+1];
		memset(buffer, 0x00, sizeof(buffer));

		if (fgets(buffer, sizeof(buffer), pFile) != NULL) {
			char* ptr = buffer;
			char* endptr = NULL;
			int i = 0;

			while (strlen(ptr)) {
				endptr = strchr(ptr, '|');
				i = endptr - ptr;

				if (strncmp(ptr, ctroutd, strlen(ctroutd)) == COMPARE_OK) {
                    strcpy(result, "");
					strncat(result, ptr, i);
					break;
				}

				ptr += i+1;
			}

			if (strlen(result) <= 0) {
				debug_sprintf(szDbgMsg, "%s: ERROR! Trans not found", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}
		}
		else {
			debug_sprintf(szDbgMsg, "%s: ERROR!", __FUNCTION__);
			APP_TRACE(szDbgMsg);
		}

		fclose(pFile);
	} else {
		debug_sprintf(szDbgMsg, "%s: ERROR! opening file", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return;
	}

	if(strlen(result) <= 0) {
		debug_sprintf(szDbgMsg, "%s: ERROR! result is 0", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		GLOBAL->http.status_code = eHTTP_OK;
		GLOBAL->appErrCode = eERR_INVALID_CTROUTD;

		return;
	}

	char* resultPtr = result;

	//skip CTROUTD
	while (*resultPtr) {
		if (*resultPtr != ' ')
			resultPtr++;
		else
			break;
	}

	if (*resultPtr == ' ')
		resultPtr++;

	//copy PRESENT_FLAG
	if (getPFlag != NULL) {
		while (*resultPtr) {
			if (*resultPtr != ' ')
				*getPFlag++ = *resultPtr++;
			else
				break;
		}
	} else {
		while (*resultPtr++ != ' ') {}
	}

	if (*resultPtr == ' ')
		resultPtr++;

	//copy PAN
	if (getPAN != NULL) {
		while (*resultPtr) {
			if (*resultPtr != ' ')
				*getPAN++ = *resultPtr++;
			else
				break;
		}
	} else {
		while (*resultPtr++ != ' ') {}
	}

	if (*resultPtr == ' ')
		resultPtr++;

	//copy Amount
	if (getAmount != NULL) {
		while (*resultPtr) {
			if (*resultPtr != ' ')
				*getAmount++ = *resultPtr++;
			else
				break;
		}
	} else {
		while (*resultPtr++ != ' ') {}
	}

	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
}


bool isNewBatch(char* batch)
{
	if (batch == NULL) {
		return false;
	}

	char debug_batch[6+1] = {0};
	if (getEnvFile("DHI", "debug_batch", debug_batch, sizeof(debug_batch)) <= 0) {
		setEnv("DHI", "debug_batch", batch);
	}

	/*
	 * Praveen_P1: 17 Feb 2016
	 * Calling getEnvFile call instead of local
	 * call getEnv, in the getEnv function, we have
	 * local buffer which is getting returned. if two calls
	 * are made at same time, may get empty value from this function
	 */
	//strcpy(debug_batch, getEnv("DHI", "debug_batch"));

	//getEnvFile("DHI", "debug_batch", debug_batch, (sizeof(debug_batch) - 1) ); //Praveen_P1: We should have extra char in the buffer and send all whole size to getenvfile funtion

	getEnvFile("DHI", "debug_batch", debug_batch, sizeof(debug_batch));

	if (strncmp(debug_batch, batch, 6) != COMPARE_OK) {
		setEnv("DHI", "debug_batch", batch);
		return true;
	}

	return false;
}

void acquireMutexLock(pthread_mutex_t * pMutex, const char * pszName)
{
	int		rv				= EXIT_SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	rv = pthread_mutex_lock(pMutex);
	if(rv != EXIT_SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to acquire %s mutex, errno = %d",
												__FUNCTION__, pszName, errno);
		APP_TRACE(szDbgMsg);
	}

	return;
}


void releaseMutexLock(pthread_mutex_t * pMutex, const char * pszName)
{
	int		rv				= EXIT_SUCCESS;
#ifdef DEBUG
	char	szDbgMsg[128]	= "";
#endif

	rv = pthread_mutex_unlock(pMutex);
	if(rv != EXIT_SUCCESS)
	{
		debug_sprintf(szDbgMsg, "%s: Failed to release %s mutex, errno = %d",
												__FUNCTION__, pszName, errno);
		APP_TRACE(szDbgMsg);
	}

	return;
}

/*
 * ============================================================================
 * Function Name: local_svcSystem
 *
 * Description	: This API is wrapper to svcSystem
 *
 * Input Params	: char*
 *
 * Output Params:
 * ============================================================================
 */
int local_svcSystem(const char *command)
{
	int retval;
	struct sigaction sigact, saveact;

	sigact.sa_handler = SIG_DFL;
	sigemptyset(&sigact.sa_mask);
	sigact.sa_flags = 0;
	//save the existing SIGCHLD handler in saveact and set the handler to SIG_DFL.
	sigaction(SIGCHLD, &sigact, &saveact);

	retval = system(command);

	//Restore the original saved handler.
	sigaction(SIGCHLD, &saveact, NULL);
	return retval;
}

/*
 * ============================================================================
 * Function Name: getMaskedPAN
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void getMaskedPAN(char *pszClearPAN, char *pszMaskedPAN)
{
	int  iPANLength       		= 0;
	int  iFirstDigitsInClear    = 6;
	int	 iLastDigitsClear 		= 4;
	int  iDigitsToMask    		= 0;
	int	 iIndex				    = 0;
	int  iTempIndex				= 0;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	iPANLength = strlen(pszClearPAN);

	debug_sprintf(szDbgMsg, "%s: Length of the PAN is %d", __FUNCTION__, iPANLength);
	APP_TRACE(szDbgMsg);

	iDigitsToMask = iPANLength - iLastDigitsClear - iFirstDigitsInClear; //4 from end and excluding the number of clear digits

	if(iDigitsToMask < 1)
	{
		debug_sprintf(szDbgMsg, "%s: Adjusting the Digits to Mask", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		iFirstDigitsInClear = iFirstDigitsInClear - 1;
		iLastDigitsClear    = iLastDigitsClear - 1;
		iDigitsToMask = iPANLength - iLastDigitsClear - iFirstDigitsInClear;
	}

	debug_sprintf(szDbgMsg, "%s: Number of Digits to Mask is %d", __FUNCTION__, iDigitsToMask);
	APP_TRACE(szDbgMsg);

    strcpy(pszMaskedPAN, "");
	strncat(pszMaskedPAN, pszClearPAN, iPANLength - iDigitsToMask - iLastDigitsClear);

	iIndex = strlen(pszMaskedPAN);

	for (iTempIndex = 1 ; iTempIndex <= iDigitsToMask; iTempIndex++)
	{
		pszMaskedPAN[iIndex] = '*';
		iIndex ++;
	}

	strcat(&pszMaskedPAN[iIndex], pszClearPAN + iFirstDigitsInClear + iDigitsToMask);

	debug_sprintf(szDbgMsg, "%s: Masked PAN is [%s]", __FUNCTION__, pszMaskedPAN);
	APP_TRACE(szDbgMsg);

}

/*
 * ============================================================================
 * Function Name: getPANFromTrackData
 *
 * Description	:
 *
 * Input Params	:
 *
 * Output Params:
 * ============================================================================
 */
void getPANFromTrackData(char *pszTrackData, int iTrackIndicator, char *pszPAN, int iAcctBufSize)
{
	char *	cCurPtr				= NULL;
	char *	cNxtPtr				= NULL;

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	if(pszTrackData == NULL || pszPAN == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Input Param is NULL!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return;
	}

	cCurPtr = pszTrackData;

	if(iTrackIndicator == 1) //Track1 data
	{
		debug_sprintf(szDbgMsg, "%s: Retrieving PAN from Track1", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		cNxtPtr = strchr(cCurPtr, '^');
		if(cNxtPtr == NULL)
		{
			/* FIXME: Currently done under assumption that some gift cards dont
			* have the '^' sentinel in their track 1 information */
			strncpy(pszPAN, cCurPtr, iAcctBufSize);
		}
		else
		{
			if((cNxtPtr - cCurPtr) > iAcctBufSize) //Praveen_P1: 27 April, want to make sure that copying upto buffer size only
			{
				memcpy(pszPAN, cCurPtr, iAcctBufSize);
			}
			else
			{
				memcpy(pszPAN, cCurPtr, cNxtPtr - cCurPtr);
			}
		}
	}
	else if(iTrackIndicator == 2) //Track 2 data
	{
		debug_sprintf(szDbgMsg, "%s: Retrieving PAN from Track2", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		cNxtPtr = strchr(cCurPtr, '=');
		if(cNxtPtr == NULL)
		{
			/* FIXME: Currently done under assumption that some gift cards dont
			 * have the '=' sentinel in their track 2 */
			strncpy(pszPAN, cCurPtr, iAcctBufSize);
		}
		else
		{
			if((cNxtPtr - cCurPtr) > iAcctBufSize) //Praveen_P1: 27 April, want to make sure that copying upto buffer size only
			{
				memcpy(pszPAN, cCurPtr, iAcctBufSize);
			}
			else
			{
				memcpy(pszPAN, cCurPtr, cNxtPtr - cCurPtr);
			}
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: Invalid track to retrieve PAN!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		return;
	}

#ifdef DEVDEBUG
	debug_sprintf(szDbgMsg, "%s:%s: PAN [%s]", DEVDEBUGMSG, __FUNCTION__, pszPAN);
#else
	debug_sprintf(szDbgMsg,"%s: Retrived PAN from the Track Data",__FUNCTION__);
#endif
	APP_TRACE(szDbgMsg);

	return;
}

/*
 * ============================================================================
 * Function Name: setDynamicFunctionPtr
 *
 * Description	: This function sets the function pointer
 * 				  host interface library depends on the configuration
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int setDynamicFunctionPtr(char *pszLibName, char *pszFuncName, int (**pFunc)(void *))
{
	int			rv				= EXIT_SUCCESS;
    void 		*pFunctionLib 	= NULL;   // Handle to shared lib file
    const char 	*pdlError;        // Pointer to error string

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

    // Open Dynamic Loadable Libary using LD_LIBRARY path
    pFunctionLib = dlopen(pszLibName, RTLD_NOW);
    pdlError = dlerror();
    if (pdlError)
    {
    	debug_sprintf(szDbgMsg, "%s - unable to dynamically open %s!, error=[%s]",
                						__FUNCTION__, ((pszLibName) ? pszLibName : "application"), pdlError);
    	APP_TRACE(szDbgMsg);

        pFunc = NULL;
        rv = EXIT_FAILURE;
    }
    else
    {
        if (pszLibName)
        {
            debug_sprintf(szDbgMsg, "%s - loaded [%s] module!", __FUNCTION__, pszLibName);
            APP_TRACE(szDbgMsg);
        }

        // Find requested function
        *pFunc = dlsym(pFunctionLib, pszFuncName);
        pdlError = dlerror();
        if (pdlError)
        {
            debug_sprintf(szDbgMsg, "%s - unable to locate function [%s] in %s, error=[%s]",
                            __FUNCTION__, pszFuncName, ((pszLibName) ? pszLibName : "application"), pdlError);
            APP_TRACE(szDbgMsg);
            *pFunc = NULL;
            rv = EXIT_FAILURE;
        }
        else
        {
            debug_sprintf(szDbgMsg, "%s - obtained [%s] pointer=%p", __FUNCTION__, pszFuncName, pFunc);
            APP_TRACE(szDbgMsg);
        }
    }

    debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
    APP_TRACE(szDbgMsg);

    return rv;
}

/*
 * ============================================================================
 * Function Name: setDynamicFunctionPtr_1
 *
 * Description	: This function sets the function pointer
 * 				  host interface library depends on the configuration
 *
 * Input Params	: None
 *
 * Output Params: SUCCESS / FAILURE
 * ============================================================================
 */
int setDynamicFunctionPtr_1(char *pszLibName, char *pszFuncName, void (**pFunc)(char *, char *, char *, char *, char *))
{
	int			rv				= EXIT_SUCCESS;
    void 		*pFunctionLib 	= NULL;   // Handle to shared lib file
    const char 	*pdlError;        // Pointer to error string

#ifdef DEBUG
	char	szDbgMsg[256]	= "";
#endif

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

    // Open Dynamic Loadable Libary using LD_LIBRARY path
    pFunctionLib = dlopen(pszLibName, RTLD_NOW);
    pdlError = dlerror();
    if (pdlError)
    {
    	debug_sprintf(szDbgMsg, "%s - unable to dynamically open %s!, error=[%s]",
                						__FUNCTION__, ((pszLibName) ? pszLibName : "application"), pdlError);
    	APP_TRACE(szDbgMsg);

        pFunc = NULL;
        rv = EXIT_FAILURE;
    }
    else
    {
        if (pszLibName)
        {
            debug_sprintf(szDbgMsg, "%s - loaded [%s] module!", __FUNCTION__, pszLibName);
            APP_TRACE(szDbgMsg);
        }

        // Find requested function
        *pFunc = dlsym(pFunctionLib, pszFuncName);
        pdlError = dlerror();
        if (pdlError)
        {
            debug_sprintf(szDbgMsg, "%s - unable to locate function [%s] in %s, error=[%s]",
                            __FUNCTION__, pszFuncName, ((pszLibName) ? pszLibName : "application"), pdlError);
            APP_TRACE(szDbgMsg);
            *pFunc = NULL;
            rv = EXIT_FAILURE;
        }
        else
        {
            debug_sprintf(szDbgMsg, "%s - obtained [%s] pointer=%p", __FUNCTION__, pszFuncName, pFunc);
            APP_TRACE(szDbgMsg);
        }
    }

    debug_sprintf(szDbgMsg, "%s: Returning [%d]", __FUNCTION__, rv);
    APP_TRACE(szDbgMsg);

    return rv;
}
