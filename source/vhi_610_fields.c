/*
 * vhi_610_fields.c
 *
 *  Created on: Nov 5, 2013
 *      Author: Dexter M. Alberto
 */


#include <string.h>
#include <svc.h>
#include "sca_xml_request.h"
#include "vhi_request.h"
#include "vhi_610_fields.h"
#include "help_function.h"
#include "logger.h"
#include "vhi_610_base.h"
#include "service_request.h"
#include "config_data.h"
#include "define_data.h"
#include "emv.h"

#ifdef LOGGING_ENABLED
static char    szDbgMsg[512];
#endif

char* getFieldBNPR(_sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(vhi_fieldinfo->bnpr, 0x00, sizeof(vhi_fieldinfo->bnpr));
	strcpy(vhi_fieldinfo->bnpr, "I2.");

	if (strlen(vhi_fieldinfo->bnpr) <= 0) {
		debug_sprintf(szDbgMsg, "%s: ERROR! vhi_fieldinfo->bnpr has no value", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NO_CONTENT;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bnpr);
	APP_TRACE(szDbgMsg);

	return vhi_fieldinfo->bnpr;
}

char* getFieldBNNR(_sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(vhi_fieldinfo->bnnr, 0x00, sizeof(vhi_fieldinfo->bnnr));
	strcpy(vhi_fieldinfo->bnnr, "E3");
	appendSpace(sizeof(vhi_fieldinfo->bnnr), vhi_fieldinfo->bnnr);

	if (strlen(vhi_fieldinfo->bnnr) != (sizeof(vhi_fieldinfo->bnnr)-1)) {
		debug_sprintf(szDbgMsg, "%s: ERROR! vhi_fieldinfo->bnnr size over or less", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NO_CONTENT;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bnnr);
	APP_TRACE(szDbgMsg);

	return vhi_fieldinfo->bnnr;
}

char* getFieldBN00( enum e00 ID, _sVHI_FIELDS_INFO *vhi_fieldinfo )
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(vhi_fieldinfo->bn00, 0x00, sizeof(vhi_fieldinfo->bn00));

	if (ID == eAUTHORIZATION_00) {
		strcpy(vhi_fieldinfo->bn00, "0100");
	}
	else if (ID == eFINANCIAL_TRANS_00) {
		strcpy(vhi_fieldinfo->bn00, "0200");
	}
	else if (ID == eFINANCIAL_TRANS_0220) {
		strcpy(vhi_fieldinfo->bn00, "0220");
	}
	else if (ID == eFILE_UPDATE_00) {
		strcpy(vhi_fieldinfo->bn00, "0300");
	}
	else if (ID == eREVERSAL_00) {
		strcpy(vhi_fieldinfo->bn00, "0400");
	}
	else if (ID == eRECON_CONTROL_00) {
		strcpy(vhi_fieldinfo->bn00, "0500");
	}
	else if (ID == eAUTHORIZATION_00) {
		strcpy(vhi_fieldinfo->bn00, "0800");
	}
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! INVALID ID", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NO_CONTENT;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn00);
	APP_TRACE(szDbgMsg);

	return vhi_fieldinfo->bn00;
}

char* getFieldBN02(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(vhi_fieldinfo->bn02, 0x00, sizeof(vhi_fieldinfo->bn02));
	char* pACCT_NUM = getXmlTagValue(eACCT_NUM, req_xml_info);

	/*temporary for de tokenization*/
	if (GCONFIG->eTokenMode == eTOKEN_TUT )
	{
		fillSpace(sizeof(vhi_fieldinfo->bn02), vhi_fieldinfo->bn02);

		debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn02);
		APP_TRACE(szDbgMsg);

		return vhi_fieldinfo->bn02;
	}

	/*end*/
#if 0
	/*temporary for de tokenization*/
	if (GCONFIG->eTokenMode == eTOKEN_TUT && GLOBAL->http.status_code != eHTTP_GATEWAY_TIMEOUT) {
		fillSpace(sizeof(vhi_fieldinfo->bn02), vhi_fieldinfo->bn02);

		debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn02);
		APP_TRACE(szDbgMsg);

		return vhi_fieldinfo->bn02;
	}
	/*end*/
#endif

	if (pACCT_NUM == NULL) {

		if (GLOBAL->http.status_code != eHTTP_GATEWAY_TIMEOUT) {
			char* pCTROUTD = getXmlTagValue(eCTROUTD, req_xml_info);
			char cPAN[76+1] = {0};
			char* pEnd = NULL;
			int iLen = 0;

			getVoidData(pCTROUTD, cPAN, NULL, NULL, GLOBAL);
			pEnd = strchr(cPAN, '=');

			if (pEnd != NULL) {
				iLen = pEnd - cPAN;
                strcpy(vhi_fieldinfo->bn02, "");
				strncat(vhi_fieldinfo->bn02, cPAN, iLen);
			}
			else {
				if (strlen(cPAN) > sizeof(vhi_fieldinfo->bn02)-1) {
					debug_sprintf(szDbgMsg, "%s: ERROR! cPAN size over", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					return NO_CONTENT;
				}

				strcpy(vhi_fieldinfo->bn02, cPAN);
			}

			if (GLOBAL->http.status_code != eHTTP_GATEWAY_TIMEOUT )
				maskPanTrack(vhi_fieldinfo->bn02);

			appendSpace(sizeof(vhi_fieldinfo->bn02), vhi_fieldinfo->bn02);
		}
		else { //TOR
			debug_sprintf(szDbgMsg, "%s: Its for TOR request", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			char* pTRACK_DATA = getXmlTagValue(eTRACK_DATA, req_xml_info);

			if (pTRACK_DATA != NULL) {

				char* cCurPtr = pTRACK_DATA;

				char* endptr = strchr(cCurPtr, '=');
				if(endptr == NULL) //Track2 is present
				{
					cCurPtr = pTRACK_DATA + 1;
					endptr = strchr(cCurPtr, '^');
					if(endptr == NULL)
					{
						debug_sprintf(szDbgMsg, "%s: Its NOT Track1/Track2", __FUNCTION__);
						APP_TRACE(szDbgMsg);
						return NO_CONTENT;
					}
				}

                strcpy(vhi_fieldinfo->bn02, "");
				strncat(vhi_fieldinfo->bn02, cCurPtr, endptr - cCurPtr);
				appendSpace (sizeof(vhi_fieldinfo->bn02), vhi_fieldinfo->bn02);
			}
			else {
				char* pCTROUTD = getXmlTagValue(eCTROUTD, req_xml_info);
				char pan[76+1] = {0};

				getVoidData(pCTROUTD, pan, NULL, NULL, GLOBAL);

				if (strlen(pan) > 0) {
					char* endptr = strchr(pan, '=');
					if (endptr != NULL) {
						int i = endptr - pan;
                        strcpy(vhi_fieldinfo->bn02, "");
                        strncat(vhi_fieldinfo->bn02, pan, i);
						appendSpace(sizeof(vhi_fieldinfo->bn02), vhi_fieldinfo->bn02);
					}
				}
				else {
					debug_sprintf(szDbgMsg, "%s: ERROR! TRACK_DATA is NULL", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					return NO_CONTENT;
				}
			}
		}
	}
	else {
		if (strlen(pACCT_NUM) > sizeof(vhi_fieldinfo->bn02)-1) {
			debug_sprintf(szDbgMsg, "%s: ERROR! ACCT_NUM size over", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NO_CONTENT;
		}

		strcpy( vhi_fieldinfo->bn02, pACCT_NUM);

		if (GLOBAL->http.status_code != eHTTP_GATEWAY_TIMEOUT )
			maskPanTrack(vhi_fieldinfo->bn02);

		appendSpace(sizeof(vhi_fieldinfo->bn02), vhi_fieldinfo->bn02);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn02);
	APP_TRACE(szDbgMsg);

	return vhi_fieldinfo->bn02;
}

char* getFieldBN03(enum _ePROC_CODE ID, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(vhi_fieldinfo->bn03, 0x00, sizeof(vhi_fieldinfo->bn03));

	char* pPAYMENT_TYPE = getXmlTagValue( ePAYMENT_TYPE, req_xml_info );
    char* pDEBIT_TYPE = getXmlTagValue( eDEBIT_TYPE, req_xml_info );
    char* pEBT_TYPE = getXmlTagValue( eEBT_TYPE, req_xml_info );
    char* pBILLPAY =  getXmlTagValue( eBILLPAY, req_xml_info );

    if (pBILLPAY == NULL) {
		pBILLPAY = "FALSE";
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: BILLPAY flag is present in the request", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	if (ID == eSALE_PROCCODE) {

		if (strncmp( pPAYMENT_TYPE, COMMAND.CREDIT, strlen(COMMAND.CREDIT) ) == COMPARE_OK)
		{
			/*
			 * Praveen_P1: 21 Jan 2016
			 * Vantiv came back regarding BILLPAY field in the request
			 * They wanted to send 50 to indicate that payment in the processing code
			 * Checking
			 *
			 */
			if( (strncmp( pBILLPAY, "TRUE", strlen("TRUE") ) == COMPARE_OK) )
			{
				debug_sprintf(szDbgMsg, "%s: BILLPAY flag is present, its CREDIT SALE transaction so need to change processing code to 50", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				strcpy( vhi_fieldinfo->bn03, "504000");
			}
			else
			{
				strcpy( vhi_fieldinfo->bn03, "004000");
			}
		}
        else if (strncmp(pPAYMENT_TYPE, "EBT", 3 ) == COMPARE_OK)
        {
            if (strncmp(pEBT_TYPE, "FOOD_STAMP", 9) == COMPARE_OK)
            {
                strcpy(vhi_fieldinfo->bn03, "008000");
            }
            else if (strncmp(pEBT_TYPE, "CASH_BENEFITS", 13) == COMPARE_OK)
            {
                strcpy(vhi_fieldinfo->bn03, "006000");
            }
            else
            {
                debug_sprintf(szDbgMsg, "ERROR! invalid EBT type %s.", pEBT_TYPE);
                APP_TRACE(szDbgMsg);
                
                return NO_CONTENT;
            }
        }
		else if (strncmp(pPAYMENT_TYPE, "DEBIT", 5 ) == COMPARE_OK) {
			if (pDEBIT_TYPE == NULL) {
				strcpy( vhi_fieldinfo->bn03, "000000" );
			}
			else {
				if (strncmp( pDEBIT_TYPE, "SAVING", strlen(pDEBIT_TYPE)) == COMPARE_OK) {
					strcpy( vhi_fieldinfo->bn03, "001000" );
				}
				else if (strncmp(pDEBIT_TYPE, "CHECKING", strlen(pDEBIT_TYPE)) == COMPARE_OK) {
					strcpy( vhi_fieldinfo->bn03, "002000" );
				}
				else {
					strcpy( vhi_fieldinfo->bn03, "000000" );
				}
			}
		}
		else if (strncmp(pPAYMENT_TYPE, "GIFT", strlen(pPAYMENT_TYPE)) == COMPARE_OK) {
			strcpy( vhi_fieldinfo->bn03, "620000" );
		}
		else {
			strcpy( vhi_fieldinfo->bn03, "000000" );
		}
	}
	else if (ID == eREFUND_PROCCODE) {
		if (strncmp(pPAYMENT_TYPE, COMMAND.CREDIT, strlen(COMMAND.CREDIT)) == COMPARE_OK) {
			strcpy(vhi_fieldinfo->bn03, "204000");
		}
        else if (strncmp(pPAYMENT_TYPE, "EBT", 3 ) == COMPARE_OK)
        {
            if (strncmp(pEBT_TYPE, "FOOD_STAMP", 9) == COMPARE_OK)
            {
                strcpy(vhi_fieldinfo->bn03, "208000");
            }
            else if (strncmp(pEBT_TYPE, "CASH_BENEFITS", 13) == COMPARE_OK)
            {
                strcpy(vhi_fieldinfo->bn03, "206000");
            }
            else
            {
                debug_sprintf(szDbgMsg, "ERROR! invalid EBT type %s.", pEBT_TYPE);
                APP_TRACE(szDbgMsg);
                
                return NO_CONTENT;
            }
        }
		else if (strncmp(pPAYMENT_TYPE, "DEBIT", 5) == COMPARE_OK) {
			if (pDEBIT_TYPE == NULL) {
				strcpy(vhi_fieldinfo->bn03, "200000");
			}
			else {
				if (strncmp(pDEBIT_TYPE, "SAVING", 6) == COMPARE_OK) {
					strcpy(vhi_fieldinfo->bn03, "201000");
				}
				else if (strncmp(pDEBIT_TYPE, "CHECKING", 8) == COMPARE_OK) {
					strcpy(vhi_fieldinfo->bn03, "202000");
				}
				else {
					strcpy(vhi_fieldinfo->bn03, "200000");
				}
			}
		}
		else if (strncmp( pPAYMENT_TYPE, "GIFT", strlen(pPAYMENT_TYPE)) == COMPARE_OK) {
			strcpy(vhi_fieldinfo->bn03, "630000");
		}
		else {
			strcpy(vhi_fieldinfo->bn03, "000000");
		}
	}
	else if (ID == eVOID_PROCCODE) {
		strcpy(vhi_fieldinfo->bn03, "000000");
	}
	else if (ID == eGIFT_ACTIVATION) {
		strcpy(vhi_fieldinfo->bn03, "610000");
	}
	else if (ID == eGIFT_RELOAD) {
		strcpy(vhi_fieldinfo->bn03, "640000");
	}
	else if (ID == eGIFT_UNLOAD) {
		strcpy(vhi_fieldinfo->bn03, "650000");
	}
	else if (ID == eGIFT_CLOSE) {
		strcpy(vhi_fieldinfo->bn03, "660000");
	}
	else if (ID == eGIFT_BALINQ) {
		strcpy(vhi_fieldinfo->bn03, "670000");
	}
	else if (ID == eGIFT_PREAUTH) {
		strcpy(vhi_fieldinfo->bn03, "580000");
	}
	else if (ID == eCDT_PRIORAUTH) {
		strcpy(vhi_fieldinfo->bn03, "004000");
	}
	else if (ID == eCDT_PRIORADJUSTMENT) {
		strcpy(vhi_fieldinfo->bn03, "024000");
	}
	else if (ID == eGIFT_COMPLETION) {
		strcpy(vhi_fieldinfo->bn03, "590000");
	}
	else if (ID == eGIFT_BATCH_TOTAL) {
		strcpy(vhi_fieldinfo->bn03, "680000");
	}
	else if (ID == eBATCH_INQUIRY) {
		strcpy(vhi_fieldinfo->bn03, "920000");
	}
	else if (ID == eBATCH_RELEASE) {
		strcpy(vhi_fieldinfo->bn03, "930000");
	}
	else if (ID == eTOKENIZATION) {
		strcpy(vhi_fieldinfo->bn03, "800000");
	}
    else if (ID == eEBT_VOID)
    {
        strcpy(vhi_fieldinfo->bn03, "007000");
    }
    else if (ID == eEBT_BALANCE)
    {
        strcpy(vhi_fieldinfo->bn03, "310000");
    }
	else if (ID == eCRED_DEBT_BAL_INQ) {
		char* pPAYMENT_TYPE = getXmlTagValue(ePAYMENT_TYPE, req_xml_info);

		if (pPAYMENT_TYPE != NULL) {
			if (strncmp(pPAYMENT_TYPE, COMMAND.CREDIT, strlen(COMMAND.CREDIT)) == COMPARE_OK)
				strcpy(vhi_fieldinfo->bn03, "304000");
			else if (strncmp(pPAYMENT_TYPE, "DEBIT", 5) == COMPARE_OK)
				strcpy(vhi_fieldinfo->bn03, "300000");
		}
		else {
			debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NO_CONTENT;
		}
	}
	else if (ID == eCREDIT_PREAUTH) {
			strcpy(vhi_fieldinfo->bn03, "004000");
	}
	else if (ID == eCREDIT_PREAUTH) {
			strcpy(vhi_fieldinfo->bn03, "524000");
	}
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! invalid ID", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NO_CONTENT;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn03);
	APP_TRACE(szDbgMsg);

	return vhi_fieldinfo->bn03;
}

char* getFieldBN04(sGLOBALS* GLOBAL, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//this is decimal point removed
	memset(vhi_fieldinfo->bn04, 0x00, sizeof(vhi_fieldinfo->bn04));
	char* pTRANS_AMOUNT = getXmlTagValue(eTRANS_AMOUNT, req_xml_info);

	/*temporary for adjustment*/
	char* pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);
	char* pCTROUTD = getXmlTagValue(eCTROUTD, req_xml_info);
#if 0
	/*temporary for debug amount*/
	if (strncmp(pCOMMAND, COMMAND.REMOVE_VALUE, strlen(COMMAND.REMOVE_VALUE)) == COMPARE_OK) {
		char cDebug_Amount[9+1] = {0};
		strcpy(cDebug_Amount, getEnv("DHI", "debug_amount"));

		if (strlen(cDebug_Amount) > 1) {
			strcpy(vhi_fieldinfo->bn04, cDebug_Amount);
			removeDecimalPoint( vhi_fieldinfo->bn04 );
			precedeZero(sizeof(vhi_fieldinfo->bn04), vhi_fieldinfo->bn04);

			return vhi_fieldinfo->bn04;
		}
	}
	/*end*/
#endif
	/*For adjustment*/
	if (pCOMMAND != NULL) {
		if (strncmp(pCOMMAND, COMMAND.ADD_TIP, strlen(COMMAND.ADD_TIP)) == COMPARE_OK) {
			if (pCTROUTD != NULL) {
				char amount[9+1] = {0};

				getAdjustmentData(NULL, NULL, amount, GLOBAL, req_xml_info);

				if (strlen(amount) <= 0) {
					debug_sprintf(szDbgMsg, "%s: WARNING!", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					return NO_CONTENT;
				}

				char* pTIP_AMOUNT = getXmlTagValue(eTIP_AMOUNT, req_xml_info);
				if (pTIP_AMOUNT == NULL) {
					debug_sprintf(szDbgMsg, "%s: ERROR! TIP_AMOUNT is NULL", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					return NO_CONTENT;
				}

				char ctip[9+1] = {0};
                strcpy(ctip, "");
				strncat(ctip, pTIP_AMOUNT, sizeof(ctip)- 1);

				double dtip = atof(pTIP_AMOUNT);
				double damount = atof(amount);
				char ctotal[9+1] = {0};
				sprintf(ctotal, "%0.2f", damount + dtip);
				strcpy(vhi_fieldinfo->bn04, ctotal);

				removeDecimalPoint( vhi_fieldinfo->bn04 );
				precedeZero(sizeof(vhi_fieldinfo->bn04), vhi_fieldinfo->bn04);

				return vhi_fieldinfo->bn04;
			}
			else {
				debug_sprintf(szDbgMsg, "%s: WARNING!", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return NO_CONTENT;
			}
		}
	}
	else {
		debug_sprintf(szDbgMsg, "%s: WARNING!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NO_CONTENT;
	}
	/*end*/

	if (pTRANS_AMOUNT == NULL) {
		fillZero (sizeof(vhi_fieldinfo->bn04), vhi_fieldinfo->bn04);
	}
	else {
		char* pCASHBACK_AMNT = getXmlTagValue(eCASHBACK_AMNT, req_xml_info);
		char* pPAYMENT_TYPE = getXmlTagValue(ePAYMENT_TYPE, req_xml_info);

		/*DEBIT with Cash back or CREDIT(with EMV) with Cash Back or EBT with cash back*/
		if (((strncmp(pPAYMENT_TYPE, "DEBIT", 5) == COMPARE_OK)  || (strncmp(pPAYMENT_TYPE, "CREDIT", 6) == COMPARE_OK) || (strncmp(pPAYMENT_TYPE, "EBT", 6) == COMPARE_OK)) && pCASHBACK_AMNT != NULL) {
			char amount[9+1] = {0};
			char cashback[9+1] = {0};
			char total[9+1] = {0};

			strcpy(amount, pTRANS_AMOUNT);
			strcpy(cashback, pCASHBACK_AMNT);

			double damount = atof(amount);
			double dcashback = atof(cashback);
			double dtotal = damount - dcashback;

			sprintf(total, "%0.2f", dtotal);
			strcpy(vhi_fieldinfo->bn04, total);
			removeDecimalPoint(vhi_fieldinfo->bn04);
			precedeZero(sizeof(vhi_fieldinfo->bn04), vhi_fieldinfo->bn04);

			return vhi_fieldinfo->bn04;
		}
		/*end*/

		strcpy(vhi_fieldinfo->bn04, pTRANS_AMOUNT);
		if (removeDecimalPoint(vhi_fieldinfo->bn04) == EXIT_FAILURE) {
			debug_sprintf(szDbgMsg, "%s: ERROR! Removing of decimal failed", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NO_CONTENT;
		}
	}

	precedeZero(sizeof(vhi_fieldinfo->bn04), vhi_fieldinfo->bn04);

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn04);
	APP_TRACE(szDbgMsg);

	return vhi_fieldinfo->bn04;
}

char* getFieldBN11(_sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(vhi_fieldinfo->bn11, 0x00, sizeof(vhi_fieldinfo->bn11));

	getEnvFile("DHI", "STAN", vhi_fieldinfo->bn11, (sizeof(vhi_fieldinfo->bn11) - 1) );

	/*
	 * Praveen_P1: 17 Feb 2016
	 * In the getENV call we are filling and
	 * returning global buffer which can be
	 * memet by some other call to getENV, thats
	 * why calling getenvfile directly here
	 */
	//strcpy(vhi_fieldinfo->bn11, getEnv("DHI", "STAN"));

	if (strlen(vhi_fieldinfo->bn11) <= 0
	|| strlen(vhi_fieldinfo->bn11) > sizeof(vhi_fieldinfo->bn11)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! STAN", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NO_CONTENT;
	}

	precedeZero(sizeof(vhi_fieldinfo->bn11), vhi_fieldinfo->bn11);

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn11);
	APP_TRACE(szDbgMsg);

	return vhi_fieldinfo->bn11;
}

char* getFieldBN12(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	char date_time[11+1] = "";

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(vhi_fieldinfo->bn12, 0x00, sizeof(vhi_fieldinfo->bn12));
	char* pdate = getXmlTagValue(eORIG_TRANS_DATE, req_xml_info); 	//YYYY.MM.DD
	char* pSAFTRAN = getXmlTagValue(eSAF_TRAN, req_xml_info);
	if(pSAFTRAN == NULL)
	{
		pSAFTRAN = "0";
	}

	/*
	 * Praveen_P1: 7 July 2016
	 * For SAF transactions, filling field13 from ORIG_TRAN_TIME field if useorigtimeforsaf config variable is set
	 * Requirement came from TJX to use same value thats been SAFed, so SCA would send that value in ORG_TRANS_TIME
	 * VHI would use this. Making it configurable so that it wont break current cusotmers for whom we have been
	 * sending current time for SAFed transactions too
	 *
	 */
	if ( (pdate != NULL) &&
			( (strncmp(pSAFTRAN, "0", 1) == COMPARE_OK) ||
					((strncmp(pSAFTRAN, "1", 1) == COMPARE_OK) && (GCONFIG->useorigtimeforsaf == true)) ) ) {
		char YY[2+1] = {0};
		char MM[2+1] = {0};
		char DD[2+1] = {0};

		pdate += 2; //skip YY -> YY.MM.DD
		strncat(YY, pdate, 2); //copy YY
		pdate += 3; //skip YY. -> MM.DD
		strncat(MM, pdate, 2); //copy MM
		pdate += 3; //skip MM. -> DD
		strncat(DD, pdate, 2); //copy DD

        strcpy(vhi_fieldinfo->bn12, "");
		strncat(vhi_fieldinfo->bn12, MM, 2);
		strncat(vhi_fieldinfo->bn12, DD, 2);
		strncat(vhi_fieldinfo->bn12, YY, 2);
	} else {
		char *pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);

		if (strncmp(pCOMMAND, COMMAND.COMPLETION, strlen(COMMAND.COMPLETION)) == COMPARE_OK) {
			char *pCTROUTD = getXmlTagValue(eCTROUTD, req_xml_info);

			if (pCTROUTD == NULL) {
				debug_sprintf(szDbgMsg, "%s: Error! CTROUTD is NULL", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			char transDate[17+3] = {0};
			char *ptr = transDate;

			getVoidData(pCTROUTD, NULL, transDate, NULL, GLOBAL);

			if (strlen(ptr) <= 0) {
				debug_sprintf(szDbgMsg, "%s: Error! ptr is NULL", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			//ptr = SMMDDYYhhmmss0.00
			ptr++; //skip S
            strcpy(vhi_fieldinfo->bn12, "");
			strncat(vhi_fieldinfo->bn12, ptr, 6); //copy MMDDYY
		} else {
			memset(date_time, 0x00, sizeof(date_time));
			strcpy(vhi_fieldinfo->bn12, getDateTime(eMMDDYY, date_time));
		}
	}

	if (strlen(vhi_fieldinfo->bn12) != (sizeof(vhi_fieldinfo->bn12)-1)) {
		debug_sprintf(szDbgMsg, "%s: Error! bn12 not 6 bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		precedeZero(sizeof(vhi_fieldinfo->bn12), vhi_fieldinfo->bn12);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn12);
	APP_TRACE(szDbgMsg);

	return vhi_fieldinfo->bn12;
}

char* getFieldBN13(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	char date_time[11+1] = "";

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(vhi_fieldinfo->bn13, 0x00, sizeof(vhi_fieldinfo->bn13));
	char* ptime 	= getXmlTagValue(eORIG_TRANS_TIME, req_xml_info); //HH:MM:SS
	char* pSAFTRAN = getXmlTagValue(eSAF_TRAN, req_xml_info);
	if(pSAFTRAN == NULL)
	{
		pSAFTRAN = "0";
	}

	/*
	 * Praveen_P1: 7 July 2016
	 * For SAF transactions, filling field13 from ORIG_TRAN_TIME field if useorigtimeforsaf config variable is set
	 * Requirement came from TJX to use same value thats been SAFed, so SCA would send that value in ORG_TRANS_TIME
	 * VHI would use this. Making it configurable so that it wont break current cusotmers for whom we have been
	 * sending current time for SAFed transactions too
	 *
	 */
	if ( (ptime != NULL) &&
				( (strncmp(pSAFTRAN, "0", 1) == COMPARE_OK) ||
						((strncmp(pSAFTRAN, "1", 1) == COMPARE_OK) && (GCONFIG->useorigtimeforsaf == true)) ) ) {
		char hh[2+1] = {0};
		char mm[2+1] = {0};
		char ss[2+1] = {0};

		debug_sprintf(szDbgMsg, "%s: Taking local time from ORIG_TRANS_TIME field", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		strncat(hh, ptime, 2); //copy HH
		ptime += 3; //skip HH: -> MM:SS
		strncat(mm, ptime, 2); //copy MM
		ptime += 3; //skip MM: -> SS
		strncat(ss, ptime, 2); //copy SS

        strcpy(vhi_fieldinfo->bn13, "");
        strncat(vhi_fieldinfo->bn13, hh, 2);
		strncat(vhi_fieldinfo->bn13, mm, 2);
		strncat(vhi_fieldinfo->bn13, ss, 2);
	} else {
		char *pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);

		if (strncmp(pCOMMAND, COMMAND.COMPLETION, strlen(COMMAND.COMPLETION)) == COMPARE_OK) {
			char *pCTROUTD = getXmlTagValue(eCTROUTD, req_xml_info);

			if (pCTROUTD == NULL) {
				debug_sprintf(szDbgMsg, "%s: Error! CTROUTD is NULL", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			char transTime[17+3] = {0};
			char *ptr = transTime;

			getVoidData(pCTROUTD, NULL, transTime, NULL, GLOBAL);

			if (strlen(ptr) <= 0) {
				debug_sprintf(szDbgMsg, "%s: Error! ptr is NULL", __FUNCTION__);
				APP_TRACE(szDbgMsg);
			}

			//ptr = SMMDDYYhhmmss0.00
			ptr += 7; //skip SMMDDYY
            strcpy(vhi_fieldinfo->bn13, "");
            strncat(vhi_fieldinfo->bn13, ptr, 6); //copy hhmmss
		} else {
			memset(date_time, 0x00, sizeof(date_time));
			strcpy(vhi_fieldinfo->bn13, getDateTime(ehhmmss, date_time));
		}
	}

	if (strlen(vhi_fieldinfo->bn13) != (sizeof(vhi_fieldinfo->bn13)-1)) {
		debug_sprintf(szDbgMsg, "%s: Error! bn13 not 6 bytes", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		precedeZero(sizeof(vhi_fieldinfo->bn13), vhi_fieldinfo->bn13);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn13);
	APP_TRACE(szDbgMsg);

	return vhi_fieldinfo->bn13;
}

char* getFieldBN22(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(vhi_fieldinfo->bn22, 0x00,sizeof(vhi_fieldinfo->bn22));

	char* pPAYMENT_TYPE = getXmlTagValue(ePAYMENT_TYPE, req_xml_info);
	char* pPRESENT_FLAG = getXmlTagValue(ePRESENT_FLAG, req_xml_info); //for CREDIT only
	char* pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);
	char* pCTROUTD = getXmlTagValue(eCTROUTD, req_xml_info);
    char* pEMV_TAGS = getXmlTagValue(eEMV_TAGS, req_xml_info);

	if (pPAYMENT_TYPE == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE should not be NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NO_CONTENT;
	}

	debug_sprintf(szDbgMsg, "%s: Present Flag is %s", __FUNCTION__, pPRESENT_FLAG);
	APP_TRACE(szDbgMsg);

#if 0
	/*temporary*/
	char dbg_pos_entry[3+1] = {0};
	strncpy(dbg_pos_entry, getEnv("DHI", "debug_pos_entry_mode"), 3);

	if (strlen(dbg_pos_entry) > 1) {
		strncpy(vhi_fieldinfo->bn22, dbg_pos_entry, 3);

		return vhi_fieldinfo->bn22;
	}
	/*end*/
#endif

	/*TUT*/
	if (GCONFIG->eTokenMode == eTOKEN_TUT) {
		strcpy(vhi_fieldinfo->bn22, "011");

		return vhi_fieldinfo->bn22;
	}
	/*end*/
#if 0
	if (GCONFIG->isRegistartMode == true //VSP registration
	|| (strncmp(pCOMMAND, COMMAND.CREDIT, strlen(COMMAND.CREDIT)) == COMPARE_OK && pCTROUTD != NULL)) //Refund with Ctroutd
	{
		pPRESENT_FLAG = SWIPED_ENTRY;
	}
#endif

	if ((strncmp(pCOMMAND, COMMAND.CREDIT, strlen(COMMAND.CREDIT)) == COMPARE_OK && pCTROUTD != NULL)) //Refund with Ctroutd
	{
        // TODO: RMW what about EMV?
		pPRESENT_FLAG = SWIPED_ENTRY;
	}
	/*
	 * PRESENT_FLAG = Card Entry
	 * payment type: credit
	 * 1 - Card not present
	 * 2 - Card Present / manual
	 * 3 - Card swiped
	 * 4 - Transaction is being passed by RFID (Radio Frequency Identification) device
     * 5 - EMV chip insert
     * 6 - EMV contactless
     * 7 - EMV fallback
	 */
	/*
	 * PAYMENT_TYPE
	 * CREDIT - Visa, MasterCard, Discover, American Express, Diner,s Club, JCB, Google Wallet, and ISIS Wallet
	 * DEBIT - ATM
	 * GIFT
	 */

    
    // -------------------- CREDIT or EMV --------------------
    if ((strncmp(pPAYMENT_TYPE,COMMAND.CREDIT, strlen(COMMAND.CREDIT)) == COMPARE_OK) ||
        ((pPRESENT_FLAG != NULL) &&
         ((strncmp(pPRESENT_FLAG, EMV_CHIP_ENTRY, 1) == COMPARE_OK) || (strncmp(pPRESENT_FLAG, EMV_CTLS_ENTRY, 1) == COMPARE_OK))))
    {
        debug_sprintf(szDbgMsg, "%s: CREDIT/EMV payment type", __FUNCTION__);
        APP_TRACE(szDbgMsg);
        
        /*Adjustment*/
        char pflag[1+1] = {0};
        
        if (strncmp(pCOMMAND, COMMAND.ADD_TIP, strlen(COMMAND.ADD_TIP)) == COMPARE_OK) {
            getAdjustmentData(pflag, NULL, NULL, GLOBAL, req_xml_info);
            pPRESENT_FLAG = pflag;
        }
        /*end*/
        
        if (pPRESENT_FLAG == NULL) {
            debug_sprintf(szDbgMsg, "%s: ERROR! PRESENT_FLAG for CREDIT", __FUNCTION__);
            APP_TRACE(szDbgMsg);
            
            return NO_CONTENT;
        }
        
        if (strncmp(pPRESENT_FLAG, SWIPED_ENTRY, 1) == COMPARE_OK) {
            
            debug_sprintf(szDbgMsg, "%s: Swiped mode", __FUNCTION__);
            APP_TRACE(szDbgMsg);
            
            char* pTRACK = getXmlTagValue(eTRACK_DATA, req_xml_info);
            
            if (pTRACK != NULL) {
                if (strchr(pTRACK, '=') != NULL) {
                    strcpy(vhi_fieldinfo->bn22, "021");
                } else {
                    strcpy(vhi_fieldinfo->bn22, "811");
                }
            } else {
                strcpy(vhi_fieldinfo->bn22, "021");
            }
        }
        else if (strncmp(pPRESENT_FLAG, MANUAL_ENTRY, 1) == COMPARE_OK) {
            debug_sprintf(szDbgMsg, "%s: Manual mode", __FUNCTION__);
            APP_TRACE(szDbgMsg);
            
            strcpy(vhi_fieldinfo->bn22, "011");
        }
        else if (strncmp(pPRESENT_FLAG, PROXIMITY_ENTRY, 1) == COMPARE_OK) {
            debug_sprintf(szDbgMsg, "%s: Contactless mode", __FUNCTION__);
            APP_TRACE(szDbgMsg);
            
            strcpy(vhi_fieldinfo->bn22, "911");
        }
        else if (strncmp(pPRESENT_FLAG, EMV_CHIP_ENTRY, 1) == COMPARE_OK)
        {
            // Was offline PIN entry done?
            // Convert first byte of CVM result to binary
            UINT8   CVMR1= 0;
            char   *cvmr_tag;
            
            if (pEMV_TAGS == NULL)
            {
                debug_sprintf(szDbgMsg, "%s: EMV_TAGS is NULL, must be Partial EMV Tran", __FUNCTION__);
                APP_TRACE(szDbgMsg);
                strcpy(vhi_fieldinfo->bn22, "051");     // set a value - missing tags will prevent request from being honoured
            }
            else
            {
                cvmr_tag= strstr(pEMV_TAGS, "9F3403");
                if (cvmr_tag != NULL)
                    char2hex(&CVMR1, (UINT8*)cvmr_tag+ 6, 2);
                if ((cvmr_tag != NULL) &&
                    (strncmp(cvmr_tag+ 10, "02", 2) == 0) &&    // successful CVM
                    ((CVMR1 & 0x01) || (CVMR1 & 0x04)))         // plaintext or enciphered PIN verified offline
                    strcpy(vhi_fieldinfo->bn22, "059");
                else
                    strcpy(vhi_fieldinfo->bn22, "051");
            }
            
            debug_sprintf(szDbgMsg, "%s: EMV insert mode, CVM B1= %02X, bn22= %s", __FUNCTION__, CVMR1, vhi_fieldinfo->bn22);
            APP_TRACE(szDbgMsg);
        }
        else if (strncmp(pPRESENT_FLAG, EMV_CTLS_ENTRY, 1) == COMPARE_OK)
        {
            // Was offline PIN entry done?
            // Convert first byte of CVM result to binary
            UINT8   CVMR1= 0;
            char   *cvmr_tag;
            if (pEMV_TAGS == NULL)
		    {
			   debug_sprintf(szDbgMsg, "%s: EMV_TAGS is NULL, must be Partial EMV Tran", __FUNCTION__);
			   APP_TRACE(szDbgMsg);
			   strcpy(vhi_fieldinfo->bn22, "071");     // set a value - missing tags will prevent request from being honoured
		    }
            else
            {
				cvmr_tag= strstr(pEMV_TAGS, "9F3403");
				if (cvmr_tag != NULL)
					char2hex(&CVMR1, (UINT8*)cvmr_tag+ 6, 2);
				if ((cvmr_tag != NULL) &&
					(strncmp(cvmr_tag+ 10, "02", 2) == 0) &&    // successful CVM
					((CVMR1 & 0x01) || (CVMR1 & 0x04)))         // plaintext or enciphered PIN verified offline
					strcpy(vhi_fieldinfo->bn22, "079");
				else
					strcpy(vhi_fieldinfo->bn22, "071");
            }
            
            debug_sprintf(szDbgMsg, "%s: EMV contactless mode, CVM B1= %02X, bn22= %s", __FUNCTION__, CVMR1, vhi_fieldinfo->bn22);
            APP_TRACE(szDbgMsg);
        }
        else if (strncmp(pPRESENT_FLAG, EMV_FALLBACK_ENTRY, 1) == COMPARE_OK)
        {
            // Vantiv has a fallback to manual (791) which is not supported by SSI
            debug_sprintf(szDbgMsg, "%s: EMV fallback mode", __FUNCTION__);
            APP_TRACE(szDbgMsg);
            
            strcpy(vhi_fieldinfo->bn22, "801");
        }
        else {
            debug_sprintf(szDbgMsg, "%s: ERROR! unknown entry", __FUNCTION__);
            APP_TRACE(szDbgMsg);
            
            return NO_CONTENT;
        }
    }
    // -------------------- DEBIT or EBT --------------------
	else if ((strncmp(pPAYMENT_TYPE,"DEBIT", 5) == COMPARE_OK) || (strncmp(pPAYMENT_TYPE, "EBT", 3) == COMPARE_OK))
    {
		if ((pPRESENT_FLAG != NULL) && (strncmp(pPRESENT_FLAG, SWIPED_ENTRY, 1) == COMPARE_OK))
		{

			debug_sprintf(szDbgMsg, "%s: Swiped mode", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			char* pTRACK = getXmlTagValue(eTRACK_DATA, req_xml_info);

			if (pTRACK != NULL) {
				if (strchr(pTRACK, '=') != NULL) {
					strcpy(vhi_fieldinfo->bn22, "021");
				} else {
					strcpy(vhi_fieldinfo->bn22, "811");
				}
			} else {
				strcpy(vhi_fieldinfo->bn22, "021");
			}
		}
		else if ((pPRESENT_FLAG != NULL) && (strncmp(pPRESENT_FLAG, PROXIMITY_ENTRY, 1) == COMPARE_OK)) {
			debug_sprintf(szDbgMsg, "%s: Contactless mode", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			strcpy(vhi_fieldinfo->bn22, "911");
		}
		else if ((pPRESENT_FLAG != NULL) && (strncmp(pPRESENT_FLAG, MANUAL_ENTRY, 1) == COMPARE_OK)) {
			debug_sprintf(szDbgMsg, "%s: Manual mode", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			strcpy(vhi_fieldinfo->bn22, "011");
		}
		else if (strncmp(pPRESENT_FLAG, EMV_FALLBACK_ENTRY, 1) == COMPARE_OK) //Praveen_P1: 04 Dec 2015 - Added this check since We are not handling for DEBIT EMV fallback case
		{
			// Vantiv has a fallback to manual (791) which is not supported by SSI
			debug_sprintf(szDbgMsg, "%s: EMV fallback mode", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			strcpy(vhi_fieldinfo->bn22, "801");
		}
		else {
			debug_sprintf(szDbgMsg, "%s: ERROR! unknown entry", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NO_CONTENT;
		}
		//strcpy(vhi_fieldinfo->bn22, "021");
	}
    
    // -------------------- GIFT --------------------
	else if (strncmp(pPAYMENT_TYPE,"GIFT", 4) == COMPARE_OK) {
		/*completion*/
		if (strncmp(pCOMMAND, COMMAND.COMPLETION, strlen(COMMAND.COMPLETION)) == COMPARE_OK) {
			char cAmount[17+3] = {0};

			getVoidData(pCTROUTD, NULL, cAmount, NULL, GLOBAL);

			if (strncmp(cAmount, "S", 1) == COMPARE_OK) {
				pPRESENT_FLAG = SWIPED_ENTRY;
			}
			else if (strncmp(cAmount, "M", 1) == COMPARE_OK) {
				pPRESENT_FLAG = MANUAL_ENTRY;
			}
            else if (strncmp(cAmount, "R", 1) == COMPARE_OK) {
                pPRESENT_FLAG = PROXIMITY_ENTRY;
            }
            else if (strncmp(cAmount, "E", 1) == COMPARE_OK) {
                pPRESENT_FLAG = EMV_CHIP_ENTRY;
            }
            else if (strncmp(cAmount, "C", 1) == COMPARE_OK) {
                pPRESENT_FLAG = EMV_CTLS_ENTRY;
            }
            else if (strncmp(cAmount, "F", 1) == COMPARE_OK) {
                pPRESENT_FLAG = EMV_FALLBACK_ENTRY;
            }
			else {
				debug_sprintf(szDbgMsg, "%s: ERROR! No PRESENT_FLAG indicator", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return NO_CONTENT;
			}
		}
		/*completion*/

		if (pPRESENT_FLAG == NULL) {
			debug_sprintf(szDbgMsg, "%s: WARNING! PRESENT_FLAG for GIFT is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			pPRESENT_FLAG = SWIPED_ENTRY;
		}

		if (strncmp(pPRESENT_FLAG, SWIPED_ENTRY, 1) == COMPARE_OK) {

			debug_sprintf(szDbgMsg, "%s: Swiped mode", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			char* pTRACK = getXmlTagValue(eTRACK_DATA, req_xml_info);

			if (pTRACK != NULL) {
				if (strchr(pTRACK, '=') != NULL) {
					strcpy(vhi_fieldinfo->bn22, "021");
				} else {
					strcpy(vhi_fieldinfo->bn22, "811");
				}
			} else {
				strcpy(vhi_fieldinfo->bn22, "021");
			}
		}
		else if (strncmp(pPRESENT_FLAG, MANUAL_ENTRY, 1) == COMPARE_OK) {
			debug_sprintf(szDbgMsg, "%s: Manual mode", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			strcpy(vhi_fieldinfo->bn22, "011");
		}
		else if (strncmp(pPRESENT_FLAG, PROXIMITY_ENTRY, 1) == COMPARE_OK) {
			debug_sprintf(szDbgMsg, "%s: Contactless mode", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			strcpy(vhi_fieldinfo->bn22, "911");
		}
		else {
			debug_sprintf(szDbgMsg, "%s: ERROR! unknown entry", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NO_CONTENT;
		}
	}
    
    // -------------------- ADMIN --------------------
	else if (strncmp(pPAYMENT_TYPE,"ADMIN", 5) == COMPARE_OK) {
		if (pPRESENT_FLAG == NULL) {
			debug_sprintf(szDbgMsg, "%s: ERROR! PRESENT_FLAG for ADMIN", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NO_CONTENT;
		}

		if (strncmp(pPRESENT_FLAG, SWIPED_ENTRY, 1) == COMPARE_OK) {

			debug_sprintf(szDbgMsg, "%s: Swiped mode", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			char* pTRACK = getXmlTagValue(eTRACK_DATA, req_xml_info);

			if (pTRACK != NULL) {
				if (strchr(pTRACK, '=') != NULL) {
					strcpy(vhi_fieldinfo->bn22, "021");
				} else {
					strcpy(vhi_fieldinfo->bn22, "811");
				}
			} else {
				strcpy(vhi_fieldinfo->bn22, "021");
			}
		}
		else if (strncmp(pPRESENT_FLAG, MANUAL_ENTRY, 1) == COMPARE_OK) {
			debug_sprintf(szDbgMsg, "%s: Manual mode", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			strcpy(vhi_fieldinfo->bn22, "011");
		}
		else if (strncmp(pPRESENT_FLAG, PROXIMITY_ENTRY, 1) == COMPARE_OK) {
			debug_sprintf(szDbgMsg, "%s: Contactless mode", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			strcpy(vhi_fieldinfo->bn22, "911");
		}
		else {
			debug_sprintf(szDbgMsg, "%s: ERROR! unknown entry", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NO_CONTENT;
		}
	}
    
    // -------------------- EBT --------------------
    else if (strncmp(pPAYMENT_TYPE, "EBT", 3) == COMPARE_OK)
    {
    }
    
	else {
		debug_sprintf(szDbgMsg, "%s: ERROR! Error in Point-of-Service Entry Mode", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NO_CONTENT;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn22);
	APP_TRACE(szDbgMsg);

	return vhi_fieldinfo->bn22;
}

char* getFieldBN25(sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	int iSetBillPay 		= 0;
	int	iSetCardNotPresent  = 0;

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(vhi_fieldinfo->bn25, 0x00, sizeof(vhi_fieldinfo->bn25));

	char* pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);

	if (pCOMMAND == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! COMMAND is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NO_CONTENT;
	}

	char* pSAFTRAN = getXmlTagValue(eSAF_TRAN, req_xml_info);
	if (pSAFTRAN == NULL) {
		debug_sprintf(szDbgMsg, "%s: pSAFTRAN is NULL, so it is not SAF transaction", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pSAFTRAN = "0";
	}

	char* pCARDPRESENT = getXmlTagValue(eCARD_PRESENT, req_xml_info);
	if (pCARDPRESENT == NULL) {
		debug_sprintf(szDbgMsg, "%s: pCARDPRESENT is NULL, Considering it as Card Present Transaction", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		iSetCardNotPresent = 0;
	}
	else
	{
		/*
		 * CARD_PRESENT field can have following combination of values
		 * TRUE|FALSE
		 * YES|NO
		 * 1|0
		 * T|F
		 */
		if( (strncmp( pCARDPRESENT, "FALSE", strlen("FALSE") ) == COMPARE_OK) 	||
			(strncmp( pCARDPRESENT, "0", strlen("0") ) == COMPARE_OK) 		  	||
			(strncmp( pCARDPRESENT, "NO", strlen("NO") ) == COMPARE_OK) 		||
			(strncmp( pCARDPRESENT, "F", strlen("F") ) == COMPARE_OK) )
		{
			iSetCardNotPresent = 1;
		}
		else
		{
			iSetCardNotPresent = 0;
		}
	}

	debug_sprintf(szDbgMsg, "%s: iSetCardNotPresent = %d", __FUNCTION__, iSetCardNotPresent);
	APP_TRACE(szDbgMsg);

	char* pBILLPAY =  getXmlTagValue( eBILLPAY, req_xml_info );
	if (pBILLPAY == NULL) {
		pBILLPAY = "FALSE";
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: BILLPAY flag is present in the request", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	char* pPAYMENT_TYPE = getXmlTagValue(ePAYMENT_TYPE, req_xml_info);
	if(pPAYMENT_TYPE == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Payment Type is NULL!!!", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NO_CONTENT;
	}

	if( (strncmp( pBILLPAY, "TRUE", strlen("TRUE") ) == COMPARE_OK) &&
			(strncmp( pCOMMAND, COMMAND.SALE, strlen(COMMAND.SALE) ) == COMPARE_OK) &&
				(strncmp(pPAYMENT_TYPE, "CREDIT", strlen("CREDIT")) == COMPARE_OK))
	{
		iSetBillPay = 1;

		debug_sprintf(szDbgMsg, "%s: BILLPAY flag is present, its CREDIT SALE tranaction so need to set bill flag in the request", __FUNCTION__);
		APP_TRACE(szDbgMsg);
	}

	switch (GCONFIG->atype1) {
	case eRETAIL_TYPE:
	case eRESTAURANT_TYPE: {
		if (GCONFIG->eTokenMode == eTOKEN_TUT) {

			if(strncmp(pSAFTRAN, "1", 1) == COMPARE_OK)
			{
				debug_sprintf(szDbgMsg, "%s: SAFed transaction, setting Presentation flag to 1 in POS Condition code", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iSetBillPay == 1)
				{
					debug_sprintf(szDbgMsg, "%s: BILLPAY flag is present, setting positional flag to 3 in POS Condition code", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					strcpy(vhi_fieldinfo->bn25, "4001003500");
				}
				else
				{
					strcpy(vhi_fieldinfo->bn25, "4001000500");
				}
			}
			else if(iSetCardNotPresent == 1)
			{
				debug_sprintf(szDbgMsg, "%s: Card Not Present transaction, setting Presentation flag to 2 in POS Condition code", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				if(iSetBillPay == 1)
				{
					debug_sprintf(szDbgMsg, "%s: BILLPAY flag is present, setting positional flag to 3 in POS Condition code", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					strcpy(vhi_fieldinfo->bn25, "4002003500");
				}
				else
				{
					strcpy(vhi_fieldinfo->bn25, "4002000500");
				}
			}
			else
			{
				if(iSetBillPay == 1)
				{
					debug_sprintf(szDbgMsg, "%s: BILLPAY flag is present, setting positional flag to 3 in POS Condition code", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					strcpy(vhi_fieldinfo->bn25, "4000003500");
				}
				else
				{
					strcpy(vhi_fieldinfo->bn25, "4000000500");
				}
			}
		}
		else if (GCONFIG->eTokenMode == eTOKEN_TRT) {
			/*Adjustment or Completion*/
			if (strncmp(pCOMMAND, COMMAND.ADD_TIP, strlen(COMMAND.ADD_TIP)) == COMPARE_OK || strncmp(pCOMMAND, COMMAND.COMPLETION, strlen(COMMAND.COMPLETION)) == COMPARE_OK) {
				if(strncmp(pSAFTRAN, "1", 1) == COMPARE_OK)
				{
					debug_sprintf(szDbgMsg, "%s: SAFed transaction, setting Presentation flag to 1 in POS Condition code", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					strcpy(vhi_fieldinfo->bn25, "4001000300");
				}
				if(iSetCardNotPresent == 1)
				{
					debug_sprintf(szDbgMsg, "%s: Card Not Present transaction, setting Presentation flag to 2 in POS Condition code", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					strcpy(vhi_fieldinfo->bn25, "4002000300");
				}
				else
				{
					strcpy(vhi_fieldinfo->bn25, "4000000300");
				}

				debug_sprintf(szDbgMsg, "%s: TRT transaction, Completion/ADDTip command, setting security condition to 3", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return vhi_fieldinfo->bn25;
			}
			/*end*/
			if (GCONFIG->isRegistartMode == true)
			{
				debug_sprintf(szDbgMsg, "%s: TRT transaction with encrytion enabled, Encrypted data indicator and Tokenize transaction", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(strncmp(pSAFTRAN, "1", 1) == COMPARE_OK)
				{
					debug_sprintf(szDbgMsg, "%s: SAFed transaction, setting Presentation flag to 1 in POS Condition code", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					if(iSetBillPay == 1)
					{
						debug_sprintf(szDbgMsg, "%s: BILLPAY flag is present, setting positional flag to 3 in POS Condition code", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						strcpy(vhi_fieldinfo->bn25, "4001003400");
					}
					else
					{
						strcpy(vhi_fieldinfo->bn25, "4001000400");
					}
				}
				else if(iSetCardNotPresent == 1)
				{
					debug_sprintf(szDbgMsg, "%s: Card Not Present transaction, setting Presentation flag to 2 in POS Condition code", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					if(iSetBillPay == 1)
					{
						debug_sprintf(szDbgMsg, "%s: BILLPAY flag is present, setting positional flag to 3 in POS Condition code", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						strcpy(vhi_fieldinfo->bn25, "4002003400");
					}
					else
					{
						strcpy(vhi_fieldinfo->bn25, "4002000400");
					}
				}
				else
				{
					if(iSetBillPay == 1)
					{
						debug_sprintf(szDbgMsg, "%s: BILLPAY flag is present, setting positional flag to 3 in POS Condition code", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						strcpy(vhi_fieldinfo->bn25, "4000003400");
					}
					else
					{
						strcpy(vhi_fieldinfo->bn25, "4000000400");
					}
				}
			}
			else
			{
				debug_sprintf(szDbgMsg, "%s: TRT transaction with out encryption enabled, Tokenize transaction (request host generate and return token)", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(strncmp(pSAFTRAN, "1", 1) == COMPARE_OK)
				{
					debug_sprintf(szDbgMsg, "%s: SAFed transaction, setting Presentation flag to 1 in POS Condition code", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					if(iSetBillPay == 1)
					{
						debug_sprintf(szDbgMsg, "%s: BILLPAY flag is present, setting positional flag to 3 in POS Condition code", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						strcpy(vhi_fieldinfo->bn25, "4001003300");
					}
					else
					{
						strcpy(vhi_fieldinfo->bn25, "4001000300");
					}
				}
				else if(iSetCardNotPresent == 1)
				{
					debug_sprintf(szDbgMsg, "%s: Card Not Present transaction, setting Presentation flag to 2 in POS Condition code", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					if(iSetBillPay == 1)
					{
						debug_sprintf(szDbgMsg, "%s: BILLPAY flag is present, setting positional flag to 3 in POS Condition code", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						strcpy(vhi_fieldinfo->bn25, "4002003300");
					}
					else
					{
						strcpy(vhi_fieldinfo->bn25, "4002000300");
					}
				}
				else
				{
					if(iSetBillPay == 1)
					{
						debug_sprintf(szDbgMsg, "%s: BILLPAY flag is present, setting positional flag to 3 in POS Condition code", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						strcpy(vhi_fieldinfo->bn25, "4000003300");
					}
					else
					{
						strcpy(vhi_fieldinfo->bn25, "4000000300");
					}
				}
			}
		}
		else {
			/*Adjustment or Completion*/
			if (strncmp(pCOMMAND, COMMAND.ADD_TIP, strlen(COMMAND.ADD_TIP)) == COMPARE_OK || strncmp(pCOMMAND, COMMAND.COMPLETION, strlen(COMMAND.COMPLETION)) == COMPARE_OK) {
				if(strncmp(pSAFTRAN, "1", 1) == COMPARE_OK)
				{
					debug_sprintf(szDbgMsg, "%s: SAFed transaction, setting Presentation flag to 1 in POS Condition code", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					strcpy(vhi_fieldinfo->bn25, "4001000000");
				}
				else if(iSetCardNotPresent == 1)
				{
					debug_sprintf(szDbgMsg, "%s: Card Not Present transaction, setting Presentation flag to 2 in POS Condition code", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					strcpy(vhi_fieldinfo->bn25, "4002000000");
				}
				else
				{
					strcpy(vhi_fieldinfo->bn25, "4000000000");
				}

				return vhi_fieldinfo->bn25;
			}
			/*end*/

			if (GCONFIG->isRegistartMode == true) //VSP mode, setting POS condition code to 1 (Encrypted data indicator)
			{
				debug_sprintf(szDbgMsg, "%s: NON-TRT transaction with encryption enabled, Encrypted data indicator, see Special Transaction Processing section (E2EE).", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(strncmp(pSAFTRAN, "1", 1) == COMPARE_OK)
				{
					debug_sprintf(szDbgMsg, "%s: SAFed transaction, setting Presentation flag to 1 in POS Condition code", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					if(iSetBillPay == 1)
					{
						debug_sprintf(szDbgMsg, "%s: BILLPAY flag is present, setting positional flag to 3 in POS Condition code", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						strcpy(vhi_fieldinfo->bn25, "4001003100");
					}
					else
					{
						strcpy(vhi_fieldinfo->bn25, "4001000100");
					}
				}
				else if(iSetCardNotPresent == 1)
				{
					debug_sprintf(szDbgMsg, "%s: Card Not Present transaction, setting Presentation flag to 2 in POS Condition code", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					if(iSetBillPay == 1)
					{
						debug_sprintf(szDbgMsg, "%s: BILLPAY flag is present, setting positional flag to 3 in POS Condition code", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						strcpy(vhi_fieldinfo->bn25, "4002003100");
					}
					else
					{
						strcpy(vhi_fieldinfo->bn25, "4002000100");
					}
				}
				else
				{
					if(iSetBillPay == 1)
					{
						debug_sprintf(szDbgMsg, "%s: BILLPAY flag is present, setting positional flag to 3 in POS Condition code", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						strcpy(vhi_fieldinfo->bn25, "4000003100");
					}
					else
					{
						strcpy(vhi_fieldinfo->bn25, "4000000100");
					}
				}
			}
			else //Non VSP mode, setting POS condition code to 0 (No security concern (default))
			{
				debug_sprintf(szDbgMsg, "%s: NON-TRT transaction with out encryption enabled, No security concern (default)", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				if(strncmp(pSAFTRAN, "1", 1) == COMPARE_OK)
				{
					debug_sprintf(szDbgMsg, "%s: SAFed transaction, setting Presentation flag to 1 in POS Condition code", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					if(iSetBillPay == 1)
					{
						debug_sprintf(szDbgMsg, "%s: BILLPAY flag is present, setting positional flag to 3 in POS Condition code", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						strcpy(vhi_fieldinfo->bn25, "4001003000");
					}
					else
					{
						strcpy(vhi_fieldinfo->bn25, "4001000000");
					}
				}
				else if(iSetCardNotPresent == 1)
				{
					debug_sprintf(szDbgMsg, "%s: Card Not Present transaction, setting Presentation flag to 2 in POS Condition code", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					if(iSetBillPay == 1)
					{
						debug_sprintf(szDbgMsg, "%s: BILLPAY flag is present, setting positional flag to 3 in POS Condition code", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						strcpy(vhi_fieldinfo->bn25, "4002003000");
					}
					else
					{
						strcpy(vhi_fieldinfo->bn25, "4002000000");
					}
				}
				else
				{
					if(iSetBillPay == 1)
					{
						debug_sprintf(szDbgMsg, "%s: BILLPAY flag is present, setting positional flag to 3 in POS Condition code", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						strcpy(vhi_fieldinfo->bn25, "4000003000");
					}
					else
					{
						strcpy(vhi_fieldinfo->bn25, "4000000000");
					}
				}
			}
		}

		break;
	}
	case eCAT_TYPE: {
		if (GCONFIG->eTokenMode == eTOKEN_TUT) {
			if(strncmp(pSAFTRAN, "1", 1) == COMPARE_OK)
			{
				debug_sprintf(szDbgMsg, "%s: SAFed transaction, setting Presentation flag to 1 in POS Condition code", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				strcpy(vhi_fieldinfo->bn25, "3001000500");
			}
			else if(iSetCardNotPresent == 1)
			{
				debug_sprintf(szDbgMsg, "%s: Card Not Present transaction, setting Presentation flag to 2 in POS Condition code", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				strcpy(vhi_fieldinfo->bn25, "3002000500");
			}
			else
			{
				strcpy(vhi_fieldinfo->bn25, "3000000500");
			}
		}
		else if (GCONFIG->eTokenMode == eTOKEN_TRT) {
			/*Adjustment or Completion*/
			if (strncmp(pCOMMAND, COMMAND.ADD_TIP, strlen(COMMAND.ADD_TIP)) == COMPARE_OK || strncmp(pCOMMAND, COMMAND.COMPLETION, strlen(COMMAND.COMPLETION)) == COMPARE_OK) {
				if(strncmp(pSAFTRAN, "1", 1) == COMPARE_OK)
				{
					debug_sprintf(szDbgMsg, "%s: SAFed transaction, setting Presentation flag to 1 in POS Condition code", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					strcpy(vhi_fieldinfo->bn25, "3001000300");
				}
				else if(iSetCardNotPresent == 1)
				{
					debug_sprintf(szDbgMsg, "%s: Card Not Present transaction, setting Presentation flag to 2 in POS Condition code", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					strcpy(vhi_fieldinfo->bn25, "3002000300");
				}
				else
				{
					strcpy(vhi_fieldinfo->bn25, "3000000300");
				}

				return vhi_fieldinfo->bn25;
			}
			/*end*/
			if(strncmp(pSAFTRAN, "1", 1) == COMPARE_OK)
			{
				debug_sprintf(szDbgMsg, "%s: SAFed transaction, setting Presentation flag to 1 in POS Condition code", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				strcpy(vhi_fieldinfo->bn25, "3001000400");
			}
			else if(iSetCardNotPresent == 1)
			{
				debug_sprintf(szDbgMsg, "%s: Card Not Present transaction, setting Presentation flag to 2 in POS Condition code", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				strcpy(vhi_fieldinfo->bn25, "3002000400");
			}
			else
			{
				strcpy(vhi_fieldinfo->bn25, "3000000400");
			}
		}
		else {
			/*Adjustment or Completion*/
			if (strncmp(pCOMMAND, COMMAND.ADD_TIP, strlen(COMMAND.ADD_TIP)) == COMPARE_OK || strncmp(pCOMMAND, COMMAND.COMPLETION, strlen(COMMAND.COMPLETION)) == COMPARE_OK) {
				if(strncmp(pSAFTRAN, "1", 1) == COMPARE_OK)
				{
					debug_sprintf(szDbgMsg, "%s: SAFed transaction, setting Presentation flag to 1 in POS Condition code", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					strcpy(vhi_fieldinfo->bn25, "3001000000");
				}
				else if(iSetCardNotPresent == 1)
				{
					debug_sprintf(szDbgMsg, "%s: Card Not Present transaction, setting Presentation flag to 2 in POS Condition code", __FUNCTION__);
					APP_TRACE(szDbgMsg);
					strcpy(vhi_fieldinfo->bn25, "3002000000");
				}
				else
				{
					strcpy(vhi_fieldinfo->bn25, "3000000000");
				}

				return vhi_fieldinfo->bn25;
			}
			/*end*/
			if(strncmp(pSAFTRAN, "1", 1) == COMPARE_OK)
			{
				debug_sprintf(szDbgMsg, "%s: SAFed transaction, setting Presentation flag to 1 in POS Condition code", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				strcpy(vhi_fieldinfo->bn25, "3001000100");
			}
			else if(iSetCardNotPresent == 1)
			{
				debug_sprintf(szDbgMsg, "%s: Card Not Present transaction, setting Presentation flag to 2 in POS Condition code", __FUNCTION__);
				APP_TRACE(szDbgMsg);
				strcpy(vhi_fieldinfo->bn25, "3002000100");
			}
			else
			{
				strcpy(vhi_fieldinfo->bn25, "3000000100");
			}
		}

		break;
	}
	default: {
		debug_sprintf(szDbgMsg, "%s: ERROR! atype1 invalid", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NO_CONTENT;
	}
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn25);
	APP_TRACE(szDbgMsg);

	return vhi_fieldinfo->bn25;
}

char* getFieldBN32(sCONFIG * GCONFIG, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//config type mode
	memset(vhi_fieldinfo->bn32, 0x00, sizeof(vhi_fieldinfo->bn32));
	//strncpy( vhi_fieldinfo->bn32, getEnv("DHI", "bankid"), sizeof(vhi_fieldinfo->bn32)-1);
    strcpy( vhi_fieldinfo->bn32, "");
    strncat( vhi_fieldinfo->bn32, GCONFIG->bankID, sizeof(vhi_fieldinfo->bn32)-1);

	if (strlen(vhi_fieldinfo->bn32) > 1) {
		precedeZero(sizeof(vhi_fieldinfo->bn32), vhi_fieldinfo->bn32);

		if (strlen(vhi_fieldinfo->bn32) != (sizeof(vhi_fieldinfo->bn32)-1)) {
			debug_sprintf(szDbgMsg, "%s: ERROR! vhi_fieldinfo->bn32 less or over", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NO_CONTENT;
		}
		debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn32);
		APP_TRACE(szDbgMsg);

		return vhi_fieldinfo->bn32;
	}

	memset(vhi_fieldinfo->bn32, 0x00, sizeof(vhi_fieldinfo->bn32));

	switch (GCONFIG->atype1) {
	case eRETAIL_TYPE:
	case eRESTAURANT_TYPE:
	case eCAT_TYPE: {
		strcpy(vhi_fieldinfo->bn32, "1340");
		break;
	}
	default: {
		debug_sprintf(szDbgMsg, "%s: ERROR! GCONFIG->atype1 invalid", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NO_CONTENT;
	}
	}

	precedeZero(sizeof(vhi_fieldinfo->bn32), vhi_fieldinfo->bn32);

	if (strlen(vhi_fieldinfo->bn32) != (sizeof(vhi_fieldinfo->bn32)-1)) {
		debug_sprintf(szDbgMsg, "%s: ERROR! vhi_fieldinfo->bn32 less or over", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NO_CONTENT;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn32);
	APP_TRACE(szDbgMsg);

	return vhi_fieldinfo->bn32;
}

char* getFieldBN41(sCONFIG * GCONFIG, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//config type mode
	memset(vhi_fieldinfo->bn41, 0x00, sizeof(vhi_fieldinfo->bn41));
	//strncpy( vhi_fieldinfo->bn41, getEnv("DHI", "tid"), sizeof(vhi_fieldinfo->bn41)-1);
    strcpy( vhi_fieldinfo->bn41, "");
    strncat( vhi_fieldinfo->bn41, GCONFIG->tid, sizeof(vhi_fieldinfo->bn41)-1);

	if (strlen(vhi_fieldinfo->bn41) > 1) {
		precedeZero(sizeof(vhi_fieldinfo->bn41), vhi_fieldinfo->bn41);

		if (strlen(vhi_fieldinfo->bn41) != (sizeof(vhi_fieldinfo->bn41)-1)) {
			debug_sprintf(szDbgMsg, "%s: ERROR! vhi_fieldinfo->bn41 less or over", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NO_CONTENT;
		}

		debug_sprintf(szDbgMsg, "%s: bn41[%s]", __FUNCTION__, vhi_fieldinfo->bn41);
		APP_TRACE(szDbgMsg);

		return vhi_fieldinfo->bn41;
	}

	memset(vhi_fieldinfo->bn41, 0x00, sizeof(vhi_fieldinfo->bn41));

	switch (GCONFIG->atype1) {
	case eRETAIL_TYPE:
	case eRESTAURANT_TYPE:
	case eCAT_TYPE: {
		strcpy(vhi_fieldinfo->bn41, "001");
		break;
	}
	default: {
		debug_sprintf(szDbgMsg, "%s: ERROR! GCONFIG->atype1 invalid", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NO_CONTENT;
	}
	}

	precedeZero(sizeof(vhi_fieldinfo->bn41), vhi_fieldinfo->bn41);

	if (strlen(vhi_fieldinfo->bn41) != (sizeof(vhi_fieldinfo->bn41)-1)) {
		debug_sprintf(szDbgMsg, "%s: ERROR! vhi_fieldinfo->bn41 less or over", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NO_CONTENT;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn41);
	APP_TRACE(szDbgMsg);

	return vhi_fieldinfo->bn41;
}

char* getFieldBN42(sCONFIG * GCONFIG, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	//config type mode
	memset(vhi_fieldinfo->bn42, 0x00, sizeof(vhi_fieldinfo->bn42));
	//strncpy( vhi_fieldinfo->bn42, getEnv("DHI", "mid"), sizeof(vhi_fieldinfo->bn42)-1);
    strcpy( vhi_fieldinfo->bn42, "");
    strncat( vhi_fieldinfo->bn42, GCONFIG->mid, sizeof(vhi_fieldinfo->bn42)-1);

	if (strlen(vhi_fieldinfo->bn42) > 1) {
		precedeZero(sizeof(vhi_fieldinfo->bn42), vhi_fieldinfo->bn42);

		if (strlen(vhi_fieldinfo->bn42) != (sizeof(vhi_fieldinfo->bn42)-1)) {
			debug_sprintf(szDbgMsg, "%s: ERROR! vhi_fieldinfo->bn42 less or over", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NO_CONTENT;
		}

		return vhi_fieldinfo->bn42;
	}
    
#ifdef DEBUG
    // Only set default values in debug
	memset(vhi_fieldinfo->bn42, 0x00, sizeof(vhi_fieldinfo->bn42));

	switch (GCONFIG->atype1) {

        case eRETAIL_TYPE:
        case eCAT_TYPE: {
            strcpy(vhi_fieldinfo->bn42, "12495077");
            break;
        }
        case eRESTAURANT_TYPE: {
            strcpy(vhi_fieldinfo->bn42, "12495085");
            break;
        }
        default: {
            debug_sprintf(szDbgMsg, "%s: ERROR! GCONFIG->atype1 invalid", __FUNCTION__);
            APP_TRACE(szDbgMsg);

            return NO_CONTENT;
        }
	}

	precedeZero(sizeof(vhi_fieldinfo->bn42), vhi_fieldinfo->bn42);

	if (strlen(vhi_fieldinfo->bn42) != (sizeof(vhi_fieldinfo->bn42)-1)) {
		debug_sprintf(szDbgMsg, "%s: ERROR! vhi_fieldinfo->bn41 less or over", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NO_CONTENT;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn42);
	APP_TRACE(szDbgMsg);

	return vhi_fieldinfo->bn42;
#else
    return NO_CONTENT;
#endif
}

char* getFieldBN43(sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);
#if 0
	char* pSAFTRAN = getXmlTagValue(eSAF_TRAN, req_xml_info);
	if (pSAFTRAN == NULL) {
		debug_sprintf(szDbgMsg, "%s: pSAFTRAN is NULL, so it is not SAF transaction", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		pSAFTRAN = "0";
	}
	memset(vhi_fieldinfo->bn43, 0x00, sizeof(vhi_fieldinfo->bn43));

	/*config type mode*/
	//strncpy( vhi_fieldinfo->bn43, getEnv("DHI", "laneid"), sizeof(vhi_fieldinfo->bn43)-1);
	if(strncmp(pSAFTRAN, "1", 1) == COMPARE_OK)
	{
		debug_sprintf(szDbgMsg, "%s: SAFed transaction, setting laneid", __FUNCTION__);
		APP_TRACE(szDbgMsg);
		//strncpy( vhi_fieldinfo->bn43, GCONFIG->safLaneID, sizeof(vhi_fieldinfo->bn43)-1);
		/*
		 * Since we are serializing the SAF and Online Transactions
		 * we need not send SAF transactions on different lane id
		 * sending SAF transaction also on same laneid as online one
		 */
        strcpy( vhi_fieldinfo->bn43, "");
        strncat( vhi_fieldinfo->bn43, GCONFIG->laneID, sizeof(vhi_fieldinfo->bn43)-1);
	}
	else
	{
        strcpy( vhi_fieldinfo->bn43, "");
        strncat( vhi_fieldinfo->bn43, GCONFIG->laneID, sizeof(vhi_fieldinfo->bn43)-1);
	}


	if (strlen(vhi_fieldinfo->bn43) > 1) {
		precedeZero(sizeof(vhi_fieldinfo->bn43), vhi_fieldinfo->bn43);
		return vhi_fieldinfo->bn43;
	}
	/*end*/

	char* pLANE = getXmlTagValue(eLANE, req_xml_info);

	if (pLANE == NULL) {
		strcpy(vhi_fieldinfo->bn43, "001"); //default for pLANE is 01 to 99
	}
	else {
		int len = sizeof(vhi_fieldinfo->bn43)-1;
        strcpy(vhi_fieldinfo->bn43, "");
		strncat(vhi_fieldinfo->bn43, pLANE, len);
		precedeZero(sizeof(vhi_fieldinfo->bn43), vhi_fieldinfo->bn43);
	}
#endif
	/*
	 * Praveen_P1: 9 November 2016
	 * Fix for RESAMDOCS-169
	 * LANE field from the SSI request needs to be honored first
	 * The priority would be
	 * LANE field in SSI Request -> laneid from config file under DHI section -> Default value of 001
	 */

	char* pLANE = getXmlTagValue(eLANE, req_xml_info);

	if (pLANE == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: LANE field is not present in the SSI request", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		if(strlen(GCONFIG->laneID) > 1)
		{
			debug_sprintf(szDbgMsg, "%s: LANEID is present in the config file, considering it", __FUNCTION__);
			APP_TRACE(szDbgMsg);
			strcpy( vhi_fieldinfo->bn43, "");
			strncat( vhi_fieldinfo->bn43, GCONFIG->laneID, sizeof(vhi_fieldinfo->bn43)-1);
			precedeZero(sizeof(vhi_fieldinfo->bn43), vhi_fieldinfo->bn43);
		}
		else
		{
			strcpy(vhi_fieldinfo->bn43, "001"); //default for pLANE is 01 to 99
		}
	}
	else
	{
		debug_sprintf(szDbgMsg, "%s: LANE field is present in the SSI request, considering it", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		int len = sizeof(vhi_fieldinfo->bn43)-1;
		strcpy(vhi_fieldinfo->bn43, "");
		strncat(vhi_fieldinfo->bn43, pLANE, len);
		precedeZero(sizeof(vhi_fieldinfo->bn43), vhi_fieldinfo->bn43);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn43);
	APP_TRACE(szDbgMsg);

	return vhi_fieldinfo->bn43;
}

char* getFieldBN45(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(vhi_fieldinfo->bn45, 0x00, sizeof(vhi_fieldinfo->bn45));
	char* pTRACK_DATA = getXmlTagValue(eTRACK_DATA, req_xml_info);
	char* pPRESENT_FLAG = getXmlTagValue(ePRESENT_FLAG, req_xml_info);
	char* pPAYMENT_TYPE = getXmlTagValue(ePAYMENT_TYPE, req_xml_info);
	char* pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);
	char* pCTROUTD = getXmlTagValue(eCTROUTD, req_xml_info);

	if (pPAYMENT_TYPE == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! PAYMENT_TYPE is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NO_CONTENT;
	}

	if (pCOMMAND == NULL) {
		debug_sprintf(szDbgMsg, "%s: ERROR! COMMAND is NULL", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NO_CONTENT;
	}

	/*temporary for de tokenization*/
	if (GCONFIG->eTokenMode == eTOKEN_TUT) {
		fillSpace(sizeof(vhi_fieldinfo->bn45), vhi_fieldinfo->bn45);

		return vhi_fieldinfo->bn45;
	}
	/*end*/

	debug_sprintf(szDbgMsg, "%s: Present Flag is %s", __FUNCTION__, pPRESENT_FLAG);
	APP_TRACE(szDbgMsg);

	/*temporary for adjustment*/
	if (strncmp(pCOMMAND, COMMAND.ADD_TIP, strlen(COMMAND.ADD_TIP)) == COMPARE_OK) {
		if (pCTROUTD != NULL) {
			char trackdata[76+1] = {0};

			getAdjustmentData(NULL, trackdata, NULL, GLOBAL, req_xml_info);

			if (strlen(trackdata) <= 0) {
				debug_sprintf(szDbgMsg, "%s: WARNING!", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return NO_CONTENT;
			}

			maskPanTrack(trackdata);
			strcpy(vhi_fieldinfo->bn45, trackdata);
			precedeSpace(sizeof(vhi_fieldinfo->bn45), vhi_fieldinfo->bn45);

			return vhi_fieldinfo->bn45;
		}
		else {
			debug_sprintf(szDbgMsg, "%s: WARNING!", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NO_CONTENT;
		}
	}
	/*end*/

	if (strncmp(pCOMMAND, COMMAND.CREDIT, strlen(COMMAND.CREDIT)) == COMPARE_OK
	&& pCTROUTD != NULL
	&& pPRESENT_FLAG == NULL)
	{
		pPRESENT_FLAG = SWIPED_ENTRY;
	}

	if (pTRACK_DATA == NULL) {
		if (strncmp(pPAYMENT_TYPE, COMMAND.CREDIT, strlen(COMMAND.CREDIT)) == COMPARE_OK) {

			if (pPRESENT_FLAG == NULL && GCONFIG->isRegistartMode != true) {
				debug_sprintf(szDbgMsg, "%s: ERROR! PRESENT_FLAG is NULL during CREDIT", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return NO_CONTENT;
			}
#if 0 //Praveen_P1: Not changing the PRESENT_FLAG value based on isRegistartMode
			if (GCONFIG->isRegistartMode == true) {

				pPRESENT_FLAG = SWIPED_ENTRY;
			}
#endif

			if (strncmp(pPRESENT_FLAG, MANUAL_ENTRY, 2) == COMPARE_OK) {

				char* pACCT_NUM = getXmlTagValue(eACCT_NUM, req_xml_info);
				char* pEXP_YEAR = getXmlTagValue(eEXP_YEAR, req_xml_info);
				char* pEXP_MONTH = getXmlTagValue(eEXP_MONTH, req_xml_info);
				char* pCVV2 = getXmlTagValue(eCVV2, req_xml_info);

				if (pACCT_NUM == NULL) {
					debug_sprintf(szDbgMsg, "%s: ERROR! ACCT_NUM is NULL", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					return NO_CONTENT;
				}

				if (pEXP_YEAR == NULL) {
					debug_sprintf(szDbgMsg, "%s: ERROR! EXP_YEAR is NULL", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					return NO_CONTENT;
				}

				if (pEXP_MONTH == NULL) {
					debug_sprintf(szDbgMsg, "%s: ERROR! EXP_MONTH is NULL", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					return NO_CONTENT;
				}

				strcpy( vhi_fieldinfo->bn45, pACCT_NUM);
				strcat( vhi_fieldinfo->bn45, "=");
				strcat( vhi_fieldinfo->bn45, pEXP_YEAR);
				strcat( vhi_fieldinfo->bn45, pEXP_MONTH);

				if (pCVV2 != NULL) {
					strncat( vhi_fieldinfo->bn45, pCVV2, strlen(pCVV2) );
				}

				precedeSpace( sizeof(vhi_fieldinfo->bn45), vhi_fieldinfo->bn45 );

			}
			else if (strncmp(pPRESENT_FLAG, SWIPED_ENTRY, 1) == COMPARE_OK
			&& strncmp(pCOMMAND, COMMAND.CREDIT, strlen(COMMAND.CREDIT)) == COMPARE_OK
			&& pCTROUTD != NULL) {
				char tmp[76+1] = {0};
				getVoidData(pCTROUTD, tmp, NULL, NULL, GLOBAL);
				strcpy(vhi_fieldinfo->bn45, tmp);
				precedeSpace(sizeof(vhi_fieldinfo->bn45), vhi_fieldinfo->bn45);
			}
			else {
				debug_sprintf(szDbgMsg, "%s: ERROR! TRACK_DATA is NULL", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				return NO_CONTENT;
			}
		}
		else if (strncmp(pPAYMENT_TYPE, "GIFT", 4) == COMPARE_OK) {
			/*COMPLETION and Refund*/
			if ((strncmp(pCOMMAND, COMMAND.COMPLETION, strlen(COMMAND.COMPLETION)) == COMPARE_OK
			  || strncmp(pCOMMAND, COMMAND.CREDIT, strlen(COMMAND.CREDIT)) == COMPARE_OK)
			  && pCTROUTD != NULL) {
				if (GLOBAL->http.status_code != eHTTP_GATEWAY_TIMEOUT) {
					char cPAN[76+1] = {0};

					getVoidData(pCTROUTD, cPAN, NULL, NULL, GLOBAL);

					if (strlen(cPAN) > sizeof(vhi_fieldinfo->bn45)-1) {
						debug_sprintf(szDbgMsg, "%s: ERROR! pan size over", __FUNCTION__);
						APP_TRACE(szDbgMsg);

						return NO_CONTENT;
					}

					strcpy(vhi_fieldinfo->bn45, cPAN);
					precedeSpace(sizeof(vhi_fieldinfo->bn45), vhi_fieldinfo->bn45);

					return vhi_fieldinfo->bn45;
				}
				else {
					precedeSpace(sizeof(vhi_fieldinfo->bn45), vhi_fieldinfo->bn45);
				}
			}
			/*end*/

			if (pPRESENT_FLAG == NULL) {
				debug_sprintf(szDbgMsg, "%s: WARNING! PRESENT_FLAG is NULL during GIFT", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				pPRESENT_FLAG = SWIPED_ENTRY;
			}

			if (strncmp(pPRESENT_FLAG, MANUAL_ENTRY, 2) == COMPARE_OK) {
				char* pACCT_NUM = getXmlTagValue(eACCT_NUM, req_xml_info);
				char* pEXP_YEAR = getXmlTagValue(eEXP_YEAR, req_xml_info);
				char* pEXP_MONTH = getXmlTagValue(eEXP_MONTH, req_xml_info);

				if (pACCT_NUM == NULL) {
					debug_sprintf(szDbgMsg, "%s: ERROR! ACCT_NUM is NULL", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					return NO_CONTENT;
				}

				if (pEXP_YEAR == NULL) {
					debug_sprintf(szDbgMsg, "%s: ERROR! EXP_YEAR is NULL", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					return NO_CONTENT;
				}

				if (pEXP_MONTH == NULL) {
					debug_sprintf(szDbgMsg, "%s: ERROR! EXP_MONTH is NULL", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					return NO_CONTENT;
				}

				strcpy( vhi_fieldinfo->bn45, pACCT_NUM);
				strcat( vhi_fieldinfo->bn45, "=");
				strcat( vhi_fieldinfo->bn45, pEXP_YEAR);
				strcat( vhi_fieldinfo->bn45, pEXP_MONTH);
				precedeSpace( sizeof(vhi_fieldinfo->bn45), vhi_fieldinfo->bn45 );
			}
		}
		else if (strncmp(pPAYMENT_TYPE, "ADMIN", 5) == COMPARE_OK) {
			if (strncmp(pPRESENT_FLAG, MANUAL_ENTRY, 2) == COMPARE_OK) {
				char* pACCT_NUM = getXmlTagValue(eACCT_NUM, req_xml_info);
				char* pEXP_YEAR = getXmlTagValue(eEXP_YEAR, req_xml_info);
				char* pEXP_MONTH = getXmlTagValue(eEXP_MONTH, req_xml_info);

				if (pACCT_NUM == NULL) {
					debug_sprintf(szDbgMsg, "%s: ERROR! ACCT_NUM is NULL", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					return NO_CONTENT;
				}

				if (pEXP_YEAR == NULL) {
					debug_sprintf(szDbgMsg, "%s: ERROR! EXP_YEAR is NULL", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					return NO_CONTENT;
				}

				if (pEXP_MONTH == NULL) {
					debug_sprintf(szDbgMsg, "%s: ERROR! EXP_MONTH is NULL", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					return NO_CONTENT;
				}

				strcpy( vhi_fieldinfo->bn45, pACCT_NUM);
				strcat( vhi_fieldinfo->bn45, "=");
				strcat( vhi_fieldinfo->bn45, pEXP_YEAR);
				strcat( vhi_fieldinfo->bn45, pEXP_MONTH);
				precedeSpace( sizeof(vhi_fieldinfo->bn45), vhi_fieldinfo->bn45 );
			}
		}
		/*
		 * Praveen_P1: EBT Manual is allowed, so looking to fill the account number
		 */
		else if (strncmp(pPAYMENT_TYPE, "EBT", 3) == COMPARE_OK){
			if (strncmp(pPRESENT_FLAG, MANUAL_ENTRY, 2) == COMPARE_OK) {
				char* pACCT_NUM = getXmlTagValue(eACCT_NUM, req_xml_info);
				char* pEXP_YEAR = getXmlTagValue(eEXP_YEAR, req_xml_info);
				char* pEXP_MONTH = getXmlTagValue(eEXP_MONTH, req_xml_info);

				if (pACCT_NUM == NULL) {
					debug_sprintf(szDbgMsg, "%s: ERROR! ACCT_NUM is NULL", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					return NO_CONTENT;
				}

				if (pEXP_YEAR == NULL) {
					debug_sprintf(szDbgMsg, "%s: ERROR! EXP_YEAR is NULL", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					return NO_CONTENT;
				}

				if (pEXP_MONTH == NULL) {
					debug_sprintf(szDbgMsg, "%s: ERROR! EXP_MONTH is NULL", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					return NO_CONTENT;
				}

				strcpy( vhi_fieldinfo->bn45, pACCT_NUM);
				strcat( vhi_fieldinfo->bn45, "=");
				strcat( vhi_fieldinfo->bn45, pEXP_YEAR);
				strcat( vhi_fieldinfo->bn45, pEXP_MONTH);
				precedeSpace( sizeof(vhi_fieldinfo->bn45), vhi_fieldinfo->bn45 );
			}
		}
		else { //DEBIT always Swiped entry
			debug_sprintf(szDbgMsg, "%s: ERROR! TRACK_DATA is NULL", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NO_CONTENT;
		}
	}
	else { //Normal for swiped entry
		if (strlen(pTRACK_DATA) > sizeof(vhi_fieldinfo->bn45)-1) {
			debug_sprintf(szDbgMsg, "%s: ERROR! TRACK_DATA size over", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NO_CONTENT;
		}

		strcpy( vhi_fieldinfo->bn45, pTRACK_DATA);
		precedeSpace( sizeof(vhi_fieldinfo->bn45), vhi_fieldinfo->bn45 );
	}

	if (strlen(vhi_fieldinfo->bn45) <= 0) {
		debug_sprintf(szDbgMsg, "%s: ERROR! 0", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NO_CONTENT;
	}

#ifdef DEVDEBUG
	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn45);
	APP_TRACE(szDbgMsg);
#else
	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	APP_TRACE(szDbgMsg);
#endif
	return vhi_fieldinfo->bn45;
}

char* getFieldBN48(_sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	/*
	 * Filled all with zero as per Vantiv specs
	 */
	memset(vhi_fieldinfo->bn48, 0x00, sizeof(vhi_fieldinfo->bn48));
	fillZero(sizeof(vhi_fieldinfo->bn48), vhi_fieldinfo->bn48);

	return vhi_fieldinfo->bn48;
}

char* getFieldBN52(_sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(vhi_fieldinfo->bn52, 0x00, sizeof(vhi_fieldinfo->bn52));
	char* pPIN_BLOCK = getXmlTagValue(ePIN_BLOCK, req_xml_info);

	if (pPIN_BLOCK == NULL) { //for credit
		fillSpace(sizeof(vhi_fieldinfo->bn52), vhi_fieldinfo->bn52);
	}
	else {
		int len = sizeof(vhi_fieldinfo->bn52) - 1;
        strcpy(vhi_fieldinfo->bn52, "");
        strncat(vhi_fieldinfo->bn52, pPIN_BLOCK, len);
		appendSpace(sizeof(vhi_fieldinfo->bn52), vhi_fieldinfo->bn52);
	}
#ifdef DEVDEBUG
	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn52);
	APP_TRACE(szDbgMsg);
#else
	debug_sprintf(szDbgMsg, "%s: Returning", __FUNCTION__);
	APP_TRACE(szDbgMsg);
#endif
	return vhi_fieldinfo->bn52;
}

char* getFieldBN55(_sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(vhi_fieldinfo->bn55, 0x00, sizeof(vhi_fieldinfo->bn55));
	char* pCASHIER_NUM = getXmlTagValue(eCASHIER_NUM, req_xml_info);

	if (pCASHIER_NUM == NULL) {
		fillZero(sizeof(vhi_fieldinfo->bn55), vhi_fieldinfo->bn55);
	}
	else {
		int len = sizeof(vhi_fieldinfo->bn55) - 1;
        strcpy(vhi_fieldinfo->bn55, "");
        strncat(vhi_fieldinfo->bn55, pCASHIER_NUM, len);
		precedeZero(sizeof(vhi_fieldinfo->bn55), vhi_fieldinfo->bn55);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn55);
	APP_TRACE(szDbgMsg);

	return vhi_fieldinfo->bn55;
}

char* getFieldBN60(_sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(vhi_fieldinfo->bn60, 0x00, sizeof(vhi_fieldinfo->bn60));
	char* pCASHBACK_AMOUNT = getXmlTagValue(eCASHBACK_AMNT, req_xml_info);

	if (pCASHBACK_AMOUNT == NULL) {
		fillZero(sizeof(vhi_fieldinfo->bn60), vhi_fieldinfo->bn60);
	}
	else {
		int len = sizeof(vhi_fieldinfo->bn60) - 1;
        strcpy(vhi_fieldinfo->bn60, "");
        strncat(vhi_fieldinfo->bn60, pCASHBACK_AMOUNT, len);
		removeDecimalPoint( vhi_fieldinfo->bn60 );
		precedeZero(sizeof(vhi_fieldinfo->bn60), vhi_fieldinfo->bn60);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn60);
	APP_TRACE(szDbgMsg);

	return vhi_fieldinfo->bn60;
}

char* getFieldBN65(sGLOBALS* GLOBAL, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(vhi_fieldinfo->bn65, 0x00, sizeof(vhi_fieldinfo->bn65));

	char* pAUTH_CODE 		= getXmlTagValue(eAUTH_CODE, req_xml_info);
	char* pSAFTRAN 			= getXmlTagValue(eSAF_TRAN, req_xml_info);
	char* pSAF_APPROVALCODE = getXmlTagValue(eSAF_APPROVALCODE, req_xml_info);

	if(pSAFTRAN == NULL)
	{
		pSAFTRAN = "0";
	}

	if (pAUTH_CODE == NULL) {
		/*
		 * Praveen_P1: 28 Sep 2016
		 * Need to send SAf Sale as POST_AUTH based on config variable
		 * For SAF transaction, need to send SAF_APPORVAL code as AUTH_ODE
		 * Checking for SAF approval code if AUTH_CODE is not present
		 */
		if( (pSAF_APPROVALCODE != NULL) && ((strncmp(pSAFTRAN, "1", 1) == COMPARE_OK)) )
		{
			debug_sprintf(szDbgMsg, "%s: Its SAF transaction and SAF Approval Code is present", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			if (strlen(pSAF_APPROVALCODE) > (sizeof(vhi_fieldinfo->bn65)-1)) {
				debug_sprintf(szDbgMsg, "%s: ERROR! pSAF_APPROVALCODE is over 6 bytes", __FUNCTION__);
				APP_TRACE(szDbgMsg);

				GLOBAL->http.status_code = eHTTP_OK;
				GLOBAL->appErrCode = eERR_INVALID_AUTH_CODE;

				return NO_CONTENT;
			}

			int len = sizeof(vhi_fieldinfo->bn65) - 1;
			strcpy(vhi_fieldinfo->bn65, "");
			strncat(vhi_fieldinfo->bn65, pSAF_APPROVALCODE, len);
			appendSpace(sizeof(vhi_fieldinfo->bn65), vhi_fieldinfo->bn65);
		}
		else
		{
			fillSpace(sizeof(vhi_fieldinfo->bn65), vhi_fieldinfo->bn65);
		}
	}
	else {
		if (strlen(pAUTH_CODE) > (sizeof(vhi_fieldinfo->bn65)-1)) {
			debug_sprintf(szDbgMsg, "%s: ERROR! pAUTH_CODE is over 6 bytes", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			GLOBAL->http.status_code = eHTTP_OK;
			GLOBAL->appErrCode = eERR_INVALID_AUTH_CODE;

			return NO_CONTENT;
		}

		int len = sizeof(vhi_fieldinfo->bn65) - 1;
        strcpy(vhi_fieldinfo->bn65, "");
        strncat(vhi_fieldinfo->bn65, pAUTH_CODE, len);
		appendSpace(sizeof(vhi_fieldinfo->bn65), vhi_fieldinfo->bn65);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn65);
	APP_TRACE(szDbgMsg);

	return vhi_fieldinfo->bn65;
}

char* getFieldBN70(_sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    strcpy(vhi_fieldinfo->bn70, "000");     // Default value when no EMV reversal
    
    // EMV reversal types
    // card removed (401), chip declines (402) (perhaps pin pad not available (403) - XPI times out waiting for response)
    // Need XML tag to get this information
    char* pEMV_REVERSAL_TYPE = getXmlTagValue(eEMV_REVERSAL_TYPE, req_xml_info);
    if (pEMV_REVERSAL_TYPE != NULL)
    {
        strcpy(vhi_fieldinfo->bn70, "");
        strncat(vhi_fieldinfo->bn70, pEMV_REVERSAL_TYPE, 3);
        
        debug_sprintf(szDbgMsg, "EMV reversal type %s", vhi_fieldinfo->bn70);
        APP_TRACE(szDbgMsg);
    }
    
    return vhi_fieldinfo->bn70;
}

char* getFieldBN90(_sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(vhi_fieldinfo->bn90, 0x00, sizeof(vhi_fieldinfo->bn90));

	/* set to zero as per vantiv specs*/
	fillZero(sizeof(vhi_fieldinfo->bn90), vhi_fieldinfo->bn90);

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn90);
	APP_TRACE(szDbgMsg);

	return vhi_fieldinfo->bn90;
}

char* getFieldBN106(_sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(vhi_fieldinfo->bn106, 0x00, sizeof(vhi_fieldinfo->bn106));

	char* pCUSTOMER_STREET = getXmlTagValue(eCUSTOMER_STREET, req_xml_info);
	char* pCUSTOMER_ZIP = getXmlTagValue(eCUSTOMER_ZIP, req_xml_info);
	char xxx[20+1] = {0};
	char yyy[9+1] = {0};

	if (pCUSTOMER_STREET == NULL) {
		fillSpace(sizeof(xxx), xxx);
	}
	else {
        strcpy(xxx, "");
        strncat(xxx, pCUSTOMER_STREET, (sizeof(xxx) - 1));
		appendSpace(sizeof(xxx), xxx);
	}

	if (pCUSTOMER_ZIP == NULL) {
		fillSpace(sizeof(yyy), yyy);
	}
	else {
        strcpy(yyy, "");
        strncat(yyy, pCUSTOMER_ZIP, (sizeof(yyy) - 1));
		appendSpace(sizeof(yyy), yyy);
	}

	strcpy(vhi_fieldinfo->bn106, xxx);
	strcat(vhi_fieldinfo->bn106, yyy);

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn106);
	APP_TRACE(szDbgMsg);

	return vhi_fieldinfo->bn106;
}

char* getFieldBN107(sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo, sGLOBALS* GLOBAL)
{

	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(vhi_fieldinfo->bn107, 0x00, sizeof(vhi_fieldinfo->bn107));

	char* pPRESENT_FLAG = getXmlTagValue(ePRESENT_FLAG, req_xml_info);
	//char* pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);
	char* pCTROUTD = getXmlTagValue(eCTROUTD, req_xml_info);

#ifdef NEVER
	if (strncmp(pCOMMAND, COMMAND.CREDIT, strlen(COMMAND.CREDIT)) == COMPARE_OK
		&& pCTROUTD != NULL
		&& pPRESENT_FLAG == NULL)
	{
		//Refund transaction with CTroutd is sent, we will not have the present_flag in the request so setting it to SWIPE mode for now
		pPRESENT_FLAG = SWIPED_ENTRY;
	}

	if (strncmp(pCOMMAND, COMMAND.VOID, strlen(COMMAND.VOID)) == COMPARE_OK
			&& pCTROUTD != NULL
			&& pPRESENT_FLAG == NULL)
	{
		//Void transaction with CTroutd is sent, we will not have the present_flag in the request so setting it to SWIPE mode for now
		pPRESENT_FLAG = SWIPED_ENTRY;
	}

    // TODO: RMW if <PRESENT_FLAG> not sent then use getVoidData
	if (strncmp(pCOMMAND, COMMAND.COMPLETION, strlen(COMMAND.COMPLETION)) == COMPARE_OK) {
		char cAmount[17+3] = {0};

		getVoidData(pCTROUTD, NULL, cAmount, GLOBAL);

		if (strncmp(cAmount, "S", 1) == COMPARE_OK) {
			pPRESENT_FLAG = SWIPED_ENTRY;
		}
		else if (strncmp(cAmount, "M", 1) == COMPARE_OK) {
			pPRESENT_FLAG = MANUAL_ENTRY;
		}
		else if (strncmp(cAmount, "R", 1) == COMPARE_OK) {
			pPRESENT_FLAG = PROXIMITY_ENTRY;
		}
        else if (strncmp(cAmount, "E", 1) == COMPARE_OK) {
            pPRESENT_FLAG = EMV_CHIP_ENTRY;
        }
        else if (strncmp(cAmount, "C", 1) == COMPARE_OK) {
            pPRESENT_FLAG = EMV_CTLS_ENTRY;
        }
        else if (strncmp(cAmount, "F", 1) == COMPARE_OK) {
            pPRESENT_FLAG = EMV_FALLBACK_ENTRY;
        }
		else {
			debug_sprintf(szDbgMsg, "%s: ERROR! No PRESENT_FLAG indicator", __FUNCTION__);
			APP_TRACE(szDbgMsg);

			return NO_CONTENT;
		}
	}
#endif

	if(pPRESENT_FLAG == NULL)
	{
		debug_sprintf(szDbgMsg, "%s: Present Flag is NULL (It should not be normal case)", __FUNCTION__);
		APP_TRACE(szDbgMsg);
        
        char cAmount[17+3] = {0};
        
        getVoidData(pCTROUTD, NULL, cAmount, NULL, GLOBAL);
        
        if (strncmp(cAmount, "S", 1) == COMPARE_OK)
        {
            pPRESENT_FLAG = SWIPED_ENTRY;
        }
        else if (strncmp(cAmount, "M", 1) == COMPARE_OK)
        {
            pPRESENT_FLAG = MANUAL_ENTRY;
        }
        else if (strncmp(cAmount, "R", 1) == COMPARE_OK)
        {
            pPRESENT_FLAG = PROXIMITY_ENTRY;
        }
        else if (strncmp(cAmount, "E", 1) == COMPARE_OK)
        {
            pPRESENT_FLAG = EMV_CHIP_ENTRY;
        }
        else if (strncmp(cAmount, "C", 1) == COMPARE_OK)
        {
            pPRESENT_FLAG = EMV_CTLS_ENTRY;
        }
        else if (strncmp(cAmount, "F", 1) == COMPARE_OK)
        {
            pPRESENT_FLAG = EMV_FALLBACK_ENTRY;
        }
        else
        {
            debug_sprintf(szDbgMsg, "%s: ERROR! No PRESENT_FLAG indicator", __FUNCTION__);
            APP_TRACE(szDbgMsg);
            
            pPRESENT_FLAG = SWIPED_ENTRY; //Currently defaulting to Swipe mode, please check why we entered this if condition
        }
	}

	switch (GCONFIG->atype1) {
	case eRETAIL_TYPE:
	case eRESTAURANT_TYPE: {
		/*
		 * Vantiv suggested to send 107 field as 45 if terminal is EMV capaable
		 * if EMV is enabled, then sending as 45 for all types of transactions,
		 * if is not enabled, leaving the old behaviour setting based on the present flag
		 * fix for PTMX-390
		 */
		if(GCONFIG->emvenabled == true)
		{
			strcpy(vhi_fieldinfo->bn107, "45");
		}
		else
		{
			if (strncmp(pPRESENT_FLAG, PROXIMITY_ENTRY, 1) == COMPARE_OK)
			{
				strcpy(vhi_fieldinfo->bn107, "48");
			}
			else if ((strncmp(pPRESENT_FLAG, EMV_CHIP_ENTRY, 1) == COMPARE_OK) || (strncmp(pPRESENT_FLAG, EMV_CTLS_ENTRY, 1) == COMPARE_OK) || (strncmp(pPRESENT_FLAG, EMV_FALLBACK_ENTRY, 1) == COMPARE_OK))
			{
				strcpy(vhi_fieldinfo->bn107, "45");
			}
			else
			{
				strcpy(vhi_fieldinfo->bn107, "42");
			}
		}
		break;
	}
	case eCAT_TYPE: {
		strcpy(vhi_fieldinfo->bn107, "32");
		break;
	}
	default: {
		debug_sprintf(szDbgMsg, "%s: ERROR! atype1 invalid", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NO_CONTENT;
	}
	}

	if (strlen(vhi_fieldinfo->bn107)<=0) {
		debug_sprintf(szDbgMsg, "%s: ERROR! vhi_fieldinfo->bn107 has no value", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NO_CONTENT;
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn107);
	APP_TRACE(szDbgMsg);

	return vhi_fieldinfo->bn107;
}

char* getFieldBN109(_sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(vhi_fieldinfo->bn109, 0x00, sizeof(vhi_fieldinfo->bn109));
	char* pCUSTOMER_CODE = getXmlTagValue(eCUSTOMER_CODE, req_xml_info);

	if (pCUSTOMER_CODE == NULL) {
		fillSpace(sizeof(vhi_fieldinfo->bn109), vhi_fieldinfo->bn109);
	}
	else {
		int len = sizeof(vhi_fieldinfo->bn109) - 1;
        strcpy(vhi_fieldinfo->bn109, "");
        strncat(vhi_fieldinfo->bn109, pCUSTOMER_CODE, len);
		precedeSpace(sizeof(vhi_fieldinfo->bn109), vhi_fieldinfo->bn109);
	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn109);
	APP_TRACE(szDbgMsg);

	return vhi_fieldinfo->bn109;
}

char* getFieldBN110(_sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(vhi_fieldinfo->bn110, 0x00, sizeof(vhi_fieldinfo->bn110));
	char* pTAX_AMOUNT = getXmlTagValue(eTAX_AMOUNT, req_xml_info);
	char* pTAX_IND = getXmlTagValue(eTAX_IND, req_xml_info);

	/*
	 *
	 	No Indicator - fillzero
		0 = no tax    (888888888)
		1 = you will use TAX Amount POS provided
		2 = (888888888)
		3 = (999999999)
	 */
	debug_sprintf(szDbgMsg, "%s: TAX Amount %s, TAX Indicator %s", __FUNCTION__, pTAX_AMOUNT, pTAX_IND);
	APP_TRACE(szDbgMsg);

	if (pTAX_IND == NULL) {
		fillZero(sizeof(vhi_fieldinfo->bn110), vhi_fieldinfo->bn110);
	}
	else {
		if (strncmp(pTAX_IND, "0", 1) == COMPARE_OK)
		{
			fillChar8(sizeof(vhi_fieldinfo->bn110), vhi_fieldinfo->bn110);
		}
		else if (strncmp(pTAX_IND, "1", 1) == COMPARE_OK)
		{
			if(pTAX_AMOUNT != NULL)
			{
				int len = sizeof(vhi_fieldinfo->bn110) - 1;
                strcpy(vhi_fieldinfo->bn110, "");
                strncat(vhi_fieldinfo->bn110, pTAX_AMOUNT, len);

				if (removeDecimalPoint(vhi_fieldinfo->bn110) == EXIT_FAILURE) {
					debug_sprintf(szDbgMsg, "%s: ERROR! Removing of decimal failed", __FUNCTION__);
					APP_TRACE(szDbgMsg);

					return NULL;
				}
				precedeZero(sizeof(vhi_fieldinfo->bn110), vhi_fieldinfo->bn110);
			}
			else
			{
				fillZero(sizeof(vhi_fieldinfo->bn110), vhi_fieldinfo->bn110);
			}
		}
		else if (strncmp(pTAX_IND, "2", 1) == COMPARE_OK)
		{
			fillChar8(sizeof(vhi_fieldinfo->bn110), vhi_fieldinfo->bn110);
		}
		else if (strncmp(pTAX_IND, "3", 1) == COMPARE_OK)
		{
			fillChar9(sizeof(vhi_fieldinfo->bn110), vhi_fieldinfo->bn110);
		}
		else//Invalid value, ideally should not enter
		{
			fillZero(sizeof(vhi_fieldinfo->bn110), vhi_fieldinfo->bn110);
		}

	}

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn110);
	APP_TRACE(szDbgMsg);

	return vhi_fieldinfo->bn110;
}

char* getFieldBN112(_sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
    debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
    APP_TRACE(szDbgMsg);
    
    // Does not support manually entered data which requires the sequence number
    strcpy(vhi_fieldinfo->bn112, "000");
    
    debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn110);
    APP_TRACE(szDbgMsg);
    
    return vhi_fieldinfo->bn112;
}

char* getFieldBN115(_sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(vhi_fieldinfo->bn115, 0x00, sizeof(vhi_fieldinfo->bn115));
	char* pCOMMAND = getXmlTagValue(eCOMMAND, req_xml_info);

	if (pCOMMAND != NULL) {
		if (strlen(pCOMMAND) < 12) {
			strcpy(vhi_fieldinfo->bn115, pCOMMAND);
		}
		strncat(vhi_fieldinfo->bn115, "_ECHO", 5);
	}
	else {
		strcpy(vhi_fieldinfo->bn115, "V_HOST_ECHO");
	}

	if (strlen(vhi_fieldinfo->bn115)> sizeof(vhi_fieldinfo->bn115)-1) {
		debug_sprintf(szDbgMsg, "%s: ERROR! bn115 bytes over", __FUNCTION__);
		APP_TRACE(szDbgMsg);

		return NO_CONTENT;
	}

	appendSpace(sizeof(vhi_fieldinfo->bn115), vhi_fieldinfo->bn115);

	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn115);
	APP_TRACE(szDbgMsg);

	return vhi_fieldinfo->bn115;
}

char* getFieldBN117(_sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo)
{
	debug_sprintf(szDbgMsg, "%s: --- enter ---", __FUNCTION__);
	APP_TRACE(szDbgMsg);

	memset(vhi_fieldinfo->bn117, 0x00, sizeof(vhi_fieldinfo->bn117));
	char* pKEY_SERIAL_NUMBER = getXmlTagValue(eKEY_SERIAL_NUMBER, req_xml_info);

	if (pKEY_SERIAL_NUMBER == NULL) {
		fillSpace(sizeof(vhi_fieldinfo->bn117), vhi_fieldinfo->bn117);
	}
	else {
		int len = sizeof(vhi_fieldinfo->bn117) - 1;
        strcpy(vhi_fieldinfo->bn117, "");
        strncat(vhi_fieldinfo->bn117, pKEY_SERIAL_NUMBER, len);
		appendSpace(sizeof(vhi_fieldinfo->bn117), vhi_fieldinfo->bn117);
	}
#ifdef DEVDEBUG
	debug_sprintf(szDbgMsg, "%s: Returning [%s]", __FUNCTION__, vhi_fieldinfo->bn117);
	APP_TRACE(szDbgMsg);
#else
	debug_sprintf(szDbgMsg, "%s: Returning ", __FUNCTION__);
	APP_TRACE(szDbgMsg);
#endif
	return vhi_fieldinfo->bn117;
}
