/*
 * vantiv_response.h
 *
 *  Created on: Nov 5, 2013
 *      Author: Dexter M. Alberto
 */


#ifndef VANTIV_RESPONSE_H_
#define VANTIV_RESPONSE_H_
#include "define_data.h"
#include "sca_xml_request.h"

#define RECON_APPROVAL_LENGTH 81
#define RECON_GBATCH_INQ_LENGTH 270
/*
 * **********************************************
 * RESPONSE TYPE INDEX
 * **********************************************
 */
enum eRESPONSE_TYPE
{
	/*
	 * eXXXX = transaction type
	 * _XX = bit map type
	 */
	/* Authorization response */
	e0110_53, //2.1.11	Token/De-token Conversion Approval
	e0110_61, //2.1.2	GC Mass Transaction/EBT Balance Inquiry Approval
	e0110_62, //2.1.7	Gift Card/EBT Error
	e0110_90, //2.1.1	Authorization Approval
	e0110_90_GC, //2.1.3	GC GC/POSA/Debit/Credit Balance Inquiry Approval
	e0110_99, //2.1.6	Debit, Credit and Check Error
	/* Financial transaction response */
	e0210_61, //2.2.6	Gift Card Approval
	e0210_62, //2.2.8	Gift Card/EBT Error
	e0210_63, //2.2.7	Gift Card Preauthorization Approval
	e0210_91, //2.2.1	Credit/Debit Approval
	e0210_99, //2.2.2	Credit/Debit Error
	/* Financial transaction response (prior authorization sale) */
	e0230_62, //2.2.8	Gift Card/EBT Error
	e0230_91, //2.2.1	Credit/Debit Approval
	e0230_99, //2.2.2	Credit/Debit Error
	/* Acquirer reversal request response */
	e0410_61, //2.4.6	Gift Card Reversal (Void) Approval
	e0410_62, //2.4.8	Gift Card Reversal (Void) Error
	e0410_63, //2.4.7	Gift Card Preauthorization Reversal (Void) Approval
	e0410_91, //2.4.1	Credit/Debit Reversal (Void) Approval
	e0410_99, //2.4.2	Credit/Debit/Check Reversal (Void) Error
	/* Acquirer reconciliation request response */
	e0510_92, //2.5.1	Approval, 2.5.2	Gift Card Batch Inquiry Response
	e0510_99, //2.5.3	Reconciliation Error
    /* Network management response */
    e0810_99, //2.6.4	Error
	/* end */
	eXXXX_XX,
};


/*
 * **********************************************
 * PAYLOAD VARIABLES
 * **********************************************
 */
typedef struct sVANTIV_RESPONSE
{
	char* buffer;
	char* base;
	char* group;
	char r007[4+80+1]; // 20 - 80
	char r008[4+9+1];
	char r011[4+11+1];
	char r017[4+68+1]; // 8 - 68
    char r023[4+999+1];
    char r029[4+999+1];
	uint bufferlen;
	uint baselen;
	uint grouplen;
	uint totalgrp;
	enum eRESPONSE_TYPE RESPONSEID;
}sVANTIV_RESPONSE;

//sVANTIV_RESPONSE vrinfo; //vantiv response info

/*
 * **********************************************
 * MESSAGE TYPE IDENTIFIER STRUCT DATA
 * **********************************************
 */
typedef struct sVANTIV_BN
{
	char bn00[4+1]; //n
	char bnbmt[2+1]; //n
	char bn03[6+1]; //n
}sVANTIV_BN;

//sVANTIV_BN svtvbn; //Praveen_P1: Made it local to the service thread


/*
 * **********************************************
 * RESPONSE BIT MAP TYPE DATA
 * **********************************************
 */
typedef struct _sVTV_FIELDS
{
	char bnbmt[2+1];			//n Bit Map Type
	char bn00[4+1];				//n Message Type Identifier
	char bn02[19+1];			//an Primary Account Number
	char bn03[6+1];				//n Processing Code
	char bn04[9+1];				//n Amount, Transaction
	char bn07[10+1];			//n Transmission Date/Time
	char bn11[6+1];				//n System Trace Audit Number (STAN)
	char bn12[6+1];				//n Local Transaction Date
	char bn13[6+1];				//n Local Transaction Time
	char bn37[8+1];				//an Retrieval Reference Number
	char bn65[6+1];				//an Authorization Identification Response
	char bn66[1+1];				//n Settlement Code
	char bn74[6+1];				//n Returns, Count
	char bn76[6+1];				//n Sales, Count
	char bn86[12+1];			//n Returns, Amount
	char bn88[12+1];			//n Sales, Amount
	char bn91[192+1];			//an Check Error Response Text
	char bn102[28+1];			//an Account ID 1 (MICR Only)
	char bn105_1[2+1];			//an Additional Response Data/AVS Result Code
	char bn105_2[1+1];			//an Payment Service Indicator
	char bn105_3[15+1];			//an Transaction Identifier/POSA SAF Reference Number
	char bn105_4[4+1];			//an VISA Validation Code
	char bn108_1[1+1];			//an Account ID 1 Type (M = MICR)
	char bn108_2[1+1];			//an Account ID 2 Type: (D = Driver�s License)
	char bn108_3[1+1];			//an Check Type
	char bn108_4[6+1];			//n Manager Number
	char bn108_5[6+1];			//n Check Number
	char bn108_6[8+1];			//n Birth Date
	char bn108_7[8+1];			//n Cashier Number
	char bn115[16+1];			//an Trace Data 1 (Echo Data)
	char bn120_1[6+1];			//n Julian Day/Batch Number
	char bn120_2[1+1];			//an Demo Merchant Flag
	char bn120_3[4+1];			//an Network Mnemonic/Card Type
	char bn123_1[20+1];			//an Error Text
	char bn123_2[3+1];			//n Response Code
	char bn124_1[16+1];			//an Working Key
	char bn127_1[3+1];			//an Payment Type/Settlement Institution/GC Trans
	char bn127_2[3+1];			//n Sales Count
	char bn127_3[9+1];			//n Sales Amount
	char bn127_4[3+1];			//n Return Count
	char bn127_5[9+1];			//n Return Amount
	char bn128[120+1];			//n Additional Amounts
}_sVTV_FIELDS;

//_sVTV_FIELDS svbninfo; //vantiv bit number info //Praveen_P1: Made it local in the service thread


/*
 * **********************************************
 * PARSING VANTIV RESPONSE FUNCTIONS
 * FOR TEMP, PAYLOAD SHOULD START BIT MAP TYPE (eg. 0210)
 * **********************************************
 */
void freeVantivResponseData(sVANTIV_RESPONSE * vrinfo);

int parseVantivResponse(char* ptr, sGLOBALS* GLOBAL, sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo, sVANTIV_BN *svtvbn, _sSCA_XML_REQUEST *req_xml_info);

int saveVantivBaseResponse();

int saveVantivGroupResponse();

int saveVantivBitNumberResponse(sVANTIV_RESPONSE * vrinfo, sVANTIV_BN *svtvbn);

int identifyVantivResponse(sVANTIV_RESPONSE * vrinfo, sVANTIV_BN *svtvbn);

int parseIdentifiedVantivResponse();

void saveR007();
void saveR008();
void saveR011();
void saveR017();
void saveR023();
void saveR029(sVANTIV_RESPONSE * vrinfo);

/*
 * **********************************************
 * PARSE FUNCTION FOR SPECIFIC RESPONSE TYPE
 * **********************************************
 */
int VTV_Parse_0110_61(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo); //2.1.2	GC Mass Transaction/EBT Balance Inquiry Approval

int VTV_Parse_0110_62(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo); //2.1.7	Gift Card/EBT Error
int VTV_Parse_0110_90(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo); //2.1.1	Authorization Approval
int VTV_Parse_0110_90_GC(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo); //2.1.3	GC GC/POSA/Debit/Credit Balance Inquiry Approval
int VTV_Parse_0110_99(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo); //2.1.6	Debit, Credit and Check Error
int VTV_Parse_0110_53(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo); //2.1.11	Token/De-token Conversion Approval

int VTV_Parse_0210_61(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo); //2.2.6	Gift Card Approval
int VTV_Parse_0210_62(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo); //2.2.8	Gift Card/EBT Error
int VTV_Parse_0210_63(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo); //2.2.7	Gift Card Preauthorization Approval
int VTV_Parse_0210_91(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo); //2.2.1	Credit/Debit Approval
int VTV_Parse_0210_99(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo); //2.2.2	Credit/Debit Error
int VTV_Parse_0230_62(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo); //2.2.8	Gift Card/EBT Error
int VTV_Parse_0230_91(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo); //2.2.1	Credit/Debit Approval
int VTV_Parse_0230_99(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo); //2.2.2	Credit/Debit Error

int VTV_Parse_0410_61(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo); //2.4.6	Gift Card Reversal (Void) Approval
int VTV_Parse_0410_62(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo); //2.4.8	Gift Card Reversal (Void) Error
int VTV_Parse_0410_63(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo); //2.4.7	Gift Card Preauthorization Reversal (Void) Approval
int VTV_Parse_0410_91(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo); //2.4.1	Credit/Debit Reversal (Void) Approval
int VTV_Parse_0410_99(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo); //2.4.2	Credit/Debit/Check Reversal (Void) Error

int VTV_Parse_0510_92(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo); //2.5.1	Approval, 2.5.2	Gift Card Batch Inquiry Response
int VTV_Parse_0510_99(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo); //2.5.3	Reconciliation Error

/*
 * **********************************************
 * INDEX FOR GETTING BIT NUMBER VALUE
 * **********************************************
 */
enum eBN_INDEX
{
	eBN00,		//Message Type Identifier
	eBN03,		//Processing Code
	eBNBMT,		//Bit Map Type
	eBN04,		//Amount, Transaction
	eBN07,		//Transmission Date and Time
	eBN11, 		//System Trace Audit Number (STAN)
	eBN12,		//Local Transaction Date
	eBN13,		//Local Transaction Time
	eBN37, 		//Retrieval Reference Number
	eBN65, 		//Authorization Identification Response
	eBN105_1,	//Additional Response Data/AVS Result Code
	eBN105_3, 	//Transaction Identifier Banknet/POSA SAF Ref. Num.
	eBN120_1, 	//Julian Day/Batch Number
	eBN120_3, 	//Network Mnemonic/Card Type
	eBN123_1,	//Error Text
	eBN123_2,	//Response Code
	eBN128,		//Additional Amounts
	eEND,
};

/*
 * **********************************************
 * FUNCTION FOR GETTING BIT NUMBER DATA
 * **********************************************
 */
char* getVantivBNResponse(enum eBN_INDEX, _sVTV_FIELDS *svbninfo);


/*
 * **********************************************
 * FUNCTION FOR GETTING RESPONSE GROUP DATA
 * **********************************************
 */
/*
 * Additional Amounts
 */
char* getR007(sVANTIV_RESPONSE * vrinfo);

/*
 * Original Authorization Retrieval Reference Number
 */
char* getR008(sVANTIV_RESPONSE * vrinfo);

/*
 * Signature Capture Reference Number
 */
char* getR011(sVANTIV_RESPONSE * vrinfo);
/*
 * End-To-End Encryption (E2EE) Response
 */
char* getR017(sVANTIV_RESPONSE * vrinfo);

/*
 * EMV Tags Response
 */
char* getR023(sVANTIV_RESPONSE * vrinfo);

/*
 * Synchrony Promo Response
 */
char* getR029(sVANTIV_RESPONSE * vrinfo);

#endif /* VANTIV_RESPONSE_H_ */
