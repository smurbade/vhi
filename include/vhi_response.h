/*
 * vhi_response.h
 *
 *  Created on: Nov 7, 2013
 *      Author: Dexter M. Alberto
 */


#ifndef VHI_RESPONSE_H_
#define VHI_RESPONSE_H_
#include "define_data.h"
#include "vantiv_response.h"
#include "sca_xml_request.h"
#include "vhi_610_fields.h"

void freeVHIResponseData();

/*
 * **********************************************
 * XML RESPONSE DATA
 * **********************************************
 */
typedef struct sVHI_RESPONSE_INFO
{
	unsigned char* buffer;
	char amnt_bal[13+1];	//Gift Available balance
	char auth_amnt[20+1];	//Gift Card/WIC Authorized Amount
	char result[15+1];
	char result_code[10+1];
	char termination_stat[40+1];
	char response_text[100+1];
	char hostresponse_code[4+1];
	char* tosend;
	char trans_date[10+1];
	char trans_time[8+1];
	char r007_available_balance[13+1];
	char r007_authorize_amount[13+1];
	char YYYYMMDD[10+1];
	char hhmmss[8+1];
	char vsp_code[10+1];
	char vsp_result_desc[100+1];
    char cb_available_balance[14+ 1];
    char cb_start_balance[14+ 1];
    char cb_end_balance[14+ 1];
    char fs_available_balance[14+ 1];
    char fs_start_balance[14+ 1];
    char fs_end_balance[14+ 1];

}sVHI_RESPONSE_INFO;

//sVHI_RESPONSE_INFO vhi_res_info; //Praveen_P1: Made it local in the service thread

/*
 * **********************************************
 * MAIN XML RESPONSE
 * **********************************************
 */

/*
 * **********************************************
 * Parsing of group response from vantiv
 * **********************************************
 */
typedef struct sR017_data
{
	char clearacc[19+1];
	char maskacc[19+1];
	char result[19+1];
	char truncated[19+1];
	char token[19+1];
	char token_id[6+1];
	char card_token[25+1];
}sR017_data;

//sR017_data sR017_info; //Praveen_P1: Made it local in the service thread

typedef struct sR008_data
{
	char ORRN[9+1];
}sR008_data;

//sR008_data sR008_info; //Praveen_P1: Made it local in the service thread

typedef struct sR011_data
{
	char signatureCapRef[11+1];
}sR011_data;

typedef struct sR023_data
{
    UINT8   tlv_data[800];
    int     tlv_length;
}sR023_data;

typedef struct sR029_data
{
    UINT8   tlv_data[800];
    int     tlv_length;
}sR029_data;

#ifdef OLD_EMV_TAG_METHOD
typedef struct sR023_data
{
    char    auth_response_code[2];
    UINT8   *script71;
    UINT8   *script72;
    UINT8   issuer_auth_data[32];
    UINT8   issuer_script_command[261];
    UINT8   issuer_script_id[8];
    UINT8   issuer_script_results[21];
    int  auth_response_code_len;
    int  script71_len;
    int  script72_len;
    int  issuer_auth_data_len;
    int  issuer_script_command_len;
    int  issuer_script_id_len;
    int  issuer_script_results_len;
}sR023_data;
#endif

int composeVhiXmlResponse(int conn, sGLOBALS* GLOBAL, sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo, sR017_data *sR017_info, sR008_data *sR008_info, sR011_data *sR011_info, sR023_data *sR023_info, sR029_data *sR029_info, sVHI_RESPONSE_INFO *vhi_res_info, sCONFIG * GCONFIG,  _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo);
int composeVhiXmlErrResponse(int conn, sGLOBALS* GLOBAL, sVHI_RESPONSE_INFO *vhi_res_info, _sSCA_XML_REQUEST *req_xml_info);
int updateLastTranDtls(sGLOBALS* GLOBAL, _sVTV_FIELDS *svbninfo, sR023_data *sR023_info, sR017_data *sR017_info, sR008_data *sR008_info, sVHI_RESPONSE_INFO *vhi_res_info, _sSCA_XML_REQUEST *req_xml_info, sCONFIG * GCONFIG, _sVHI_FIELDS_INFO *);

/*
 * **********************************************
 * RESPONSE TYPE
 * **********************************************
 */
char* composeCreditResponse(sGLOBALS* , sVANTIV_RESPONSE * , _sVTV_FIELDS *, sR017_data *, sR008_data *, sR011_data *, sR023_data *sR023_info, sR029_data *sR029_info, sVHI_RESPONSE_INFO *, sCONFIG * , _sSCA_XML_REQUEST * , _sVHI_FIELDS_INFO * );
char* composeDebitResponse(sGLOBALS* , sVANTIV_RESPONSE * , _sVTV_FIELDS *, sR017_data *, sR008_data *, sVHI_RESPONSE_INFO *, sCONFIG * , _sSCA_XML_REQUEST * , _sVHI_FIELDS_INFO *);
char* composeGiftResponse(sGLOBALS* , sVANTIV_RESPONSE * , _sVTV_FIELDS *, sR017_data *, sR008_data *, sVHI_RESPONSE_INFO *, sCONFIG * , _sSCA_XML_REQUEST * , _sVHI_FIELDS_INFO *);
char* composeGiftPreAuthResponse(sGLOBALS* , sVANTIV_RESPONSE * , _sVTV_FIELDS *, sR017_data *, sR008_data *, sVHI_RESPONSE_INFO *, sCONFIG * , _sSCA_XML_REQUEST * , _sVHI_FIELDS_INFO *);
char* composeGiftCompletionResponse(sGLOBALS* , sVANTIV_RESPONSE * , _sVTV_FIELDS *, sR017_data *, sR008_data *, sVHI_RESPONSE_INFO *, sCONFIG * , _sSCA_XML_REQUEST * , _sVHI_FIELDS_INFO *);
char* composeSignatureResponse(sVHI_RESPONSE_INFO *vhi_res_info);
char* composeBatchResponse(sGLOBALS* GLOBAL, _sVTV_FIELDS *svbninfo, sR008_data *sR008_info, sVHI_RESPONSE_INFO *vhi_res_info, sCONFIG * , _sSCA_XML_REQUEST * );
char* composeReportResponse(sVHI_RESPONSE_INFO *vhi_res_info, _sSCA_XML_REQUEST *req_xml_info);
char* composeRegistrationResponse(sVHI_RESPONSE_INFO *vhi_res_info);
char* composeLastTranResponse(sVHI_RESPONSE_INFO *vhi_res_info);
char* composeTemporaryResponse(); //temporary
char* composeProcessorInquireResponse(sGLOBALS* GLOBAL, _sVTV_FIELDS *svbninfo, sR008_data *sR008_info, sVHI_RESPONSE_INFO *vhi_res_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info);
char* composeAdminResponse(sGLOBALS* GLOBAL, _sVTV_FIELDS *svbninfo, sR017_data *sR017_info, sR008_data *sR008_info, sVHI_RESPONSE_INFO *vhi_res_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo);
void composeErrorResponse(sGLOBALS* GLOBAL, sVHI_RESPONSE_INFO *vhi_res_info);
char *composeVersionResponse(sVHI_RESPONSE_INFO *vhi_res_info, sCONFIG * GCONFIG);
char* composeErrResponse(sGLOBALS* GLOBAL, sVHI_RESPONSE_INFO *vhi_res_info, _sSCA_XML_REQUEST *req_xml_info);
/*
 * **********************************************
 * ELEMENT'S VALUES
 * **********************************************
 */
char* VHI_AUTH_CODE(_sVTV_FIELDS *svbninfo);
char* VHI_AMOUNT_BALANCE(sVANTIV_RESPONSE * vrinfo, _sVTV_FIELDS *svbninfo, sVHI_RESPONSE_INFO *vhi_res_info, _sSCA_XML_REQUEST *req_xml_info);
char* VHI_APPROVED_AMOUNT(_sVTV_FIELDS *svbninfo, sVHI_RESPONSE_INFO *vhi_res_info, _sSCA_XML_REQUEST *req_xml_info);
char* VHI_CARDHOLDER( _sSCA_XML_REQUEST *req_xml_info);
char* VHI_ACCT_NUM( _sSCA_XML_REQUEST *req_xml_info);
char* VHI_TRACK_DATA( _sSCA_XML_REQUEST *req_xml_info);
char* VHI_SERVERID(_sSCA_XML_REQUEST *req_xml_info);
char* VHI_CLIENT_ID( _sSCA_XML_REQUEST *req_xml_info);
char* VHI_COMMAND( _sSCA_XML_REQUEST *req_xml_info);
char* VHI_CTROUTD(sR008_data *sR008_info);
char* VHI_SIGNATUREREF(sR011_data *sR011_info);
char* VHI_DEVICEKEY( _sSCA_XML_REQUEST *req_xml_info);
char* VHI_DELIMITER( _sSCA_XML_REQUEST *req_xml_info);
char* VHI_DEVTYPE( _sSCA_XML_REQUEST *req_xml_info);
char* VHI_END_TRANS_DATE( _sSCA_XML_REQUEST *req_xml_info);
char* VHI_FORMAT( _sSCA_XML_REQUEST *req_xml_info);
char* VHI_FUNCTION_TYPE( _sSCA_XML_REQUEST *req_xml_info);
char* VHI_INTRN_SEQ_NUM(_sVTV_FIELDS *svbninfo);
char* VHI_INVOICE( _sSCA_XML_REQUEST *req_xml_info);
char* VHI_LOCAL_TRANS_DATE(_sVTV_FIELDS *svbninfo, sVHI_RESPONSE_INFO *vhi_res_info);
char* VHI_LOCAL_TRANS_TIME(_sVTV_FIELDS *svbninfo, sVHI_RESPONSE_INFO *vhi_res_info);
char* VHI_MAX_NUM_RECORDS_RETURNED(_sSCA_XML_REQUEST *req_xml_info);
char* VHI_NUM_RECORDS_FOUND();
char* VHI_PAYMENT_MEDIA(_sVTV_FIELDS *svbninfo,  _sSCA_XML_REQUEST *req_xml_info);
char* VHI_PAYMENT_TYPE(_sSCA_XML_REQUEST *req_xml_info);
char* VHI_PROCESSING_CODE(_sVTV_FIELDS *svbninfo);
char* VHI_REFERENCE(_sVTV_FIELDS *svbninfo);
char* VHI_RESPONSE_TEXT(sGLOBALS* GLOBAL, _sVTV_FIELDS *svbninfo, sVHI_RESPONSE_INFO *vhi_res_info, _sSCA_XML_REQUEST *req_xml_info);
char* VHI_RESULT(sGLOBALS* GLOBAL, _sVTV_FIELDS *svbninfo, sR008_data *sR008_info, sVHI_RESPONSE_INFO *vhi_res_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info);
char* VHI_RESULT_CODE(sVHI_RESPONSE_INFO *vhi_res_info);
char* VHI_RETURN_FLD_HDRS( _sSCA_XML_REQUEST *req_xml_info);
char* VHI_SERIAL_NUM( _sSCA_XML_REQUEST *req_xml_info);
char* VHI_START_TRANS_DATE();
char* VHI_TERMINATION_STATUS(sVHI_RESPONSE_INFO *vhi_res_info);
char* VHI_TRANS_AMOUNT( _sSCA_XML_REQUEST *req_xml_info);
char* VHI_TRANS_DATE(sVHI_RESPONSE_INFO *vhi_res_info);
char* VHI_TRANS_SEQ_NUM(_sVTV_FIELDS *svbninfo);
char* VHI_TRANS_TIME(sVHI_RESPONSE_INFO *vhi_res_info, _sVHI_FIELDS_INFO *vhi_fieldinfo);
char* VHI_TROUTD(_sVTV_FIELDS *svbninfo);
char* VHI_TRACE_CODE(_sVTV_FIELDS *svbninfo);
char* VHI_TRANSACTION_CODE();
char* VHI_CARD_TOKEN(sR017_data *sR017_info);
char* VHI_HOST_RESP_CODE(sVHI_RESPONSE_INFO *vhi_res_info);
char* VHI_VSP_CODE(sVHI_RESPONSE_INFO *vhi_res_info);
char* VHI_VSP_RESULT_DESC(sVHI_RESPONSE_INFO *vhi_res_info);
char* VHI_EMBOSSED_ACCT_NUM(_sSCA_XML_REQUEST *req_xml_info, char *pszEmbossedCardNum);
char* VHI_CVV2_CODE(_sVTV_FIELDS *svbninfo, char *pszResultCode);
char* VHI_AVS_CODE(_sVTV_FIELDS *svbninfo, char *pszResultCode);

void parseR007(sVANTIV_RESPONSE * vrinfo, sVHI_RESPONSE_INFO *vhi_res_info);
void parseR008(sVANTIV_RESPONSE * vrinfo, sR008_data *sR008_info);
void parseR011(sVANTIV_RESPONSE * vrinfo, sR011_data *sR011_info);
void parseR017(sVANTIV_RESPONSE * vrinfo, sR017_data *sR017_info, sCONFIG * GCONFIG);
void parseR023(sVANTIV_RESPONSE * vrinfo, sR023_data *sR023_info);
void parseR029(sVANTIV_RESPONSE * vrinfo, sR029_data *sR029_info);

void composeR029Response(xmlNodePtr root_node, sR029_data *sR029_info);

#endif /* VHI_RESPONSE_H_ */
