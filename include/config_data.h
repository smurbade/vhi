/*
 * config_data.h
 *
 *  Created on: Oct 25, 2013
 *      Author: Dexter M. Alberto
 */

#ifndef CONFIG_DATA_H_
#define CONFIG_DATA_H_


#define CONFIG_MAX_SECTION 31
#define CONFIG_MAX_LABEL 32
#define CONFIG_MAX_VALUE 512

char cENV_VAL[512];

int setConfigData();
int addEnvSection(char * section);
int setEnv(char* section, char* label, char* value);
char* getEnv(char* section, char* label);

#endif /* CONFIG_DATA_H_ */
