/*
 * Logger.h
 *
 *  Created on: Sep 25, 2013
 *      Author: Dexter M. Alberto
 */


#ifndef LOGGER_H_
#define LOGGER_H_

#include <stdarg.h>
#include "define_data.h"
/*
 * **********************************************
 * LOGS
 * **********************************************
 */
#ifdef LOGGING_ENABLED

#define debug_sprintf(...) sprintf(__VA_ARGS__)
extern void APP_TRACE(char *pszMessage);
int chDebug;

#define VHI_DEBUG "VHI_DEBUG"
#define DEVDEBUGMSG	"DEVDEBUG"
void setupDebug();

void broadcastLog(const char * buffer);
void logger(int entryType, const char* file, int line, const char* func, const char* format, ...);
#define LOG_MSG(_format, ...) logger(0, __FILE__, __LINE__, __PRETTY_FUNCTION__, _format, ## __VA_ARGS__)

bool isDebugMode();

#else
#define LOG_MSG(_message) ;
#define debug_sprintf(...)
#define APP_TRACE(a)
#endif

#endif /* LOGGER_H_ */
