/*
 * threadPool.h
 *
 *  Created on: 10-Jul-2014
 *      Author: Praveen_P1
 */

#ifndef THREADPOOL_H_
#define THREADPOOL_H_

/* Individual job */
typedef struct __thpooljob
{
	void*  (*function)(int arg);              	 /* function pointer         */
	void*               arg;                     /* function's argument     */
	struct __thpooljob *next;                    /* pointer to next job     */
	struct __thpooljob *prev;                    /* pointer to previous job  */
}
THPOOLJOB_STYPE, *THPOOLJOB_PTYPE;

typedef struct
{
	THPOOLJOB_PTYPE head;                        /* function pointer         */
	THPOOLJOB_PTYPE tail;                        /* function pointer         */
	int				iJobsCount;                  /* function pointer         */
	sem_t        	semaphore;                   /* semaphore                */
}
THPOOLJOBQUE_STYPE, *THPOOLJOBQUE_PTYPE;


/* The threadpool */
typedef struct
{
	pthread_t*       	ptThreads;              /* pointer to threads' ID   */
	int              	iThreadCount;           /* Number of threads        */
	THPOOLJOBQUE_PTYPE	pstThPooljobqueue;      /* Thread Pool Job Queue    */
}
THPOOL_STYPE, * THPOOL_PTYPE;

#endif /* THREADPOOL_H_ */
