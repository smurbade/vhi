/*
 * tcpip_server.h
 *
 *  Created on: Nov 20, 2013
 *      Author: Dexter M. Alberto
 */


#ifndef TCPIP_SERVER_H_
#define TCPIP_SERVER_H_

/*
 * **********************************************
 * MAIN TCPIP SERVER HANDLER
 * **********************************************
 */
#define LOCAL_HOST	"127.0.0.1"
#define SERVER_PORT	(8888)
pthread_mutex_t lock;

int mainServerThreaded();

#endif /* TCPIP_SERVER_H_ */
