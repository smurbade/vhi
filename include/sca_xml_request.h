/*
 * sca_xml_request.h
 *
 *  Created on: Nov 5, 2013
 *      Author: Dexter M. Alberto
 */


#ifndef SCA_XML_REQUEST_H_
#define SCA_XML_REQUEST_H_

#include <libxml/parser.h>
#include <libxml/xpath.h>
#include "define_data.h"
#include "sca_xml_request.h"

/*FUNCTION_TYPE struct*/
typedef struct sFUNCTION_TYPE {
	char PAYMENT[7+1];
	char BATCH[5+1];
	char REPORT[6+1];
}sFUNCTION_TYPE;

extern const sFUNCTION_TYPE FUNCTION_TYPE;
/*end*/

/*COMMAND struct*/
typedef struct sCOMMAND {
	char PRE_AUTH[8+1];
	char SALE[4+1];
	char CREDIT[6+1];
	char VOID[4+1];
	char POST_AUTH[9+1];
	char ADD_TIP[7+1];
	char COMPLETION[10+1];
	char ADD_VALUE[9+1];
	char REMOVE_VALUE[12+1];
	char ACTIVATE[8+1];
	char DEACTIVATE[10+1];
	char BALANCE[7+1];
	char SIGNATURE[9+1];
	char TOKEN_QUERY[11+1];
	char SETTLE[6+1];
	char DAYSUMMARY[10+1];
	char VERSION[7+1];
	char DEVADMIN[8+1];
	char TOR_REQ[7+1];
	char LAST_TRAN[9+1];
    char EMV_ADVICE[10+1];
}sCOMMAND;

extern const sCOMMAND COMMAND;
/*end*/

/*
 * **********************************************
 * MAIN BUFFER FOR XML DATA REQUEST
 * **********************************************
 */
char* sca_buffer;

/*
 * **********************************************
 * INDEX FOR XML REQUEST ELEMENT DATA
 * index proportion in "sca_xml_request.inc"
 * **********************************************
 */
enum _eSCA_XML_REQUEST
{
	eCLIENT_ID = 0,
	eCOMMAND,
	eCTROUTD,
	eDEVICEKEY,
	eDEVTYPE,
	eFUNCTION_TYPE,
	eLANE,
	ePAYMENT_TYPE,
	eSAF,
	eSERIAL_NUM,

	eACCT_NUM,
	eAUTH_CODE,
	eBATCH_TRACE_ID,
	eCARD_ID_CODE,
	eCARDHOLDER,
	eCASHBACK_AMNT,
	eCASHIER_NUM,
	eCDD_DATA,
	eCELLULARSKUAMOUNT,
	eCELLULARSKUDATA,
	eCELLULARSKULD,
	eCOL_X,
	eCURRENCY_CODE,
	eCURRENCY_EXP,
	eCUSTOMER_CODE,
	eCUSTOMER_STREET,
	eCUSTOMER_ZIP,
	eCV_RSP_TYPE,
	eCVV2,
	eDEBIT_TYPE,
	eEMP_NUM,
	eEXP_MONTH,
	eEXP_YEAR,
	eFORCE_FLAG,
	eGIFT_SEQ_NUM,
	eGIFT_UNITS,
	eINVOICE,
	eKEY_POINTER,
	eKEY_SERIAL_NUMBER,
	eLOYALTY_FLAG,
	eMAC_BLOCK,
	eMIMETYPE,
	eORIG_SEQ_NUM,
	ePARTIAL_AUTH,
	ePARTIAL_REDEMPTION_FLAG,
	ePIN_BLOCK,
	ePRESENT_FLAG,
	ePROMO_CODE,
	ePURCHASE_ID,
	ePY_TRANS_SEQ_NUM,
	eREFERENCE,
	eREF_TROUTD,
	eSERVER_ID,
	eSHIFT_ID,
	eSIGNATUREDATA,
	eSOURCE_ACCT_NUM,
	eSTATE,
	eSURCHARGE_AMOUNT,
	eTAX_AMOUNT,
	eTAX_IND,
	eTIP_AMOUNT,
	eTOT_NUM_CARDS,
	eTPP_ID,
	eTRACK_DATA,
	eTRAN_SEQ_FLAG,
	eTRANS_AMOUNT,
	eTRANSACTION_CODE,
	eTRANSACTION_ID,
	eTROUTD,
	eVISA_AUAR,
	eVISA_BID,

	eMAX_NUM_RECORDS_RETURNED,
	eFORMAT,
	eDELIMITER,
	eRETURN_FLD_HDRS,
	eRESPONSEFIELDS,
	eSEARCHFIELDS,
	eBATCH_SEQ_NUM,
	eREQUEST_COMMAND,
	eSTART_TRANS_AMOUNT,
	eSTART_TRANS_DATE,
	eSTART_TRANS_TIME,
	eSTART_TROUTD,
	eEND_TRANS_TIME,
	eEND_TRANS_DATE,
	eEND_TRANS_AMOUNT,
	eEND_TROUTD,

	eENCRYPTION_PAYLOAD,
	eORIG_TRANS_DATE,
	eORIG_TRANS_TIME,
	eCARD_TOKEN,

	eSERIALNUM,
	eTERMINAL,
	eADMIN,
	eSAF_TRAN,
	eVSP_REG,
	eTRACK_IND,
    ePIN_CODE,
    
    // FSA amounts
    eAMOUNT_HEALTHCARE,
    eAMOUNT_PRESCRIPTION,
    eAMOUNT_VISION,
    eAMOUNT_CLINIC,
    eAMOUNT_DENTAL,
    
    // EBT FOOD_STAMP or CASH_BENEFITS
    eEBT_TYPE,

    // All EMV tags are sent as TLVs encoded as a string of hex digits
    eEMV_TAGS,
    eEMV_TAGS_RESPONSE,
    eEMV_REVERSAL_TYPE,
    
    eBILLPAY,

    ePAYMENT_MEDIA,

    ePROMO_NEEDED,

    eUSER_DEFINED1,
    eUSER_DEFINED2,
    eUSER_DEFINED3,

    eCARD_PRESENT,

    eLOCATOR_ID,

    eSAF_APPROVALCODE,

    eMERCHANT_CAT_CODE,

    eLEVEL3_ITEMS,

	eREQ_ELEMENT_MAX
};

#ifdef OLD_EMV_TAG_METHOD
#define FIRST_EMV_REQUEST_TAG   eAPPINTERCHANGEPROFILE
#define LAST_EMV_REQUEST_TAG    eUNPREDICTABLENUMBER
#endif

/*
 * **********************************************
 * STRUCT DATA FOR ELEMENT VALUE
 * **********************************************
 */
typedef struct _sSCA_XML_REQUEST
{
	char*element;
}_sSCA_XML_REQUEST;


/*
 * **********************************************
 * GLOBAL STRUCT ARRAY FOR THE ELEMENT CONTAINER
 * **********************************************
 */
//_sSCA_XML_REQUEST req_xml_info[eREQ_ELEMENT_MAX]; //Praveen_P1: Made this local in the service request thread


/*
 * **********************************************
 * FUNCTIONS IN PARSING THE XML REQUESTS
 * **********************************************
 */

/*
	Initialize SCA Request XML data
*/
void initializeXmlData(_sSCA_XML_REQUEST *req_xml_info);

/*
	Free the SCA Request XML data memory
*/
void freeXmlData();


/*
	Copy the SCA Request XML data memory
*/
int copyXml(char* buffer);


/*
 * Get element value
 */
char* getXmlTagValue(enum _eSCA_XML_REQUEST, _sSCA_XML_REQUEST *req_xml_info);


/*
 * used in SCA_Parse_XML_Request()
 */
xmlXPathObjectPtr getNodeTest(xmlDocPtr doc, xmlChar *xpath);


/*
 * parse the SCA XML request
 */
int parseXmlRequest(char* buffer, sGLOBALS* GLOBAL, _sSCA_XML_REQUEST *req_xml_info);


#endif /* SCA_XML_REQUEST_H_ */
