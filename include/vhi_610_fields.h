/*
 * vhi_610_fields.h
 *
 *  Created on: Nov 5, 2013
 *      Author: Dexter M. Alberto
 */


#ifndef VHI_610_FIELDS_H_
#define VHI_610_FIELDS_H_

#include "sca_xml_request.h"

/*
 * **********************************************
 * FIELD DESCRIPTION INFO
 * **********************************************
 */
typedef struct _sVHI_FIELDS_INFO
{
	char bnpr[3+1];			//ans Processor Routing
	char bnnr[6+1];			//an Network Routing
	char bn00[4+1];			//n Message Type Identifier
	char bnbmt[2+1];		//n Bit Map Type
	char bn02[19+1];		//an Primary Account Number
	char bn03[6+1];			//n Processing Code
	char bn04[9+1];			//n Amount, Transaction
	char bn07[10+1];		//n Transmission Date/Time
	char bn11[6+1];			//n System Trace Audit Number (STAN)
	char bn12[6+1];			//n Local Transaction Date
	char bn13[6+1];			//n Local Transaction Time
	char bn22[3+1];			//n Point-of-Service Entry Mode
	char bn25[10+1];		//n Point-of-Service Condition Code
	char bn32[4+1];			//n Acquiring Institution Identification Code (Bank ID)
	char bn41[3+1];			//n Card Acceptor Terminal Identification (Term ID)
	char bn42[12+1];		//n Card Acceptor Identification Code (Merchant ID)
	char bn43[3+1];			//n Lane Number
	char bn45[76+1];		//ans Track Data
	char bn48[8+1];			//n Additional Data (Last Retrieval Reference Number)
	char bn52[16+1];		//an Personal Identification Number (PIN_BLOCK in SSI)
	char bn55[8+1];			//n Clerk Number
	char bn60[9+1];			//n Cash Back Amount
	char bn65[6+1];			//an Authorization Identification Response
	char bn67[2+1];			//n Extended Payment Code (JCB Installments)
	char bn70[3+1];			//n Network Management Information Code
	char bn74[6+1];			//n Returns, Count
	char bn76[6+1];			//n Sales, Count
	char bn86[12+1];		//n Returns, Amount
	char bn88[12+1];		//n Sales, Amount
	char bn90[8+1];			//n Original Data Elements (Retrieval Ref. Number)
	char bn105_1[2+1];		//an Additional Response Data/AVS Result Code
	char bn105_2[1+1];		//an Payment Service Indicator
	char bn105_3[15+1];		//an Transaction Identifier
	char bn105_4[4+1];		//an VISA Validation Code
	char bn106[29+1];		//an Cardholder Identification Data (AVS)
	char bn107[2+1];		//an Point-of-Service Device Capability Code
	char bn109[20+1];		//ans P.O. Number/Customer Code
	char bn110[9+1];		//n Tax Amount
    char bn112[3+1];        //n card sequence number
	char bn115[16+1];		//ans Trace Data 1 (Echo Data)
	char bn117[20+1];		//an DUKPT Serial Number
	char bn129[4+1];		//n Auth Timer
	char bn136[12+1];		//an POSA SAF Reference Number
	char bn139[8+1];		//n Token Original Transaction Date
	char bn140[6+1];		//n Token Original Transaction Time

}_sVHI_FIELDS_INFO;

//_sVHI_FIELDS_INFO vhi_fieldinfo; //Praveen_P1: Made this local in the Service Request thread

/*
 * **********************************************
 * FIELD DESCRIPTION FUNCTIONS
 * **********************************************
 */

/*
 * 	-- Processor Routing
 */
char* getFieldBNPR(_sVHI_FIELDS_INFO *vhi_fieldinfo);

/* Network Routing */
char* getFieldBNNR(_sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * 	00 Message Type Identifier
 */
enum e00 {
	eAUTHORIZATION_00, //01XX
	eFINANCIAL_TRANS_00, //02XX
	eFINANCIAL_TRANS_0220, //0220
	eFILE_UPDATE_00, //03XX
	eREVERSAL_00, //04XX
	eRECON_CONTROL_00, //05XX
	eNETWORK_MANAGEMENT, //08XX
};
char* getFieldBN00(enum e00, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * 	02 Primary Account Number
 */
char* getFieldBN02(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * 	03 Processing Code
 */
enum _ePROC_CODE
{
	eSALE_PROCCODE = 0,
	eREFUND_PROCCODE,
	eVOID_PROCCODE,
	eGIFT_ACTIVATION,
	eGIFT_RELOAD,
	eGIFT_UNLOAD,
	eGIFT_CLOSE,
	eGIFT_BALINQ,
	eGIFT_PREAUTH,
	eCDT_PRIORAUTH,
	eCDT_PRIORADJUSTMENT,
	eGIFT_COMPLETION,
	eGIFT_BATCH_TOTAL,
	eBATCH_INQUIRY,
	eBATCH_RELEASE,
	eTOKENIZATION,
	eCRED_DEBT_BAL_INQ,
    eEBT_VOID,
    eEBT_BALANCE,
    eCREDIT_PREAUTH,
    eCREDIT_PREAUTHAVS,
};

char* getFieldBN03(enum _ePROC_CODE, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * 	04 Amount, Transaction
 */
char* getFieldBN04(sGLOBALS* GLOBAL, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * System Trace Audit Number (STAN)
 */
char* getFieldBN11(_sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * Local Transaction Date
 */
char* getFieldBN12(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * Local Transaction Time
 */
char* getFieldBN13(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * 	22 Point-of-Service Entry Mode
 */
char* getFieldBN22(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * 	25 Point-of-Service Condition Code
 */
char* getFieldBN25(sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * 32 Acquiring Institution Identification Code (Bank ID)
 */
char* getFieldBN32(sCONFIG * GCONFIG, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * 41 Card Acceptor Terminal Identification (Term ID)
 */
char* getFieldBN41(sCONFIG * GCONFIG, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * 42 Card Acceptor Identification Code (Merchant ID)
 */
char* getFieldBN42(sCONFIG * GCONFIG, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * 	43 Lane Number
 */
char* getFieldBN43(sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * 	45 Track Data
 */
char* getFieldBN45(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * 	48 Additional Data
 */
char* getFieldBN48(_sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * 	52 Personal Identification Number
 */
char* getFieldBN52(_sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * 	55 Clerk Number
 */
char* getFieldBN55(_sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * 	60 Cash Back Amount
 */
char* getFieldBN60(_sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * 	65 Authorization Identification Response
 */
char* getFieldBN65(sGLOBALS* GLOBAL, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * 	70 Ntwork management information code
 */
char* getFieldBN70(_sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * 	90 Original Data Elements
 */
char* getFieldBN90(_sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * 106 Cardholder Identification (AVS)
 */
char* getFieldBN106(_sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * 107 Point-of-Service Device Capability Code
 */
char* getFieldBN107(sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo, sGLOBALS* GLOBAL);

/*
 * 	109 PO or Customer Code
 */
char* getFieldBN109(_sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * 	110 Tax Amount
 */
char* getFieldBN110(_sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * 	112 Card Sequence Number (EBT)
 */
char* getFieldBN112(_sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * 	115 Trace Data 1
 */
char* getFieldBN115(_sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * 	117 DUKPT Serial Number
 */
char* getFieldBN117(_sSCA_XML_REQUEST *req_xml_info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

#endif /* VHI_610_FIELDS_H_ */
