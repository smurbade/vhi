/*
 * emv.h
 *
 *  Created on: Feb 16 2015
 *      Author: B2
 */


#ifndef EMV_H_
#define EMV_H_

#include "define_data.h"
#include "sca_xml_request.h"
#include "vantiv_response.h"
#include "vhi_response.h"


/*
 * G035 - EMV Tags
 * 1 to 999 bytes
 */
void composeG034(char** ptr, _sSCA_XML_REQUEST *req_xml_info, sCONFIG * GCONFIG);
void composeG035(char** ptr, _sSCA_XML_REQUEST *req_xml_info, sGLOBALS* GLOBAL);
void composeG036(char** ptr, _sSCA_XML_REQUEST *req_xml_info, sGLOBALS* GLOBAL);
void parseR023(sVANTIV_RESPONSE * vrinfo, sR023_data *sR023_info);

void composeR023Response(xmlNodePtr root_node, sR023_data *sR023_info);


void char2hex(UINT8* destination, UINT8* source, int sourceSize);
void hex2char(UINT8* destination, UINT8* source,  int sourceSize);

#endif
