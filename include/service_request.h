/*
 * service_request.h
 *
 *  Created on: Nov 21, 2013
 *      Author: Dexter M. Alberto
 */


#ifndef SERVICE_REQUEST_H_
#define SERVICE_REQUEST_H_
#include "define_data.h"

/*
 * **********************************************
 * MAIN SERVICE OF REQUEST
 * **********************************************
 */

int initializeServiceData(sGLOBALS* GLOBAL);
void freeServiceData(sGLOBALS* GLOBAL);
int responseHttpError(int conn, sGLOBALS* GLOBAL);
int responseHttpSuccess(int conn, sGLOBALS* GLOBAL);

void *onServiceRequestThread(int conn_socket);

#endif /* SERVICE_REQUEST_H_ */
