/*
 * vhi_610_group.h
 *
 *  Created on: Nov 12, 2013
 *      Author: Dexter M. Alberto
 */


#ifndef VHI_610_GROUP_H_
#define VHI_610_GROUP_H_

#include "define_data.h"
#include "sca_xml_request.h"

void free610GroupData();

/*
 * **********************************************
 * GROUP DATA INFO
 * **********************************************
 */
typedef struct _sG001_
{
	char group[4+1]; //GXXX
	char field_01[11+1]; //ans Draft Locator Id
	char field_02[17+1]; //ands Merchant Reference Number
}_sG001_;

typedef struct _sG004_
{
	char group[4+1]; //GXXX
	char field_01[3+1];  //n Lane Number
	char field_02[8+1];  //n Cashier Number
	char field_03[4+1];  //n Merchant Category Code
}_sG004_;

typedef struct _sG009_
{
	char group[4+1]; //GXXX
	char field_01[1+1]; //an 1 Visa/MasterCard Partial Approval Indicator
	char field_02[1+1]; //an 1 Discover Partial Approval Indicator
	char field_03[1+1]; //an 1 AMEX Partial Approval Indicator
	char field_04[1+1]; //an
	char field_05[1+1]; //an Y Balance Inquiry accepting capable
	char field_06[1+1]; //an Y Return Authorization Retrieval Reference Number
	char field_07[1+1]; //an
	char field_08[1+1]; //an
	char field_09[1+1]; //an
	char field_10[1+1]; //an
	char field_11[1+1]; //an Gift Card PIDN (Premier Issue Dual Number) Capability flag
	char field_12[1+1]; //an Y Host Capture Adjustment Capability Flag
	char field_13[1+1]; //an 1 Debit Partial Approval Indicator
	char field_14[1+1]; //an
	char field_15[1+1]; //an Y Capable of receiving extended host error response details in response details in response group R998
	char field_16[1+1]; //an
}_sG009_;

typedef struct _sG015_
{
    char group[4+1]; //GXXX
    char field_01[6][2+1];
    char field_02[6][2+1];
    char field_03[6][3+1];
    char field_04[6][13+1];
}_sG015_;

typedef struct _sG014_
{
	char group[4+1]; //GXXX
	char field_01[9+1]; //n Original Retrieval Reference Number
}_sG014_;

typedef struct _sG018_
{
    char group[4+1];     //GXXX
    char field_01[1+1];  //n CVV2 indicator
    char field_02[4+1];  //CVV2 data
    char field_03[12+1]; //Security Code
}_sG018_;

typedef struct _sG023_
{
	char group[4+1]; //GXXX
	char field_01[9+1]; //n Tip Amount
}_sG023_;

typedef struct _sG026_
{
	char group[4+1]; //GXXX
	char field_01[1+1]; //an "L" POS Encryption Format
	char field_02[1+1]; //an "M" Requested Response
	char field_03[1+1]; //an Encrypted Data Format
	char field_04[1+1]; //an
	char field_05[1+1]; //an
	char field_06[1+1]; //an
	char field_07[1+1]; //an
	char field_08[1+1]; //an
	char field_09[1+1]; //an
	char field_10[999+1]; //an "Encryption Format L" Key Data
}_sG026_;

typedef struct _sG027_
{
	char group[4+1]; //GXXX
	char field_01[1+1]; //an Encrypted Track Indicator
	char field_02[76+1]; //an Encrypted Track Data
}_sG027_;

typedef struct _sG028_
{
	char group[4+1]; //GXXX
	char field_01[19+1]; //an Token
	char field_02[6+1]; //an Token ID
	char field_03[4+1]; //an Card Expiration Date MMYY
}_sG028_;

typedef struct _sG029_
{
    char group[4+1]; //GXXX
    char field_01[15+1];
}_sG029_;

typedef struct _sG030_
{
    char group[4+1]; //GXXX
    char field_01[999+1];
}_sG030_;

typedef struct _sG031_
{
    char group[4+1]; //GXXX
    char field_01[999+1];
}_sG031_;

typedef struct _sG034_
{
    char group[4+ 1];       // GXXX
    char field_01[6+1];  //ans VAR name
    char field_02[6+1];  //ans VAR version
    char field_03[6+1];  //ans Gateway name
    char field_04[6+1];  //ans Gateway version
    char field_05[10+1]; //ans POS app name
    char field_06[6+1];  //ans POS app version
    char field_07[10+1]; //ans Device make/model
    char field_08[10+1]; //ans Terminal app name
    char field_09[6+1];  //ans Terminal app version
    char field_10[16+1]; //ans Serial number
}_sG034_;

typedef struct _sG035_
{
    char group[4+ 1];       // GXXX
    char field_01[999+ 1];  // ans base64 encoded BER_TLV EMV tags
}_sG035_;

typedef struct _sG036_
{
    char group[4+ 1];       // GXXX
    char field_01[20+ 1];   // an DUKPT serial number
    char field_02[16+ 1];   // an DUKPT PIN block
    char field_03[2+ 1];    // n PIN pad digit capability
}_sG036_;

typedef struct _sG038_
{
    char group[4+ 1];       // GXXX
    char field_01[35+ 1];   // an User Defined Field 01
    char field_02[20+ 1];   // an User Defined Field 02
    char field_03[20+ 1];    //an User Defined Field 03
}_sG038_;

typedef struct _level3Items_
{
	char field_01[35+1];  //ans Item Description
	char field_02[12+1];  //ans Unit of Measure
	char field_03[12+1];  //an Unit Price
	char field_04[1+1];   //an Unit Price Decimal
	char field_05[12+1];  //an Item Quantity
	char field_06[1+1];   //an Item Quantity Decimal
	char field_07[15+1];  //ans Product Code
	char field_08[12+1];  //an Item Discount Amount
	char field_09[5+1];   //an Item Discount Rate
	char field_10[1+1];   //an Item Discount Rate Decimal
}_level3Items_;

typedef struct _sG043_
{
    char group[4+1]; //GXXX
}_sG043_;

typedef struct _sG045_
{
    char group[4+1]; //GXXX
    char field_01[12+1];
}_sG045_;

typedef struct _Level3Details_
{
    int	iNumberofLineItems;
    _level3Items_ level3Dtls[25]; // level3 item details upto 25 items
}_Level3Details_;

/*
 * **********************************************
 * 610 OPTIONAL GROUP MESSAGES
 * **********************************************
 */

/*
 * G001 Merchant Reference Data
 * 11+17 bytes
 */
void composeG001(char** ptr, _sSCA_XML_REQUEST *req_xml_info);

/*
 * G001 Discretionary Data
 * 3+8+4 bytes
 */
void composeG004(char** ptr, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info);

/*
 * G009 Optional Processing Indicators
 * 16 bytes
 */
void composeG009( char** ptr, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info );

/*
 * G014 � Original Authorization Retrieval Reference Number
 * 9 bytes
 */
void composeG014( char** ptr, sGLOBALS* GLOBAL, _sSCA_XML_REQUEST *req_xml_info );

/*
 * G015 Additional Amounts
 * 20-120 bytes
 */
void composeG015( char** ptr, _sSCA_XML_REQUEST *req_xml_info );

/*
 * G018 Gift Card CVV2 & Security Code
 * 9 - 17 bytes
 */
void composeG018(char** ptr, sGLOBALS* GLOBAL, _sSCA_XML_REQUEST *req_xml_info);

/*
 * G023 Restaurant Tip Amount
 * 9 bytes
 */
void composeG023( char** ptr, _sSCA_XML_REQUEST *req_xml_info );

/*
 * G026 - POS Encrypted Data
 * 10-1008 bytes
 */
void composeG026( char** ptr, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info );

/*
 * G027 - Encrypted Track
 * 77 bytes
 */
void composeG027( char** ptr, _sSCA_XML_REQUEST *req_xml_info );

/*
 * G028 - Token Utilization
 * 29 bytes
 */
void composeG028( char** ptr,sGLOBALS* GLOBAL,sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info );

//  EBT WIC Merchant ID
void composeG029(char** ptr, _sSCA_XML_REQUEST *req_xml_info);

//  EBT WIC Pass-thru Data Field #1
void composeG030(char** ptr, _sSCA_XML_REQUEST *req_xml_info);

//  EBT WIC Pass-thru Data Field #2
void composeG031(char** ptr, _sSCA_XML_REQUEST *req_xml_info);

/*
 * G034 - POS identification data
 * 82 bytes
 */
void composeG034( char** ptr, _sSCA_XML_REQUEST *req_xml_info, sCONFIG * GCONFIG );

/*
 * G035- EMV tag data
 * up to 999 bytes
 */
void composeG035( char** ptr, _sSCA_XML_REQUEST *req_xml_info, sGLOBALS* GLOBAL );

/*
 * G036- EMV PIN block
 * 38 bytes
 */
void composeG036( char** ptr, _sSCA_XML_REQUEST *req_xml_info, sGLOBALS* GLOBAL );

/*
 * **********************************************
 * 610 OPTIONAL GROUP LENGTH
 * **********************************************
 */
enum eGROUP_IDX
{
    eGROUP001,
    eGROUP004,
    eGROUP009,
    eGROUP014,
    eGROUP015,
    eGROUP018,
	eGROUP023,
	eGROUP026,
	eGROUP027,
    eGROUP028,
    eGROUP029,
    eGROUP030,
    eGROUP031,
    eGROUP034,
    eGROUP035,
    eGROUP036,
    eGROUP038,
    eGROUP043,
    eGROUP045,
};


void append610Group(enum eGROUP_IDX, char** buffer, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info);

#endif /* VHI_610_GROUP_H_ */
