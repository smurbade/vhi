/*
 * http_handler.h
 *
 *  Created on: Nov 21, 2013
 *      Author: Dexter M. Alberto
 */


#ifndef HTTP_HANDLER_H_
#define HTTP_HANDLER_H_
#include "define_data.h"

int parseHttpRequest(int conn, sGLOBALS* GLOBAL);

int parseHttpHeader(char* ptr, sGLOBALS* GLOBAL);
void readHeaderLine(int conn, void* ptr);
void continueHttpResponse(int conn);

#endif /* HTTP_HANDLER_H_ */
