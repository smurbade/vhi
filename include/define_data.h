/*
 * define_data.h
 *
 *  Created on: Nov 4, 2013
 *      Author: Dexter M. Alberto
 */


#ifndef DEFINE_DATA_H_
#define DEFINE_DATA_H_

typedef int bool;
typedef unsigned int  		uint;

#define VHI_APP_VERSION	 "V_1.00.09"
#define VHI_BUILD_NUMBER "1"
#define G34_APP_VERSION  "010009"       // for G034

#define	APP_NAME "VHI"

// Entry Tyrp for Transactional App Logging
#define ENTRYTYPE_INFO 		"INFO"
#define ENTRYTYPE_ERROR 	"ERROR"
#define ENTRYTYPE_FAILURE 	"FAILURE"
#define ENTRYTYPE_WARNING 	"WARNING"
#define ENTRYTYPE_SUCCESS 	"SUCCESS"

#define STARTUP_INDICATOR			"######################### STARTUP VHI ###################"
#define START_INDICATOR 			"######################### START #########################"
#define END_INDICATOR 			    "#########################  END  #########################"

// App Entry Id for Transactional App Logging
#define ENTRYID_START_UP			"START_UP"
#define	ENTRYID_START				"START"
#define	ENTRYID_COMMAND_END			"END"
#define ENTRYID_RECEIVE				"RECEIVE"
#define ENTRYID_PROCESSING			"PROCESSING"
#define ENTRYID_PROCESSED			"PROCESSED"
#define ENTRYID_SENT				"SENT"
#define ENTRYID_PROCESSING			"PROCESSING"
#define ENTRYID_DHI_REQUEST			"VHI_REQUEST"
#define ENTRYID_DHI_RESPONSE		"VHI_RESPONSE"
#define ENTRYID_RESPONDED			"RESPONDED"
#define ENTRYID_INFOMESSAGE			"INFO_MESSAGE"
#define ENTRYID_VANTIV_REQUEST		"VANTIV_REQUEST"
#define ENTRYID_VANTIV_RESPONSE		"VANTIV_RESPONSE"

#define true 1
#define false 0

typedef unsigned char UINT8;

// NOTE: OLD_EMV_TAG_METHOD is being used to hide old XML EMV tag transport protocol

/*main HTTP data*/
#define MAX_LINE_TO_RECEIVE 1024
#define MAX_DATA_TO_RECEIVE 1024

//#define VANTIV_TEST_URL	"https://cert.ssl53.com/AUTH"
//#define VANTIV_USERPWD	"Gne7d43z:59fgRT2k"
#define CA_CERT_FILE	"ca-bundle.crt"

#define TOR_RECORD_FILE_NAME        		"./flash/TORRecords.db"
#define TOR_RECORD_FILE_NAME_TEMP			"/var/tmp/TORRecords.db"

#define LAST_TRAN_FILE_NAME         		"./flash/lasttran.txt"

typedef enum
{
	PRIMARY_URL = 0,
	SECONDARY_URL
}
HOST_URL_ENUM;

enum eMETHOD {
	ePOST,
	eHEAD,
	eUNSUPPORTED,
};

enum eHTTP_STATUS_CODE {
	eHTTP_PROCESSING					= 102,
	eHTTP_OK							= 200,
	eHTTP_BAD_REQUEST					= 400,
	eHTTP_NOT_FOUND						= 404,
	eHTTP_METHOD_NOT_ALLOWED			= 405,
	eHTTP_NOT_ACCEPTABLE				= 406,
	eHTTP_REQUEST_TIMEOUT				= 408,
	eHTTP_LENGTH_REQUIRED				= 411,
	eHTTP_REQUEST_ENTITY_TOO_LARGE		= 413,
	eHTTP_UNSUPPORTED_MEDIA_TYPE		= 415,
	eHTTP_INTERNAL_SERVER_ERROR			= 500,
	eHTTP_SERVER_UNAVAILABLE			= 503,
	eHTTP_GATEWAY_TIMEOUT				= 504,
	eHTTP_HTTP_VERSION_NOT_SUPPORTED	= 505,
};

enum eAPP_ERR_CODE {
	eERR_NOERROR = 0,
	eERR_INVALID_COMMAND,
	eERR_INVALID_FUNCTIONTYPE,
	eERR_INVALID_PAYMENTTYPE,
	eERR_INVALID_CTROUTD,
	eERR_INVALID_AUTH_CODE,
	eERR_REQUEST_ERROR,
	eERR_INTERNAL_ERROR,
	eERR_UNSUPPORT_PROTOCOL,
	eERR_HOST_URL_MALFORMAT,
	eERR_HOST_HTTP_ERROR,
	eERR_HOST_TIMED_OUT,
	eERR_LAST_TRAN_CMD,
	eERR_RESP_ERR_100,
	eERR_RESP_ERR_101,
	eERR_RESP_ERR_121,
};

enum eATYPE1 {
	eRETAIL_TYPE = 0,
	eRESTAURANT_TYPE,
	eCAT_TYPE,
};

enum eTOKEN_MODE {
	eTOKEN_OFF = 0,
	eTOKEN_TRT,
	eTOKEN_TUT,
};

typedef struct sHTTP_INFO {
	enum eMETHOD method;
	enum eHTTP_STATUS_CODE status_code;
	int firstHeader;
	int requestLength;
	int responseLength;
	char *content_length;
	char *content_type;
	char *expect;
	char *xml_request;
	char *xml_response;
}sHTTP_INFO;

typedef struct sCONFIG {
	int torTimeout;
	int connTimeout;
	int torRetry;
	bool isRegistartMode;
	bool isSAFEnabled;
    bool emvenabled;
    bool verifyHost;
    bool verifyHost2;
    bool sigcapturerefenabled;
    bool useorigtimeforsaf;
    bool sendSAFdeclineaspostauth;
	enum eATYPE1 atype1;
	enum eTOKEN_MODE eTokenMode;
	char adjcapable;
	char *vantivURL;
	char *vantivSecdURL;
	char *hostUsrPwd;
	char  bankID[30];
	char  mid[30];
	char  laneID[10];
	char  safLaneID[10];
	char  tid[30];
	char  safErrCodes[21][5]; //Will store upto 10 TPS error codes to consider them as host not available case
}sCONFIG;

sCONFIG GCONFIG;

typedef struct sGLOBALS {
	sHTTP_INFO http;
	bool isProcessorAvailable;
	enum eAPP_ERR_CODE appErrCode;
}sGLOBALS;

//sGLOBALS* GLOBAL;

/*end*/

#define  RS "\x1E"
#define  GS "\x1D"
#define  FS "\x1C"
#define  ACK "\x06"
#define  NACK "\x15"
#define  CR "\x0D" //carriage return
#define  LF "\x0A" //newline

#define NOTPRESENT_ENTRY	"1"
#define MANUAL_ENTRY		"2"     // M for save void data
#define SWIPED_ENTRY		"3"     // S
#define PROXIMITY_ENTRY		"4"     // R
#define EMV_CHIP_ENTRY      "5"     // E
#define EMV_CTLS_ENTRY      "6"     // C
#define EMV_FALLBACK_ENTRY  "7"     // F
#define iNOTPRESENT_ENTRY	1
#define iMANUAL_ENTRY		2
#define iSWIPED_ENTRY		3
#define iPROXIMITY_ENTRY	4
#define iEMV_CHIP_ENTRY     5
#define iEMV_CTLS_ENTRY     6
#define iEMV_FALLBACK_ENTRY 7

#define NO_CONTENT ""
#define COMPARE_OK 0

#endif /* DEFINE_DATA_H_ */
