/*
 * vhi_610_base.h
 *
 *  Created on: Nov 5, 2013
 *      Author: Dexter M. Alberto
 */

#ifndef VHI_610_BASE_H_
#define VHI_610_BASE_H_

#include "sca_xml_request.h"
#include "vhi_610_fields.h"

/*
 * **********************************************
 * 610 BASE STRUCT DATA
 * **********************************************
 */
typedef struct sVHI_610_BASE
{
	char dbtsale[251+1];
	char dbtrefnd[251+1];
	char dbtvoid[165+1];
	char crdsale[246+1];
	char crdsaleavs[275+1];
	char crdrefnd[246+1];
	char crdvoid[129+1];
	char crdpreauth[265+1];
	char gftsale[203+1]; //203/239
	char gftrefnd[203+1]; //203/239
	char gftvoid[129+1]; //129/145
	char gftactv[203+1]; //203/240
	char gftrload[203+1]; //203/240
	char gftunload[203+1]; //203/239
	char gftclose[203+1]; //203/239
	char gftblnce[203+1]; //203/227
	char gftpreauth[207+1];
	char gftcmplte[203+1];
	char batchinquiry[109+1];
	char batchrelease[109+1];
	char gftbtchtotl[109+1];
	char tokenconv[211+1];
	char debcrebalinq[234+1];
    char emvadvice[265+1];
    char ebtsale[254+1];
    char ebtrefnd[254+1];
    char ebtvoid[227+1];
    char ebtbalance[228+1];
}sVHI_610_BASE;

//sVHI_610_BASE vhi610info; //Praveen_P1: Made this local in the service request

/*
 * **********************************************
 * 610 BASE MESSAGES
 * **********************************************
 */

/*
 * Financial Transaction Request messaged---02XX
 * Debit Card Sale (DUKPT Key) 251 bytes
 */
char* compose610DebitSale(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * Financial Transaction Request messaged---02XX
 * Credit Card Sale Request 246 bytes
 */
char* compose610CreditSale(char **transtype, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);


/*
 * Financial Transaction Request messaged---02XX
 * Gift Card Sale Request 203/239 bytes
 */
char* compose610GiftSale(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);


/*
 * Financial Transaction Request messaged---02XX
 * Credit Card Sale w/ AVS Request 275 bytes
 */
char* compose610CreditSaleAVS(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);


/*
 * Financial Transaction Request messaged---02XX
 * Debit Card Return (DUKPT Key) 251 bytes
 */
char* compose610DebitReturn(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);


/*
 * Financial Transaction Request messaged---02XX
 * Credit Card Return Request 246 bytes
 */
char* compose610CreditReturn(char **transtype, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);


/*
 * Financial Transaction Request messaged---02XX
 * Gift Card Return Request 203/239 bytes
 */
char* compose610GiftReturn(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);


/*
 * Reversal (Void) Request Messages---0400
 * Credit Card Reversal (Void) 129 bytes
 */
char* compose610CreditVoid(char **transtype, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);


/*
 * Reversal (Void) Request Messages---0400
 * Debit Card/EBT Reversal (Void) (DUKPT) 165 bytes
 */
char* compose610DebitVoid(char **transtype, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);


/*
 * Reversal (Void) Request Messages---0400
 * Gift Card Reversal (Void) 129/145 bytes
 */
char* compose610GiftVoid(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);


/*
 * Financial Transaction Request messaged---02XX
 * Gift Card Activation 203/240 bytes
 */
char* compose610GiftActivation(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);


/*
 * Financial Transaction Request messaged---02XX
 * Gift Card Reload 203/240 bytes
 */
char* compose610GiftReload(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);


/*
 * Financial Transaction Request messaged---02XX
 * Gift Card Unload 203/239 bytes
 */
char* compose610GiftUnload(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);


/*
 * Financial Transaction Request messaged---02XX
 * Gift Card Close 203/239 bytes
 */
char* compose610GiftClose(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);


/*
 * Authorization Request messaged---01XX
 * Gift Card Balance Inquiry/Mini-Statement 203/227 bytes
 */
char* compose610GiftBalance(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * Financial Transaction Request messaged---02XX
 * Gift Card Preauthorization 207 bytes
 */
char* compose610GiftPreauthorization(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * Financial Transaction Request messaged---01XX
 * Credit Card Preauthorization 244 bytes
 */
char* compose610CreditPreauthorization(char **transtype,sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * Financial Transaction Request messaged---01XX
 * Credit Card Preauthorization 273 bytes
 */
char* compose610CreditPreauthorizationAVS(char **transtype,sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);
/*
 * Financial Transaction Request messaged---02XX
 * Gift Card Completion 203 bytes
 */
char* compose610GiftCompletion(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);


/*
 * Reconciliation Request messaged---0500
 * Batch Inquiry 109 bytes
 */
char* compose610BatchInquiry(sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * Reconciliation Request messaged---0500
 * Batch Release 109 bytes
 */
char* compose610BatchRelease(sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * Reconciliation Request messaged---0500
 * Gift Card Batch Totals 109 bytes
 */
char* compose610GiftBatchTotals(sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * Financial Transaction Request messaged---02XX
 * Credit Card Prior Authorization/Adjustment 265 bytes
 */
char* compose_610CreditPriorAuthAdj(char **transtype, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * Authorization Request messaged---01XX
 * Debit/Credit Card Balance Inquiry (DUKPT Key) 234 bytes
 */
char* compose610DebitCreditBalance(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * Authorization Request messaged---01XX
 * Tokenization Conversion 211 bytes
 */
char* compose610Tokenization(sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * EMV offline approval advice
 */
char *compose610EMVOfflineAdvice(char **transtype, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * EBT sale
 */
char *compose610EBTSale(char **transtype, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * EBT return
 */
char *compose610EBTReturn(char **transtype, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * EBT void
 */
char *compose610EBTVoid(char **transtype, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * EBT balance
 */
char *compose610EBTBalance(char **transtype, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);


#endif /* VHI_610_BASE_H_ */
