#ifndef _TCPIPDBG_H
#define _TCPIPDBG_H

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

#define NO_DEBUG        0
#define CONSOLE_DEBUG   1
#define ETHERNET_DEBUG  2

#define INVALID_SOCKET      -1
#define MAX_DEBUG_MSG      	2048+1

/* Open debug socket to debug host (SocketListener) */
extern int  iOpenDebugSocket(void);

/* Close debug socket */
extern int  iCloseDebugSocket(void);

/* Send null-terminated debug string to debug host (SocketListener) */
extern void DebugMsg(char *pszMessage);

/* Send debug string of specified length to debug host (SocketListener) */
extern void DebugMsgEx(unsigned char *puchMessage, unsigned short ushMsgLen);

#ifdef DEBUGOPT // 23-Nov-10: added
// Function to save debug logs to flash/USB stick on app closure
extern void SaveDebugLogs(void);
#endif // DEBUGOPT

#endif /* _TCPIPDBG_H */


