/*
 * vhi_request.h
 *
 *  Created on: Nov 5, 2013
 *      Author: Dexter M. Alberto
 */


#ifndef VHI_REQUEST_H_
#define VHI_REQUEST_H_
#include "define_data.h"
#include "sca_xml_request.h"
#include "vhi_610_base.h"
#include "vhi_610_fields.h"

/*
 * **********************************************
 * Struct Data for VHI request
 * **********************************************
 */
typedef struct sVHI_REQ
{
	char* main;
	char* transtype;
	char* basegroup;
	char tps[8+21+1];
}sVHI_REQ;

//sVHI_REQ vhi_req_info;


/*
 * **********************************************
 * Deallocation of allocated memories
 * **********************************************
 */
void freeVhiRequestData(sVHI_REQ * vhi_req_info);

/*
 * **********************************************
 * Specific request with group appended
 * **********************************************
 */
char* composeSaleRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);
char* composePostAuthRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);
char* composeCreditRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);
char* composeVoidRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);
char* composeActivateRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);
char* composeReloadRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);
char* composeUnloadRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);
char* composeGiftCloseRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);
char* composeBalanceRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);
char* composePreAuthRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);
char* composeCompletionRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);
char* composeBatchInquiryRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);
char* composeBatchReleaseRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);
char* composeTokenizationRequest(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);
char *composeEMVOfflineAdvice(sGLOBALS* GLOBAL, sVHI_REQ * vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * **********************************************
 * Return the appropriate request
 * **********************************************
 */
char* composeVhiRequest(sGLOBALS* GLOBAL, sVHI_REQ * pstvhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);


/*
 * **********************************************
 * TPS Header
 * **********************************************
 */
typedef struct _sTPS_HEADER {
	char REQUEST[8+1]; //a
	char f1[2+1]; //BT = a 2byte
	char f2[4+1]; //nnnn = n 4byte length starting in field 4
	char f3[15+1]; //echo = an 15byte
}_sTPS_HEADER;

char* composeTpsHeader();

/*
 * **********************************************
 * Compose the final request
 * return 0 if success
 * return -1 if failed
 * **********************************************
 */
int composeVantivRequest(sGLOBALS* GLOBAL, sVHI_REQ *vhi_req_info, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info, sVHI_610_BASE *vhi610info, _sVHI_FIELDS_INFO *vhi_fieldinfo);

/*
 * **********************************************
 * Get the main request
 * **********************************************
 */
char* getVantivRequest();

#endif /* VHI_REQUEST_H_ */
