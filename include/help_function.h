/*
 * help_function.h
 *
 *  Created on: Oct 16, 2013
 *      Author: Dexter M. Alberto
 */


#ifndef HELP_FUNCTION_H_
#define HELP_FUNCTION_H_
#include "define_data.h"
#include "vantiv_response.h"
#include "sca_xml_request.h"

/*
 * **********************************************
 * HELPER DATA
 * **********************************************
 */

#define TEXTFILE_VOIDS			"./flash/vhi_voids.txt"
#define TEXTFILE_TIPADJUST		"./flash/tipadjust.txt"
#define TEXTFILE_HOSTUSRPWD 	"./flash/hostuserpwd.txt"
#define TEXTFILE_TPSCODES		"./flash/tpscodesforSAF.txt"
#define TEXTFILE_URLEXCEPTION 	"./flash/hosturlexception.txt"

enum e_DATE_TIME_{
	eMMDDYYhhmm,
	eMMDDYY,
	ehhmmss,
	eYYYY_MM_DD, // _ is dot used in xml response
	eHH_MM_SS, // _ is colon used in xml response
};

typedef union CMDBYTELEN
{
	unsigned char b1b2[2];
	short sLen;
}cmdByteLen;

#if 0 //Praveen_P1: This structure is not required
typedef struct sHELP_INFO
{
	char date_time[11+1];
}sHELP_INFO;

sHELP_INFO shelp_info;
#endif

/*
 * **********************************************
 * HELPER FUNCTION
 * **********************************************
 */

void freeHelpData();

/*
 * Common
 */
char* getDateTime(enum e_DATE_TIME_, char *date_time);
void appendHexRs(char* ptr);
void appendHexGs(char* ptr);
void capitalize(char* ptr);
int removeDecimalPoint(char* buffer);

/*
 * usage : adds 2 decimal points
 * ex:
 * 		12345 -> 123.45
 * params:
 * 		buffer-valid allocated char* all in digits
 * Returns:
 * 		0-success 1-failure
 */
int addDecimalPoint( char* buffer );

/*
 * Safer Functions
 * where:
 * 		size = data size including 1 null
 * 		buffer = char array null terminated
 */
void fillSpace(int size, char* buffer);
void fillZero(int size, char* buffer);
void fillChar9(int size, char* buffer);
void fillChar8(int size, char* buffer);
void appendSpace(int size, char* buffer);
void precedeSpace(int size, char* buffer);
void precedeZero(int size, char* buffer);

/*
 * For debugging purposes function
 */
void printToFile(char* filename, char* buffer);
void incrementStan(sGLOBALS* GLOBAL, _sSCA_XML_REQUEST *req_xml_info);
void maskPanTrack(char* pan);

/*
 * FILE I/O
 */
void createTextFile(char* filename, bool bNewFile);
bool isFileExist(char* filename);
void removeTextFile(char* filename);
int getTxtFileBufferLength(char* fileName);

void saveVoidData(char* pCtroutd, sGLOBALS* GLOBAL, _sVTV_FIELDS *svbninfo, _sSCA_XML_REQUEST *req_xml_info);
void getVoidData(char* key, char* getPan, char* getAmount, char* getPaymentMedia, sGLOBALS* GLOBAL);

void saveAdjustmentData(char* CTROUTD, char* approvedAmount, sGLOBALS* GLOBAL, sCONFIG * GCONFIG, _sSCA_XML_REQUEST *req_xml_info);
void getAdjustmentData(char* getPFlag, char* getPAN, char* getAmount, sGLOBALS* GLOBAL, _sSCA_XML_REQUEST *req_xml_info);

bool isNewBatch(char* batch);
void acquireMutexLock(pthread_mutex_t * pMutex, const char * pszName);
void releaseMutexLock(pthread_mutex_t * pMutex, const char * pszName);
int local_svcSystem(const char *command);
void getMaskedPAN(char *pszClearPAN, char *pszMaskedPAN);
void getPANFromTrackData(char *pszTrackData, int iTrackIndicator, char *pszPAN, int iAcctBufSize);
int setDynamicFunctionPtr_1(char *pszLibName, char *pszFuncName, void (**pFunc)(char *, char *, char *, char *, char *));
int setDynamicFunctionPtr(char *pszLibName, char *pszFuncName, int (**pFunc)(void *));
#endif /* HELP_FUNCTION_H_ */
