/*
 * appLog.h
 *
 *  Created on: Dec 22, 2014
 *      Author: Praveen_p1
 */

#ifndef APPLOG_H_
#define APPLOG_H_

extern int initializeAppLog();
extern int closeAppLog();
extern int isAppLogEnabled();
extern void addAppEventLog(char*, char* , char* , char* , char* );

#endif /* APPLOG_H_ */
