/*
 * free_data.h
 *
 *  Created on: Nov 25, 2013
 *      Author: Dexter M. Alberto
 */

#ifndef FREE_DATA_H_
#define FREE_DATA_H_

#include "vhi_request.h"
#include "vantiv_response.h"
#include "vhi_response.h"
#include "sca_xml_request.h"


/*
 * **********************************************
 * FREE ALL CHAR * USED
 * **********************************************
 */
void freeAllocatedData(sVHI_REQ * vhi_req_info, sVANTIV_RESPONSE * vrinfo, sVHI_RESPONSE_INFO *vhi_res_info, _sSCA_XML_REQUEST *req_xml_info);

#endif /* FREE_DATA_H_ */
