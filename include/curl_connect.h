/*
 * curl_connect.h
 *
 *  Created on: Feb 20, 2014
 *      Author: Dexter M. Alberto
 */

#ifndef CURL_CONNECT_H_
#define CURL_CONNECT_H_
#include "define_data.h"
#include "sca_xml_request.h"

/*
 * **********************************************
 * Send the main request, receive the response via Curl
 * **********************************************
 */

struct string {
	char* ptr;
	size_t nLen;
};

bool isProcessorOnline(sCONFIG * GCONFIG);
void freeCurlData();
void initializeString(struct string *s);
size_t writeFunc(void *ptr, size_t size, size_t nmemb, struct string *s);
int sendRecvViaCurl( char* request, char* getdata, sGLOBALS* GLOBAL, sCONFIG * GCONFIG , _sSCA_XML_REQUEST *req_xml_info);


#endif /* CURL_CONNECT_H_ */
