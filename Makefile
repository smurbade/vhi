# -----------------------------------------------------------------------------
# Project: PaaS
# -----------------------------------------------------------------------------

STAGING_DIR = c:/staging
STRIP_FLAGS	:= --strip-unneeded -F elf32-littlearm
SYSROOT_CFLAGS =--sysroot=$(STAGING_DIR)
ARCH = arm-verifone-linux-gnueabi-
CPP = $(ARCH)gcc.exe $(SYSROOT_CFLAGS)
SDK = $(STAGING_DIR)
STRIP := $(ARCH)strip.exe $(STRIP_FLAGS)

INC_PATH = ./include/
SRC_PATH = source/
D_OBJ_PATH = ./appExecData/DebugObj/
R_OBJ_PATH = ./appExecData/ReleaseObj/
OUTDIR = ./appExecData/output

R_OBJ = $(patsubst $(SRC_PATH)%.c,$(R_OBJ_PATH)%.o,$(wildcard $(SRC_PATH)*.c))
D_OBJ = $(patsubst $(SRC_PATH)%.c,$(D_OBJ_PATH)%.o,$(wildcard $(SRC_PATH)*.c))

INC_PATHS = -I"$(INC_PATH)" \
			-I"$(SDK)/usr/include" \
			-I"$(SDK)/usr/local/include/svcmgr" \
			-I"$(SDK)/usr/local/include/" \
			-I"$(SDK)/usr/local/include/fancypants" \
			-I"$(SDK)/usr/include/libxml2" 

LIB_PATHS = -L"$(SDK)/lib" \
			-L"$(SDK)/usr/local/lib"\
			-L"$(SDK)/usr/lib" \
			-L"appExecData/lib" \
			-L"$(SDK)/usr/local/lib/svcmgr"
			

CLIBS = -lvfisvc -lxml2 -lssl -lcrypto -lvfisvc -lvfisec -lmxstub -lcurl -lsvc_net -lsvc_ethernet -lrt -lvfirtc -lc -lvfimodem -lvfisvc -lsvc_utility
RLIBS = 
DLIBS = -ltcpipdbg9xx
CONFIG_RLIB = -lconfigusr1applib
CONFIG_DLIB = -lconfigusr1applib 

OUTPUT = VHIApp
R_BIN = $(OUTDIR)/$(OUTPUT).exe
D_BIN = $(OUTDIR)/$(OUTPUT)D.exe
CFLAGS = $(INC_PATHS) -fPIC -O2 -Wall -D_REENTRANT -D__USE_GNU -DMUTEX_CONTROL -fexpensive-optimizations
DCFLAGS = -DDEBUG -DLOGGING_ENABLED
LFLAGS = -rdynamic -Wl,-rpath="./lib"
RM = rm -rf
MKDIR = mkdir -p $(OUTDIR)

.PHONY: REL DEB all clean clean-custom post-build

#all: $(D_BIN) del
all: $(R_BIN) $(D_BIN) 
REL: $(R_BIN) del
DEB: $(D_BIN) del

del:
	-$(RM) $(OUTDIR)

clean: clean-custom
	@echo ""
	@echo "Cleaning Release objects/binaries........"
	@echo ""
	${RM} $(R_OBJ) $(R_BIN)
	@echo ""
	@echo "Cleaning Debug objects/binaries........"
	@echo ""
	${RM} $(D_OBJ) $(D_BIN)


$(R_BIN): $(R_OBJ)
	$(MKDIR)
	@echo ""
	@echo "Linking $(notdir $(R_BIN)) .........."
	@echo ""
	$(CPP) $(R_OBJ) -o "$(OUTDIR)/$(OUTPUT).exe" $(LFLAGS) $(LIB_PATHS) $(CONFIG_RLIB) $(CLIBS) $(RLIBS)
	@echo ""
	@echo "Stripping the Exe $(R_BIN) ................"
	$(STRIP) -o $(R_BIN)  $(R_BIN)
	@echo ""
	
$(D_BIN): $(D_OBJ)
	$(MKDIR)
	@echo ""
	@echo "Linking $(notdir $(D_BIN)) .........."
	@echo ""
	$(CPP) $(D_OBJ) -o "$(OUTDIR)/$(OUTPUT)D.exe" $(LFLAGS) $(LIB_PATHS) $(CONFIG_DLIB) $(CLIBS) $(DLIBS)
	@echo ""
	@echo "Stripping the Exe $(D_BIN) ................"
	$(STRIP) -o $(D_BIN)  $(D_BIN)
	@echo ""
	
$(R_OBJ_PATH)%.o: $(SRC_PATH)%.c
	$(CPP) $(CFLAGS) -c $< -o $@

$(D_OBJ_PATH)%.o: $(SRC_PATH)%.c
	$(CPP) $(CFLAGS) $(DCFLAGS) -c $< -o $@
