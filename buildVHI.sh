# --------------------------------
# Author: BLR SCA Team
# --------------------------------

ANS=""
DATA=0
COMPILE=0
BUNDLE=0

VER=""

# Defining the path for the application package data
cd appExecData
APP_PATH=`pwd`
cd -

# Defining the path for the VHIFiles 
cd VHIFiles
VHIFiles_PATH=`pwd`
cd -

# Defining the path where the application package will be created
VHI_PKG_DIR=./package

echo ""
# Get Compilation option
if [ "$1" = "y" ] || [ "$1" = "Y" ]
then
	echo "Compilation Required"
	COMPILE=1
elif [ "$1" = "n" ] || [ "$1" = "N" ]
then
	echo "Compilation Not Required"
	COMPILE=0
else
	while [ $DATA = 0 ]
	do
		echo "Want to Compile VHI Application?[Y/N]"
		read ANS
		case $ANS in
			"Y" | "y" )
			COMPILE=1
			DATA=1
			;;

			"N" | "n" )
			COMPILE=0
			DATA=1
			;;
		esac
	done
fi

echo ""
# Get the bundling option
if [ "$2" = "y" ] || [ "$2" = "Y" ]
then
	echo "VHI Bundling Enabled"
	BUNDLE=1
elif [ "$2" = "n" ] || [ "$2" = "N" ]
then
	echo "VHI Bundling Not Required"
	BUNDLE=0
else
	DATA=0
	while [ $DATA = 0 ]
	do
		echo "Want to create VHI App bundle?[Y/N]"
		read ANS
		case $ANS in
			"Y" | "y" )
			BUNDLE=1
			DATA=1
			;;

			"N" | "n" )
			BUNDLE=0
			DATA=1
			;;
		esac
	done
fi

echo ""
# if the bundling is YES, ask for version
if [ $BUNDLE = 1 ]
then
	if [ "$3" = "" ]
	then
		while [ "$VER" = "" ]
		do
			echo "Please enter the version no for applicatioon"
			read VER
		done
	else
		VER=$3
	fi
fi

# Start the compilation if needed
if [ $COMPILE = 1 ]
then
	echo ""
	echo " ------- Running Make ---------"
	export CYGPATH=cygpath
	make
	if [ $? = 0 ]
	then
		echo ""
		echo " ------- Make is SUCCESSFUL ------- "
	else
		echo ""
		echo " ------- Make FAILED --------- "
		return $?
	fi
fi

# Start the bundling if needed
if [ $BUNDLE = 1 ]
then
	echo "Bundling the PaaS Application"
	cd $VHI_PKG_DIR
	./mkpkg.sh $VER $APP_PATH $VHIFiles_PATH
	if [ $? = 0 ]
	then
		echo ""
		echo " ----------- Bundling SUCCESSFUL ----------"
	else
		echo ""
		echo " ----------- Bundling FAILED -------------- "
		return $?
	fi
	cd -
fi
