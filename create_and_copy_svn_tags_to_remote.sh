
#!/bin/bash

#change URL in below command as per your remote repository URL

git remote add VHI https://smurbade@bitbucket.org/smurbade/vhi.git

# git branch -a command returns output in 'remotes/origin/tags/VER_1.00.00' format in each line. below command removes 'remotes/' at start of each line. Remaining is SVN tag branch name. 'VER_1.00.00' is the tag name. This loop will checkout to that branch and tag it with tag name. 

#if git branch -a commands returns tag instead of tags in output then replace 'tags' with 'tag' as necessary in grep below

git branch -a | grep tags | cut -b 11- | while read tags; do
        echo $tags

        ver=`echo $tags | sed -n -e 's/^.*tags//p' | cut -b 2-`

        echo $ver

        git checkout $tags; git tag -a $ver -m "Tagged through script" 

done

# below command will push all tags to remote repository

git checkout master ; git push VHI --tags
