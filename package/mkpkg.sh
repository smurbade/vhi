appPrefix="VHI"
STG_DIR=c:/staging
home=`pwd`

# Defining the path for the application package data
APP_EXEC_DIR=../appExecData

if [ "$1" = "" ]
then
	while [ "$ver" = "" ]
	do
		echo "Please enter the version no for applicatioon"
		read ver
	done
else
	ver=$1
fi
	
cleanup()
{
	echo "Removing directories if present"

	if [ -d APP_PKG ]; then
		echo "Removing APP_PKG"
		rm -f -r APP_PKG
	fi
	
	if [ -d VHIFiles_PKG ]; then
		echo "Removing VHIFiles_PKG"
		rm -f -r VHIFiles_PKG
	fi
	
	if [ -d PKG ]; then
		echo "Removing PKG"
		rm -f -r PKG
	fi
}

APP_EXEC_DIR=$2
VHIFiles=$3

if ! [ -d "$APP_EXEC_DIR" ]; then
	echo "ERROR: Invalid Path given: $APP_EXEC_DIR.. Exiting..."
	return 1
fi

if ! [ -d "$VHIFiles" ]; then
	echo "ERROR: Invalid Path given: $VHIFiles.. Exiting..."
	return 1
fi

today=$(date +%d%m%y)
appName="$appPrefix-$ver-$today"

cleanup

echo "***********************************************************************"
echo "Creating the application bundle"
echo "***********************************************************************"
mkdir -p APP_PKG/lib APP_PKG/CONTROL

#Copy the data into the folder for the application
#Added this so that it could be easily restarted after 
#using Secins_Start_App
chmod 777 $APP_EXEC_DIR/output/*.exe

cp $APP_EXEC_DIR/output/*.exe APP_PKG
cp $APP_EXEC_DIR/lib/*.so APP_PKG/lib
cp $APP_EXEC_DIR/CONFIG/config.usr1 APP_PKG/CONTROL
cp $APP_EXEC_DIR/CONFIG/.profile APP_PKG/CONTROL
cp $APP_EXEC_DIR/CONFIG/startVHI.sh APP_PKG

touch control
echo "Package: $appPrefix-App" > control
echo "Version: $ver" >> control
echo "User: usr1" >> control

mv control APP_PKG/CONTROL

#create the tar of the application
cd APP_PKG
tar -cvf ../$appPrefix-App.tar ./*

cd $home
rm -f -r APP_PKG

echo "***********************************************************************"
echo "Creating the VHI Files bundle"
echo "***********************************************************************"
mkdir -p VHIFiles_PKG/CONTROL VHIFiles_PKG/crt

touch control
echo "Package: $appPrefix-VHIFiles" > control
echo "Version: $ver" >> control
echo "User: usr1" >> control
echo "Type: userflash" >> control

mv control VHIFiles_PKG/CONTROL
cp ./appsignerA1.crt VHIFiles_PKG/crt
cp -R $VHIFiles/* VHIFiles_PKG

cd VHIFiles_PKG
#create the tar of the VHIFiles deck
tar -cvf ../$appPrefix-VHIFiles.tar ./*

cd $home
rm -f -r VHIFiles_PKG

echo "***********************************************************************"
echo "Creating the download bundle"
echo "***********************************************************************"
mkdir -p PKG/CONTROL PKG/crt
touch control
echo "Package: $appPrefix" > control
echo "Version: $ver" >> control
echo "User: usr1" >> control

mv control PKG/CONTROL
cp ./appsignerA1.crt PKG/crt
mv $appPrefix-App.tar PKG
mv $appPrefix-VHIFiles.tar PKG

cd PKG

echo
echo "Signing the application tar ball"
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ../appsignerA1.crt --key ../appsignerA1.key $appPrefix-App.tar

echo
echo "Signing the VHIFiles tar ball"
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ../appsignerA1.crt --key ../appsignerA1.key $appPrefix-VHIFiles.tar

tar -cvzf ../USR1.$appName.tgz ./*

cd $home

echo
echo "Signing the usr1 package tar ball"
$STG_DIR/tools/vfisigner/vfisigner.exe --cert ./appsignerA1.crt --key ./appsignerA1.key USR1.$appName.tgz

rm -f -r PKG

echo
echo "Creating the download package dl-$appName.tgz"
tar -cvzf dl-$appName.tgz USR1.$appName.tgz*

rm -f -r USR1.$appName.tgz*

echo "***********************************************************************"
echo "Bundling the application successful"
echo "***********************************************************************"